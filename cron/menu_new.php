<?

$header_menu =  [
                  [
                    'name' => 'Новости',
                    'link' => '',
                    'sub' => [
                                [
                                  'title' => '',
                                  'type' => 'tab',
                                  'items' => [
                                                [
                                                'title' => 'Рубрики',
                                                'img' => 'https://www.abw.by/images/banner-abw.png',
                                                'link' => 'https://www.abw.by/forum/?utm_source=menu&utm_medium=abwby',
												'target' => '1',
                                                'items' => [
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => '/news/autotravel/',
                                                                              'name' => 'Автопутешествия',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/automarket/',
                                                                              'name' => 'Авторынок',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/autosport/',
                                                                              'name' => 'Автоспорт',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/video/',
                                                                              'name' => 'Видеосюжеты',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/law/',
                                                                              'name' => 'Законодательство',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/commercial/',
                                                                              'name' => 'Коммерческий транспорт',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/moto/',
                                                                              'name' => 'Мототехника',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/industry/',
                                                                              'name' => 'Новости автопрома',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => '/news/rb/',
                                                                              'name' => 'Новости Беларуси',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/experience/',
                                                                              'name' => 'Опыт эксплуатации',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/used_cars/',
                                                                              'name' => 'Подержанные автомобили',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/incidents/',
                                                                              'name' => 'Происшествия',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/other/',
                                                                              'name' => 'Разное',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/test-drive/',
                                                                              'name' => 'Тест-драйв',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/news/tires/',
                                                                              'name' => 'Шины',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                            ],
                                                  ],
                                            ],
                                ],
                                [
                                  'title' => '',
                                  'type' => 'link',
                                  'items' => 
                                            [
                                              [
                                                'name' => 'Лента новостей',
                                                'link' => '/news/',
                                              ],
                                              [
                                                'link' => '/top/',
                                                'name' => 'Популярное',
                                              ],
                                              [
                                                'link' => '/discuss/',
                                                'name' => 'Обсуждаемое',
                                              ],
                                              [
                                                'link' => '/rss/',
                                                'name' => 'Подписка',
                                              ],
                                              [
                                                'link' => '/archive/',
                                                'name' => 'Архив газеты',
                                              ],
                                            ],
                                ],
                                [
                                  'title' => 'Подписка на новости',
                                  'type' => 'block',
                                  'content' => 'subscription'
                                ],
                             ], 
                  ],
 
  
                  [
                    'name' => 'Объявления',
                    'link' => '',
                    'sub' => [
                                [ 
							      'title' => '',
                                  'type' => 'tab',
                                  'items' =>
                                            [
                                              [
                                                'title' => 'Транспорт',
                                                'img' => STATIC_WWW.'/temp/banner/micro_leasing_19042017.png',
												'img_mob' => STATIC_WWW.'/temp/banner/micro_leasing_19042017_little.png',
                                                'link' => 'http://www.mikro-leasing.by/fizicheskim-licam?prm=21&utm_source=abwby&utm_medium=menu-branding&utm_campaign=fizlica',
													'target' => '1',
													'items' => [ 
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => '/car/sell/',
                                                                              'name' => 'Легковые авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/car/buy/',
                                                                              'name' => 'Покупка авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/showtrucks/sell/',
                                                                              'name' => 'Грузовые авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => '/index.php?set_small_form_1=1&act=public_search&do=search&index=1&adv_type=1&adv_group=&marka%5B%5D=&model%5B%5D=&type_engine=&transmission=&vol1=&vol2=&year1=1960&year2=2016&cost_val1=&cost_val2=&u_country=+&u_city=&period=&sort=&na_rf=&type_body=14&privod=&probeg_col1=&probeg_col2=&key_word_a=',
                                                                              'name' => 'Микроавтобусы',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => 'https://moto.abw.by/search',
                                                                              'name' => 'Мототехника',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => 'https://boats.abw.by/search',
                                                                              'name' => 'Водный транспорт',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => 'https://www.abw.by/showtrucks/sell/%D0%9F%D1%80%D0%B8%D1%86%D0%B5%D0%BF-%D0%B4%D0%B0%D1%87%D0%B0/',
                                                                              'name' => 'Прицеп-дача',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=18',
                                                                              'name' => 'Велосипеды, гироскутеры',
                                                                              'desc' => '',
                                                                              'style' => '',
																			  'target' => '1',
                                                                            ],
																			[
                                                                              'link' => '/prices/',
                                                                              'name' => 'График цен',
                                                                              'desc' => '',
                                                                              'style' => 'line',
                                                                            ],
																			[
                                                                              'link' => '/price/',
                                                                              'name' => 'Оценка авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => '/dealer/',
                                                                              'name' => 'Автодилеры',
                                                                              'desc' => 'Каталог новых автомобилей официальных дилеров Беларуси',
                                                                              'style' => 'line',
                                                                            ],
																			[
                                                                              'link' => '/autohaus_truck/',
                                                                              'name' => 'Автохаусы грузовые',
                                                                              'desc' => 'Каталог услуг автохаусов и автосалонов грузовых авто',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => '/autohaus/',
                                                                              'name' => 'Автохаусы легковые',
                                                                              'desc' => 'Каталог услуг автохаусов и автосалонов легковых авто',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                                ],
                                                            ],
                                                ],
                                                [
                                                  'title' => 'Запчасти',
                                                  'img' => 'https://www.abw.by/images/bamper/bamper_menu.png',
                                                  'link' => 'https://bamper.by/?utm_source=abwby&utm_medium=links&utm_campaign=enot',
												  'target' => '1',
                                                  'items' => [ 
                                                                [
                                                                  'title' => 'Запчасти б/у',
                                                                  'items' => 
                                                                            [
                                                                              [
                                                                                'link' => '/parts/sell/',
                                                                                'name' => 'Запчасти к легковым авто',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => '/truckparts/sell/',
                                                                                'name' => 'Запчасти к грузовым авто',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => '/index.php?set_small_form_3=1&act=search_adv&act_adv=step_3&cat_id=2&sub_id=7&marka_id=&model_id=0&year_id=0&year_id_max=0&city_id=&see_period=0&order_id=9&key_word=&hidden_field_s_flag=&engine_value=&engine_value_max=&a_body=14&condition_id=0',
                                                                                'name' => 'Запчасти к микроавтобусам',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://moto.abw.by/market',
                                                                                'name' => 'Запчасти к мотоциклам',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market',
                                                                                'name' => 'Магнитолы, аксессуары',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => 'http://motorland.by/?utm_source=abw&utm_medium=referal_parts&utm_campaign=referal_top_parts',
                                                                                'name' => 'Запчасти от MotorLand',
                                                                                'desc' => '',
                                                                                'style' => 'line bold',
																				'target' => '1',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://goo.gl/RFJxkE',
                                                                                'name' => 'Запчасти от Ф-Авто',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
																			  [
                                                                                'link' => 'http://www.autopriwos.by/?utm_source=abw_link&utm_medium=cpc&utm_campaign={campaignid}&utm_content={creative}&utm_term={keyword}',
                                                                                'name' => 'Запчасти от Автопривоза',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://goo.gl/vsZGTf',
                                                                                'name' => 'Двигатели дешево от MOTORBOR',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
																			  [
                                                                                'link' => 'https://company.abw.by/zapchasti-bu',
                                                                                'name' => 'Б/у запчасти - каталог компаний',
                                                                                'desc' => '',
                                                                                'style' => 'bold',      
																				'target' => '1',
                                                                              ],
																			  [
                                                                                'link' => 'https://bamper.by/?utm_source=abwby&utm_medium=links&utm_campaign=menu_poisk',
                                                                                'name' => 'Поиск запчастей BAMPER.BY',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                            ],
                                                                ],
                                                                [
                                                                  'title' => 'Новые запчасти',
                                                                  'items' => 
                                                                            [
                                                                              [
                                                                                'link' => 'https://atr.by/?utm_source=abwby&utm_medium=menu',
                                                                                'name' => 'Автозапчасти в Минске ATR.BY',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://atr.by/pages/dostavka-avtozapchastej?utm_source=abwby&utm_campaign=ATR&utm_medium=menu',
                                                                                'name' => 'З/Ч все города Беларуси ATR.BY',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                              [
                                                                                'link' => 'http://www.byopel.com/?utm_source=abwby&utm_medium=link-menu&utm_campaign=rm-market',
                                                                                'name' => 'Запчасти НЕДОРОГО byopel.com',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://goo.gl/pKFHxU',
                                                                                'name' => 'Запчасти VW от VolksMarket',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																				'target' => '1',
                                                                              ],
                                                                            ],
                                                                ],
                                                              ],
                                                ],
                                                [
												
                                                  'title' => 'Шины',
                                                  'img' => 'https://www.abw.by/images/loyal.png',
                                                  'link' => 'https://goo.gl/iiTNAI',
												  'target' => '1',
                                                  'items' => [ 
                                                                [
                                                                  'title' => '',
                                                                  'items' => 
                                                                            [
                                                                              [
                                                                                'link' => '/index.php?set_small_form_4=1&act=search_adv&act_adv=step_3&cat_id=4&sub_id=15&model_id=&shina_size_id=0&shina_size=&shina_size_2=&shina_model=0&shina_season=&shina_type=1&shina_condition=&disk_num=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Легковые шины',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => '/index.php?set_small_form_4=1&act=search_adv&act_adv=step_3&cat_id=4&sub_id=15&model_id=&shina_size_id=0&shina_size=&shina_size_2=&shina_model=0&shina_season=&shina_type=3&shina_condition=&disk_num=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Грузовые шины',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => '/index.php?set_small_form_4=1&act=search_adv&act_adv=step_3&cat_id=4&sub_id=15&model_id=&shina_size_id=0&shina_size=&shina_size_2=&shina_model=0&shina_season=&shina_type=4&shina_condition=&disk_num=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Внедорожные шины',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
																		      [
                                                                                'link' => 'https://moto.abw.by/market?Card_Filter%5Btype_id%5D=3',
                                                                                'name' => 'Мотошины',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
											       							  [                                                                             
 																			    'link' => '/index.php?act=calc_t',
                                                                                'name' => 'Шинный калькулятор',
                                                                                'desc' => '',
                                                                                'style' => 'line',
                                                                              ],
										  									  [
                                                                                'link' => 'https://goo.gl/aSt38y',
                                                                                'name' => 'Шины и диски от Motorland',
                                                                                'desc' => '',
                                                                                'style' => 'line bold',
										  									    'target' => '1',
                                                                              ],
																	  		  [
                                                                                'link' => 'https://goo.gl/MO55K9',
                                                                                'name' => 'Шины от оф.дилера COLESA.BY',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
											  								    'target' => '1',
                                                                              ],
																	  		  [
                                                                                'link' => 'https://bamper.by/shiny/?utm_source=abwby&utm_medium=links&utm_campaign=menu_shiny',
                                                                                'name' => 'Поиск шин bamper.by',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
																  			    'target' => '1',
                                                                              ],
                                                                            ],
                                                              ],
                                                            ],
                                                ],
                                                [
												
                                                  'title' => 'Диски',
                                                  'img' => '',
                                                  'link' => '',
                                                  'items' => [ 
                                                                [
                                                                  'title' => '',
                                                                  'items' => 
                                                                            [ 
                                                                              [
                                                                                'link' => '/disks/sell/',
                                                                                'name' => 'Автомобильные диски',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => '/index.php?set_small_form_6=1&act=search_adv&act_adv=step_3&cat_id=5&sub_id=18&model_id=&marka_id=&model_id=0&shina_size_id=&disk_type=1&n_holes=&pcd%5B%5D=&pcd%5B%5D=&dia%5B%5D=&dia%5B%5D=&et%5B%5D=&et%5B%5D=&width%5B%5D=&width%5B%5D=&shina_type=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Литые диски',
                                                                                'desc' => '',
                                                                                'style' => 'line',
                                                                              ],
                                                                              [
                                                                                'link' => '/index.php?set_small_form_6=1&act=search_adv&act_adv=step_3&cat_id=5&sub_id=18&model_id=&marka_id=&model_id=0&shina_size_id=&disk_type=3&n_holes=&pcd%5B%5D=&pcd%5B%5D=&dia%5B%5D=&dia%5B%5D=&et%5B%5D=&et%5B%5D=&width%5B%5D=&width%5B%5D=&shina_type=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Стальные диски',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => '/index.php?set_small_form_6=1&act=search_adv&act_adv=step_3&cat_id=5&sub_id=18&model_id=&marka_id=&model_id=0&shina_size_id=4&disk_type=0&n_holes=&pcd%5B%5D=&pcd%5B%5D=&dia%5B%5D=&dia%5B%5D=&et%5B%5D=&et%5B%5D=&width%5B%5D=&width%5B%5D=&shina_type=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Диаметр R15',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
																		      [
                                                                                'link' => '/index.php?set_small_form_6=1&act=search_adv&act_adv=step_3&cat_id=5&sub_id=18&model_id=&marka_id=&model_id=0&shina_size_id=5&disk_type=0&n_holes=&pcd%5B%5D=&pcd%5B%5D=&dia%5B%5D=&dia%5B%5D=&et%5B%5D=&et%5B%5D=&width%5B%5D=&width%5B%5D=&shina_type=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Диаметр R16',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
																		      [
                                                                                'link' => '/index.php?set_small_form_6=1&act=search_adv&act_adv=step_3&cat_id=5&sub_id=18&model_id=&marka_id=&model_id=0&shina_size_id=6&disk_type=0&n_holes=&pcd%5B%5D=&pcd%5B%5D=&dia%5B%5D=&dia%5B%5D=&et%5B%5D=&et%5B%5D=&width%5B%5D=&width%5B%5D=&shina_type=&key_word=&price_value=&price_value_max=&city_id=&see_period=0&order_id=0',
                                                                                'name' => 'Диаметр R17',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
											       							  [                                                                              
																			    'link' => 'https://goo.gl/MLyPsk',
                                                                                'name' => 'Диски б/у от Motorland',
                                                                                'desc' => '',
                                                                                'style' => 'line bold',
										  									    'target' => '1',
                                                                              ],
											       							  [                                                                              
																			    'link' => 'https://goo.gl/KPYC8n',
                                                                                'name' => 'Литые и стальные диски от LUMA.BY',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
										  									    'target' => '1',
                                                                              ],
										  									  [
                                                                                'link' => 'https://bamper.by/zchbu/zapchast_disk-litoy/?utm_source=abwby&utm_medium=links&utm_campaign=disk_litoy',
                                                                                'name' => 'Поиск литых дисков на bamper.by',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
										  									    'target' => '1',
                                                                              ],
																	  		  [
                                                                                'link' => 'https://bamper.by/zchbu/zapchast_disk-shtampovannyy/?utm_source=abwby&utm_medium=links&utm_campaign=disk_stal',
                                                                                'name' => 'Поиск штампованных дисков на bamper.by',
                                                                                'desc' => '',
                                                                                'style' => 'bold',
											  								    'target' => '1',
                                                                              ],
                                                                            ],
                                                              ],
                                                            ],
                                                ],
                                                [
                                                  'title' => 'Автобазар',
                                                  'img' => '',
                                                  'link' => '',
                                                  'items' => [ 
                                                                [
                                                                  'title' => '',
                                                                  'items' => 
                                                                            [
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=1',
                                                                                'name' => 'Аккумуляторы и зарядные',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=2',
                                                                                'name' => 'Аксессуары и тюнинг',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=3',
                                                                                'name' => 'Аудио-, видеоустройства для авто',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=18',
                                                                                'name' => 'Велосипеды, гироскрутеры, сигвеи',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=4',
                                                                                'name' => 'Газовое оборудование и комплектующие',
                                                                                'desc' => '',
                                                                                'style' => '',                                                                  
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=6',
                                                                                'name' => 'Инструменты и оборудование для авто',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=7',
                                                                                'name' => 'Инструменты и оборудование для СТО',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=8',
                                                                                'name' => 'Инструменты строительные',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=9',
                                                                                'name' => 'Колпаки, секретки для колёс',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
																			  [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=5',
                                                                                'name' => 'Масла и автохимия',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                            ],
                                                                ],
                                                                [
                                                                  'title' => '',
                                                                  'items' => 
                                                                            [
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=10',
                                                                                'name' => 'Навигаторы, трекеры, GSM',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=11',
                                                                                'name' => 'Отопление/охлаждение',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=12',
                                                                                'name' => 'Радиостанции, рации и комплектующие',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=13',
                                                                                'name' => 'Книги по эксплуатации и ремонту авто',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=14',
                                                                                'name' => 'Свет: ксенон, лампы и комплектующие',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=15',
                                                                                'name' => 'Сигнализации и комплектующие',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=16',
                                                                                'name' => 'Электронные устройства',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=17',
                                                                                'name' => 'Техника для сада и огорода',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                              [
                                                                                'link' => 'https://car.abw.by/market?Card_Filter%5Btype_id%5D=19',
                                                                                'name' => 'Прочее',
                                                                                'desc' => '',
                                                                                'style' => '',
                                                                              ],
                                                                            ],
                                                                ],
                                                              ],
                                                ],
												[
                                                  'title' => 'Гаражи',
                                                  'img' => '/images/enot-garag.png',
												  'img_mob' => '/images/enot-garag-215.png',
                                                  'link' => 'https://garage.abw.by/search?utm_source=abwby&utm_medium=enot&utm_campaign=garage',
												  'target' => '1',
                                                  'items' => [ 
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => 'https://garage.abw.by/search',
                                                                              'name' => 'Все объявления',
                                                                              'desc' => 'Гаражи и стоянки',
                                                                              'style' => '',
                                                                            ],
																			[
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Btype_id%5D=1',
                                                                              'name' => 'Гаражи',
                                                                              'desc' => 'Покупка, продажа, аренда гаражей',
                                                                              'style' => 'line',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Btype_id%5D=2',
                                                                              'name' => 'Машиноместа',
                                                                              'desc' => 'Покупка, продажа, аренда машиномет',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Bcat_id%5D=2',
                                                                              'name' => 'Продам',
                                                                              'desc' => 'Продажа гаражей или машиномест',
                                                                              'style' => 'line',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Bcat_id%5D=1',
                                                                              'name' => 'Куплю',
                                                                              'desc' => 'Покупка гаражей или машиномест',
                                                                              'style' => '',
                                                                            ],
																																						[
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Bcat_id%5D=3',
                                                                              'name' => 'Сдам',
                                                                              'desc' => 'Сдать в аренду гараж или машиноместо',
                                                                              'style' => '',
                                                                            ],
																																						[
                                                                              'link' => 'https://garage.abw.by/search?Card_Filter%5Bcat_id%5D=4',
                                                                              'name' => 'Сниму',
                                                                              'desc' => 'Взять в аренду гараж или машиноместо',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                            ],
                                                ],
                                              ],




                                ],
                                [
                                  'title' => '',
                                  'type' => 'link',
                                  'items' => [
                                                [
                                                  'name' => 'Запчасти для легковых авто',
                                                  'link' => '/parts/sell/'
                                                ], 
                                                [
                                                  'name' => 'Мотоциклы',
                                                  'link' => 'https://moto.abw.by/search?Card_Filter%5Bcat_id%5D=1'
                                                ],
												[
                                                  'name' => 'Выкуп авто',
                                                  'link' => 'https://company.abw.by/vykup_avto?utm_source=abwby&utm_medium=hot-links&utm_campaign=vykup'
                                                ],												
                                                [
                                                  'name' => 'Все объявления',
                                                  'link' => '/allpublic/'
                                                ],
                                              ],
                                ],
                                [
                                  'title' => 'Разместить объявление',
                                  'type' => 'block',
                                  'content' => 'add'
                                ],
                             ],

                  ],
  
  
  
                  [
                    'name' => 'Автокомпании',
                    'link' => '',
                    'sub' => [
                                [
								  'title' => '',
                                  'type' => 'tab',
                                  'items' =>
                                            [
                                              [
                                                'title' => 'Услуги',
                                                'img' => '/images/rm-market-200417.png',
												'img_mob' => '/images/rm-market-200417-little.png',
                                                'link' => 'http://www.byopel.com/info/selected_info:v:2029.htm?utm_source=abwby&utm_medium=menu-branding&utm_campaign=rm-market',
												'target' => '1',
                                                'items' => [ 
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => 'https://company.abw.by/avtovozy',
                                                                              'name' => 'Аренда автовоза',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/avtolombardy',
                                                                              'name' => 'Автоломбарды',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/avtoobuchenie',
                                                                              'name' => 'Автомобильное обучение',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/avtostekla',
                                                                              'name' => 'Замена автостекол',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/arenda_spectekhniki',
                                                                              'name' => 'Аренда спецтехники',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/vykup_avto',
                                                                              'name' => 'Выкуп авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/gazovoe_oborudovanie',
                                                                              'name' => 'Газовое оборудование',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => 'https://company.abw.by/gruzoperevozki_1-5_t',
                                                                              'name' => 'Грузоперевозки 1-5тонн',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/gruzoperevozki_5-15_t',
                                                                              'name' => 'Грузоперевозки 5-15тонн',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/gruzoperevozki_15-25_t',
                                                                              'name' => 'Грузоперевозки 15-25тонн',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/passazhirskie_perevozki',
                                                                              'name' => 'Пассажирские перевозки',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/prokat-avto',
                                                                              'name' => 'Прокат авто и прицепов',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => '/sto/',
                                                                              'name' => 'СТО',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/evakuaciya-avto',
                                                                              'name' => 'Эвакуация авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                            ],
                                              ],
                                              [
                                                'title' => 'Автомагазины',
                                                'img' => '',
                                                'link' => '',
                                                'items' => [ 
                                                              [
                                                                'title' => '',
                                                                'items' => 
                                                                          [
                                                                            [
                                                                              'link' => 'https://company.abw.by/company/sales/',
                                                                              'name' => 'Акции и скидки компаний',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/zapchasti-bu',
                                                                              'name' => 'Запчасти б/у',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/zapchasti_gruzovye',
                                                                              'name' => 'Запчасти к грузовым авто',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/zapchasti-novye',
                                                                              'name' => 'Запчасти новые',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/motornoe_maslo',
                                                                              'name' => 'Масла и автохимия',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                            [
                                                                              'link' => 'https://company.abw.by/shiny_diski',
                                                                              'name' => 'Шины и диски новые',
                                                                              'desc' => '',
                                                                              'style' => '',
                                                                            ],
                                                                          ],
                                                              ],
                                                            ],
                                              ],
                                            ],
 
                                ],
                                [
                                  'title' => '',
                                  'type' => 'link',
                                  'items' => [ 
                                                [
                                                  'name' => 'Выкуп авто',
                                                  'link' => 'https://company.abw.by/vykup_avto'
                                                ],
                                                [
                                                  'name' => 'СТО',
                                                  'link' => STATIC_WWW.'/sto/'
												], 
                                                [
                                                  'name' => 'Замена автостекол',
                                                  'link' => 'https://company.abw.by/avtostekla'
                                                ], 
                                                [
                                                  'name' => 'Прокат авто и прицепов',
                                                  'link' => 'https://company.abw.by/prokat-avto'
                                                ],
                                              ],
                                ],
                                [
                                  'title' => 'Для рекламодателей',
                                  'type' => 'block',
                                  'content' => 'company'
                                ],
                             ],

                  ], 
  
  
                  [
                    'name' => 'Форум',
                    'link' => STATIC_WWW.'/forum/',
                  ]
                ];

$user_keys = isset($_SESSION['user']['balance'])? $_SESSION['user']['balance'] : '';
$login = isset($_SESSION['login']) ? iconv('windows-1251','utf-8',$_SESSION['login']) : '-';
$balance = isset($_SESSION['user']['balance']) ? $_SESSION['user']['balance'].' И.Е.' : '';
$menu_user = [
	['name'=>$login,
	 'link'=>STATIC_WWW.'/profile_edit/edit',
     'type'=>'login' 
	],
	['name'=>$balance,
	 'link'=>STATIC_WWW.'/index.php?act=balance_history',
     'type'=>'balance'
	],
    ['menu'=>
        [
          ['type'=>'menu',
           'name'=>'Пополнить баланс',
           'item'=>
                  [
                      ['name'=>'Cистема "Расчёт" (ЕРИП)', 'link'=>STATIC_WWW.'/index.php?act=balance&sact=erip'],
                      ['name'=>'СМС-оплата (iPay)', 'link'=>STATIC_WWW.'/index.php?act=balance&sact=ipay'],
                      ['name'=>'Интернет-карта ABW.BY', 'link'=>STATIC_WWW.'/index.php?act=balance&sact=card'],
                      ['name'=>'Visa, MasterCard, Белкарт', 'link'=>STATIC_WWW.'/index.php?act=balance&sact=assist'],
                      ['name'=>'Webmoney', 'link'=>STATIC_WWW.'/balance/webmoney/'],
                      ['name'=>'EasyPay', 'link'=>STATIC_WWW.'/index.php?act=balance&sact=easypay']
                  ]
          ],
          ['name'=>'Мои объявления (продлить/ред.)',
           'item'=>
                  [
                      ['name'=>'Авто и микроавтобусы', 'link'=>STATIC_WWW.'/index.php?act=public_edit_list'],
                      ['name'=>'Мототехника (скутер, квадро, мото)', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/moto'],
                      ['name'=>'Мотобазар (з/ч, мотошины, экипировка)', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/moto/market'],
                      ['name'=>'Водный транспорт', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/boats'],
                      ['name'=>'Лодочный мотор', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/boats/market'],
                      ['name'=>'Лодочный базар', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/boats/engine'],
                      ['name'=>'Грузовые авто, спец.техника, прицепы', 'link'=>STATIC_WWW.'/index.php?act=adv&act_adv=delete_advertisement&adv_trucks=1'],
                      ['name'=>'Шины и диски для легковых и груз.авто', 'link'=>STATIC_WWW.'/index.php?act=adv&act_adv=delete_advertisement&adv_trucks=2'],
                      ['name'=>'Автобазар (магнитолы, оборудование, аксессуары)', 'link'=>'https://users.abw.by/default/externalauth?key='.$user_keys.'&url=users.abw.by/car/market'],
                      ['name'=>'Запчасти, гаражи', 'link'=>'/index.php?act=adv&act_adv=delete_advertisement'],
                      ['name'=>'Для платных услуг (обгон, продление)', 'link'=>STATIC_WWW.'/index.php?act=public_list_all']
                  ]
          ]
        ]
      ],
	['name'=>'Блокнот объявлений',
	 'link'=>STATIC_WWW.'/index.php?act=notebook',
     'type'=>'notebook'
	],
	['name'=>'Выход',
	 'link'=>STATIC_WWW.'/new/exituser',
     'type'=>'exit' 
	]
];

