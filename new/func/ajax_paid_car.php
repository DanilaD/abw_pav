<?
// for test
// http://localhost/abw/new/?ajax=ajax_paid_car&type=3&adv_id=10734130&user=1690091&step=2&np_week=2&np_type=4&&bets=2

$res = ['status'=>TRUE,'text'=>'Остановка... Требуется помощь профессионалов.'];

$arg = [
// type  1 - ОБГОН, 2 - СПЕЦПРЕДЛОЖЕНИЕ, 3 - В ГАЗЕТУ, 4 - ОБНУЛИТЬ СЧЁТЧИК
  'type' => FILTER_SANITIZE_NUMBER_INT,
// adv_id
  'adv_id' => FILTER_SANITIZE_NUMBER_INT,
// login
  'user' => FILTER_SANITIZE_NUMBER_INT,
// step 1 - проверка данных и вывод сообщения, 2 - подтверждение оплаты  
  'step' => FILTER_SANITIZE_NUMBER_INT,
// bets - количество ставок
  'bets' => FILTER_SANITIZE_NUMBER_INT,
// np_type - тип в газету
  'np_type' => FILTER_SANITIZE_NUMBER_INT,
// np_week - кол-во выходов в газете
  'np_week' => FILTER_SANITIZE_NUMBER_INT  
];
$data = filter_input_array(INPUT_POST,$arg);

// check
if (empty($data['type']) || empty($data['step'])){
  $res=['status'=>FALSE,'text'=>'Непредвиденная ошибка. Пожалуйста, повторите действие.'];
}elseif ($data['step'] == '1') {
  
  switch ($data['type']) {
    
    case '1':
      $res = ['status'=>TRUE,'text'=>'Услуга платная!','desc'=>'Подъем позиции в ленте объявлений сайта.','cost'=>CAR_UP, 'cost_ie'=>COST_IE];
    break;
  
    case '2':
      $res = ['status'=>TRUE,'text'=>'Услуга платная!','desc'=>'Объявление отображается на главной странице, на внутренних страницах и на странице Спецпредложения. Каждый час сгорает 1 ставка. У кого больше ставок, то объявление находится выше. Обновление происходит каждый час: в 9:00, 10:00, 11:00, 12:00, 13:00 и т.д. круглые сутки. Т.е. если вы сделали ставку в 9:01, объявление появится в 10:00.','cost'=>CAR_VIP_BET,
        'bets'=>[
          '1' => '1 ставка',
          '2' => '2 ставка',
          '3' => '3 ставка',
          '4' => '4 ставка',
          '5' => '5 ставка',
          '10' => '10 ставок',
          '15' => '15 ставок',
        ], 
        'cost_ie'=>COST_IE
      ];
    break;
  
    case '3':
      $res = ['status'=>TRUE,'text'=>'Услуга платная!','desc'=>'Размещается на сайте как приоритетное с фоновой подложкой и в газете "Автобизнес" с возможностью выбора оформления.','cost'=>['4'=>SER_COST4,'5'=>SER_COST5,'6'=>SER_COST6,'7'=>SER_COST7,],
        'np_type'=>[
          '4' => 'Обычный шрифт',
          '5' => 'Жирный шрифт',
          '6' => 'Жирный шрифт и рамка',
          '7' => 'Объявление с фото',
        ],
        'np_week'=>[
          '1' => '1 выход',
          '2' => '2 выхода',
          '3' => '3 выхода',
          '4' => '4 выхода',
        ], 
        'cost_ie'=>COST_IE
      ];
    break;

    case '4':
      $res = ['status'=>TRUE, 'text'=>'Услуга платная!', 'desc'=>'Обнуление просмотров. Стоимость - '.SER_VIEWS.' И.Е.', 'cost'=>SER_VIEWS, 'cost_ie'=>COST_IE];
    break;
  
    default:  
      $res = ['status'=>FALSE,'text'=>'Выберите вид объявления для оплаты.'];
  }

}elseif ($data['step'] == '2'){
  
  // проверка объявления
    $car_class = NEW Cars($_db);
    $main = $car_class->GetCar($data['adv_id']);
    if (empty($main['0'])){
       $res = ['status'=>FALSE,'text'=>'Объявление ID '.$data['adv_id'].' не найдено. Возможно объявление уже удалено.'];   
    }else{
      
    // проверка пользователя
      $user_class = new Users($_db);  
      $user = $user_class->validateUserId($data['user']);
      if (empty($user)){
        $res = ['status'=>FALSE,'text'=>'Пользователь не найден. Войдите на сайт под своим логином и повторите действие.'];
      }else{
        // расчет суммы списания
          switch ($data['type']) {
    
          case '1':
            $cost = CAR_UP;
          break;
        
          case '2':
            if (empty($data['bets'])){
              $res = ['status'=>FALSE,'text'=>'Выберите ставки для спецпредложения.'];
            }else{
              $cost = CAR_VIP_BET * $data['bets'];
            }
          break;
        
          case '3':              
            if (empty($data['np_type']) || !in_array($data['np_type'], ['4','5','6','7','8']) || $data['np_week'] < 1){
              $res = ['status'=>FALSE,'text'=>'Выберите тип объявления в газету и кол-во выходов.'];
            }else{  
              $price = $car_class->GetPaidInfo($data['np_type']);
              if (isset($price['price'])){
                $cost = $price['price'] * $data['np_week'];
              }else{
                $cost = 0;
              }
            }
          break;
        
          case '4':
            $cost = SER_VIEWS;
          break;
        
          default:  
            $res = ['status'=>FALSE,'text'=>'Выберите услугу для оплаты.'];
          }
          
        // проверка средств для оплаты
        if (empty($cost) || !isset($user['balance']) || empty($data['adv_id'])){
          $res = ['status'=>FALSE,'text'=>'Возникла непредвиденная ошибка. Повторите своё дейстие.'];
        }elseif ($user['balance'] < $cost){
          $res = ['status'=>FALSE,'text'=>'Недостаточно средств. Пополните баланс.'];
        }else{
          // выполняем действия
             switch ($data['type']) {
                case '1':
                  // обгон
                  $res = $car_class->car_add_up($data['adv_id']);
                break;
              
                case '2':
                  // спецпредложение
                  $phone = '';
                  $user = $_SESSION['login'];
                  $res = $car_class->car_add_sp($data['adv_id'], $data['bets'], $user, $phone,'');
                break;
              
                case '3':
                  // в газету
                  $res = $car_class->car_add_np($data['adv_id'], $data['np_type'], $data['np_week']);
                break;
              
                case '4':
                  // обнуление
                  $res = $car_class->car_reset_show($data['adv_id']);
                break;
              
             }
          
          if ($res == TRUE){
          // проводим оплату
            $res = $user_class->UpdateBalanceUser($data['user'],$cost);
            if ($res == TRUE){
              // записываем лог для каждого типа
               switch ($data['type']) {
    
                case '1':
                  // обгон
                  $type = SER_UP;
                  $tx = 'car '.$data['adv_id'];
                  $user_class->SaveLogBalance($data['user'], $type, 2, $cost, $tx);
                  $res = ['status'=>TRUE,'text'=>'Обгон успешно совершён. С вашего счёта списано '.$cost.' И.Е.'];
                break;
              
                case '2':
                  // спецпредложение
                  $type = SER_VIP;
                  $tx = 'car '.$data['adv_id'];
                  $user_class->SaveLogBalance($data['user'], $type, 2, $cost, $tx);
                  $res = ['status'=>TRUE,'text'=>'Ставки в размере '.$data['bets'].' добавлены к объявлению. С вашего счёта списано '.$cost.' И.Е.'];
                break;
              
                case '3':
                  // размещение в газету
                  $type = $car_class->GetPaidInfo($data['np_type'])['type'];
                  $tx = 'car '.$data['adv_id'];
                  $user_class->SaveLogBalance($data['user'], $type, 2, $cost, $tx);
                  $res = ['status'=>TRUE,'text'=>'Ваше объявление будет размещено в газете "Автобизнес". С вашего счёта списано '.$cost.' И.Е.'];
                break;
              
                case '4':
                // обнуление счетчика
                  $type = SER_VIEWS_T;
                  $tx = 'car '.$data['adv_id'];
                  $user_class->SaveLogBalance($data['user'], $type, 2, $cost, $tx);
                  $res = ['status'=>TRUE,'text'=>'Счётчик объявлений обнулён. С вашего счёта списано '.$cost.' И.Е.'];
                break;
                }
            }
            
          }else{
            $res = ['status'=>FALSE,'text'=>'Действие не может быть выполнено и отменено. Интернет единицы не списаны.'];
          }
            
        }
      }
    }
}

echo json_encode($res);

exit;
