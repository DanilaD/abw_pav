<?
$arg =[
  'adv_id' => FILTER_VALIDATE_INT,
  'phone' => FILTER_SANITIZE_SPECIAL_CHARS,
  'mail' => FILTER_VALIDATE_EMAIL,
  'text' => FILTER_SANITIZE_SPECIAL_CHARS
];
$data = filter_input_array(INPUT_POST,$arg);

$sendmail = NEW MailSend($_db);
$res = $sendmail->CheckUserMail($data);
echo json_encode($res);
exit;