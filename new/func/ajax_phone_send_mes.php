<?
header("Content-Type: text/html; charset=UTF-8");
$arg=[
  'adv_id' => FILTER_SANITIZE_NUMBER_INT,
// type 1 - car
  'type' => FILTER_SANITIZE_NUMBER_INT,
// type 1 - sales 2 - wrong number phone
  'mes' => FILTER_SANITIZE_NUMBER_INT,
// url
  'url' => FILTER_SANITIZE_SPECIAL_CHARS,
];
$ar = filter_input_array(INPUT_POST,$arg);

//error
if (empty($ar['adv_id']) || empty($ar['mes']) || empty($ar['url'])){
  $res = ['status'=>FALSE, 'text'=>'Возникла проблема. Обновите страницу'];
}else{
  // отправка сообщения
  $sendmail = NEW MailSend($_db);
  $res = $sendmail->SendMesPhone($ar);
}
echo json_encode($res);
exit;