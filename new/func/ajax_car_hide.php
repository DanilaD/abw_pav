<?
header("Content-Type: text/html; charset=UTF-8");

$arg =[
// ID объявления
  'adv_id' => FILTER_SANITIZE_NUMBER_INT,
// ТИП авто = 1
  'type' => FILTER_SANITIZE_NUMBER_INT,
// ДЕЙСТВИЕ 1 - проверить, добавлено ли объявление в блокнот. 2 - добавить/удалить в/из блокнота
  'action' => FILTER_SANITIZE_NUMBER_INT 
];

$ar = filter_input_array(INPUT_POST,$arg);

// если что-то некорректно
$res=['status'=>FALSE, 'text'=>''];

empty($ar['adv_id']) ? exit : '';

switch ($ar['action']) {
  // проверка добавлено ли объявление
  case 1:
    // проверить на пустоту
    if (empty($ar)){
      echo json_encode($res);
      exit;
    }
    // проверить категорию авто
    if ($ar['type'] == '1'){
      $json = $_COOKIE['hide']; 
      if (!empty($json) || $json != "null"){ 
        // раскодировать JSON
          $arr = json_decode($json);
        // проверить есть ли объявление
          if (in_array($ar['adv_id'],$arr)){
            $res=['status'=>TRUE, 'text'=>''];
          }
      }
    }
  break;

  // добавить/удалить
  case 2:
    $json = isset($_COOKIE['hide']) ? $_COOKIE['hide'] : '';    
    if (empty($json) || $json == "null"){
      $res=['status'=>TRUE, 'text'=>'Объявление скрыто'];
      $arr = [$ar['adv_id']];  
    }else{
      
      // раскодировать JSON
        $arr = json_decode($json, true);
  
      // проверить есть ли объявление
        if (in_array($ar['adv_id'],$arr)){
          $arr = array_diff($arr,[$ar['adv_id']]);
          $res=['status'=>FALSE, 'text'=>'Объявление открыто'];
        }else{
          $res=['status'=>TRUE, 'text'=>'Объявление скрыто'];
          array_unshift($arr,$ar['adv_id']);  
        } 
    }
    
    // поставить лимит
      array_slice($arr, 0, 100);
        
    // записать куки
      setcookie('hide', json_encode($arr), strtotime( '+50 days' ), "/" );    
  break;    
  
  default:
    break;
}

// отдать ответ
echo json_encode($res);
exit;


