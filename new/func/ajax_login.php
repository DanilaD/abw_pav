<?php
// get data
$arg=[
  'login' => FILTER_SANITIZE_MAGIC_QUOTES,
  'pass' => FILTER_SANITIZE_MAGIC_QUOTES,
  'autologin' => FILTER_SANITIZE_SPECIAL_CHARS
];

$data = filter_input_array(INPUT_POST,$arg);

$res='';
// check login & password
if (empty($data['login']) || empty($data['pass'])) {
  $res=['status'=>FALSE,'text'=>'Укажите логин и пароль.'];
}else{
  $user = new Users($_db);  
  $res = $user->checkUserAuth($data);
}
echo json_encode($res);
exit;