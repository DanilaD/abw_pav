<?
header("Content-Type: text/html; charset=UTF-8");

$arg =[
// ID объявления
  'adv_id' => FILTER_SANITIZE_NUMBER_INT,
// ID объявления МАССИ
  'advs' => ['filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => FILTER_REQUIRE_ARRAY],
// ТИП авто = 1
  'type' => FILTER_SANITIZE_NUMBER_INT,
// ДЕЙСТВИЕ 1 - проверить, добавлено ли объявление в блокнот. 2 - добавить/удалить в/из блокнота
  'action' => FILTER_SANITIZE_NUMBER_INT 
];

$ar = filter_input_array(INPUT_POST,$arg);

// если что-то некорректно
$res=['status'=>FALSE, 'text'=>''];

empty($ar['action']) ? exit : '';

switch ($ar['action']) {
  // проверка добавлено ли объявление в блокнот 
  case 1:
    // проверить на пустоту
    empty($ar) ? exit : '';
    
    // проверить категорию авто
    if ($ar['type'] == '1'){
      $note = filter_input(INPUT_COOKIE, 'note');      
      if (empty($note) || $note == "null"){
        $res = ['status' => FALSE, 'text' => ''];
        $mes = '';
      }else{
        // раскодировать JSON
          $arr = json_decode($note);
        // проверить есть ли объявление
        $arr = is_array($arr) ? $arr : [$arr]; 
          if (in_array($ar['adv_id'],$arr)){
            $res=['status'=>TRUE, 'text'=>''];
          }
        // подсчет объявлений
          $count = count($arr);          
        // склоняем ставки
          if ($count > 0){
            require_once ROOT_DIR.'/libs/getwordform_lib.php';
            $getwordform = new getwordform();
            $forms = array('объявление','объявления','объявлений');
            $mes = $count.' '.$getwordform->getword($count, $forms); 
          }else{
            $mes = '';
          }  
      }
    
    // сообщение
      $res['message'] = $mes;
      
    }
  break;

  // добавить/удалить в/из блокнота
  case 2:
    $json = filter_input(INPUT_COOKIE, 'note'); 
    if (empty($json) || $json == "null"){
      $arr = [$ar['adv_id']];
      $res = ['status' => TRUE, 'text' => 'Объявление добавлено в избранное'];
    }else{
      
      // раскодировать JSON
        $arr = json_decode($json, true);
  
      // проверить есть ли объявление
        if (in_array($ar['adv_id'],$arr)){
          $arr = array_diff($arr,[$ar['adv_id']]);
          $res = ['status' => FALSE, 'text' => 'Объявление удалено из избранного'];
        }else{
          array_unshift($arr,$ar['adv_id']);
          $res = ['status' => TRUE, 'text' => 'Объявление добавлено в избранное'];
        } 
    }
    
    // поставить лимит
      array_slice($arr, 0, 50);
    
    // количество объявлений
      $count = count($arr);
      
    // склоняем ставки
      if ($count > 0){
        require_once ROOT_DIR.'/libs/getwordform_lib.php';
        $getwordform = new getwordform();
        $forms = array('объявление','объявления','объявлений');
        $mes = $count.' '.$getwordform->getword($count, $forms); 
      }else{
        $mes = '';
      }
      $res['message'] = $mes;
    
    // записать куки
      setcookie('note', json_encode($arr), strtotime( '+50 days' ), "/" );    
  break;    
  
    // удалить массив объявлений
  case 3:
    $json = filter_input(INPUT_COOKIE, 'note'); 
		
    if (empty($json) || $json == "null"){
      $res = ['status' => FALSE, 'text' => 'Нет сохраненных объявлений'];
    }else{
      
      // раскодировать JSON
        $arr = json_decode($json, true);

      // перебрать массив объявлений		
				$itog = array_diff($arr, $ar['advs']);

      // записать куки
        setcookie('note', json_encode($itog), strtotime( '+50 days' ), "/" );    
    
      // сообщение
        $res = ['status' => TRUE, 'text' => 'Очистка совершена из избранного'];
    }
  break;    
  
    // считаем объявления
  case 4:
    $json = filter_input(INPUT_COOKIE, 'note'); 
    if (empty($json) || $json == "null"){
      $count = '0';
    }else{  
      // раскодировать JSON
        $arr = json_decode($json, true);        
      // количество объявлений
        $count = count($arr);
    }
    
    // склоняем ставки
      if ($count > 0){
        require_once ROOT_DIR.'/libs/getwordform_lib.php';
        $getwordform = new getwordform();
        $forms = array('объявление','объявления','объявлений');
        $mes = $count.' '.$getwordform->getword($count, $forms); 
      }else{
        $mes = '';
      }      
    // сообщение
      $res = ['status' => TRUE, 'text' => '', 'message' => $mes];
  break;    
  
  default:
    break;
}

// отдать ответ
echo json_encode($res);
exit;


