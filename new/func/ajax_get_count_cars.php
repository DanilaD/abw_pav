<?php
header("Content-Type: text/html; charset=UTF-8");

require_once ROOT_DIR.'/libs/getwordform_lib.php';
//$ar['marka'] = filter_input(INPUT_POST, 'marka', FILTER_SANITIZE_NUMBER_INT);
//$ar['model'] = filter_input(INPUT_POST, 'model', FILTER_SANITIZE_NUMBER_INT);
$ar['marka'] = isset($_POST['marka']) ? $_POST['marka'] : '';
$ar['model'] = isset($_POST['model']) ? $_POST['model'] : '';

$ar['search'] = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_NUMBER_INT);

// для старого варианта
if ($ar['search'] == '2'){
  $arg =[
    'marka' => FILTER_VALIDATE_INT,
    'model' => FILTER_VALIDATE_INT,
    'year1' => FILTER_VALIDATE_INT,
    'cost_val2' => FILTER_VALIDATE_INT,
    'adv_type' => FILTER_VALIDATE_INT,
  ];
    
  $ar = filter_input_array(INPUT_POST, $arg);

  $car = NEW Cars($_db);
  $res = $car->GetCountCars($ar);
  
  // склоняем ставки
  $getwordform = new getwordform();
  $forms = array('объявление','объявления','объявлений');
  $res = $res.' '.$getwordform->getword($res, $forms); 

}elseif ($ar['search'] == '1'){
  $car = NEW Cars($_db);
  // получить значения из запроса и сформировать для sql
  $in['post'] = 'post';
  $data = $car->SearchCar($in);
  $count = $car->GetAllCars($data, 'count')['0']['count'];
  // вывод на экран
  $res = 'Найти ('.$count.')';
}



echo json_encode($res);
exit;