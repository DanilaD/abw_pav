<?
header("Content-Type: text/html; charset=UTF-8");
$ar['adv_id'] = filter_input(INPUT_POST, 'adv_id', FILTER_SANITIZE_NUMBER_INT);

if (empty($ar['adv_id'])){
  $res = ['status'=>FALSE, 'text'=>''];
}else{
  $car = new Cars($_db);   
  $res = $car->GetViewsCArs($ar['adv_id']);
  $res = ['status'=>TRUE, 'text'=>$res];
}
echo json_encode($res);
exit;