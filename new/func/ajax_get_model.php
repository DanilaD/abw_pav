<?php
header("Content-Type: text/html; charset=UTF-8");

// 1 - грузовики
$cat_id = filter_input(INPUT_POST, 'cat_id', FILTER_SANITIZE_NUMBER_INT);
$id = $_POST['name']; // может быть массив

// check
(empty($id)) ? exit : '';

// мультиселект моделей
if (is_array($id)){
  foreach ($id as $key => $value) {
    if ($value > 0){
      if (array_key_exists($value,$marks)) { 
        $res[$key]['marka'] = $marks[$value];
        $res[$key]['marka_id'] = $value;
      }
      (array_key_exists($value,$models)) ? $res[$key]['model'] = $models[$value] : '';
    }
  }
}else{
// выбр моделей определенной марки
  $res = ['Любая модель'];
  $check_models = (in_array($cat_id, ['1'])) ? $models_truck : $models;
  (array_key_exists($id,$check_models)) ? $res = $check_models[$id] + $res : '';
}

echo json_encode($res);
exit;