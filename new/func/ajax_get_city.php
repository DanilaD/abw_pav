<?
// get data
$ar['country'] = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_NUMBER_INT);

// check country
if (empty($ar['country'])) {
  $res=['status'=>FALSE,'text'=>''];
}else{
  $car_class = new Cars($_db);  
  $res = $car_class->getRegionId($ar['country']);
}
echo json_encode($res);
exit;