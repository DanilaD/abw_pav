<?
// ?cron=getsto
error_reporting(E_ALL);
ini_set('display_errors', 1);

$res = 'SELECT 
            `sale`.`id`, `sale`.`company_id`, `sale`.`title`, `sale`.`percent`
          FROM          
            `i_sto_sales` AS `sale`
          WHERE
            `sale`.`status`=\'1\'
          ';
$sql = $_db->sql($res); 

foreach ($sql as $value) {  
  //photo
  $photo = (file_exists(DIR.'/images/company_sales/'.$value["id"].'.jpg')) ? '/images/company_sales/'.$value["id"].'.jpg' : '/images/company_sales/noimg.png';
  //url
  $url = '/index.php?act=sto_search&stid='.$value['company_id'].'&do=2#sales';
  //sales
  $sale = empty($value['percent']) ? '' : $value['percent'].'%';
  // for templates
  $sales[] = [
          'title'=>$value['title'],   
          'photo'=>$photo,
          'url'=>$url,
          'sale'=>$sale,
        ];        
}
(count($sales) < 4) ? exit : '';
shuffle($sales);
// array safe to file
$content = "<?\n";
$content .= arr2str($sales);
$path = ROOT_DIR.'/array/sto_sales.php';
$handle = fopen($path, 'w');
fwrite($handle, $content);
fclose($handle);

function arr2str($arr, $cont = '', $bf = '')
{
  foreach ($arr as $k => $v)
  {
    $cbf = ($bf == '') ? "\$sto_sales['".$k."']" : $bf."['".$k."']";
    if (is_array($v))
    {
      $cont = arr2str($v, $cont, $cbf);
    }
    else
    {
      $cont .= $cbf." = '".$v."';\n";
    }
  }

  return $cont;
}