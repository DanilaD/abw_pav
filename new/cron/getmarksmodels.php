<?php
// ?cron=getmarksmodels
$car = new Cars($_db);

// unset
  unset($marks);
  unset($mar_count);
  unset($mar_all_count);
  unset($models);
  unset($marks_truck);
  unset($models_truck);
  unset($shina_size);
  unset($shina_profile);
  unset($shina_width);
  unset($shina_season);

//get marks
  $arr_marks = $car->GetAllMarka('`marka_id`,`name_eng`');
  foreach ($arr_marks as $value) {
    $marks[$value['marka_id']] = $value['name_eng']; 
  }

//get marks and count adv
  $arr_mar_count = $car->GetAllMarka('`marka_id`,`name_eng`,`adv_type_1`,`adv_type_2`',array('main'=> '0'),array('DESC'=> 'adv_type_1'),array('from' => 0, 'to' => 35));
  foreach ($arr_mar_count as $value) {
     ($value['adv_type_1'] > 0) ? $mar_count[$value['marka_id']] = ['marka' => $value['name_eng'], 'count' => $value['adv_type_1'], 'count_buy' => $value['adv_type_2']] : '';
  }
  (count($mar_count) < 30) ? exit : '';
  asort($mar_count);
 
//get all count all advs for marks
  $mar_all_count['car']='0';
  $arr_all_mar_count = $car->GetAllMarka('`adv_type_1`','','','');
  foreach ($arr_all_mar_count as $value) {
    $mar_all_count['car']+=$value['adv_type_1'];
  }
  
//get models
  $arr_models = $car->GetAllModel('`marka_id`,`model_id`,`name_eng`');
  foreach ($arr_models as $value) {
    $models[$value['marka_id']][$value['model_id']] = $value['name_eng'];
  }
/*  
//get count models
  $arr_models_count = $car->GetAllModel('`marka_id`, `name_eng`, `adv_type_1`, `adv_type_2`');
  foreach ($arr_models_count as $value) {
    $models_count[$value['marka_id']][] = ['model' => $value['name_eng'], 'sell' => $value['adv_type_1'], 'buy' => $value['adv_type_2']];
  }
*/  
//get marks truck
  $arr_marks_truck = $car->GetAllMarkaTruck('`marka_id`,`name_eng`');
  foreach ($arr_marks_truck as $value) {
    $marks_truck[$value['marka_id']] = $value['name_eng'];
  }
  
//get models
  $arr_models_truck = $car->GetAllModelTruck('`marka_id`,`model_id`,`name_eng`');
  foreach ($arr_models_truck as $value) {
    $models_truck[$value['marka_id']][$value['model_id']] = $value['name_eng'];
  }
  
// tires - get diametr
  $arr_shina_size = $car->GetAllShinaSize();
  foreach ($arr_shina_size as $value) {
    $shina_size[$value['id']] = $value['name'];
  }
  
// tires - get profile
  $arr_shina_profile = $car->GetAllShinaProfile();
  foreach ($arr_shina_profile as $value) {
    $shina_profile[$value['name']] = $value['name'];
  }
  
// tires - get weight
  $arr_shina_width = $car->GetAllShinaWidth();
  foreach ($arr_shina_width as $value) {
    $shina_width[$value['name']] = $value['name'];
  }
  
// tires - get season
  $arr_shina_season = $car->GetAllShinaSeason();
  foreach ($arr_shina_season as $value) {
    $shina_season[$value['id']] = $value['name'];
  }
  
 // unset
  unset($arr_marks);
  unset($arr_mar_count);
  unset($arr_all_mar_count);
  unset($arr_models);
  unset($arr_marks_truck);
  unset($arr_models_truck);
  unset($arr_shina_size);
  unset($arr_shina_profile);
  unset($arr_shina_width);
  unset($arr_shina_season);
  
//check
(count($marks) < 1 || count($mar_count) < 1 || count($models) < 1 || count($marks_truck) < 1 || count($models_truck) < 1 || count($shina_size) < 1 || count($shina_profile) < 1 || count($shina_width) < 1 || count($shina_season) < 1) ? exit : '';
  
// file preparation
  $content = "<?\n";  
    $content .= $car->arr2str($marks,'','$marks');
    $content .= $car->arr2str($mar_count,'','$mar_count');
    $content .= $car->arr2str($mar_all_count,'','$mar_all_count');
    $content .= $car->arr2str($models,'','$models');
    $content .= $car->arr2str($marks_truck,'','$marks_truck');
    $content .= $car->arr2str($models_truck,'','$models_truck');
    $content .= $car->arr2str($shina_size,'','$shina_size');
    $content .= $car->arr2str($shina_profile,'','$shina_profile');
    $content .= $car->arr2str($shina_width,'','$shina_width');
    $content .= $car->arr2str($shina_season,'','$shina_season');

// unset
  unset($marks);
  unset($mar_count);
  unset($mar_all_count);
  unset($models);
  unset($marks_truck);
  unset($models_truck);
  unset($shina_size);
  unset($shina_profile);
  unset($shina_width);
  unset($shina_season);
  
// save
  $car->SaveFile(ROOT_DIR.'/array/marks.php',$content);
  unset($content);
exit;