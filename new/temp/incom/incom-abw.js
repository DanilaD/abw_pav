(function($){
	
	jQuery.fn.incomAbw = function(){
		
		var content = '<div id="incom" style="width: 468px; height: 60px; outline: 1px solid #ddd; margin: 0 auto; font: normal 12px Arial;"><div style="float: left; width: 368px; line-height: 20px; text-align: center;"><p style="text-align: center; font-size: 14px; font-weight: bold; color: #c61444; margin: 0;">Купи этот автомобиль в лизинг</p><p style="margin: 0 10px;">Сумма недостающих средств <select id="incom-sum" style="width: 70px; height: 20px; line-height: 20px; margin: 0 5px; padding: 0 5px; font-size: 14px; border: none; outline: 1px solid #02245a;"><option value="3 000" data-old="30 000 000" data-p="159">3 000</option><option value="4 000" data-old="40 000 000" data-p="212">4 000</option><option value="5 000" data-old="50 000 000" data-p="265">5 000</option><option value="6 000" data-old="60 000 000" data-p="318">6 000</option><option value="7 000" data-old="70 000 000" data-p="371">7 000</option><option value="8 000" data-old="80 000 000" data-p="424">8 000</option><option value="9 000" data-old="90 000 000" data-p="477">9 000</option><option value="10 000" data-old="100 000 000" data-p="530">10 000</option><option value="11 000" data-old="110 000 000" data-p="583">11 000</option><option value="12 000" data-old="120 000 000" data-p="636">12 000</option></select> бел. руб.</p><p style="margin: 0 10px; font-size: 12px; font-weight: bold; color: #3e9c18;">Ваш платеж в месяц от <span id="incom-payment">159</span> бел. руб.</p></div><div id="incom-show-form-btn" style="float: right; width: 100px; height: 60px; color: #fff; background: #53acdf; cursor: pointer;"><p style="line-height: 15px; margin: 15px 0 0 0; text-align: center; text-transform: uppercase;">Отправить заявку</p></div><div class="clear: both;"></div><div id="incom-form" style="display: none; position: fixed; width: 100%; height: 100%; top: 0; left: 0; background: rgba(0,0,0,.5); overflow-y: auto; z-index: 100;"><div style="position: relative; width: 400px; height: 400px; margin: 0px auto; background: #fff; text-align: center; box-shadow: 0 0 20px rgba(0,0,0, 0.5); z-index: 101;"><div style="padding: 15px; text-align: center; border-bottom: 1px solid #189c9b;"><a href="http://incomleasing.by/" target="_blank"><img src="http://incomleasing.by/tmpl/site/images/logo.png" style="height: 35px;"/></a><div style="margin: 10px 0 0 0;">Лизинг на легковые автомобили для физических лиц и ИП</div></div><div id="incom-form-content" style="display: block; height: 240px; margin: 15px 0 0 0;"><div>Запрашиваемая сумма</div><div id="incom-form-sum" style="margin: 5px 0 0 0; font-size: 20px; color: #189c9b;">3 000</div><div style="margin: 5px 0 0 0;">белорусских рублей</div><div style="margin: 15px 0 0 0;"><input id="incom-form-name" type="text" value="" placeholder="Ваше имя" style="width: 150px; height: 30px; line-height: 30px; padding: 0 10px;  border: none; outline: 1px solid #02245a;"/></div><div style="margin: 15px 0 0 0;"><input id="incom-form-phone" type="text" value="" placeholder="Номер телефона" style="width: 150px; height: 30px; line-height: 30px; padding: 0 10px;  border: none; outline: 1px solid #02245a;"/></div><div style="margin: 15px 0 0 0;"><input id="incom-form-send" type="button" value="Отправить" style="width: 170px; height: 30px; line-height: 30px; color: #fff; text-transform: uppercase; background: #189c9b; box-shadow: 0px 1px 2px #000; cursor: pointer; outline: none;"/></div></div><div id="incom-form-result" style="display: none; height: 240px; margin: 15px 15px 0 15px; font-size: 14px;">Ваша заявка отправлена! В ближайшее время мы свяжемся с Вами!</div><div id="incom-form-close" style="margin: 15px 0 0 0; color: #777; cursor: pointer;">ЗАКРЫТЬ</div></div></div></div>';
		
		this.html(content);
		
		$('#incom-sum').on('change', function(){
			
			$('#incom-payment').text($(this).children('option:selected').data('p'));
		});
		
		$('#incom-show-form-btn').on('click', function(){
			
			window.open('http://incomleasing.by/abw.html','_blank');
			
			return false;
		
			$('#incom-form').show();
			$('#incom-form-sum').text($('#incom-sum').val());
			$('#incom-form-result').hide();
			$('#incom-form-content').show();
			$('#incom-form-name, #incom-form-phone').val('');
		 
			var h = $(window).height();
			if (h > 400) { $('#incom-form > div').css('margin-top', (h - 400)/2); }
		});
		
		$('#incom-form, #incom-form-close').on('click', function(){ $('body').css('overflow', 'auto'); $('#incom-form').hide(); });
		$('#incom-form > div').on('click', function(event){ event.stopPropagation(); });
		
		$('#incom-form-send').on('click', function(){
		
			var sum = $('#incom-form-sum').text();
			var name = $('#incom-form-name').val();
			var phone = $('#incom-form-phone').val();
			var url = location.href;
			
			if (name == '') { $('#incom-form-name').css('outline', '1px solid red').focus(); return false; }
			if (phone == '') { $('#incom-form-phone').css('outline', '1px solid red').focus(); return false; }
			
			$('#incom-form-send').hide();
			
			$.ajax({
				
				url:		'http://incomleasing.by/blocks/zayavka/action.php',
				type:	'post',
				data: {
					'task':	'abw',
					'sum':	sum,
					'name':	name,
					'phone':	phone,
					'url':	url
				},
				dataType: 'script'
			});
		});
		
		$('#incom-form-name, #incom-form-phone').on('change keyup', function(){
			
			$(this).css('outline', '1px solid #02245a');
		});
	};
	
})(jQuery);