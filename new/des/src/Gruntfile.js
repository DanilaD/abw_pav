module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Maybe move to package.json
		include_files: {
			js: [
				'bower_components/jquery/dist/jquery.min.js',
				'bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.min.js',
				'bower_components/owl.carousel/dist/owl.carousel.min.js',
				'bower_components/magnific-popup/dist/jquery.magnific-popup.js',
				// 'js/libs/bootstrap-select/bootstrap-dropdown.js', 
				'bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
				'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
				'bower_components/jssocials/dist/jssocials.min.js',
				'js/libs/modernizr/modernizr-custom.js', 
				// 'js/libs/social-likes-next/social-likes-next.js', 

				'js/meta/jquery-open.js',

				// preload
				'js/pre-load.js',
				'js/functions.js',

				'js/constructors/custom-popup.js',
				'js/constructors/product-slider.js',
				'js/constructors/message-popup.js',
				'js/constructors/set-options.js',
				'js/constructors/set-form-result.js',
				'js/constructors/tips-for-button.js',
				'js/constructors/fixed-content.js',

				// 'js/breakpoints.js',

				// onReady
				'js/meta/jquery-open-ready.js',
				// 'js/ready/custom-popup.js',
				'js/ready/common.js',
				'js/ready/navigation.js',
				'js/ready/filter-form.js',
				'js/ready/product-slider-ready.js',
				'js/ready/adds-archive.js',
				'js/ready/categories-list.js',
				'js/ready/stock.js',
				'js/ready/menu-tabs.js',
				'js/ready/search.js',
				'js/ready/login.js', 
				'js/ready/logout.js', 
				'js/ready/login-navigation.js',
				'js/ready/front-tips.js',
				'js/ready/social-share.js',
				'js/ready/ajax-product-load.js',
				'js/ready/sticky-header.js',
				'js/ready/favourites.js',
				'js/ready/form-submit.js',
				'js/ready/custom-tabs.js',
				'js/ready/special-offer.js',
				'js/ready/show-number.js',
				'js/ready/single-add.js',
				'js/ready/error-notification.js',
				'js/ban/incom-abw.js',
				// 'js/ban/mt-bank.js',

				'js/meta/jquery-close-ready.js',

				// onLoad
				'js/meta/jquery-open-onload.js',
				'js/onload/common.js',
				
				'js/onload/product-slider-load.js',
				'js/onload/fixed-content.js',
				// 'js/onload/front-tips.js',
				

				'js/meta/jquery-close-onload.js',

				// load after
				'js/after-load.js', 

				// cloase jquery
				'js/meta/jquery-close.js',
			],
			js_header_footer: [
				'bower_components/jquery/dist/jquery.min.js',
				'bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.min.js',
				'bower_components/magnific-popup/dist/jquery.magnific-popup.js',
				'bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
				'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
				'js/libs/modernizr/modernizr-custom.js', 

				'js/meta/jquery-open.js',

				// preload
				'js/pre-load.js',
				'js/functions.js',
				// 'js/breakpoints.js',
				// 
				'js/constructors/message-popup.js',
				'js/constructors/set-options.js',
				'js/constructors/set-form-result.js',

				// onReady
				'js/meta/jquery-open-ready.js',
				'js/ready/common_header_footer.js',
				'js/ready/navigation.js',
				'js/ready/filter-form.js',
				'js/ready/menu-tabs.js',
				'js/ready/search.js',
				'js/ready/login.js', 
				'js/ready/logout.js', 
				'js/ready/login-navigation.js',
				'js/ready/form-submit.js',
				'js/ready/error-notification.js',
				// 'js/ban/mt-bank.js',
				// 'js/ready/sticky-header.js',

				'js/meta/jquery-close-ready.js',

				// onLoad
				'js/meta/jquery-open-onload.js',
				'js/onload/common.js',
				// 'js/onload/front-tips.js',
				

				'js/meta/jquery-close-onload.js',

				// load after
				'js/after-load.js', 

				// cloase jquery
				'js/meta/jquery-close.js',
			],
			scss: [
				'scss/screen.scss'
			],
			scss_header_footer: [
				'scss/screen_header_footer.scss'
			]
		},

		sass: {
			dist: {
				options: {
					// noCache: true
				},
				files: {
					'<%= pkg.build.css %>/styles.css': '<%= include_files.scss %>',
					'<%= pkg.build.css %>/styles_header_footer.css': '<%= include_files.scss_header_footer %>',
				}
			},
			dist_compressed: {
				options: {
					// noCache: true,
					style: 'compressed'
				},
				files: {
					'<%= pkg.build.css %>/styles.min.css': '<%= pkg.build.css %>/styles.css',
					'<%= pkg.build.css %>/styles_header_footer.min.css': '<%= pkg.build.css %>/styles_header_footer.css',
				}
			}
		},

		postcss: {
			options: {
				map: true,
				// We need to `freeze` browsers versions for testing purposes.
				// browsers: ['last 2 versions', 'opera 12', 'ie 8', 'ie 9']
				processors: [
					require('pixrem')(),
					require('postcss-sorting')({ /* options */ }),
					require('autoprefixer')({browsers: ['last 2 versions', 'opera 12', 'ie 8', 'ie 9'], 'remove': false}),
				]
			},
			// prefix the specified file
			// single_file: {
			// 	src: '<%= pkg.build.css %>/styles.css',
			// 	dest: '<%= pkg.build.css %>/styles.css'
			// }
			// multiple_files: {
			// 	expand: true,
			// 	flatten: true,
			// 	src:  '<%= pkg.build.css %>/*.css',
			// 	dest: '<%= pkg.build.css %>/'
			// },
			dist: {
				src: '../css/*.css'
			}
		},


		concat: {
			dist: {
				files: {
					'<%= pkg.build.js %>/common.js': '<%= include_files.js %>',
					'<%= pkg.build.js %>/common_header_footer.js': '<%= include_files.js_header_footer %>'
				}
			}
		},

		uglify: {
			my_target: {
				options: {
					mangle: false
				},
				files: {
					'<%= pkg.build.js %>/common.min.js': '<%= pkg.build.js %>/common.js',
					'<%= pkg.build.js %>/common_header_footer.min.js': '<%= pkg.build.js %>/common_header_footer.js'
				}
			}
		},

		// clean: {
		// 	images: ['images/icons_1x']
		// },
		responsive_images: {
			options: {
				engine: 'gm',
				separator: '',
				units: {
					percentage: ''
				}
			},
			myTask: {
				options: {
					sizes: [
						{
							width: '50%',
							height: '50%',
							name: '@1x',
						}
					],
					engine: 'gm'
				},
				files: [{
					expand: true,
					src: ['**.{jpg,gif,png}'],
					cwd: 'images/icons_2x/',
					dest: 'images/icons_1x/'
				}]
			}
		},


		sprite:{
			// common: {
			// 	src: 'images/common/*.png',
			// 	dest: '../images/sprites/common.png',
			// 	imgPath: '../images/sprites/common.png',
			// 	destCss: 'scss/sprites/common.scss',
			// 	cssFormat: 'scss_maps',
			// 	algorithm: 'binary-tree',
			// 	padding: 2,
			// 	cssOpts: {
			// 		functions: false
			// 	},
			// 	cssSpritesheetName: 'common',
			// },
			icons_1x: {
				src: 'images/icons_1x/*.png',
				dest: '../img/sprites/icons_1x.png',
				// imgPath: '../images/sprites/icons_1x.png',
				destCss: 'scss/sprites/icons_1x.scss',
				cssFormat: 'scss_maps',
				algorithm: 'binary-tree',
				padding: 2,
				cssOpts: {
					functions: false
				},
				cssSpritesheetName: 'icons-1x'
			},
			icons_2x: {
				src: 'images/icons_2x/*.png',
				dest: '../img/sprites/icons_2x.png',
				// imgPath: '../images/sprites/icons_2x.png',
				destCss: 'scss/sprites/icons_2x.scss',
				cssFormat: 'scss_maps',
				algorithm: 'binary-tree',
				padding: 2,
				cssOpts: {
					functions: false
				},
				cssSpritesheetName: 'icons-2x'
			},

		},

		clean: ['svg/svg-min', 'svg/output/*', 'images/svg2png/*'], //removes old data 
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'svg/svg-default',
					src: ['*.svg'],
					dest: 'svg/svg-min',
					ext: '.svg'
				}]
			}
		},
		svgcss: {
			toCrlf: {
				options: {
					csstemplate: 'svg/scss-map-template2.hbs',
					previewhtml: null,
				},
				files: {
					'scss/sprites/svg-to-css.scss': ['svg/svg-min/*.svg']
				}
			}
		},

		watch: {
			grunt: { files: ['Gruntfile.js'] },
			sass: {
				files: ['scss/**/*.scss'],
				tasks: ['sass-handle', 'notify:watch'],
				// options: {
				// 	livereload: true,
				// 	// livereload: {
				// 	// 	port: 35729
				// 	// }
				// }
			},
			js: {
				files: ['js/**/*.js'],
				tasks: ['concat', 'uglify', 'notify:def'],
				options: {
					livereload: true,
				}
			},
			png: {
				files: ['images/**/*.png'],
				tasks: ['clean:images','responsive_images','sprite'],
				options: {
					livereload: true,
				}
			},
			livereload: {
				options: { 
					livereload: true 
				},
				files: ['<%= pkg.build.css %>/*.css'],
			},
		},

		notify: {
			def: {
				options: {
					message: 'Task Complete', //required
				}
			},
			watch: {
				options: {
					title: 'Task Complete',  // optional
					message: 'SASS and Uglify finished running', //required
				}
			},
		},

	});
	// Load in `grunt-spritesmith`
	grunt.loadNpmTasks('grunt-spritesmith');
	grunt.loadNpmTasks('grunt-responsive-images');

	grunt.loadNpmTasks('grunt-postcss');

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.loadNpmTasks('grunt-svgmin');
	grunt.loadNpmTasks('grunt-svg-css');

	grunt.loadNpmTasks('grunt-notify');

	grunt.registerTask('sass-handle', ['sass', 'postcss'/*, 'cssmin'*/]);
	grunt.registerTask('build', ['sass-handle', 'concat', 'uglify']);
	grunt.registerTask('png', ['clean:images','responsive_images','sprite']);
	grunt.registerTask('svg', ['clean', 'svgmin', 'svgcss', 'sass-handle', 'notify:def']);
};