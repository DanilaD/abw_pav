function messagePopup() {
    var self = this;

    var $body       = $('body');
    var $popupBl    = null;
    var $title       = null;

    self.init = function() {
        setPopupBlock();
    }

    self.setMessage = function( title, status, content ) {
        title   = ( typeof title !== 'undefined' ) ? title : '';
        status  = ( typeof status !== 'undefined' ) ? status : true;
        content = ( typeof content !== 'undefined' ) ? content : '';

        if( !$popupBl )
            setPopupBlock();

        $title.html( title );
        $content.html( content );

        toggle( !status );
    }

    self.show = function() {
        $.magnificPopup.open({
            items: {
                src: $popupBl[0]
            },
            type: 'inline',
            closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>',
        }, 0);
    }

    self.hide = function() {
        if( !$popupBl )
            return;

        toggle( false );
    }

    function setPopupBlock() {
        $popupBl = $('<div class="mfp-hide white-popup-block white-popup-block-inner js-popup-message">');

        $title = $('<div class="popup-title">');
        $title.html('<br>');

        $content = $('<div class="content">');

        $popupBl.append( $title );
        $content.insertAfter( $title );
        $body.append( $popupBl );

    }

    function toggle( toggle ) {
        $popupBl.toggleClass('error-message', toggle);
    }
}