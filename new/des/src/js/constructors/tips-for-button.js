function TipForButton( selector ) {
    var self    = this;

    var $tipAll = null;
    var $btnAll = null;
    var timer;
    var timerEnter;
    var timerLeave;
    // var lastAction = null;

    self.activeClass = 'shown';

    self.init = function() {
        if( !selector )
            return;

        setTips();

        if( !$tipAll.length )
            return;

        // console.log($btnAll)
        // console.log($tipAll)

        $btnAll.add( $tipAll )
            .on('mouseenter', toggleTip)
            .on('mouseleave', toggleTip);
    }

    function toggleTip( e ) {
        var $curTip = getCurrentTip( $(this) );

        if( e.type == 'mouseenter' )
	        showTip( $curTip );
        else
        	hideTip( $curTip );
    }

    function getCurrentTip( $el ) {
        if( $tipAll.is( $el ) )
            return $el;

        var $tip = $el.data('tip');

        if( !$tip )
            $tip = setDataTip( $el );

        return $tip;
        
    }

    function showTip( $tip ) {
        if( timerLeave )
        	clearTimeout(timerLeave);

        if( $tip.hasClass( self.activeClass ) )
            return;

        deactivate();

        timerEnter = setTimeout(function(){
            $tip.addClass( self.activeClass );
        }, 300);
    }

    function hideTip( $tip ) {
        if( timerEnter )
        	clearTimeout(timerEnter);

        if( !$tip.hasClass( self.activeClass ) )
            return;

        timerLeave = setTimeout(function(){
            $tip.removeClass( self.activeClass );
        }, 300);
    }

    function deactivate() {
    	// console.log( $tipAll.filter('.'+self.activeClass).length )
    	$tipAll.filter('.'+self.activeClass).removeClass( self.activeClass );
    }


    function setTips() {
        $tipAll = $( selector );
        $btnAll = $( selector ).prev();
    }

    function setDataTip( $btn ) {
        $tip = $btn.next();
        $btn.data('tip', $btn.next());

        return $tip;
    }
}