function SetFilterFormResult( $form ) {
    var self               = this;

    self.$block            = $form.parent();
    self.$resultBlock      = null;
    self.$resultBlockOuter = null;

    var $selectAll         = $('select, input:not([type=hidden]):not([type=submit])', $form);
    var isInput            = null;
    var defaultVal         = null;
    var isActivated        = false;
    var isAvailable        = false;


    self.init = function() {
        setResultBlock();

        checkAvailability();

        // console.log($selectAll)

        if( !self.isAvailable )
            return;


        setDefaultResultData();
    }

    self.refresh = function() {
        if( !isAvailable )
            return;

        // e.preventDefault();
        formReset();
    }

    self.setResult = function() {
        if( !isAvailable )
            return;

        setResult();
    }

    self.isAvailable = function() {
        return isAvailable;
    }

    function checkAvailability() {
        if( self.$resultBlock.length > 0 )
            isAvailable = true;
    }

    function selectChangeInit() {
        if( isActivated )
            return;
        // console.log( isActivated + ' isActivated selectChangeInit')

        $selectAll.on( 'change', function(){
            // console.log($(this).val())
            // if( !$(this).val() )
            //     return;
            
            setResult();
        });

        isActivated = true;
    }

    function setResult() {
        var data = $form.serializeArray();
        // console.table(data) 
        $.ajax({
            method: "POST",
            url: BASE_PATH + '?ajax=ajax_get_count_cars',
            data: data,
            success: function(response) {
                var result = JSON.parse(response);
                var status = (result.length) ? true : false;

                // console.log( result )

                if( result )
                    setHtmlResult( result );

                toggleResultBlock( status );
            },
            complete: selectChangeInit
        });
    }

    function getData() {
        var arr = [];
        $selectAll.each(function() {
            arr[arr.length] = { name: $(this).attr('name'), value: $(this).val() };
        });

        return arr;
    }

    function setHtmlResult( result ) {
        if( isInput )
            self.$resultBlock.val( result );
        else
            self.$resultBlock.text( result );
    }

    function toggleResultBlock( toggle ) {
        toggle = typeof toggle == 'undefined' ? true : toggle;

        if( self.$resultBlock.is(':visible') && toggle )
            return;

        self.$resultBlockOuter.toggleClass( 'show', toggle );
    }

    function formReset() {
        $form[0].reset();

        $.each($selectAll, function(key, el) {
            if( $(el).is('select') ){
                $(el).selectpicker('val', '');
                 $(el).selectpicker('refresh');
            } 
            // else {
            //     $(el).val('');
            // }

            if( $(el).hasClass('js-select-model') || $(el).hasClass('js-select-region') ){
                if( $(el).data('current') )
                    $(el).data('current', '');
                //     
                // $(el).trigger('change');
                
                $(el).attr('disabled', true);
                $(el).selectpicker('refresh');
            }
        });

        setResult( defaultVal );
    }

    function setResultBlock() {
        self.$resultBlock = $('#js-filter-result', self.$block);
        self.$resultBlockOuter = self.$resultBlock.parent();
    }

    function setDefaultResultData() {
        isInput = self.$resultBlock.prop("tagName") == 'INPUT';

        defaultVal = ( isInput ) ? self.$resultBlock.val() : self.$resultBlock.text();
    }
}