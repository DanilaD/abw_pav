function CustomPopup( $popup ) {
	var self 				= this;

	var $html 				= $('html');
	var $body 				= $('body');
	var $window 			= $(window);
	var $document           = $(document);
	var $popupBlock 		= null;
	var $popupContainer		= null;
	var $popupContainerIn	= null;
	var $popupBg 			= null;
	var $btnCl 				= null;

	var isActivated		= false;
	var isOpen 			= false;

	self.init = function() {
		buildBg();
		buildHtml();

		activate();

		$document.on('keyup', handleKeyup);
		$btnCl.on('click', self.close);
	}

	self.open = function() {
		if( !$popupBlock.length )
			return;

		// console.log('open')

		toggleHtml( true );

		$body.addClass('custom-popup-active');
		$popupBlock.addClass('custom-popup-open');

		isOpen = true;
	}

	self.close = function() {
		// console.log('close');
		toggleHtml( false );

		$body.removeClass('custom-popup-active');
		$popupBlock.removeClass('custom-popup-open');
	}

	self.isOpen = function() {
		return isOpen;
	}


	function activate() {
		$body.addClass('js-custom-popup-was-actived');
		$window.on('click', closeWindow );
		isActivated = true;
		// console.log('activated')
	}

	function buildBg() {
		if( $('.custom-popup-bg').length > 0 )
			return;

		$popupBg = $('<div class="custom-popup-bg">');

		$body.append( $popupBg );
	}

	function buildHtml() {
		$popupBlock = $('<div class="custom-popup-wrap">');
		$popupContainer = $('<div class="custom-popup-container">');
		$popupContainerIn = $('<div class="custom-popup-content">');
		$btnCl = $('<button title="Close (Esc)" type="button" class="mfp-close">×</button>');

		$popupContainerIn.html($popup);
		$popupContainerIn.append($btnCl);

		$popupContainer.append($popupContainerIn);
    	$popupBlock.append( $popupContainer );
    	
    	$body.append( $popupBlock );
	}

	function closeWindow(e) {
		if( !$popupContainer.is(e.target) && !$popupContainerIn.is(e.target) )
			return;
		
		if( !isOpen )
			return;

		if( $('.hide-custom-popup').is(e.target) )
			return;

		if( $(e.target).closest('.hide-custom-popup').length > 0 ) 
			return;

		self.close();
	}

	function toggleHtml( toggle ) {
		toggle = ( typeof toggle == 'boolean' ) ? toggle : true;

		var styles = {
			'overflow': ( toggle ) ? 'hidden' : '',
			// 'margin-right': ( toggle ) ? scrollbarWidth() : '',
		}

		$html.css( styles );
	}

	function handleKeyup( e ) {
        if (e.keyCode == 27)
        	self.close();
    }
}
