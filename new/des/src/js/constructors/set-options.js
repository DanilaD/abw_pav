function SetOptions( $form ) {
    var self   = this;

    self.selectMain      = null;
    self.selectDependent = null;
    self.url             = null;
    self.valueName       = 'name';
    self.afterInit       = null;
    self.sort            = false;

    var ajaxUrl          = null;
    var catId            = null;
    var isActivated      = false;
    var lastSelexctedVal = null;

    self.init = function() {
        if( !self.selectMain.length || !self.selectDependent.length )
            return;

        if( !self.url )
            return;

        setUrl();

        setCategory();

        toggleDependent();

        self.selectMain.on( 'change', setDependentVal ).trigger( 'change' );
        self.selectDependent.on( 'change', setDataSelected );

        
    }

    self.refresh = function() {
        lastSelexctedVal = {};
        toggleDependent( false );
        setDependentData();
    }

    function setDependentVal() {
        var valMain = $(this).val();

        cleanDataSeledted(valMain);

        if( !valMain || !valMain.length ){
            disableDependent();
            afterInit();
            return;
        }

        var data = getData( valMain );

        sendAjax( data );
    }

    function setDataSelected() {
        var selectedVal = self.selectDependent.val();

        if( !selectedVal || !selectedVal.length )
            return;
        // console.log( 'setDataSelected' );
        // console.log( selectedVal );
        var arrId = {};

        // setDependentData( selectedVal );

        self.selectDependent.find(':selected').each( function() {
            var id = $(this).closest('optgroup').data('id');

            if( !id )
                return;

            var value = $(this).attr('value');
 
            if( !arrId[id] )
                arrId[id] = [];

            arrId[id].push(value);

            // lastSelexctedVal[id] = selectedVal;
        })

        lastSelexctedVal = arrId;

        setDependentData();

        // console.log('setDataSelected')
        // console.log(lastSelexctedVal)
        // console.log(arrId.length)
    } 

    function cleanDataSeledted( values ) {
        // console.log(lastSelexctedVal.length)
        if( !values || !lastSelexctedVal )
            return;


        $.each(lastSelexctedVal, function(key, data) {
            if( values.indexOf(key) < 0 )
                delete lastSelexctedVal[key]; 
            // console.log(data)
        });

        setDependentData();
    }

    function toggleDependent(toggle) {
        toggle = typeof toggle == 'undefined' ? true : toggle;

        self.selectDependent.attr('disabled', toggle);
        self.selectDependent.selectpicker('refresh');

   } 

    function sendAjax( data ) {
        // console.log(data)
        // console.log(ajaxUrl)
        
        $.ajax({
            method: "POST",
            url: ajaxUrl,
            data: data,
            success: function(response) {

                var result = JSON.parse(response);

                fullModel( result );
            },
            complete: afterInit
        });
    }

    function afterInit() {
        if( !self.afterInit || isActivated )
            return;

        setDataSelected();

        // console.log('afterInit')

        self.afterInit();
        isActivated = true;
    }

    function setUrl() {
        ajaxUrl = BASE_PATH + self.url;
    }

    function setCategory() {
        if( !$form.find('[name=cat_id]').length )
            return;

        catId = $form.find('[name=cat_id]').val();
    }

    function getData( valMain ) {
        var data = {};
        data[self.valueName] = valMain;

        if( catId )
             data.cat_id = valMain;

        return data;
    }

    function getCurrentDependent() {
        var data = self.selectDependent.data('current');

        if( !data )
            return;

        data = data.toString().split(',');

        return data;
    }

    function fullModel( options ) {
        clearDependentOptions();

        if( self.selectMain.is('[multiple]') )
            setMultipleOptions( options );
        else
            setSimpleOptions( options );
        
        toggleDependent( false );
    }

    function setMultipleOptions( options ) {
        var html = '';

        $.each(options, function(key, data) {
            var $optgroup = $('<optgroup label="'+data.marka+'" data-id="'+data.marka_id+'">');
            var optgroupHtml = getOptions( data.model );

            $optgroup.html( optgroupHtml );

            self.selectDependent.append( $optgroup );
        });
    }

    function setSimpleOptions( options ) {
        var html = getOptions( options );

        self.selectDependent.html(html);
    }

    function getOptions( options, markaId ) {
        var html = '';

        markaId = typeof markaId == 'undefined' ? '' : markaId;

        var currentModel = getCurrentDependent();

        if( self.sort ){
            sortOptions = getAlphabeticalOptions( options );

            $.each(sortOptions, function(key, data) {
                var value = Object.keys(data)[0];
                var name = data[Object.keys(data)[0]];

                html += getOptionHtml(value, name, currentModel);
            });
        } else {
            $.each(options, function(value, name) {
                html += getOptionHtml(value, name, currentModel);
            });
        }

        return html;
    }

    function getOptionHtml(value, name, currentModel) {
        var selected = ( currentModel && currentModel.indexOf(value) >= 0 ) ? ' selected': '';
                
        return '<option value="' + value + '" '+selected+'>' + name + '</option>';
    }

    function clearDependentOptions() {
        var options = self.selectDependent.find('option:not(:disabled)');
        options.remove();
    }

    function disableDependent() {
        if( self.selectDependent.attr('disabled') )
            return;

        clearDependentOptions();

        toggleDependent( true );

        setDependentData();
    }

    function setDependentData() {
        var data = getDependentData();

        if( data == self.selectDependent.data('current') )
            return;

        // console.log('replace')
        // console.log(data)
 
        self.selectDependent.data('current', data );
    }

    function getDependentData() {
        if( !lastSelexctedVal )
            return;

        var data = '';


        $.each(lastSelexctedVal, function(k, d) {
            if( !d )
                return;

            if( data )
                data += ',';

            data += d.toString();
        });

        return data;
    }
}

function getAlphabeticalOptions( options ) {
    if( !options )
        return;


    var optionsArray = $.map(options, function(value, index) {
        var obj = {};
        obj[index] = value;

        return obj;
    });

    var sortOptions = optionsArray.sort( function(a, b) {
        var keyA = Object.keys(a)[0];
        var keyB = Object.keys(b)[0];
        var valueA = a[keyA].toUpperCase();
        var valueB = b[keyB].toUpperCase();

        // check key on defaul option 0:Любай модель
        if( valueA > valueB ) {
           return keyA == '0' ? -1 : 1;
        } else {
            return keyB == '0' ? 1 : -1;
        }

    });

    return sortOptions;
}