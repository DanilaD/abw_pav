function FixedBlock( $basis, $block ) {
    var self = this;

    self.$fixedBasis        = $basis;
    self.$fixedBlock        = $block;
    self.mobilePoint        = '<=sm';

    var $window             = $(window);

    var fixedBlockHeight    = self.$fixedBlock.outerHeight();
    var fixedBlockWidth     = self.$fixedBlock.outerWidth();
    
    var isMobile            = false;
    var isAvailable         = true;
    var currentScrollTop    = $window.scrollTop();
    var lastTopPos          = null;
    var basisBottom         = null;
    var blockStartPosition  = null;
    var isFixed             = false;

    self.init = function() {
        if( !self.$fixedBasis.length || !self.$fixedBlock.length )
            return;

        checkIsMobile();

        getPositions();

        checkIsAvailable();

        // console.log(self.$fixedBasis)
        // console.log(basisBottom)
        // console.log(self.$fixedBlock)

        // if( !isAvailable )
        //     return;

        basisStyles();

        handleWindowScroll();

        onWindowResize();
        
    }

    self.refresh = function() {
        if( isMobile )
            return;

        getBasisBottom();
        checkIsAvailable();
        handleScroll();
        // console.log('refresh5')
    }

    function getPositions() {
        if( !blockStartPosition )
            getBlockStartPosition();

        if( !basisBottom )
            getBasisBottom();

    }


    function getBlockStartPosition() {
        if( isMobile ) 
            return;

        if( isFixed )
            deactivate();

        blockStartPosition = {
            'top':    self.$fixedBlock.offset().top,
            'bottom': self.$fixedBlock.offset().top + fixedBlockHeight,
            'left':   self.$fixedBlock.offset().left,
        }
        // console.log(blockStartPosition)
        
    }

    function getBasisBottom() {
        // console.log( isMobile )
        if( isMobile ) 
            return;

        var fixedBasisHeight = self.$fixedBasis.outerHeight();

        basisBottom = {
            'start': blockStartPosition.top,
            'middle': self.$fixedBasis.offset().top + fixedBasisHeight - fixedBlockHeight,
            'end': self.$fixedBasis.offset().top + fixedBasisHeight,
        }
        // console.log(basisBottom)
    }

    

    function handleWindowScroll() {
        var resizeTimer;

        $window.scroll(function() {
            if( isMobile )
                return;
            
            handleScroll();
        });
    }

    function handleScroll() {
        // getPositions();

        if( basisBottom.start == basisBottom.middle )
            return;

        if( !isAvailable )
            return;

        currentScrollTop = $window.scrollTop();
        // console.log(currentScrollTop)

        if ( currentScrollTop >= basisBottom.start && currentScrollTop <= basisBottom.middle ){
            activateFixed( 0 );
        } else if( currentScrollTop > basisBottom.middle && currentScrollTop <= basisBottom.end ){
            recalcTop();
        } else if( isFixed ){
            deactivate();
        }

    }

    function activateFixed( top ) {
        if( lastTopPos == top )
            return;
        // console.log('activateFixed');

        var styles = {
            'position': 'fixed', 
            'bottom': blockStartPosition.bottom, 
            'left': blockStartPosition.left, 
            'top': top, 
            'z-index': '5',
            'width': fixedBlockWidth
        };

        lastTopPos = top;

        self.$fixedBlock.css(styles);

        isFixed = true;
    }

    function recalcTop() {
        // console.log('recalcTop');

        var top = basisBottom.end - currentScrollTop - fixedBlockHeight;
        
        if( !isFixed ){
            activateFixed( top );
        }
        else {
            self.$fixedBlock.css('top', top);
            lastTopPos = top;
        }
    }

    function _refresh() {
        getBlockStartPosition();
        handleScroll();
    }

    function deactivate() {
        // console.log('deactivate');

        var styles = {
            'position': '', 
            'bottom': '', 
            'left': '', 
            'top': '', 
            'z-index': '',
            'width': ''
        };

        self.$fixedBlock.css(styles);

        isFixed = false;
        lastTopPos = null;
    }

    function basisStyles() {
        self.$fixedBasis.css('position', 'relative');
    }

    function onWindowResize() {
        var resizeTimer;

        $window.resize(
            viewport.changed(function() {
                checkIsMobile();
            })
        );

        $window.resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                getPositions();

                if( isMobile )
                    deactivate();
                else
                    _refresh();
            }, 350);
        });
        
    };

    function checkIsMobile() {
        isMobile = viewport.is( self.mobilePoint );
    }

    function checkIsAvailable() {
        if( isMobile ) 
            return;

        if( Math.floor(basisBottom.end) == Math.floor(blockStartPosition.bottom) )
            isAvailable = false;
    }
}