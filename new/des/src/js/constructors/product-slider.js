var ProductSlider = function( $block ) {
    var self = this;

    self.thumbActiveCl = 'thumb-activated';
    self.loadingCl     = 'loading';
    self.sliderSpeed   = 200;
    self.sliderAutoheight = true;
    self.loop          = true;
    self.thumbPoint    = '<sm';
    self.popupPoint    = '<=sm';
    self.video         = false;
    self.thumbResponsiveData = {
        'xs': 3,
        'sm': 5,
        'md': 4,
        'lg': 5,
    }

    var $window        = $(window);
    var $document      = $(document);

    var $slider        = {};
    $slider.main       = $( '[data-type=main]', $block);

    var $items         = null;

    var $thumbNav      = $('.js-thumb-nav', $block);
    var $thumbVideo    = $('.js-thumb-video', $block);
    var $popup         = null;
    var $loadBl        = null;

    var isTriggered    = false;
    var isPopup        = $block.data('popup');
    var isActivated    = {
        'main': false,
        'thumb': false,
        'popup': false
    };
    var sliderInit     = false;
    var isHideThumb    = false;
    var isHidePopup    = false;

    self.init = function() {
        if( !$slider.main.length )
            return;

        checkIsMobile();

        checkVideo();

        setAdditionalObjects();

        $items.main.on('click', this, openPopup);

        if( $items.thumb )
            $items.thumb.on('click', this, handleThumbItem);

        $thumbNav.on('click', this, thumbNavAction);
        $thumbVideo.on('click', handleThumbItem);
        // $thumbVideo.on('click', self.handleThumbVideo);

        $document.on('keyup', handleKeyup);

        onWindowResize();
    }

    self.initLoadBlock = function() {
        if( !$slider.main.length )
            return;

        setLoadBlock();
    }

    function setAdditionalObjects() {
        setPopupContent();
        setOtherSlidersData();
        setMainSlider();
        setPopupSlider();
        setThumbSlider();
        setItems();

        sliderInit = true;
    }


    function setOtherSlidersData() {
        if( $( '[data-type=thumb]', $block).length && !isHideThumb && !$slider.thumb )
            $slider.thumb = $( '[data-type=thumb]', $block);

        if( isPopup && !isHidePopup && !$slider.popup ){
            $slider.popup = $( '[data-type=popup]' );
        }

        // console.log($slider)
    }

    function setItems() {
        $items = {};

        for (key in $slider) {
            $items[key] = $('.owl-item', $slider[key] );

            if( key == 'thumb' && !sliderInit )
                activateThumb( $($items[key][0]) );
            
            if( key == 'popup' && $popup )
                $slider[key].on('click', closePopup);
            
        }
        
        // console.log($items)
    }

    function setMainSlider() {
         if( isActivated.main )
            return;

        $slider.main.owlCarousel({
            autoHeight: self.sliderAutoheight,
            loop: self.loop,
            margin: 0,
            items: 1,
            video: self.video,
            center: true,
            lazyLoad: true,
            responsive : {
                0 : {
                    dots: true,
                },
                768 : {
                     dots: false,
                },
            },
            initialized: setActivated('main')
        }).on('changed.owl.carousel', syncOnChange);
    }

    function setPopupSlider() {
        if( !$slider.popup )
            return;

        if( isActivated.popup )
            return;

        // console.log('setPopupSlider');

        $slider.popup.owlCarousel({
            // autoHeight: self.sliderAutoheight,
            loop: self.loop,
            margin: 0,
            items: 1,
            nav: true,
            navText: ['',''],
            video: self.video,
            lazyLoad: true,
            center: true,
            // autoWidth: true,
            responsive : {
                0 : {
                    dots: true,
                },
                992 : {
                     dots: false,
                },
            },
            initialized: setActivated('popup'),
            // info: function() {
            //     console.log( arguments );
            // }
        })
        .on('changed.owl.carousel', syncOnChange);
    }

    function setThumbSlider() {
       if( !$slider.thumb )
            return;

        if( isActivated.thumb )
            return;

        // console.log('setThumbSlider');

        $slider.thumb.owlCarousel({
            margin: 0,
            dots: false,
            nav: false,
            responsive : {
                480 : {
                    items: (self.video) ? self.thumbResponsiveData.xs - 1 : self.thumbResponsiveData.xs,
                },
                768 : {
                     items: (self.video) ? self.thumbResponsiveData.sm - 1 : self.thumbResponsiveData.sm,
                },
                992 : {
                     items: (self.video) ? self.thumbResponsiveData.md - 1 : self.thumbResponsiveData.md,
                },
                1200 : {
                     items: (self.video) ? self.thumbResponsiveData.lg - 1 : self.thumbResponsiveData.lg,
                },
            },
            initialized: setActivated('thumb')
        });
    }

    function setActivated(type) {
        isActivated[type] = true; 

        if( type == 'main' && $loadBl )
            removeLoadBlock();

        // console.log( type + ' onInitialized ')
    }

    function setPopupContent() {
        if( !isPopup )
            return;

        if( isHidePopup )
            return;

        if( $popup && $popup.isActivated)
            return;

        var $popupBl = $('<div id="js-popup-slider" class="popup-slider hide-custom-popup">');

        $slider.popup = $('<div class="owl-carousel" data-type="popup">');
        // console.log( $popupBl );

        var $popupItems = $slider.main.find('>*');
        var popupLength = $popupItems.length;

        $.each( $popupItems, function(key, item){
            var $item = $(item).clone();
            var serialNum = key + 1;
            var $itemDesc = $('<div class="desc">');
            $itemDesc.text(serialNum+' из '+popupLength);
            $item.append( $itemDesc );
            $slider.popup.append( $item );
        });

        $popupBl.append( $slider.popup );

        // console.log( 'setPopupContent' );
        

        // $popupBl.html('<div class="owl-carousel" data-type="popup">'+$slider.main[0].innerHTML+'</div>');

        $block.append( $popupBl );

        $popup = new CustomPopup( $popupBl );
        $popup.init();
    }
 
    function openPopup() {
        if( !$popup )
            return;

        if( $(this).data('video') )
            return;

        $popup.open();
    }

    function closePopup(e) {
        if( $items.popup.is(e.target) )
            return;

        if( $(e.target).closest('.owl-item').length > 0 )
            return;

        if( $(e.target).closest('.owl-nav').length > 0 )
            return;

        $popup.close();
    }

    function synchronize( sliderType, current ) {
        if( !sliderType )
            return;

        if( current == null || current < 0 )
            return;

        // console.log(current);

        for (type in $slider) {
            if( type != sliderType) {
                // console.log(type)
                // console.log('trigger')
                isTriggered = true;
                // self.sliderLast = type;
                $slider[type].trigger('to.owl.carousel', [current, self.sliderSpeed]);
                // self.sliderLast = null;

                if( type == 'thumb' )
                    activateThumb( $($items.thumb[current]) );
            }
        }
    }

    function syncOnChange( el ) {
        // if( self.isMobile )
     //        return;

        // console.log(isTriggered + ' <--isTriggered')

        if( !isTriggered ) {
            var currentType = $( el.target ).data('type');

            var isVideo = $($items[currentType][el.item.index]).find('.item-video').length;

            if( !isVideo )
                synchronize(currentType, getPosition( el.item.index, currentType ));
            else
                handleThumbItem.bind( $thumbVideo )();
        }

        isTriggered = false;
    }

    function handleThumbItem() {
        var $item = $(this);

        if( $item.hasClass( self.thumbActiveCl ) )
            return;

        var index = ( !$item.is($thumbVideo) ) ? $item.index() : getVideoItem();

        activateThumb( $item );

        synchronize( 'thumb', index );
    }

    function activateThumb( $item ) {
        deactivateSimpleThumbs();

        if( !$item.is($thumbVideo) )
            deactivateVideoThumb();

        $item.addClass( self.thumbActiveCl );
    }

    function deactivateSimpleThumbs() {
        if( $items.thumb )
            $items.thumb.filter( '.' + self.thumbActiveCl ).removeClass( self.thumbActiveCl );
    }

    function deactivateVideoThumb() {
        if( $thumbVideo && $thumbVideo.hasClass( self.thumbActiveCl ) )
            $thumbVideo.removeClass( self.thumbActiveCl );
    }

    function thumbNavAction() {
        var $nav = $(this);
        var direction = $nav.data('direction');

        $slider.main.trigger(direction+'.owl.carousel', [self.sliderSpeed]);
    }

    function handleKeyup( e ) {
        var isOpen = $popup.isOpen();

        if( !$popup )
            return;

        if( !isOpen )
            return;

        if (e.keyCode == 37) {
            $slider.popup.trigger('prev.owl.carousel', [self.sliderSpeed]);
        } else if (e.keyCode == 39) {
            $slider.popup.trigger('next.owl.carousel', [self.sliderSpeed]);
        }
    }

    function getVideoItem() {
        // console.log($items.main.filter('[data-video]:not(.cloned)'));
        // dataset
        var videoIndex = $slider.main.data('cache-videoIndex');

        if( !videoIndex ) {
            videoIndex = $items.thumb.length;

            $slider.main.data('cache-videoIndex', videoIndex);
        }
        // console.log(videoIndex)

        return videoIndex;
    }

    function getPosition( index, currentType ) {
        var current = null;

        if( self.loop ) 
            current = getLoopPosition( index, currentType );
        else
            current = index;
 
        return current;
    }

    function getLoopPosition( index, currentType ) {
        var current = null;

        var $item = $($items[currentType][index]);
        var count = $items[currentType].length;
        
        // console.log($item)

        var notCloned = $items[currentType].filter( isNotCloned );

        notCloned.each( function(index, value){
            if( current )
                return;

            if( $(value).is($item) )
                return current = index;
        });
        
        if( current > count - 1 )
            current = 0;

        if( current == null )
            current = 0;

        return current;
    }

    function isNotCloned( index, item ) {
        if( !$(item).hasClass('cloned') )
            return $(item);
    }

    function checkVideo() {
        if( !$slider.main.find('.owl-video').length )
            self.video = false;
        else
            self.video = true;
    }


    function checkIsMobile() {
        // self.isMobile = viewport.is( self.mobilePoint );

        isHideThumb   = viewport.is( self.thumbPoint );
        isHidePopup   = viewport.is( self.popupPoint );


        // console.log(self.isMobile + ' <--self.isMobile'); 
        // console.log(isHideThumb + ' <--isHideThumb'); 
        // console.log(isHidePopup + ' <--isHidePopup'); 
    }

    function getCurrentViewport() {
        return viewport.current();
    }

    function refresh() {
        setAdditionalObjects();
    }

    function onWindowResize() {
        $window.resize(
            viewport.changed(function() {
                checkIsMobile();
                refresh();
            })
        );
    }

    function setLoadBlock() {
        if( $loadBl )
            return;

        $block.toggleClass(self.loadingCl, true);

        var currentV = getCurrentViewport();
        var thumbAmount = self.thumbResponsiveData[currentV];
        var thumbWidth = 100/thumbAmount;

        $loadBl = $('<div class="loading-block">');

        var firstImgSrc = getFirstImgSrc();

        var $main = $('<div class="main"><img src="'+firstImgSrc+'">');

        var $thumb = $('<div class="thumb">');
        for (i = 0; i < thumbAmount; i++) {
            var $thumbItem = $('<div class="item" style="width:'+thumbWidth+'%"><div class="item-inner"><div class="la-ball-clip-rotate"><div>');
            $thumb.append($thumbItem);
        }

        $loadBl.append($main);
        $loadBl.append($thumb);

        $block.append($loadBl);
    }

    function getFirstImgSrc() {
        var $firstImg = $($slider.main.find('img')[0]);
        var firstImgSrc = $firstImg.attr('src');

        if( !firstImgSrc )
            firstImgSrc = $firstImg.data('src');

        return firstImgSrc;

    }

    function removeLoadBlock() {
        $loadBl.remove();
        $block.toggleClass(self.loadingCl, false);
    }
}

// var $productSlider = new ProductSlider( $('#js-product-slider') );