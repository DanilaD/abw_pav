$('.js-fixed-content').each( function(){
    var $block = $(this);
    var $basis = $block.closest('#js-fixed-content-basis');
    var $fixedBlock = new FixedBlock( $basis, $block );
    $fixedBlock.init();

 
	var $hideBtns = document.querySelectorAll('.js-toggle-product-visibility');
	var $favBtns = $('.item-with-checkbox');

	for (i = 0; i < $hideBtns.length; ++i) {
	  $hideBtns[i].addEventListener('transitionend', toggleRefresh);
	}

	function toggleRefresh(e) {
		if( e.propertyName == 'background-position-x')
			$fixedBlock.refresh();
	}

	if( $favBtns.length )
		$favBtns.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', $fixedBlock.refresh);

});
