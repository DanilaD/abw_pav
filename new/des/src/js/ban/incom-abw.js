function incomAbw( $block ) {
	var self = this;
	self.$block = $block;

	self.$select 		= $block.find('#incom-sum');
	self.$incomPayment 	= $block.find('#incom-payment');
	// self.$incomFormSum 	= $('#incom-form-sum');
	self.$btn 			= $block.find('#incom-show-form-btn');

	self.init = function() {
		self.$select.on('change', self.changePayment);
		self.$btn.on('click', self.openIncomPage);
	};

	self.changePayment = function() {
		self.$incomPayment.text( self.$select.children('option:selected').data('p') );
		// self.$incomFormSum.text( self.$select.val() );
	};

	self.openIncomPage = function(e) {
		e.preventDefault();
		window.open('http://incomleasing.by/abw.html','_blank');
	}

}

var $incomContainer = $('#incom-container');

if( $incomContainer.length ){
	var $incom = new incomAbw( $incomContainer );
	
	$incom.init();
}