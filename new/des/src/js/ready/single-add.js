var getProductViews = function( $block ) {
    var self = this;

    self.$block      = $block;
    self.$viewsBlock = $block.find('[data-adv_id]');
    self.data        = self.$viewsBlock.data();

    self.init = function() {
        self.sendAjax();
    }

    self.sendAjax = function() {
        $.ajax({
            method: "POST",
            data: self.data,
            url: BASE_PATH + '/new/?ajax=ajax_show_views_car',
            success: function(response) {

                var result = JSON.parse(response);

                var status = result.status;
                var views = result.text;

                if( status )
                    self.setViews(views);
            }
        });
    }

    self.setViews = function(views) {
        self.$viewsBlock.text( views );
        self.$block.removeClass('hide');
    }

}

$('.js-ajax-views').each( function(){
    var $views = new getProductViews( $(this) );
    $views.init();
});


var SetToZero = function( $btn ) {
    var self = this;

    var type           = 4;
    var $popupMessage  = null;
    var $btnSubmit     = null;
    var btnSubmitHtml  = '<button class="btn-blue submit js-zero-submit">Оплатить</button>';

    // self.class = 'inactive';

    self.init = function() {
        handleAjax();
        
        $btn.on('click', openPopup);
    }

    function handleAjax() {
        var isActive = getStatus();

        var data = getData( isActive );

        if( data )
            sendAjax( data );
    }

    function setSubmitBtn() {
        $btnSubmit = $('.js-zero-submit');
        $btnSubmit.on('click', handleAjax);
    }

    function getStatus() {
        return !$btn.is(':disabled');
    }

    function getData( isActive ) {
        var data = {};
        data.type = type;

        if( isActive ){
            data.step = 2;

            var commonData = $btn.data();
            data = Object.assign(data, commonData);
        } else {
            data.step = 1;
        }

        return data;
    }

   

    function sendAjax( data ) {
        // console.log(data)
        $.ajax({
            method: "POST",
            data: data,
            url: BASE_PATH + '/?ajax=ajax_paid_car',
            success: function(response) {

                var result = JSON.parse(response);

                var status = result.status;
                var message = result.text;
                var content = '';

                // console.log( result )

                toggleBtn( !status );

                if( data.step == 1 )
                    content = getPopupContent(result.desc);

                setPopupMessage( message, status, content );
            },
            complete: function() {
                setSubmitBtn();
            },

        
        });
    }

    function getPopupContent( text ) {
        var html = '';

        // console.log($btnSubmit)

        if( text ) 
            html += text;

        html += btnSubmitHtml;

        return html;
        // return $btnSubmit;
    }

    function toggleBtn( toggle ) {
        toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;
        $btn.attr('disabled', toggle);
    }

    function setPopupMessage( message, status, content ) {
        if( !$popupMessage )
            setPopupBlock();

        $popupMessage.setMessage( message, status, content );
    }

    function openPopup() {
        if( !$popupMessage )
            return;

        $popupMessage.show();
    }
 
    function setPopupBlock() {
        $popupMessage = new messagePopup();
        $popupMessage.init();
    }
}

$('.js-set-to-zero').each( function(){
    var $setToZeroBtn = new SetToZero( $(this) );
    $setToZeroBtn.init();
});


// $('.js-dropdown-buttons').on( 'click', toggleActionButtons);

function toggleActionButtons( $btn ) {
    var self     = this;

    self.activeClass = 'open';

    var $block   = $btn.parent();
    // var $buttons = $btn.find('a').not( $(this) );
    var $window  = $(window);

    self.init =  function() {
        $btn.on('click', toggleDropdown );
        $window.on('click', closeDropdown );
    }

    function toggleDropdown() {
        var toggle = $btn.hasClass( self.activeClass );
        $btn.add( $block ).toggleClass('open', !toggle);
    }

    function closeDropdown(e) {
        if( !$btn.hasClass( self.activeClass ) )
            return;

        if( $block.is(e.target) || $(e.target).closest($block).length > 0 )
            return;

        toggleDropdown();
    }
}

$('.js-dropdown-buttons').each( function(){
    var actionBtns = new toggleActionButtons( $(this) );
    actionBtns.init();
});

var ToggleVisibilityOnScroll = function( $block ) {
    var self         = this;

    self.visibleCl   = 'show';

    var $window       = $(window);
    var scrollTop     = null;
    var isVisible     = null;
    var currentScroll = $window.scrollTop();

    self.init = function() {
        setData();
        handleBlock();
        handleWindowScroll();
    }

    function handleBlock() {
        if( currentScroll > scrollTop && !isVisible )
            toggleBlockVisibility( true );
        else if( currentScroll <= scrollTop && isVisible )
            toggleBlockVisibility( false );
    }

    function toggleBlockVisibility( toggle ) {
        toggle = ( typeof toggle !== 'undefined') ? toggle : true;
        $block.toggleClass( self.visibleCl, toggle );
        isVisible = toggle;
    }

    function setData() {
        var target = $block.data('visibility-target');
        var $targetBlock = $('#'+target);

        scrollTop = $targetBlock.offset().top + $targetBlock.outerHeight();
        isVisible = $block.hasClass( self.visibleCl );
    }

    function handleWindowScroll() {
        var resizeTimer;

        $window.scroll(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {

                currentScroll = $window.scrollTop();
                handleBlock();
  
            }, 150);
        });
    }

}
$('[data-visibility-target]').each( function(){
    var block = new ToggleVisibilityOnScroll( $(this) );
    block.init();
});
