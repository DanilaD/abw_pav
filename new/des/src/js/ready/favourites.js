function HandleFavoritekBtn( $btn ) {
    var self   = this;

    self.activeCl    = 'shown';
    self.animationCl = 'animated pulse';

    var $clBtn       = $btn.find('.js-close');
    var $messageBl   = $btn.find('.js-message');

    self.init = function() {
        if( $clBtn.length )
            $clBtn.on('click', hideBtn);
    }

    Object.defineProperty(this, "setMessage", {
       value: setMessage
    });

    function setMessage( message ) {
        if( !$messageBl && !$messageBl.length & !message )
            return;

        $messageBl.text( message );
        showBtn();

    }

    function hideBtn() {
        $btn.removeClass( self.activeCl );   
    }

    function showBtn() {
        $btn.toggleClass( self.activeCl, true );   

        activateAnimation();
    }

    function activateAnimation() {
        $btn.addClass(self.animationCl);

        $btn.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', removeAnimation);
    }

    function removeAnimation() {
        $btn.removeClass(self.animationCl);
    }
}


var favoriteBtn = new HandleFavoritekBtn( $('#js-btn-to-notebook') );
favoriteBtn.init();

function HandleFavourites( $btn ) {
    var self          = this;

    self.messageBlock  = null;
    self.removeProduct = false;
    self.type          = null;
    self.advs          = null;


    self.init = function() {
        checkStatus();

        $btn.on('click', actionOnClick);
    }

    Object.defineProperty(this, "checkFavoritesBlock", {
       value: checkBlock
    });
    Object.defineProperty(this, "removeAllFavorites", {
       value: removeAll
    });

    function toggleFavourites( toggle ) {
        if( !$btn || !$btn.length )
            return;

        toggle = ( typeof toggle !== 'undefined') ? toggle : true;
        
        $btn.toggleClass('active', toggle);

        if( self.removeProduct && !toggle )
            removeProductAction();
    }

    function actionOnClick() {
        sendAjax(2);
    }

    function sendAjax( action ) {
        var data = getData( action );
        // data.action = action;
        // console.log( data )

        $.ajax({
            method: "POST",
            data: data,
            url: BASE_PATH + '?ajax=ajax_car_note',
            success: function(response) {
                var result = JSON.parse(response);
                // console.log(result)

                if( action == 3 )
                    removeProductsAll( data.advs );
                else
                    handleResult( result );

            }
        });
    }

    function handleResult( result ) {
        var status = result.status;
        var message = ( result.message ) ? result.message : result.text; 

        toggleFavourites( status );

        if( self.messageBlock && message )
            self.messageBlock.setMessage( message );

    }


    function getData(action) {
        var data = {};

        if( self.type )
            data.type = self.type;

        if( action == 1 || action == 2 ) {
            data = $btn.data();
        } else if( action == 3  ) {
            if( self.advs )
                data.advs = self.advs;
        } 

        data.action = action;

        return data;
    }

    function checkStatus() {
        if( !$btn.is(':disabled') )
            return;

        sendAjax( 1 );
        $btn.attr('disabled', false);
    }

    function checkBlock() {
        if( !self.messageBlock )
            return;

        sendAjax( 4 );
    }

    function removeAll( advs ) {
        self.advs = advs;
        sendAjax( 3 );
    }

    function removeProductsAll(advs) {
        $.each( advs, function( key, val ) {
            var $product = $('.js-favourites[data-adv_id="'+val+'"]').closest('.js-product');
            removeItem( $product );
        })
    }

    function removeProductAction() {
        var $product = $btn.data('product');

        if( !$product ) {
            $product = $btn.closest('.js-product');
            $btn.data('product', $product);
        }

        removeItem( $product );
    }
}

$('.js-favourites').each( function() {
    var $btn = new HandleFavourites( $(this) );

    $btn.messageBlock = favoriteBtn;

    if( $(this).closest('.item-with-checkbox').length )
        $btn.removeProduct = true;

    $btn.init();
});

function checkboxAddiction( mainName, addName, $btn ) {
    var self          = this;

    self.action       = false;

    var $checkboxMain = $('[name="'+mainName+'"]');
    var $checkboxAll  = $('[name="'+addName+'"]');
    var data          = null;

    self.init = function() {
        if( !$checkboxMain.length || !$checkboxAll.length )
            return;

        $checkboxMain.on('change', toggleCheckboxAll);
        $checkboxAll.on('change', toggleCheckboxItem);

        if( self.action )
            $btn.on('click', btnAction);
    }

    function toggleCheckboxAll() {
        var isChecked = $(this).is(':checked');
        
        $checkboxAll.prop( 'checked', isChecked );
        toggleBtn();
    }
    function toggleCheckboxItem() {
        toggleBtn();
    }

    function toggleBtn() {
        if( !$btn || !$btn.length )
            return;

        setData();

        var status = ( !data.length ) ? false : true;
        $btn.attr('disabled', !status);

    }

    function setData() {
        var allVals = [];
        $checkboxAll.filter(':checked').each(function() {
           allVals.push($(this).val());
        });

        data = allVals;
    }

    function btnAction() {
        self.action.removeAllFavorites( data );

    }

}

if( $('body').hasClass('archive-products') ){
    var favourites = new HandleFavourites();
    favourites.messageBlock = favoriteBtn;
    favourites.type = $('.js-favourites').first().data('type');

    if( $('#js-btn-to-notebook').length )
        favourites.checkFavoritesBlock();

    var $defavoriteAllBtn = $('#defavorite-all');

    var checkAll = new checkboxAddiction( 'favorites-all', 'favorites-item', $defavoriteAllBtn );
    checkAll.action = favourites;
    checkAll.init();
}

function removeItem( $el ) {
    $el.addClass( 'animated bounce-out-right' );

    $el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $el.remove();
    });
}