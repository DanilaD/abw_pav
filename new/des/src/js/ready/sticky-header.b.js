function stickyHeader() {
	var self = this;

	self.$header 		= $('#site-header');
	self.$page 			= $('#page');

	self.$window 		= $(window);

	self.isMobile 				= false;
	self.lastScrollTop 			= self.$window.scrollTop();
	self.lastScrollDirection 	= null;
	self.mobilePoint 			= '<=sm';
	self.headerStartPosition	= 0;

	self.init = function() {
		self.checkIsMobile();

		self.getHeaderStartPosition();

		self.$header.css('transition', 'unset')

		// self.toggleHeaderSticky();

		// self.transformHeader();

		self.handleWindowScroll();

		self.onWindowResize();
		
	}

	self.getHeaderStartPosition = function() {
		self.headerStartPosition = {
			'top': 	 self.$page.offset().top,
			'left':  self.$page.offset().left,	
			'right': self.$window.width() - self.$page.offset().left - self.$page.outerWidth()
		}
	}

	self.handleWindowScroll = function() {
		var resizeTimer;

		self.$window.scroll(function() {
			if( self.isMobile )
				return;

			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				self.handleScroll();
			}, 250);
		});
	}

	self.handleScroll = function() {
		var currentScrollTop = self.$window.scrollTop();
		var	currentScrollDirection = self.lastScrollDirection;

		if ( currentScrollTop > self.lastScrollTop ){
		   currentScrollDirection = 'down';
		} else {
		   currentScrollDirection = 'up';
		}
		self.lastScrollTop = currentScrollTop;

		if( currentScrollDirection !== self.lastScrollDirection ) {
			self.lastScrollDirection = currentScrollDirection;

			self.transformHeader();
		}

		if( currentScrollTop <= self.headerStartPosition.top + 100 )
			self.destroy();
		else
			self.toggleHeaderSticky( true );
			// self.transformHeader();

	}

	self.toggleHeaderSticky = function( toggle ) {
		toggle = ( typeof toggle !== 'undefined') ? toggle : !self.isMobile;

		if ( toggle && self.$header.hasClass('sticky-header') )
			return;

		console.log(toggle + ' <--toggleHeaderSticky')

		var headerStyles = ''; 
		var pageStyles = ''; 
		
		if ( toggle ) {
			var headerStyles = {
				'top': 0,
				'z-index': 100
			};
			var pageStyles = {
				'padding-top': self.$header.outerHeight()
			}
		} else {
			var headerStyles = {
				'position': '', 
				'left': '', 
				'right': '', 
				'top': '', 
				'z-index': '',
				'transform': ''
			};
			var pageStyles = {
				'padding-top': ''
			}
		}

		self.$header.css(headerStyles);
		self.$page.css(pageStyles);
		self.$header.toggleClass( 'sticky-header', toggle );
	}

	self.setHeaderPosition = function() {
		if( self.lastScrollTop >= self.headerStartPosition.top ) {
			var styles = {
				'position': 'fixed',
				'left': self.headerStartPosition.left, 
				'right': self.headerStartPosition.right
			};

			setTimeout(function() {
				self.$header.css('transition', '');
			}, 350);

		} else {
			var styles = {
				'position': 'absolute',
				'left': 0, 
				'right': 0,
				'transition': 'unset'
			};
		}
		self.$header.css(styles);
	}

	self.transformHeader = function() {
		console.log('transform')
		var transform = '';

		if( self.lastScrollDirection == 'up'  )
			transform = 'matrix(1, 0, 0, 1, 0, 0)';
		else
			transform = 'translate(0%, -100%)';

		if( self.lastScrollTop <= self.headerStartPosition.top )
			transform = '';

		self.$header.css('transform', transform);

		if( !self.isMobile ) 
			self.setHeaderPosition();
	}

	self.refresh = function() {
		console.log('refresh');
		
		self.getHeaderStartPosition();
		self.transformHeader();
	}

	self.destroy = function() {
		console.log('destroy');

		self.toggleHeaderSticky( false );
	}

	self.onWindowResize = function() {
		var resizeTimer;

		self.$window.resize(
		    viewport.changed(function() {
		        self.checkIsMobile();

		        if( self.isMobile )
		        	self.destroy();

		        self.toggleHeaderSticky();
		    })
		);

		self.$window.resize(function() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				self.refresh();
			}, 350);
		});
		
	};

	self.checkIsMobile = function() {
		var self = this;

		self.isMobile = viewport.is( self.mobilePoint );
		// console.log(self.isMobile+' <--checkIsMobile')
	}
}
var $stickyHeader = new stickyHeader();

$stickyHeader.init();