// Ajax Product Load
function ajaxProductLoad($block) {
    var self = this;

    self.$block = $($block);

    self.dataAttr = self.$block.data();

    self.init = function() {
        if( !self.$block.length )
            return;

        self.getData();
        self.load_posts();
    }

    self.getData = function() {
        self.data = {};

        $.each(self.dataAttr, function(key, value) {
            self.data[key] = value;
        });
    }

    self.load_posts = function() {
        // console.log(self.data)

        $.ajax({
            method: "POST",
            url: BASE_PATH + '?ajax=ajax_get_model',
            data: self.data,
            success: function( html ) {
                self.$block.append( html );
            }
        });
    }

}
$('.js-ajax-product-load').each( function() {
    var $ajaxProductLoad = new ajaxProductLoad( $(this) );
    $ajaxProductLoad.init();
});