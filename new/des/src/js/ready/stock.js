var stockCarousel = $('.js-stock-carosel');
stockCarousel.owlCarousel({
	items: 1,
	autoHeight:false,
	loop: false,
	dots: false,
	nav: true,
	margin: 10,
	navText: ['',''],
	responsive : {
    0 : {
        items: 1,
    },
    480 : {
       items: 2,
    },
    768 : {
        items: 3,
    },
    930 : {
        items: 4,
    }
}
});