$('.js-popup-with-form').magnificPopup({
	type: 'inline',
	preloader: false,
	showCloseBtn: false,
	closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>',
});

$('#site-header select').selectpicker({
	showTick: true,
	size: 10
});
