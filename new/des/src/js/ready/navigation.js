var headerNavigation = function() {
	var self = this;
	
	self.openCl 				= 'open';
	self.mobileCl 				= 'mobile-menu-active';
	self.submenuCl 				= 'js-submenu';
	self.isMobile				= false;
	self.isDekstopMenuActive	= false;
	self.isMobileMenuActive		= false;
	self.isOpen       			= false;
 
	self.timer;
	self.submenuIsOpen			= false;

	self.$menu 					= $('#js-primary-navigation');
	self.$body 					= $('body');
	self.$headerNavigationBtn 	= $('#js-header-navigation-toggler');
	self.$hasChildren 			= $('>ul>li:has(.'+this.submenuCl+')', this.$menu);
	self.$submenuItems 			= $('.js-top-menu-subitem', this.$menu);
	self.$submenuAll 			= $('.'+self.submenuCl, self.$menu);
}

headerNavigation.prototype.mobilePoint = '<md';

headerNavigation.prototype.init = function() {
    var self = this;

    self.checkIsMobile();

	self.onWindowResize();

	self.handleSubmenu();

	self.handleMenu();

	self.$headerNavigationBtn.on('click', toggleHeaderBtn);

	function toggleHeaderBtn( e ) {
		e.preventDefault();

 		self.toggleHeaderBtn( !self.$headerNavigationBtn.hasClass( self.mobileCl ) );
	}
}

headerNavigation.prototype.handleMenu = function() {
	var self = this;

	if( self.isMobile ) {
		self.setMobileMenu();
	} else {
		self.setDekstopMenu();
	}
}

headerNavigation.prototype.setDekstopMenu = function() {
	var self = this;

	self.setSubmenuPos();
	self.setSubmenuCommonHeight();

	if( self.isDekstopMenuActive )
		return;

	// console.log('setDekstopMenu');

	self.$hasChildren.find('.js-top-menu-item').on('mouseenter', function(){
		if( self.isMobile )
			return;
 
		var $item = $(this).parent();
	
		if( self.submenuIsOpen ) {
			// if some submenu was already open - don't set timeout
			if( self.timer ) {
				self.toggleSubmenu( $item, true );
			}
		} else {
			// if any submenu wasn't already open - set timeout
			self.timer = setTimeout(function(){
		       self.toggleSubmenu( $item, true );
		       self.submenuIsOpen = true;
		    }, 200);
		}
	}).mouseleave(function(e) {
		if( self.isMobile )
			return;

		clearTimeout(self.timer);

		if( !$('.js-top-menu-item').is(e.relatedTarget) )
			self.submenuIsOpen = false;
	});

	self.$hasChildren.on('mouseleave', function(){
		// console.log('leave')
		if( self.isMobile )
			return;

		self.toggleSubmenu( $(this), false );
	});

	self.isDekstopMenuActive = true;
}

headerNavigation.prototype.setMobileMenu = function() {
	var self = this;

	if( self.isMobileMenuActive )
		return;

	// console.log('setMobileMenu');

	self.$hasChildren.find('.js-top-menu-item').on('click', function( e ){
		if( !self.isMobile )
			return;

		e.preventDefault();

		var openItems = self.$hasChildren.filter('.'+self.openCl);
		var isActive = $(this).closest('.has-children').hasClass( self.openCl );
		var $item = $(this).parent();

		if( openItems.length && !isActive ) {
			$.each(openItems, function(key, item) {
		       self.toggleSubmenu( $(item), false );
		    });
		}

		if( !isActive ) {
			self.$menu.animate({
		        scrollTop: $item.position().top
		    }, 200);
		}

		self.toggleSubmenu( $item, !isActive );
			

	});

	self.isMobileMenuActive = true;
}

headerNavigation.prototype.handleSubmenu = function() {
	var self = this;

	self.$hasChildren.each( function(){
		$(this).addClass('has-children');
		$(this).find('.top-menu-item').addClass('js-top-menu-item');
	});
}

headerNavigation.prototype.onWindowResize = function() {
    var self = this;

	$(window).resize(
	    viewport.changed(function() {
	        self.checkIsMobile();

	        self.restart();
    		self.handleMenu();
	    })
	);
};

headerNavigation.prototype.setSubmenuPos = function() {
	var self = this;

	if( self.isMobile )
		return;

	var menuPos = self.$menu.parent().offset();

	self.$submenuAll.each(function() {
		var item = $(this).closest('.has-children');

		if( !item.length )
			return;

		var itemPos = item.offset();

		var subPos = itemPos.left - menuPos.left;

		$(this).css('left', subPos);

	});
}

headerNavigation.prototype.setSubmenuCommonHeight = function() {
	var self = this;

	if( self.isMobile )
		return;

	var submenuHeightAll = [];

	self.$submenuAll.each(function() {

		$('.js-column', $(this)).each(function() {
			submenuHeightAll.push($(this).height());
		});
	});

	// console.log(submenuHeightAll);

	var height = submenuHeightAll.reduce( function(pre, cur){
    	return Math.max( pre, cur );
    }, 0);

    // console.log(height);

	self.$submenuAll.each(function() {
		$('.submenu-inner', $(this)).height( height );
	});

}

headerNavigation.prototype.toggleSubmenu = function( $item, toggle ) {
	var self = this;

	var $submenu = $( '.'+self.submenuCl, $item );
	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	$item.add('.js-top-menu-item', $item).toggleClass( self.openCl, toggle );

	// self.$body.toggleClass( 'menu-active', toggle );

	// console.log( self.isMobile )

	if( self.isMobile )
		$submenu.toggleClass( self.openCl, toggle );
	else
		self.$body.toggleClass( 'dekstop-menu-active', toggle );
}

// headerNavigation.prototype.setSubmenuHeight = function( $item, toggle ) {
// 	var self = this;

// 	var $submenu = $( '.'+self.submenuCl, $item );
// 	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;


// 	var height = ( toggle ) ? $('.submenu-inner', $submenu).outerHeight() : 0;

// 	$submenu.css('height', height);
// }

headerNavigation.prototype.toggleHeaderBtn = function( toggle ) {
	var self = this;

	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	self.$menu.add(self.$body).add(self.$headerNavigationBtn).toggleClass( self.mobileCl, toggle );

	self.isOpen = toggle;

	// hide search block in header
	if( toggle && $searchBlock.isOpen )
		$searchBlock.toggleSearch( false );

	// hide login menu in header
	if( toggle && $loginNavigation.isOpen )
		$loginNavigation.toggleBlock( false );
}

headerNavigation.prototype.restart = function() { 
	var self = this;

	self.toggleHeaderBtn( false );

	self.$hasChildren.each( function() {
		self.toggleSubmenu( $(this), false );

		$(this).find('.'+self.submenuCl).add('.submenu-inner', $(this)).height('');
	})
	
}

headerNavigation.prototype.checkIsMobile = function() {
	var self = this;

	self.isMobile = viewport.is( self.mobilePoint );
}


var $primaryNavigation = new headerNavigation();
$primaryNavigation.init();