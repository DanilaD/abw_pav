function stickyHeader() {
	var self = this;

	self.$header 		= $('#site-header');
	self.$page 			= $('#page');
	self.$body 			= $('body');

	self.$window 		= $(window);

	self.headerHeight			= self.$header.outerHeight();
	self.scrollInterval 		= 0;

	self.isMobile 				= false;
	self.lastScrollTop 			= self.$window.scrollTop();
	self.lastScrollDirection 	= null;
	self.mobilePoint 			= '<=sm';
	self.headerStartPosition	= 0;
	self.top					= 0;

	self.isSticky				= false;

	self.init = function() {
		if( !self.$body.hasClass('front-page') )
			return;
		
		self.checkIsMobile();

		self.handleWindowScroll();

		self.onWindowResize();
		
	}

	self.getHeaderStartPosition = function() {
		if( self.isMobile ) 
			return;

		self.headerStartPosition = {
			'bottom': self.$page.offset().top + self.headerHeight,
			'top': 	  self.$page.offset().top,
			'left':   self.$page.offset().left,	
			'right':  self.$window.width() - self.$page.offset().left - self.$page.outerWidth(),
		}
		// console.log(self.headerStartPosition)

	}

	self.handleWindowScroll = function() {
		var resizeTimer;

		$(window).scroll(function() {
			if( self.isMobile )
				return;

			// clearTimeout(resizeTimer);
			// resizeTimer = setTimeout(function() {
				if( !self.headerStartPosition )
					self.getHeaderStartPosition();

				self.handleScroll();
			// }, 150);
		});
	}

	self.handleScroll = function() {
		// console.log(self.$page.offset().left)
		var currentScrollTop = self.$window.scrollTop();
		var	currentScrollDirection = self.lastScrollDirection;

		if ( currentScrollTop > self.lastScrollTop ){
		   currentScrollDirection = 'down';
		} else {
			currentScrollDirection = 'up'; 
		}
		self.scrollInterval = currentScrollTop - self.lastScrollTop;
		self.lastScrollTop = currentScrollTop;


		if( currentScrollDirection !== self.lastScrollDirection ) {
			self.lastScrollDirection = currentScrollDirection;

			if( currentScrollTop > self.headerStartPosition.top ) {
				if( !self.isSticky )
					self.setSticky();

				self.setHeaderTop();
			}
		}

		if( currentScrollTop <= self.headerStartPosition.top )
			self.destroy();

		if ( self.top < 0 && self.top > -self.headerHeight )
			self.setHeaderTop();
	}

	self.setSticky = function() {
		self.toggleHeaderSticky(true);
		self.setHeaderLeftRight();
	}

	self.setHeaderTop = function() {
		if( self.isMobile )
			return;
		// console.log(self.lastScrollDirection)
		self.getHeaderTopPosition();
		

		self.$header.css('top', self.top);
	}

	self.setHeaderLeftRight = function() {
		if( self.isMobile )
			return;

		if( !self.isSticky )
			return;

		var styles = {
			'left': self.headerStartPosition.left, 
			'right': self.headerStartPosition.right,
		};

		self.$header.css(styles);
	}

	self.getHeaderTopPosition = function() {
		var top = '';

		if( self.scrollInterval <= self.top + self.headerHeight ) {
			top = self.top - self.scrollInterval;
		} else {
			if ( self.lastScrollDirection == 'up' )
				top = 0;
			else
				top = -self.headerHeight;
		}

		if( top > 0 ) {
			top = 0;
		}

		self.top = top;
		// console.log(self.top)
	}

	self.toggleHeaderSticky = function( toggle ) {
		toggle = ( typeof toggle !== 'undefined') ? toggle : true;


		var position = ( toggle ) ? 'fixed' : '';
		var zindex = ( toggle ) ? 100 : '';

		self.$header.css({'position': position, 'z-index': zindex});

		self.isSticky = toggle;
	}


	self.refresh = function() {
		// console.log('refresh');
		
		self.getHeaderStartPosition();
		self.setHeaderLeftRight();
	}

	self.destroy = function() {

		if ( !self.isSticky )
			return;

		// console.log('destroy');

		var headerStyles = {
			// 'position': '', 
			'left': '0', 
			'right': '0', 
			'top': '0', 
			'z-index': ''
		};

		self.$header.css(headerStyles);

		self.toggleHeaderSticky( false );

		// self.toggleHeaderSticky( false );
	}

	self.onWindowResize = function() {
		var resizeTimer;

		self.$window.resize(
		    viewport.changed(function() {
		        self.checkIsMobile();

		        if( self.isMobile )
		        	self.destroy();

		        // self.toggleHeaderSticky();
		    })
		);

		self.$window.resize(function() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				self.refresh();
			}, 350);
		});
		
	};

	self.checkIsMobile = function() {
		var self = this;

		self.isMobile = viewport.is( self.mobilePoint );
		// console.log(self.isMobile+' <--checkIsMobile')
	}
}
var $stickyHeader = new stickyHeader();
setTimeout(function() {
	$stickyHeader.init();
}, 450);
