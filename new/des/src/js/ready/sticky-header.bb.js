function stickyHeader() {
	var self = this;

	self.$header 		= $('#site-header');
	self.$page 			= $('#page');

	self.$window 		= $(window);

	self.isMobile 				= false;
	self.lastScrollTop 			= self.$window.scrollTop();
	self.lastScrollDirection 	= null;
	self.mobilePoint 			= '<=sm';
	self.headerStartPosition	= 0;

	self.init = function() {
		self.checkIsMobile();

		// self.getHeaderStartPosition();

		self.toggleHeaderSticky();

		self.setHeaderPosition();

		// self.transformHeader();

		self.handleWindowScroll();

		self.onWindowResize();
		
	}

	self.getHeaderStartPosition = function() {
		if( self.isMobile ) 
			return;

		self.headerStartPosition = {
			'bottom': self.$page.offset().top + self.$header.outerHeight() + 200,
			'top': 	  self.$page.offset().top,
			'left':   self.$page.offset().left,	
			'right':  self.$window.width() - self.$page.offset().left - self.$page.outerWidth(),
		}
		// console.log(self.headerStartPosition)

	}

	self.handleWindowScroll = function() {
		var resizeTimer;

		self.$window.scroll(function() {
			if( self.isMobile )
				return;

			// clearTimeout(resizeTimer);
			// resizeTimer = setTimeout(function() {
				if( !self.headerStartPosition )
					self.getHeaderStartPosition();

				self.handleScroll();
			// }, 150);
		});
	}

	self.handleScroll = function() {
		// console.log(self.$page.offset().left)
		var currentScrollTop = self.$window.scrollTop();
		var	currentScrollDirection = self.lastScrollDirection;

		if ( currentScrollTop > self.lastScrollTop ){
		   currentScrollDirection = 'down';
		} else {
		   currentScrollDirection = 'up'; 
		}
		self.lastScrollTop = currentScrollTop;


		if( currentScrollDirection !== self.lastScrollDirection ) {
			self.lastScrollDirection = currentScrollDirection;

			self.transformHeader();
		}

		if( currentScrollTop <= self.headerStartPosition.bottom )
			self.transformHeader();

	}

	self.toggleHeaderSticky = function( toggle ) {
		toggle = ( typeof toggle !== 'undefined') ? toggle : !self.isMobile;

		if ( toggle && self.$header.hasClass('sticky-header') )
			return;

		var headerStyles = ''; 
		var pageStyles = ''; 
		
		if ( !self.isMobile ) {
			var headerStyles = {
				'top': 0,
				'z-index': 100
			};
			var pageStyles = {
				'padding-top': self.$header.outerHeight()
			}
		} else {
			var headerStyles = {
				'position': '', 
				'left': '', 
				'right': '', 
				'top': '', 
				'z-index': '',
				'transform': ''
			};
			var pageStyles = {
				'padding-top': ''
			}
		}

		self.$header.css(headerStyles);
		self.$page.css(pageStyles);
		self.$header.toggleClass( 'sticky-header', !self.isMobile );


	}

	self.setHeaderPosition = function() {
		if( self.isMobile ) 
			return;

		if( self.lastScrollTop >= self.headerStartPosition.bottom ) {
			var styles = {
				'position': 'fixed',
				'left': self.headerStartPosition.left, 
				'right': self.headerStartPosition.right
			};

			setTimeout(function() {
				self.$header.css('transition', '');
			}, 350);

		} else {
			var styles = {
				'position': 'absolute',
				'left': 0, 
				'right': 0,
				'transition': 'unset'
			};
		}
		self.$header.css(styles);
	}

	self.transformHeader = function() {
		if( self.isMobile ) 
			return;

		var transform = '';

		if( self.lastScrollDirection == 'up'  )
			transform = 'matrix(1, 0, 0, 1, 0, 0)';
		else
			transform = 'translate(0%, -100%)';

		if( self.lastScrollTop <= self.headerStartPosition.bottom )
			transform = '';

		// console.log(transform)
		self.$header.css('transform', transform);

		self.setHeaderPosition();
	}

	self.refresh = function() {
		// console.log('refresh');
		
		self.getHeaderStartPosition();
		self.transformHeader();
	}

	self.destroy = function() {
		// console.log('destroy');

		self.toggleHeaderSticky( false );
	}

	self.onWindowResize = function() {
		var resizeTimer;

		self.$window.resize(
		    viewport.changed(function() {
		        self.checkIsMobile();

		        if( self.isMobile )
		        	self.destroy();

		        self.toggleHeaderSticky();
		    })
		);

		self.$window.resize(function() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				self.refresh();
			}, 350);
		});
		
	};

	self.checkIsMobile = function() {
		var self = this;

		self.isMobile = viewport.is( self.mobilePoint );
		// console.log(self.isMobile+' <--checkIsMobile')
	}
}
var $stickyHeader = new stickyHeader();
setTimeout(function() {
	$stickyHeader.init();
}, 350);
