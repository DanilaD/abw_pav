function handleFormLogout( $btn ) {
    var self = this;

    self.$body      = $('body');
    self.$btn       = $btn;
    self.$messageBl = null;

    self.init = function() {
        self.setMessageBlock();
        
        self.$btn.on('click', self.logout);
    }

    self.openPopup = function() {
        $.magnificPopup.open({
            callbacks: {
                afterClose: function() {
                    location.reload();
                }
            },
            items: {
                src: self.$messageBl[0]
            },
            type: 'inline',
            closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>',
        }, 0);
    }

    self.logout = function() {
        $.ajax({
            method: "POST",
            url: BASE_PATH + '?ajax=ajax_exit',
            success: function(response) {

                var result = JSON.parse(response);

                var message = result.text;

                if( !message )
                    return;

                self.setMessage( message );
                self.openPopup();
            }
        });
    }

    self.setMessageBlock = function() {
        self.$messageBl = $('<div class="mfp-hide white-popup-block white-popup-block-inner js-form-logout-message">');
        self.$body.append( self.$messageBl );

        self.setMessage('<br>');
    }

    self.setMessage = function( message ) {
        self.$title = $('<div class="popup-title">');
        self.$title.html( message );
        self.$messageBl.html( self.$title );
    }
}


$('.js-form-logout').each( function() {
    var $btn = new handleFormLogout( $(this) );

    $btn.init();
});