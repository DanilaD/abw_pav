function handleFormSubmit( $form ) {
    var self = this;

    self.$body       = $('body');
    self.$form       = $form;
    self.$submitBtn = $('[type=submit]', self.$form);
    self.$popupBl  = null;
    self.$errorBl  = null;

    self.url             = $form.data('url');
    self.submitSleepmode = '';
    self.submitValue     = '';

    self.init = function() {
        self.setSubmitTexts();
        self.$form.on('submit', self.submit);
    }

    // self.openPopup = function() {
    //     $.magnificPopup.open({
    //         items: {
    //             src: self.$popupBl[0]
    //         },
    //         type: 'inline',
    //         closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>',
    //     }, 0);
    // }

    self.submit = function( e ) {
        e.preventDefault();

        var data = self.$form.serializeArray();
        var formUrl = BASE_PATH + '/?ajax=' + self.url;

        $.ajax({
            method: "POST",
            url: formUrl,
            data: data,
            beforeSend:  function() {
                self.sleepmode( true );
            },
            success: function(response) {
                // console.log(response)
                var result = JSON.parse(response);
                var message = result.text;
                var status = result.status;
                // console.log(message)
                // console.log(status)

                if( !message )
                    return;

                if( status ){
                    self.setPopupMessage( message );
                    // self.openPopup();
                } else {
                    self.setErrorMessage( message );
                }

            },
            complete: function() {
                self.sleepmode( false );
            }
        });
    }

    self.setSubmitTexts = function() {
        self.submitSleepmode = ( self.$submitBtn.data('sleepmode') ) ? self.$submitBtn.data('sleepmode') : 'Пожалуйста, подождите';
        self.submitValue     = self.$submitBtn.text();
    }

    self.setPopupBlock = function() {
        self.$popupBl = new messagePopup();
        self.$popupBl.init();
    }

    self.setPopupMessage = function( message ) {
        if( !self.$popupBl )
            self.setPopupBlock();


        self.$popupBl.setMessage( message );
        self.$popupBl.show();
    }

    self.setErrorBlock = function() {
        self.$errorBl = $('<div class="js-error error-message">');
        self.$errorBl.insertAfter( self.$form.find('.title') );
    }

    self.setErrorMessage = function( message ) {
        if( !self.$errorBl )
            self.setErrorBlock();

        self.$errorBl.text( message );
    }

    self.sleepmode = function( toggle ) {
        toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

        var text = ( toggle ) ? self.submitSleepmode : self.submitValue;

        self.$submitBtn.attr('disabled', toggle).text( text );
    }
}


$('.js-form-submit').each( function() {
    var $form = new handleFormSubmit( $(this) );

    $form.init();
});