function ShowNumber( $block ) {
    var self = this;

    self.$btn          = $block.find('[data-adv_id]');
    self.$mobileBl     = $block.find('.js-mobile');
    self.$userBl       = $block.find('.js-user-buttons');
    self.$userShowBtn  = self.$userBl.find('.js-show-ajax-buttons');
    self.$userAjaxBtn  = self.$userBl.find('.js-ajax-button');

    var data          = self.$btn.data();

    var $errorBlock    = null;
    var $loadBl        = null;

    self.init = function() {
        initLoadBlock();
        self.$btn.on('click', sendAjax);
        self.$userShowBtn.on('click', showAjaxBtns);
    }

    function sendAjax() {
        $.ajax({
            method: "POST",
            data: data,
            url: BASE_PATH + '/new/?ajax=ajax_show_phone',
            beforeSend: function() {
                if( $loadBl )
                    $loadBl.show();
            },
            success: function(response) {

                var result = JSON.parse(response);

                // console.log(result);
                var status = result.status;
                var message = result.text;

                if( status ){
                    if( self.$errorBl )
                        setErrorMessage( '' );

                    showPhones( message );
                } else {
                    self.$userBl.hide();
                    setErrorMessage( message );
                }
            },
            complete: function() {
                if( $loadBl )
                    $loadBl.hide();
            },
        });
    }

    function showPhones( message ) {
        var phonesHtml = '';
        $.each( message, function( key, value ) {
            phonesHtml += '<div>'+value+'</div>';
        });

        self.$mobileBl.html(phonesHtml);
        self.$userBl.show();
        
    }

    function showAjaxBtns() {
        self.$userShowBtn.hide();
        self.$userAjaxBtn.show();
    }

    function setErrorMessage( message ) {
        if( !$errorBl )
            setErrorBlock();

        $errorBlock.text( message );
    }

    function setErrorBlock() {
        $errorBlock = $('<div class="js-error error-message">');
        $block.append( $errorBlock );
    }

    function initLoadBlock() {
        if( $loadBl )
            return;

        $loadBl = new Loading( $block );
        $loadBl.init();
    }
}


$('.js-show-phone').each( function() {
    var $showBtn = new ShowNumber( $(this) );

    $showBtn.init();
});


function Loading( $block ) {
    var self = this;

    self.mainCl     = 'loading-block';
    self.activeCl   = 'loading-block-activate';

    var $loadBl     = null;

    self.init = function() {
        setLoadBlock();
    }

    self.show = function() {
        toggleLoadBlock( true );
    }

    self.hide = function() {
        toggleLoadBlock( false );
    }

    function setLoadBlock() {
        if( $loadBl )
            return;

        $block.toggleClass(self.mainCl, true);

        $loadBl = $('<div class="la-animation la-ball-clip-rotate"><div>');
        $block.append( $loadBl );

    }
    function toggleLoadBlock( toggle ) {
        toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

        $block.toggleClass(self.activeCl, toggle);
    }
}