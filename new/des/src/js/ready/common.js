$('.js-popup-with-form').magnificPopup({
	type: 'inline',
	preloader: false,
	showCloseBtn: true,
	closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>'
});

$('select').selectpicker({
	showTick: true,
	size: 10,
	// showContent: false,
	// selectedTextFormat: 'count > 3'
});

$('.js-close-magnific-popup').on('click', function() {
	$.magnificPopup.close();
})

