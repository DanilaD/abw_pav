function specialOffers( $offerBlock ) {
    var self = this;

    self.$tabsBl        = $('.js-custom-tabs');
    var $sendAjaxBtn   = self.$tabsBl.find('.js-send-special-offer');

    var $body          = $('body');
    var $btnAll        = $('[data-popup="'+$offerBlock.attr('id')+'"]');
    // var $popupBl       = null;
    var $errorBl       = null;
    var $popupMessage  = null;

    var $tabs          = null;
    var tabsActive     = {};
    var iu             = null;


    self.init = function() {
        setTabs();

        activateTabsAll();

        $btnAll.on('click', openPopup);

        $sendAjaxBtn.on('click', sendOffer);
    }


    function setTabs() {
        $tabs = new Tabs( self.$tabsBl );

        $tabs.init();
        // $tabs.stop = true;
        // $tabs.before = beforeTabs;
        // $tabs.after = openPopup;
    }

    // function beforeTabs() {
    //     var $curentTab = this.$curentTab;
    //     var type = $curentTab.data('type');
    //     var data = {'type': type};

    //     if( tabsActive[type] )
    //         return;

    //     console.log(type)

    //     sendAjax_1( data, $curentTab );
    //     // else
    //     //     openPopup();

    //     // console.log( type + ' <--is not activated' )

    //     // sendAjax_1( data, $curentTab );
    // }
    // 
    // function showTab() {
    //     var target = $(this).data('target');
    //     $tabs.show( target );
    // }

    function activateTab() {
        var target = $(this).data('target');
        var $curentTab = $(target);
        var type = $curentTab.data('type');
        var data = {'type': type};

        if( tabsActive[type] )
            return;

        sendAjax_1( data, $curentTab, target );

        
        // $tabs.show( target );
    }

    function activateTabsAll() {
        $tabs.$tabsNavEl.each( activateTab );
        // console.log($tabs.$tabsNavEl)
    }

    function openPopup() {
        var target = $(this).data('target');
        
        $.magnificPopup.open({
            items: {
                src: $offerBlock[0]
            },
            type: 'inline',
            closeMarkup: '<button title="Закрыть" type="button" class="mfp-close">&#215;</button>',
            callbacks: {
                beforeOpen: $tabs.show( target )
            }
        }, 0);
    }

    function sendOffer() {
        var $currentPane = getPane( $(this) );

        var data = getAllData($currentPane);

        data.type = $currentPane.data('type');

        sendAjax_2( data, $(this) );
    }

    function getAllData( $block ) {
        var serializeData = $( ' *', $block ).serializeArray();
        var data = {};

        $.each( serializeData, function( k, v ) {
              data[v.name] = v.value;
        });

        return data;
    }


    function getPane( $el ) {
        var currentPane = $el.data('pane');

        if( !currentPane ){
            currentPane = $el.closest('.tab-pane');
            $el.data('pane', currentPane);
        }

        return currentPane;
    }

    function sendAjax_1( data, $curentTab, target ) {
        data = ( typeof data !== 'undefined' ) ? data : {};
        data.step = 1;

        // console.log( data );
// 
        $.ajax({
            method: "POST",
            data: data,
            url: BASE_PATH + '/?ajax=ajax_paid_car',
            success: function(response) {

                var result = JSON.parse(response);
                var status = result.status;

                if( status )
                    activeTarget( data.type, target );
                else
                    deactiveTarget( data.type, target, result.text );

                if( result.cost_ie ){
                    setCostIe( result.cost_ie );
                    fillContent( 'js-cost_ie', result.cost_ie, $curentTab );
                }

                if( result.np_type && typeof result.cost == 'object' && result.cost )
                    handleNewsType( result.np_type, result.cost, $curentTab );

                if( result.np_week )
                    handleNewsWeek( result.np_week, $curentTab );

                if( result.cost )
                    handleCost( result.cost, $curentTab );
                    

                if( result.desc )
                    fillContent( 'js-desc', result.desc, $curentTab );

                if( result.bets )
                    handleBets( result.cost, result.bets, $curentTab );

                // console.log( result );
            }
        });
    }

    function sendAjax_2( data, $btn ) {
        data = ( typeof data !== 'undefined' ) ? data : {};
        data.step = 2;

        // console.log(data)

        var commonData = self.$tabsBl.data();
        var resultData = Object.assign(data, commonData);

        // console.log( resultData );

        $.ajax({
            method: "POST",
            data: resultData,
            url: BASE_PATH + '/?ajax=ajax_paid_car',
            beforeSend: function() {
                toggleSubmutBtn( $btn, false )
            },
            success: function(response) {

                var result = JSON.parse(response);
                var status = result.status;
                var message = result.text;

                setResultMessage( result.text, result.status );
 
                // console.log( result );
            },
            complete: function() {
                toggleSubmutBtn( $btn, true )
            },
        });
    }
    
    function activeTarget( type, target ) {
        tabsActive[type] = true;

        toggleBtn( target, true );

        // openPopup();
        // $tabs.stop = false;

        if( $errorBl )
            $errorBl.hide();
    }

    function deactiveTarget( type, target, message ) {
        toggleBtn( target, false );
        // $tabs.stop = true;

        setErrorMessage( message );
    }

    function toggleBtn( target, toggle ) {
        toggle = ( typeof toggle !== 'undefined') ? toggle : true;
        $btnAll.filter('[data-target="'+target+'"]').attr('disabled', !toggle);
    }

    function setErrorMessage( message ) {
        if( !$errorBl )
            setErrorBlock();

        $errorBl.show( message );
    }

    function setErrorBlock() {
        $errorBl = new errorNotification();
        $errorBl.init();
    }


    function setCostIe( costIe ) {
        if( iu )
            return;

        iu = costIe;
        $('[data-iu]').data('ui', iu);
    }

    function handleCost( cost, $curentTab ) {
        if( !$curentTab.length)
            return;

        // console.log( cost )

        fillContent( 'js-cost', cost, $curentTab );

        var $elIU = $curentTab.find('.js-conversion-iu');
        var $elBYN = $curentTab.find('.js-conversion-byn');

        if( $elIU.length > 0 && $elBYN.length > 0){
            var conversion = new uiConversion( cost, $elIU, $elBYN );
            conversion.setData( 1 );
        }
    }

    function handleNewsType( options, cost, $curentTab ) {
        if( !$curentTab.length)
            return;

        var newsType = new setNewsType( options, cost, $curentTab );
        newsType.init();
    }

    function handleBets( cost, betsOptions, $curentTab ) {
        if( !$curentTab.length)
            return;

        var bets = new betsSelect( cost, betsOptions, $curentTab );
        bets.init();
    }

    function handleNewsWeek( options, $curentTab ) {
        if( !$curentTab.length)
            return;

        var newsWeek = new setSelect( 'np_week', options );
        var $select = newsWeek.getSelect();

        $curentTab.find('.js-news-week').append( $select );

        newsWeek.updateSelect();
    }

    function setResultMessage( message, status ) {
        if( !$popupMessage )
            setPopupBlock();

        $popupMessage.setMessage( message, status );
        $popupMessage.show();
    }

    function setPopupBlock() {
        $popupMessage = new messagePopup();
        $popupMessage.init();
    }

    function toggleSubmutBtn( $btn, toggle ) {
        toggle = ( typeof toggle !== 'undefined') ? toggle : true;
        $btn.attr('disabled', !toggle);
    }
}


var specialOffers = new specialOffers( $('#js-popup-offers') );
specialOffers.init();

// $('.js-popup-tabs').each( function() {
//     var $btn = new specialOffers( $(this) );

//     $btn.init();
// });


function fillContent( elClass, content, $block ) {
    if( !$block.length)
        return;

    $block.find('.'+elClass).html(content);
}

function setNewsType( options, cost, $curentTab ) {
    var self = this;

    self.$block      = $curentTab;
    self.$newsTypeBl = self.$block.find('.js-news-type');
    self.$newsInput  = null;
    self.options     = options;
    self.cost        = cost;

    self.init = function() {
        if( !self.$newsTypeBl.length)
            return;

        self.setHtml();

        self.$newsInput =  $('input[name="np_type"]');

        self.setFirst();
    }


    self.setFirst = function() {
        var firstKey = Object.keys(self.cost)[0];

        self.$newsInput.filter('[value='+firstKey+']').attr('checked', 'checked');
    }

    self.setHtml = function() {
        $.each( self.options, function( key, value ) {
            var elId = 'np_type_'+key;

            var $el = $('<div>');
            var $input = self.getInput( key, elId );
            var $label = self.getLabel( key, value, elId )

            $el.append( $input );
            $el.append( $label );

            self.$newsTypeBl.append( $el );
        });
    }

    self.getInput = function( key, elId ) {
        var $input = $('<input>');

        $input
            .attr('type', 'radio')
            .attr('name', 'np_type')
            .attr('value', key)
            .attr('id', elId);

        return $input;
    }

    self.getLabel = function( key, value, elId ) {
        var $label = $('<label>');
        var $labelIcon = self.getLabelIcon( key );
        var $labelContent = self.getLabelContent( key, value );
       

        $label.attr('for', elId);

        $label.append( $labelIcon );
        $label.append( $labelContent );
       

        return $label;
    }

    self.getLabelIcon = function( key ) {
        var $labelIcon = $('<span class="label-icon news-icon-'+key+'">'); 

        return $labelIcon;
    }

    self.getLabelContent = function( key, value ) {
        var $labelContent = $('<span class="label-content">'); 
        var $labelValue = self.getLabelValue( value );
        var $labelSpan = self.getLabelSpan( key, value );

        $labelContent.append( $labelValue );
        $labelContent.append( $labelSpan );

        return $labelContent;
    }

    self.getLabelValue = function( value ) {
        var $labelValue = $('<span>'); 
        $labelValue.text( value );

        return $labelValue;
    }

    self.getLabelSpan = function( key ) {
        var $labelSpan = $('<span class="desc">');

        var $elIU = $('<span class="js-label-iu">');
        var $elBYN = $('<span class="js-label-byn">'); 

        var conversion = new uiConversion( cost[key], $elIU, $elBYN );
        conversion.setData( 1 );

        var text = $elIU.text() + ' И.Е. - ' + $elBYN.text() + ' руб.';

        $labelSpan.text( text );

        return $labelSpan;
    }
}

function betsSelect( cost, options, $block ) {
    var self = this;

    self.$block    = $block;
    self.$selectBl = $block.find('.js-bets');
    self.$select   = null;

    self.cost  = cost;
    self.options = options;


    self.init = function() {
        self.setBetsHtml();

        self.changeBets();

        self.$select.on('change', self.changeBets);
    }

    self.setBetsHtml = function() {
        var selectObj = new setSelect( 'bets', self.options );

        self.$select = selectObj.getSelect();

        self.$selectBl.append( self.$select );

        selectObj.updateSelect();
    }

    self.changeBets = function() {
        var betsAmount = self.$select.val();

        if( !self.$bets ){
            var $elIU = $block.find('.js-count-iu');
            var $elBYN = $block.find('.js-count-byn');

            self.$bets = new uiConversion( self.cost, $elIU, $elBYN );
        }

        self.$bets.setData( betsAmount );
    }
}

function setSelect( name, options ) {
    var self = this;

    self.$select = null;

    self.getSelect = function() {
        self.$select = $('<select>');

        self.$select.attr('name', name);

        var optionsHtml = '';

        $.each( options, function( key, value ) {
            optionsHtml += '<option value="'+key+'">'+value+'</option>';
        });

        self.$select.html(optionsHtml);

        return self.$select;
    }

    self.updateSelect = function() {
        self.$select.selectpicker({
            showTick: true,
            size: 10 
        });
    }

    

    return self.$select;
}

function uiConversion( serviceCost, $elIU, $elBYN ) {
    var self = this;

    self.$elIU  = $elIU;
    self.$elBYN = $elBYN;

    iu     = $('[data-iu]').data('ui');
    self.cost   = serviceCost;

    self.countIU  = null;
    self.countBYN = null;

    self.setData = function( amount ) {
        self.count( amount );
        self.setHtml();
    }

    self.count = function( amount ) {
        var countIU = amount * self.cost;
        var countBYN = countIU * iu;

        self.countIU = self.round( countIU );
        self.countBYN = self.round( countBYN );
    }

    self.setHtml = function() {
        self.$elIU.html( self.countIU );
        self.$elBYN.html( self.countBYN );
    }

    self.round = function( num ) {
        return Math.round(num * 100) / 100;
    }
}