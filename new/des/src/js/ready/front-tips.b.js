function Tip( $block ) {
	var self = this;

	self.$tipsBlock 	= $($block);
	self.$closeBtnAll 	= $('.js-close-popup', self.$tipsBlock);
	self.$nextBtnAll	= $('.js-next', self.$tipsBlock);
	self.$nav 			= null;
	self.$targets 		= null;


	self.$window = $(window);
	// self.windowPar = null;
	// self.windowWidth = self.$window.width();

	self.$body = $('body');


	// self.tipsParametrs = {};
	// self.targetParametrs = {};
	self.isTipsOpen = false;

	// self.popupMaddingTop = 10;
	// self.popupMaddingLeft = 10;

	self.init = function() {
		if( !self.$tipsBlock.length )
			return;

		if( !self.$body.hasClass('js-tips-enable') )
			return;

		self.setItems();

		self.setNav();

		self.setArrow();

		self.setTargets();

		self.toggleTip( true );

		self.handleItem( 0 );

		self.$nav.on('click', function(e) {
			e.stopPropagation();
			
			self.handleItem( $(this).data('index') );
		});

		self.$nextBtnAll.on('click', self.showNext );

		self.$closeBtnAll.on('click', self.toggleTip);

		self.$window.click(function(e) {
	    	e.stopPropagation();

			if ( !self.isTipsOpen )
				return;

			if (self.$tipsBlock.is(e.target) || $('.owl-dots').is(e.target) || $('.tips-item').parent().is(e.target) || $(e.target).closest('.tips-item').length > 0)
				return;

				self.toggleTip(false);
		});

		// self.onWindowResize();
	}


	self.setItems = function() {
		self.$items = self.$tipsBlock.find('.tips-item');
	}

	self.setNav = function() {
		var $navBlock = $('<div class="owl-dots">');

		for (var i = 0; i < self.$items.length; i++) {
			var $navItem = $('<div class="owl-dot" data-index="'+i+'"><span>');
			$navBlock.append( $navItem );
		}


	    self.$tipsBlock.append( $navBlock );

	    self.$nav = $navBlock.find('.owl-dot');
	}

	self.setArrow = function() {
		self.$arrow = $('<div id="target-arrow">');
		self.$tipsBlock.append( self.$arrow );
	}
	self.setArrowPos = function( direction, itemPos, targetPr ) {
		if( !self.$arrow.length )
			return;

		var arrowW = 20;
		console.log(itemPos)

		var left = targetPr.left + targetPr.width/2 - arrowW/2;

		if( direction == 'down' )
			var top = itemPos.top - arrowW;
		else
			var top = itemPos.top + itemPos.height + arrowW;

		self.$arrow.removeAttr('class');

		self.$arrow.addClass( direction );

		self.$arrow.css( {'left': left, 'top': top} );

		console.log('setArrowPos')
	}


	self.setTargets = function() {
		self.$targets = $('.js-tips-target');
	}

	self.showNext = function(e) {
		e.stopPropagation();

		var closest = $(this).closest('.tips-item');

		var showIndex = $(this).data('cache-index');

		if( !showIndex ) {
			var index = self.$items.map( function(key, item) {
				if( $(item).is(closest) )
		        	return key;
		    })[0];

		    showIndex = ( index < self.$items.length ) ? index + 1 : 0;

		    $(this).data('cache-index', showIndex);
		}

	    self.handleItem( showIndex );
	}

	self.handleItem = function( index ) {
		var $item = $(self.$items[index]);

		if( !$item.length )
			return;

		var doActivate = self.doActivate( $item );

		if( !doActivate )
			return;
		
		self.deactivate();
		self.activate( $item, index );
	}

	self.doActivate = function( $item ) {
		if( !$item.hasClass( 'active' ) )
        	return true;

    	return false;
	}

	self.activate = function( $item, index ) {
		
		self.setPosition( $item );

		$item
			.add( self.$tipsBlock )
			.add( $(self.$nav[index]) )
				.addClass( 'active' );

		// var $target = this.$targets.filter( $item.data('target') );

		// self.targetActivate( $target );
	}

	self.deactivate = function() {
		$.each( [ '$items', '$nav' ], function() {
	        self[ this ]
	            .filter( '.active' )
	            .removeClass( 'active' )
		});

		// self.targetDeactivate();

		self.$tipsBlock.removeClass( 'active' );
	}

	// self.targetActivate = function( $target ) {
	// 	if( !$target.length )
	// 		return;

	// 	self.setTargetProperties( $target );

	// 	$target.addClass( 'target-active' );
	// 	console.log($target)
	// }

	// self.targetDeactivate = function() {
	// 	$target = self.$targets.filter('.target-active');

	// 	$target.removeClass( 'target-active' );

	//     self.removeTargetProperties( $target );
	// }

	self.setPosition = function( $item ) {
		var itemPos = $item.data('cache-position');
		var direction = ( $item.data('direction') ) ? $item.data('direction') : null;

		if( !itemPos ){
			var tipPosition = new getTipPosition( $item );
			itemPos = tipPosition.getPosition();

			$item.data('cache-position', itemPos );
		}

		// console.log(itemPos)

		

		self.$tipsBlock.css({
    		'left': itemPos.left,
    		'top': 	itemPos.top
    	});

    	if( direction ) {
    		var $target = this.$targets.filter( $item.data('target') );

    		var targetPr = $target.data('cache-properties');
    		
			if( !targetPr ){
				var targetProperties = new getTargetProperties( $target );
				targetPr = targetProperties.getProperties();
				$target.data('cache-properties', targetPr );
			}
    	
    		self.setArrowPos( direction, itemPos, targetPr );
    	}
	}

	// self.setTargetProperties = function( $target ) {
	// 	var targetPr = $target.data('cache-properties');

	// 	if( !targetPr ){
	// 		var targetProperties = new getTargetProperties( $target );
	// 		targetPr = targetProperties.getProperties();

	// 		$target.data('cache-properties', targetPr );
	// 	}

	// 	console.log(targetPr)

	// 	$target.css({
 //    		'left': 	targetPr.left,
 //    		'top': 		targetPr.top,
 //    		'height': 	targetPr.height,
 //    		'width': 	targetPr.width
 //    	});
	// }

	// self.removeTargetProperties = function( $target ) {
	// 	$target.css({
 //    		'left': 	'',
 //    		'top': 		'',
 //    		'height': 	'',
 //    		'width': 	''
 //    	});
	// }


	self.toggleTip = function( toggle ) {
		toggle = ( typeof toggle == 'boolean' ) ? toggle : !self.isTipsOpen;

		self.$body.toggleClass('js-tips-active', toggle);

		if( !toggle )
			self.deactivate();

		self.isTipsOpen = !self.isTipsOpen;
	}
	

	// self.onWindowResize = function() {
	//     self.$window.resize(function() {
	//     	self.getWindowPar();
	// 	});

	// };
} 

function getTipPosition( $tip ) {
	var self = this;

	self.$tip = $tip;
	self.target = self.$tip.data('target');

	self.top = 0;
	self.left = 0;
	self.height = 0;
	self.width = 0;

	self.$window = $(window);
	self.windowPar = null;

	self.posOffset = 10;


	self.getPosition = function() {
		self.getWindowPar();
		self.getTipProperties();


		if( !self.target )
			self.placeCenter();
		else
			self.placeCustom();


		return {
			'top': 		self.top,
			'left': 	self.left,
			'height': 	self.height,
			'width': 	self.width
		}
	}

	self.getTipProperties = function() {
		self.height = self.$tip.outerHeight();
		self.width = self.$tip.width();
	}

	self.placeCenter = function() {
		console.log('placeCenter')

		self.top = (self.windowPar.height - self.height)/2;
		self.left = (self.windowPar.width - self.width)/2;
	}

	self.placeCustom = function() {
		var $target = $( self.target );

		var targetPr = $target.data('cache-properties');
		if( !targetPr ){
			var targetProperties = new getTargetProperties( $target );
			targetPr = targetProperties.getProperties();

			$target.data('cache-properties', targetPr );
		}

		console.log(targetPr)

		var direction = ( self.$tip.data('direction') ) ? self.$tip.data('direction') : 'up';

		var left = targetPr.left - self.posOffset;

		if( left + self.width > self.windowPar.width )
			left = self.windowPar.width - self.width - self.posOffset;

		if( left < self.posOffset )
			left = self.posOffset;

		if( direction = 'down' ) 
			var top = targetPr.top + targetPr.height + self.posOffset;
		else 
			var top = targetPr.top - self.posOffset - self.height;

		if( top + self.height > self.windowPar.height )
			top = self.windowPar.height - self.height - self.posOffset;

		if( top < self.posOffset )
			top = self.posOffset;

		self.left = left;
		self.top = top;

		console.log('placeCustom')
		console.log(direction)

	}

	self.getWindowPar = function() {
		self.windowPar = {
			'height': self.$window.height(),
			'width': self.$window.width(),
		};
	}
}

function getTargetProperties( $target ) {
	var self = this;

	self.$target = $target;

	self.top = 0;
	self.left = 0;
	self.height = 0;
	self.width = 0;

	self.getProperties = function() {
		self.setProperties();

		return {
			'top': 		self.top,
			'left': 	self.left,
			'height': 	self.height,
			'width': 	self.width
		}
	}

	self.setProperties = function() {
		var pos = $target.offset();

		self.top = pos.top;
		self.left = pos.left;
		self.height = $target.outerHeight();
		self.width = $target.width();
	}
}

var tips = new Tip('.js-tips');
tips.init();
