$('.js-filter-form').each( function() {
    var $form = $(this);

    var result = new SetFilterFormResult( $form );

    if( $form.closest('.js-get-filter-form-result').length )
        result.$block = $form.closest('.js-get-filter-form-result');

    result.init();
    $('.js-form-clean', $form).on('click', fullFormReset);

    var isResult

    var model = new SetOptions( $form );
    model.selectMain = $form.find('select.js-select-marka');
    model.selectDependent = $form.find('select.js-select-model');
    model.url = '?ajax=ajax_get_model';
    model.valueName = 'name';
    model.sort = true;

    // console.log(result.isAvailable())
    if( result.isAvailable() )
        model.afterInit = result.setResult;

    model.init();

    var region = new SetOptions( $form );
    region.selectMain = $form.find('select.js-select-country');
    region.selectDependent = $form.find('select.js-select-region');
    region.url = '/new/?ajax=ajax_get_city';
    region.valueName = 'country';
    region.init();

    function fullFormReset(e) {
        e.preventDefault();

        result.refresh();
        model.refresh();
        region.refresh();
    }

});