var loginNavigation = function() {
	var self = this;

	self.activeCl 				= 'active';
	self.openCl 				= 'open';

	self.$activeBtn 			= $('.js-login-btn');
	self.$navigationBlock 		= $('#js-login-navigation');
	self.$menu 					= $('.js-login-menu', this.$menu);
	self.$hasChildren 			= $('>li:has(>ul)>.js-item', this.$menu);
	self.$window 				= $(window);
	self.$body 					= $('body');
}


loginNavigation.prototype.init = function() {
    var self = this;

    self.handleSubmenu();

    self.$activeBtn.on('click', toggleBlock);

    self.$hasChildren.on('click', toggleMenu);

    self.$window.click(function(e) {
		if( self.$navigationBlock.hasClass( self.activeCl )
			&& !self.$navigationBlock.is(e.target)
			&& !self.$activeBtn.is(e.target)
			&& $(e.target).closest('#js-login-navigation').length < 1
			&& $(e.target).closest('.js-login-btn').length < 1) {
			self.toggleBlock( false );
		}
	});

    function toggleBlock( e ) {
		e.preventDefault();

		self.toggleBlock( !self.$navigationBlock.hasClass( self.activeCl ) );
	}

	function toggleMenu( e ) {
		e.preventDefault();

		self.toggleMenu( $(this), !$(this).hasClass( self.openCl ) );
	}
}
loginNavigation.prototype.toggleMenu = function( $item, toggle ) {
	var self = this;

	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	var $parent = $item.data( 'cache-content');

	if ( !$parent ) {
	    $parent = $($item).closest('li');
	    $item.data( 'cache-content', $parent ); 
	}

	$item.add($parent).toggleClass( self.openCl, toggle );
}

loginNavigation.prototype.toggleBlock = function( toggle ) {
	var self = this;

	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	self.$navigationBlock.add(self.$activeBtn).toggleClass( self.activeCl, toggle );

	self.$body.toggleClass( 'login-menu-active', toggle );

	 // hide menu block in header
    if( toggle && $primaryNavigation.isOpen )
        $primaryNavigation.toggleHeaderBtn( false );

    // hide search block in header
	if( toggle && $searchBlock.isOpen )
		$searchBlock.toggleSearch( false );
}

loginNavigation.prototype.handleSubmenu = function() {
	var self = this;

	self.$hasChildren.each( function(){
		$(this).addClass('has-children');
	});
}

var $loginNavigation = new loginNavigation();
$loginNavigation.init();