var categoriesListHandler = function() {
	var self = this;

	self.$showBtn 				= $('#js-categories-btn');
	// self.$showBlock 			= $('.js-show-more');
	self.$catList 				= $('#js-categories-list');
	self.$catItems 				= $('.category-item', self.$catItems );
	self.$window 				= $(window);

	self.showAmount 			= self.$catList.data('show_amount');

	self.isCarousel       	    = false;
	self.isPopup       	        = false;
	self.isMobile 				= false;

	self.popupOpenCl			= 'open';
}

categoriesListHandler.prototype.mobilePoint = '<sm';

categoriesListHandler.prototype.init = function() {
    var self = this;

    if( !self.$catList.length )
    	return;

	self.checkIsMobile();

	self.onWindowResize();

	self.handleAction();

	self.setPopupAction();

	self.$showBtn.on('click', togglePopup);

	function togglePopup() {
		self.togglePopup( !self.$showBlock.hasClass( self.popupOpenCl ) );
	}
	
}

categoriesListHandler.prototype.handleAction = function() {
	var self = this;

	if( !self.isMobile ) {
		self.setPopup();
	} else {
		self.setCarousel();
	}
}

categoriesListHandler.prototype.setPopup = function() {
	var self = this;

	self.destroyCarousel();

	if( self.isPopup )
		return;

	self.setPopupBlock();

	self.isPopup = true;
}

categoriesListHandler.prototype.togglePopup = function( toggle ) {
	var self = this;
	console.log('togglePopup categories')

	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	self.$showBlock.add(self.$showBtn).toggleClass( self.popupOpenCl, toggle );
}

categoriesListHandler.prototype.destroyPopup = function() {
	var self = this;

	if( !self.isPopup )
		return;

	self.$catList.append( self.$showBlock.html() );

	self.$showBlock.remove();

	self.togglePopup( false );

	self.isPopup = false;
}

categoriesListHandler.prototype.setPopupBlock = function() {
	var self = this;

	self.$showBlock = $('<div class="js-show-more show-more">');
	// self.$showBlock.append( $('<div class="show-more-inner">') );

	for (var i = self.showAmount; i < self.$catItems.length; i++) {
		self.$showBlock.append( self.$catItems[i] );
	}

	self.$catList.append( self.$showBlock );
}
categoriesListHandler.prototype.setPopupAction = function() {
	var self = this;

	if( self.isMobile )
		return;

	// console.log('setPopupAction')

	// self.$showBtn.on('click', togglePopup);

	self.$window.click(function(e) {
		if( self.isPopup 
			&& self.$showBlock.hasClass( self.popupOpenCl )
			&& !self.$showBlock.is(e.target)
			&& !self.$showBtn.is(e.target)
			&& $(e.target).closest('.js-show-more').length < 1) {
			self.togglePopup( false );
		}
			
	});

	// function togglePopup(e) {
	// 	// e.stopPropagation();
	// 	self.togglePopup( !self.$showBlock.hasClass( self.popupOpenCl ) );
	// }
}

categoriesListHandler.prototype.setCarousel = function() {
	var self = this;

	self.destroyPopup();

	if( self.isCarousel )
		return;

	self.$catList.owlCarousel({
	    loop:true,
	    margin:0,
	    // merge:true,
	    // mergeFit:true,
	    dots:false,
	    // autoWidth:true,
	    responsive : {
		    0 : {
		    	// loop: false,
		    	items:2,
		    },
		    480 : {
    			items:3,
		    },
		    660 : {
		        items:3,
		    },
		    700 : {
		        items:4,
		    },
		    768 : {
		        items:5,
		    },
		}
	});

	// console.log('carousel')
	self.isCarousel = true;
}

categoriesListHandler.prototype.destroyCarousel = function() {
	var self = this;

	if( !self.isCarousel )
		return;

	self.$catList.trigger('destroy.owl.carousel');

	self.isCarousel = false;
}

categoriesListHandler.prototype.onWindowResize = function() {
	var self = this;

	self.$window.resize(
	    viewport.changed(function() {
	        self.checkIsMobile();
	        self.handleAction();
	    })
	);
}

categoriesListHandler.prototype.checkIsMobile = function() {
	var self = this;

	self.isMobile = viewport.is( self.mobilePoint );
}


var $categoriesList = new categoriesListHandler();
$categoriesList.init();