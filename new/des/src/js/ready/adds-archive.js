var showOptions = function( $btn ) {
    var self = this;

    self.$showBtn       = $btn;
    self.$form          = $btn.closest('form');
    self.$hiddenOptions = $('.hide', self.$form);

    self.init = function() {
        self.$showBtn.on('click', self.toggleOptions);
    }

    self.toggleOptions = function(e) {
        e.preventDefault();

        var toggle = self.$showBtn.hasClass('open');

        self.$showBtn.toggleClass( 'open', !toggle );
        self.$hiddenOptions.toggleClass( 'hide', toggle );
    }

}
$('.js-show-options').each( function(){
    var $showOptions = new showOptions( $(this) );
    $showOptions.init();
});

var optionsCarousel = function( $carousel ) {
    var self = this;

    self.$carousel      = $carousel;
    self.$window        = $(window);
    self.mobilePoint    = 'xs';

    self.init = function() {
        self.checkIsMobile();
        self.setCarousel();
        self.onWindowResize();
    }

    self.setCarousel = function() {
        if( !self.isMobile )
            return;

        self.$carousel.addClass('owl-carousel');

        self.$carousel.owlCarousel({
            loop:false,
            margin:10,
            dots:false,
            autoWidth:true,
            items: 3
        });
    }

    self.checkIsMobile = function() {
        self.isMobile = viewport.is( self.mobilePoint );
    }

    self.onWindowResize = function() {
        self.$window.resize(
            viewport.changed(function() {
                self.checkIsMobile();
                // console.log(self.isMobile)
            })
        );
    }

}
$('.js-carousel-options').each( function(){
    var $optionsCarousel = new optionsCarousel( $(this) );
    $optionsCarousel.init();
});


var HandleProductVisibility = function( $btn ) {
    var self = this;

    self.attr      = 'set-hide';

    var $product     = $btn.closest('.js-product');
    var data         = $btn.data();
    var defaultTitle = $btn.attr('title');
    var hideTitle    = $btn.attr('hide-title');

    self.init = function() {
        if( !$product.length )
            return;

        if( $product.attr( self.attr ) == '1' )
            setTitle(true);

        $btn.on('click', sendAjax);
    }

    function sendAjax( e ) {
        e.preventDefault();
        // console.log( data )

        $.ajax({
            method: "POST",
            data: data,
            url: BASE_PATH + '/?ajax=ajax_car_hide',
            success: function(response) {

                var result = JSON.parse(response);

                toggleVisibility( result.status );
            }
        });
    }

    function toggleVisibility( toggle ) {
        toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

        var status = ( toggle ) ? '1' : '0';
        
        $product.attr( self.attr, status );

        setTitle( toggle );

        if( self.after )
            self.after();
    }

    function setTitle( toggle ) {
        var title = ( toggle ) ? hideTitle : defaultTitle;

        $btn.attr('title', title);
    }
}

$('.js-toggle-product-visibility').each( function(){
    var $vsbBtn = new HandleProductVisibility( $(this) );
    // $vsbBtn.after = test;
    $vsbBtn.init();
});

$('.js-filter-options-redirect [data-url]').on('click', filterOptionRedirect);

$('.js-filter-options-redirect select').on('changed.bs.select', filterOptionRedirect);

function filterOptionRedirect() {
    var dataUrl = '';

    if( $(this).is('select') )
        dataUrl = $(this).find(':checked').data('url');
    else
        dataUrl = $(this).data('url');

    if( dataUrl )
        window.location = dataUrl;
}

var $tipsButton = new TipForButton( '.js-button-tip' );
$tipsButton.init();

// function test() {
//     console.log('test')
//     console.log($fixedBlock)
// }
