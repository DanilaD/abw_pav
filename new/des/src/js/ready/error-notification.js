function errorNotification() {
    var self = this;

    self.$body       = $('body');
    self.$errorBl  = null;

    self.init = function() {
        self.setErrorBlock();
    }

    self.setErrorBlock = function() {
        self.$errorBl = $('<div class="js-error-notification error-notification">');
        self.$body.append( self.$errorBl );
    }

    self.show = function( message ) {
        if( !message )
            return;

        if( !self.$errorBl )
            self.setErrorBlock();

        self.$errorBl.html( message );

        self.toggle( true );
    }

    self.hide = function() {
        if( !self.$errorBl )
            return;

        self.toggle( false );
    }

    self.toggle = function( toggle ) {
        toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

        self.$errorBl.toggleClass('active', toggle);
    }
}