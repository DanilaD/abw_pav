var handleFormLogin = function( $form ) {
    this.$form = $form;
    this.$popUp = this.$form.closest('.white-popup-block');

    this.$submitBtn = $('[type=submit]', this.$form);
    this.submitSleepmode = this.$submitBtn.data('sleepmode');
    this.submitValue = this.$submitBtn.text();
}

handleFormLogin.prototype.init = function() {
    var self = this;

    self.$form.on( 'submit', function(e){
        e.preventDefault();
        self.submitAction( );
    });
}

handleFormLogin.prototype.submitAction = function() {
    var self = this;

    var data = self.$form.serializeArray();

    $.ajax({
        method: "POST",
        url: BASE_PATH + '?ajax=ajax_login',
        data: data,
        beforeSend:  function() {
        	self.sleepmode( true );
        },
        success: function(response) {

			var result = JSON.parse(response);

			var message = ( result.status == false ) ? result.text : '';

			self.toggleForm( result.status );

			self.addMessage( message );

			if( result.status == true )
				self.closePopup();
        },
        complete: function() {
        	self.sleepmode( false );
        }

    });
}

handleFormLogin.prototype.toggleForm = function( toggle ) {
	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	this.$form.toggleClass( 'error-inputs', !toggle );
}

handleFormLogin.prototype.addMessage = function( message ) {
	var self = this;

	if( !self.$errorBlock )
		self.addErrorBlock();

	self.$errorBlock.html( message );
}

handleFormLogin.prototype.addErrorBlock = function( message ) {
	var self = this;

	self.$errorBlock = $('<div class="js-error error-message">');

	self.$errorBlock.insertAfter( self.$form.find('.title') );
}

handleFormLogin.prototype.closePopup = function() {
	var self = this;

	if( self.$popUp.length > 0 )
		$.magnificPopup.close();

	location.reload();
}

handleFormLogin.prototype.sleepmode = function( toggle ) {
	toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

	var text = ( toggle ) ? this.submitSleepmode : this.submitValue;

	this.$submitBtn.attr('disabled', toggle).text( text );
}


$('.js-form-login').each( function() {
    var $form = new handleFormLogin( $(this) );

    $form.init();
});