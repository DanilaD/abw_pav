var menuTab = function ( parentNav ) {
    var self = this;

    self.$parentNav         = $(parentNav);
    self.$nav               = self.getNav();
    self.$tracker           = $('.js-tracker', self.$parentNav);
    self.$trackerPar           = self.$tracker.parent();
    self.$menuTabs              = null;
    self.$menu              = $('#js-primary-navigation');
    self.$menuUl            = $('ul', self.$menu);
    self.currentTarget      = null;

    self.isMobile           = false;
    self.timer;

};
 
menuTab.prototype.mobilePoint = '<md';

menuTab.prototype.init = function () {
    var self = this;

    self.checkIsMobile();

    self.setLastNav();

    self.$nav.on('mouseenter', function( e ){
        if( self.isMobile )
            return;

        e.preventDefault();

        var $item = $(this);

        self.timer = setTimeout(function(){
            self.actionOnClick( $item );
            self.setTracker( $item );
        }, 100);
    }).mouseleave(function() {
        clearTimeout(self.timer);
    });

    self.$nav.on('click', function( e ){
        if( !self.isMobile )
            return;

        e.preventDefault();

        self.actionOnClick( $(this) );
    });

    self.onWindowResize();
};


menuTab.prototype.getNav = function() {
    var self = this;

    self.target = '';

    return self.$parentNav.find( '.js-tab-nav' ).filter( function() {
        var target = $(this).data('target').replace( /[0-9]+$/i, '' );

        if ( 0 == self.target.length )
            self.target = target;
        
        return ( self.target == target ); 
    });
};

menuTab.prototype.setLastNav = function() {
    var self = this;

    $(self.$nav[self.$nav.length-1]).addClass('last');

    // console.log(self.$nav[self.$nav.length-1])
};

menuTab.prototype.onWindowResize = function() {
    var self = this;

    $(window).resize(
        viewport.changed(function() {
            self.checkIsMobile();

            var $item = self.$nav.filter( '.active:visible' );

            self.setTracker( $item );
        })
    );
};

menuTab.prototype.setCurrentTarget = function() {
    var self = this;

    return self.$parentNav.find( '.js-tab-nav.active:visible' ).data('target');
}


menuTab.prototype.actionOnClick = function( $link ) {

    var self = this;

    var selector = $link.data('target');

    if( !self.currentTarget )
        self.currentTarget = self.setCurrentTarget();

    var $menuTab = !self.$menuTabs ? $( selector, self.$parentNav ) : self.$menuTabs.filter( selector );

    var doActivate = self.doActivate( $menuTab );


    if( !self.$menuTabs )
        self.setmenuTabs( $menuTab );

    if( !self.isMobile && selector != self.currentTarget ) {
        self.deactive();
    } else if( self.isMobile ) {
        self.deactive();
    }

    if ( doActivate ) 
        self.activate( $menuTab );
         
 
    self.currentTarget = selector;
};

menuTab.prototype.doActivate = function( $menuTab ) {
    var self = this;

     if( !$menuTab.hasClass( 'active' ) )
        return true;

    return false;
};

menuTab.prototype.activate = function( $menuTab ) {
    var self = this;

    var $itemNav = this.$nav.filter( '[data-target="#'+ $menuTab.attr( 'id' ) +'"]' );

    var $currentNav = $itemNav.filter(":visible");
     
    if( self.isMobile ) {

        self.$menu.animate({
            scrollTop: $currentNav.offset().top - self.$menuUl.offset().top
        }, 200);
    }
    
    $itemNav.addClass( 'active' );

    $menuTab.addClass( 'active' );
};

/**
 * Deactivate menuTabs and menuTab navs
 */
menuTab.prototype.deactive = function() {
    var self = this;

    $.each( [ '$nav', '$menuTabs' ], function() {
        self[ this ]
            .filter( '.active' )
            .removeClass( 'active' )
            // .trigger( 'thememenuTab.deactive' );
    });
};

menuTab.prototype.setTracker = function( $nav ) {
    var self = this;

    if( self.isMobile )
        return;

    if( self.$tracker < 1 || self.$trackerPar.length < 1)
        return;

    var navPos = $nav.offset();
    var parentPos = self.$trackerPar.offset();
    var trackerTop = navPos.top - parentPos.top;

    self.$tracker.css('top', trackerTop);
};

menuTab.prototype.setmenuTabs = function( $menuTab ) {
    this.$menuTabs = $menuTab.parent().find( '.js-tab-item' );
};

menuTab.prototype.checkIsMobile = function() {
    var self = this;

    self.isMobile = viewport.is( self.mobilePoint );
}

$('.js-tabs').each( function(){
    var $menuTabs = new menuTab( $(this) );
    $menuTabs.init();
});

