function Tabs( $tabs ) {
    var self            = this;

    var $tabs          = $tabs;
    var $tabsNav       = null;
    var $tabsContent   = null;
    var $item          = null;

    self.$curentTab     = null;
    self.$tabsNavEl     = null;
    self.activeClass    = 'active';
    self.before         = null;
    self.after          = null;
    self.stop           = null;

    self.init = function() {
        $tabsNav = getNav();

        self.$tabsNavEl = $tabsNav;

        $tabsNav.on('click', handleTab)

        // console.log($tabsNav)
    };

    self.show = function( target ) {
        self.$curentTab = getcurentTab( target );
        $item = $tabsNav.filter( '[data-target="#'+ self.$curentTab.attr( 'id' ) +'"]' );

        var isActivate  = doActivate( self.$curentTab );

        if ( self.before )
            beforeActivate();

        // console.log( target + ' <--self.stop')
        // console.log( self.stop + ' <--self.stop')

        if( self.stop )
            return;

        if( !$tabsContent )
            setTabsContent( self.$curentTab );

        if ( !isActivate )
            return;

        deactive();

        activate( self.$curentTab );

        if ( self.after )
            afterActivate();
    }

    function getNav() {
        self.target = '';

        return $tabs.find( '[role="tabs-nav"]>*' ).filter( function() {
            var target = $(this).data('target').replace( /[0-9]+$/i, '' );

            if ( 0 == self.target.length )
                self.target = target;
            
            return ( self.target == target ); 
        });
    };

    function handleTab(e) {
        e.preventDefault();

        var target = $(this).data('target');

        self.show( target );
    };

    

    function beforeActivate() {
         self.before();
    }

    function afterActivate() {
        self.after();
    }

    function activate() {
        $item.addClass( 'active' );
        self.$curentTab.addClass( self.activeClass );
    };

    function deactive() {
        $.each( [ $tabsNav, $tabsContent ], function() {
            this
                .filter( '.' + self.activeClass )
                .removeClass( self.activeClass )
        });
    };

    function getcurentTab( target ) {
        var $targetTab = !$tabsContent ? $( target, $tabs ) : $tabsContent.filter( target );
        
        return $targetTab;
    };

    function doActivate() {
        if( !self.$curentTab.hasClass( self.activeClass ) )
            return true;

        return self.$curentTab.hasClass( self.activeClass );
    };

    function setTabsContent() {
        $tabsContent = self.$curentTab.parent().find( '>*' );
        // console.log($tabsContent)
    };

    // function setTabsContent() {
    //     var target = $($tabsNav[0]).data('target');
    //     // console.log($($tabsNav[0]).data('target'))
    //     console.log(target)
    //     var $firstTab = getcurentTab( target );
    //     $tabsContent = $firstTab.parent().find( '>*' );
    //     console.log($tabsContent)
    // };

}

// $('.js-custom-tabs').each( function() {
//     var $tabs = new Tabs( $(this) );

//     $tabs.init();
// });