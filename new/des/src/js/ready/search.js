var searchTabs = function ( searchBlock ) {
    var self = this;

    self.$searchBlock       = $(searchBlock);
    self.$nav               = $('select.js-select-type', self.$searchBlock );
    self.$tabs              = self.setTabs();
    self.target             = '#search-tab-';
    self.tabActiveCl        = 'active';
};

searchTabs.prototype.init = function () {
    var self = this;

    self.$nav.on('change', function( e ){
        e.preventDefault();

        self.actionOnChange();
    }); 
};

searchTabs.prototype.actionOnChange = function() {
    var self = this;

    var selector = self.$nav.find('option:selected').data('target');

    var $tab = !self.$tabs ? $( selector, self.$searchBlock ) : self.$tabs.filter( selector );

    var doActivate = self.doActivate( $tab, selector );

    if ( doActivate ) {
        self.deactive();
        self.activate( $tab );
    }

};

searchTabs.prototype.doActivate = function( $tab, selector ) {
    var self = this;

     if( !$tab.hasClass( self.tabActiveCl ) )
        return true;

    return false;
};

searchTabs.prototype.activate = function( $tab ) {
    var self = this;

    $tab.addClass( self.tabActiveCl );
};

/**
 * Deactivate tabs and tab navs
 */
searchTabs.prototype.deactive = function() {
    var self = this;

    self.$tabs.filter( '.'+self.tabActiveCl ).removeClass( self.tabActiveCl );
};

searchTabs.prototype.setTabs = function( $tab ) {
   var self = this;

   return self.$searchBlock.find( '.js-select-content' );
};

$('.js-search-tabs').each( function(){
    var $searchTabs = new searchTabs( $(this) );
    $searchTabs.init();
});


var searchBlock = function () {
    var self = this;

    self.$btn          = $('#js-header-search');
    self.$searchBlock  = $('#js-search-block');
    self.$body         = $('body');
    self.openCl        = 'open-search-block';
    self.isOpen        = false;
}

searchBlock.prototype.init = function () {
    var self = this;

    self.$btn.on('click', function( e ){
        e.preventDefault();

        self.toggleSearch( !self.$btn.hasClass( self.openCl ) );
    });
};

searchBlock.prototype.toggleSearch = function( toggle ) {
   var self = this;

   toggle = ( typeof toggle !== 'undefined' ) ? toggle : true;

   self.$searchBlock.add(self.$body).add(self.$btn).toggleClass( self.openCl, toggle );
   self.isOpen = toggle;

    // hide menu block in header
    if( toggle && $primaryNavigation.isOpen )
        $primaryNavigation.toggleHeaderBtn( false );

    // hide login menu in header
    if( toggle && $loginNavigation.isOpen )
        $loginNavigation.toggleBlock( false );
};


var $searchBlock = new searchBlock();
$searchBlock.init();