<?
$social =[
  'fb' => ['name' => 'Facebook', 'link' => 'https://www.facebook.com/abw.by'],
  'vk' =>['name' => 'Вконтакте', 'link' => 'https://vk.com/myabwby'],
  'od' =>['name' => 'Одноклассники', 'link' => 'https://ok.ru/abwby'],
  'yt' =>['name' => 'Youtube', 'link' => 'https://www.youtube.com/channel/UCRiV78AOBPdPAUR1i_D9z7w'],
];

$link =[
  'car' => ['name' => 'Все объявления', 'link' => '/car/sell/'],
  'registration' => ['name' => 'Регистрация нового пользователя', 'link' => '/reg.php'],
  'profile' => ['name' => 'Профиль', 'link' => '/profile_edit/edit'],
  'foget' => ['name' => 'Востановить доступ', 'link' => '/forgot/'],
  'add' => ['name' => 'Подать объявление', 'link' => '/index.php?act=adv&act_adv=pre_add_form_adventure'],
];

$application =[
  'ios'=>'https://itunes.apple.com/by/app/abw.by-avto-i-moto-v-belarusi/id857625755?mt=8',
  'android'=>'https://play.google.com/store/apps/details?id=by.vegasystems.abw&hl=ru'
];