<?

// над фотографией
$car_links_top[] = [
  [
    'name' => '<span class="red-link">АВТО С ПРОБЕГОМ В ЛИЗИНГ!</span> <span class="blue-link">ЛЕГКО И ДОСТУПНО С</span> <span class="red-link">МИКРО ЛИЗИНГ!</span>',
    'link' => 'http://www.mikro-leasing.by/fizicheskim-licam?prm=21&utm_source=abwby&utm_medium=auto-link&utm_campaign=fizlica',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="blue-link">Б/У авто в лизинг.</span> <span class="red-link">Старое в зачёт!</span>',
    'link' => 'http://www.mikro-leasing.by/fizicheskim-licam?prm=21&utm_source=abwby&utm_medium=auto-link&utm_campaign=fizlica',
    'target' => '_blank'
  ],
];
// под фотографией
$car_links_under_photo[] = [
  [
    'name' => '<span class="red-link">Надоело долго продавать свой авто</span> <span class="blue-link">мы вам поможем</span> <span class="red-link">покупка вашего авто быстро!</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Купим ваше авто в любом состоянии</span> <span class="blue-link">в т.ч. не на ходу или аварийное.</span> <span class="red-link">Выкупаем дорого</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Быстрый выкуп авто.</span> <span class="blue-link">95% рыночной стоимости.</span> <span class="red-link">Подробности ТУТ.</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Рынок замер,</span> <span class="blue-link">но не для тебя -</span> <span class="red-link">продай авто ВЫГОДНО!</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">СРОЧНЫЙ ВЫКУП АВТО</span> <span class="blue-link">ЛЮБОЙ МАРКИ И ГОДА ВЫПУСКА</span> <span class="red-link">ВЫЕЗД НА ОЦЕНКУ</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Быстро ПРОДАЙ свою машину</span> <span class="blue-link">и ПОЛУЧИ деньги</span> <span class="red-link">в день обращения</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Срочно нужны деньги?</span> <span class="blue-link">Выкуп авто</span> <span class="red-link">в день обращения!</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Покупка авто</span> <span class="blue-link">любой марки и года выпуска,</span> <span class="red-link">кредитные и залоговые</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">СПЕЦИАЛЬНЫЕ УСЛОВИЯ</span> <span class="blue-link">выкупа</span> <span class="red-link">Звони!</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
  [
    'name' => '<span class="red-link">Куплю авто</span> <span class="blue-link">в любом состоянии.</span> <span class="blue-link">В течение 30 минут</span> <span class="red-link">после осмотра автомобиля!</span>',
    'link' => 'https://goo.gl/zCVAy4',
    'target' => '_blank'
  ],
];
// под опциями
$car_links_under_options[] = [
  [
    'name' => '<span class="red-link">Возьми деньги на авто</span> <span class="blue-link">у Альфа-Банк</span> <span class="red-link">до 40 тыс. BYN.</span> <span class="blue-link">Без залога и поручителей</span>',
    'link' => 'https://goo.gl/5QJ7kr',
    'target' => '_blank'
  ],
];
// под описанием
/*
$car_links_bot[] = [
  [
    'name' => '',
    'link' => '',
    'target' => '_blank'
  ],
];
 */