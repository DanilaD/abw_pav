<header class="site-header" id="site-header">
	<div class="site-header-inner clearfix">
		<div class="logo"><a href="{$static_www}"></a></div>
		{include file='section/header/menu.tpl'}
		<div class="navigation">
			{nocache}<a id="js-tips-item-login" href="{if isset($ses.login)}{$link.profile.link}{else}#form-login{/if}" class="js-tips-target login{if isset($ses.login)} is-logined js-login-btn{else} js-popup-with-form{/if}"><span class="icon-login"></span></a>{*
			*}{/nocache}{if isset($link['add']) && $link['add']}<a href="{$link['add']['link']}" class="add-product"><span class="icon-plus"></span>{$link['add']['name']}</a>{/if}{*
			*}<div class="header-search js-tips-target" id="js-header-search"><span class="icon-search"></span></div>{*
			*}<div class="header-navigation-toggler" id="js-header-navigation-toggler"><span class="icon-toggle"></span></div>
		</div>
		{include file='section/header/search.tpl'}
	</div>
	{*include file='section/header/hamburger.tpl'*}
	{include file='section/header/menu-login.tpl' nocache}
</header>
{include file='section/header/form-login.tpl'}