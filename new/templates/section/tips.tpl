<div class="js-tips tips">
	<div data-target="" class="special-tip tips-item">
		<div class="tips-item-inner">
			<div class="title">Новый дизайн!</div>
			<div class="text">Мы запустили обновленную версию<br>abw.by. Теперь все понятнее и проще. <br>Узнайте, что изменилось - а затем вперед <br>к новым достижениям!</div>
			<div class="nav"><div class="btn-blue js-next">Продолжить</div><div class="btn-dismiss js-close-popup">Пропустить</div></div>
		</div>
	</div>
	<div data-target="#js-tips-item-login" data-direction="down" class="tips-item">
		<div class="tips-item-inner">
			<div class="text">Ваш личный кабинет теперь<br>находится здесь. Зарегистрируйтесь,<br>чтобы узнать больше возможностей<br>нового личного кабинета.</div>
			<div class="nav"><div class="btn-blue js-next">Продолжить</div><div class="btn-dismiss js-close-popup">Пропустить</div></div>
		</div>
	</div>
	<div data-target="#js-header-search" data-direction="down" class="tips-item">
		<div class="tips-item-inner">
			<div class="text">Поиск находится здесь. Можно <br>искать по категориям объявлений и <br>по новостям.</div>
			<div class="nav"><div class="btn-blue js-next">Продолжить</div><div class="btn-dismiss js-close-popup">Пропустить</div></div>
		</div>
	</div>
	<div data-target="#js-categories-main" data-direction="up" class="tips-item">
		<div class="tips-item-inner">
			<div class="text">Быстрый поиск по всем категориям <br>объявлений. Теперь все разложено <br>по местам!</div>
			<div class="nav"><div class="btn-blue js-close-popup">Вернуться к сайту</div></div>
		</div>
	</div>
</div>
