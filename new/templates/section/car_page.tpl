{include file='section/breadcrumbs.tpl'}
{nocache}
<div class="product-header">
	<div class="product-header-title clearfix">
		<h1 class="title-h1">{$data.main.title}</h1>
		{include file='section/adds/autohouse.tpl'}
		{include file='section/adds/auto-onorder.tpl'}
	</div>
</div>
{/nocache}
<div class="{$isProductOwner}">
{if $mobile.is_true_mobile}
	{include file='section/adds/car-mobile.tpl'}
{else}
	{include file='section/adds/car-dekstop.tpl'}
{/if}
</div>
{include file='section/adds-archive/btn-favourites-page.tpl'}
{include file='section/adds/watched-adds.tpl' nocache}
{include file='ban/ban-yandex.tpl'}
{*include file='section/adds/similar-adds.tpl' nocache}
{include file='section/adds/news-list.tpl'}
{include file='section/stock.tpl'*}