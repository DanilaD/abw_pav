{assign 'show_amount' 5}
{assign 'merge_amount' $main_page_cat|count - $show_amount}
<div class="categories-link-outer">
	<div class="categories-link-block" id="js-categories-list">{*
		*}{if (isset($main_page_cat)) }{*
			*}{assign 'i' 0}{*
			*}{foreach from=$main_page_cat item=cat_data key=cat}{*
				*}{if $cat == 'car' && $i <= $show_amount}{*
					*}<div class="category-item main js-tips-target" id="js-categories-main"><div class="category-main-inner">
						<div class="category-item-inner">
							<div class="cat-icon cat-{$cat}"></div><div class="title">{$cat_data['name']}</div>
						</div>
					</div></div>{*
				*}{else}{if $i == $show_amount}<div class="show-more js-show-more" data-merge="{$merge_amount}"><div class="show-more-inner">{/if}{*
					*}<div class="category-item" style="width: calc(100%/{$merge_amount})">
						<a href="{$cat_data['link']}"><div class="category-item-inner">
							<div class="cat-icon cat-{$cat}"></div><div class="title">{$cat_data['name']}</div>
						</div></a>
					</div>{*
				*}{/if}{*
				*}{assign 'i' $i+1}{*
			*}{/foreach}{*
			*}{if $i >= $show_amount}</div></div>{/if}{*
		*}{/if}{*
	*}</div>
	<div class="btn-show-more" id="js-categories-btn"><div class="btn-show-more-inner"></div></div>
</div>