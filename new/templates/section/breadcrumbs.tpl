{if isset($breadcrumbs) && $breadcrumbs}
<div class="breadcrumbs">
  <span><a href="{$breadcrumbs.link1}">{$breadcrumbs.name1}<span class="icon icon-arrow-breadcrumbs"></span></a></span>{if isset($breadcrumbs.link2)}<span><a href="{$breadcrumbs.link2}">{$breadcrumbs.name2}<span class="icon icon-arrow-breadcrumbs"></span></a></span>{/if}{if isset($breadcrumbs.link3)}<span><a href="{$breadcrumbs.link3}">{$breadcrumbs.name3}<span class="icon icon-arrow-breadcrumbs"></span></a></span>{/if}{if isset($breadcrumbs.link4)}<span><a href="{$breadcrumbs.link4}">{$breadcrumbs.name4}<span class="icon icon-arrow-breadcrumbs"></span></a></span>{/if}<span>{$breadcrumbs.text}</span>
</div>
{/if}