<footer class="site-footer" id="site-footer">
	<div class="footer-navigation">
		{if isset($social)}
		<div class="social-icons-block">
			{foreach from=$social item=v key=k}
				<a href="{$v.link}" rel="nofollow" class="icon-item {$k}" target="_blank" title="{$v.name}"><span class="icon"></span></a>
			{/foreach}  
		</div>
		{/if}
		<ul class="footer-menu">
			<li><a href="/partner/">Рекламодателям</a></li>
			<li><a href="/about/">О нас</a></li>
			<li><a href="/vacancy/">Вакансии</a></li>
			<li><a href="/contact/">Свяжитесь с нами</a></li>
		</ul>
	</div>
	<div class="footer-info" id="footer-info">
		<div class="footer-info-item">
			<div>© ООО "Бонусторг" УНП101505322 от 7 августа 2001г</div>
			{*<div>Поддержка сервера: support.by</div>*}
		</div>
		<div class="footer-info-item">
			Все права защищены и охраняются законом<br>
			Использование информации запрещено<br>
			<a href="/print_rules/">Правила перепечатки</a>
		</div>
		<div class="footer-info-item footer-download">
			<div>Скачайте приложение ABW.BY</div>
			{*if isset($application.ios) && $application.ios}
				<a href="{$application.ios}" target="_blank" class="social social-appstore"></a>
			{/if*}
			{if isset($application.android) && $application.android}
				<a href="{$application.android}" target="_blank" class="social social-googleplay"></a>
			{/if}

		</div>
	</div>
</footer>