{if isset($smarty.session.login) && $smarty.session.login}
	<div class="login-navigation" id="js-login-navigation" style="text-align:left;">
		<div class="login-navigation-inner">
		{foreach from=$menu_user item=item}
			{if isset($item.type) && $item.type == "login"}
				{if isset($item.name)}
	    			<a href="{$item.link}" class="user">{$item.name}</a>
	    		{/if}
			{/if}
			{if isset($item.type) && $item.type == "balance"}
				{if isset($item.name)}
	    			<a href="{$item.link}" class="balance">{$item.name}</a>
	    		{/if}
			{/if}
			{if isset($item.menu)}
				<ul class="login-menu js-login-menu">
			    {foreach from=$item.menu item=item_data}
			    	<li>
			    		<div class="cat-title js-item">{$item_data.name}</div>
			    		{if $item_data.item}
					    	<ul class="login-submenu">
						        {foreach from=$item_data.item item=menu_item}          
						          <li><a href="{$menu_item.link}" class="login-item">{$menu_item.name}</a></li>
						        {/foreach}
					        </ul>
				        {/if}
				    </li>
			    {/foreach}
			    </ul>
			{/if}
			{if isset($item.type) && $item.type == "notebook"}
				<div class="notebook">
					<a href="{$item.link}" class="cat-title">{$item.name}</a>
				</div>
			{/if}
			{if isset($item.type) && $item.type == "exit"}
				<a href="{$item.link}" class="exit">{$item.name}</a>
			{/if}
		{/foreach}
		</div>
	</div>
{/if}
