{if isset($sales)}
<div class="section section-background">
	<h2 class="title-h2 color-pink">Акции и скидки компаний</h2>
	<div class="stock-carousel">
		<div class="owl-carousel js-stock-carosel">
			{foreach from=$sales key=key item=item}
				{include file='content/stock-item.tpl' 
					link="https://www.abw.by`$item.url`"
					title=$item.title
					label=$item.sale
					thumb=$item.photo}
			{/foreach}
		</div>
	</div>
</div>
{/if}