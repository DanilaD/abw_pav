<form action="" method="POST">
  <input type="text" id="login" class="form-control" name="login" placeholder="Логин" required/>
  <input type="password" name="pass" placeholder="Пароль" required/>
  <input type="hidden" name="redirect" value="">
  <input type="checkbox" name="autologin">
  <button type="submit" class="btn btn-success btn-user-login">Вход</button>
</form>
<a href="https://www.abw.by/reg.php" target="_blank" class="btn btn-primary">Регистрация</a>
<a href="https://www.abw.by/forgot/" target="_blank">Забыли пароль?</a>

<form action="" method="POST">						
  <input type="hidden" name="do" value="regnew">
  <input type="text" name="login" value="" placeholder="Только буквы латинского алфавита и цифры" required/>
  <input type="password" name="pass" value="" placeholder="Введите пароль" required/>
  <input type="email" name="email" value=""  placeholder="Введите e-mail" required/>
  <div class="g-recaptcha" data-sitekey="6LfwUwkTAAAAANAqDFQPdQogCuOgPNm7XamEo-VH"></div><noscript style="color: red">Включите JavaScript, обновите свой браузер.</style></noscript>
  <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Зарегистрироваться</button>
</form>