{if isset($ses.status) && ($ses.status == 4 || $ses.status == 1)}
<div class="admin-buttons">
    <button class="first js-dropdown-buttons">Изменить объявление</button>
    <a rel="nofollow" href="/add_car/edit.php?id={$data.main.adv_id}">Редактировать</a>
    {if $data.main.hide == 1}
    <a rel="nofollow" href="/index.php?act=adv_delete&id={$data.main.adv_id}&type_a=1&do=4">Открыть</a>
    {else}
    <a rel="nofollow" href="/index.php?act=adv_delete&id={$data.main.adv_id}&type_a=1&do=1">Скрыть</a>
    {/if}
    <a rel="nofollow" href="/index.php?act=adv_delete&id={$data.main.adv_id}&type_a=1&do=3">Удалить</a>
    <a rel="nofollow" href="/index.php?act=adm_news_advs&user={$data.main.user}">Модерировать</a>
</div>
{elseif isset($ses.login) && $ses.login == $data.main.user}
<div class="admin-buttons">
    <a rel="nofollow" href="/add_car/edit.php?id={$data.main.adv_id}" class="show">Редактировать</a>
</div>
{/if}