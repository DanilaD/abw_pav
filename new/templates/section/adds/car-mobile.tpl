{include file='section/adds/control-panel-mobile.tpl'}
<div class="single-page-mobile-content clearfix">
	{include file='content/adds/slider.tpl'}
	{include file='content/adds/product-data.tpl'}
	<div class="before-content clearfix">
		{include file="ban/BN2.tpl"}
		{include file="ban/BN3.tpl"}
	</div>
	<div class="content">
		{include file='content/adds/product-main-data.tpl'}
		{include file='content/adds/content.tpl'}
	</div>
</div>
{include file='content/adds/owner-data.tpl'}