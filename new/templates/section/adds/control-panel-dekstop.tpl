<div class="single-product-panel dekstop clearfix">
	<div class="panel-item left-sidebar">
		{include file='content/adds/price.tpl'}
	</div>
	<div class="panel-item right-sidebar">
		{include file='content/adds/data.tpl'}
	</div>
	<div class="panel-item main-content">
		{include file='content/adds/control-buttons.tpl'}
		{include file='content/adds/action-buttons.tpl'}
		{include file='section/adds/admin-buttons.tpl' nocache}
	</div>
</div>