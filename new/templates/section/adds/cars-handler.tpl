{if isset($cars)}
	<div class="row row-no-paddings row-4 row-flex row-products">{*
		*}{foreach from=$cars key=key item=car}{*
		*}<div class="col">
			{if isset($car.type) && $car.type == 'img'}
				{include file='content/adds/add-banner.tpl' 
					link=$car.link 
					thumb=$car.img}
			{else}
				{assign var=car_data value=[$car.year,$car.volume,$car.trans,$car.probeg]}
				{assign var=bets_amount value=0}
				{if isset($car.curr_bets) && $car.curr_bets}
					{assign var=bets_amount value=$car.curr_bets}
				{/if}
				{include file='content/adds/add-simple.tpl'}
			{/if}
		</div>{*
	*}{/foreach}{*
	*}</div>{*
*}{/if}