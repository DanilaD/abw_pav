{if isset($data.autohouse) && $data.autohouse}
<div class="product-autohouse clearfix">
	<div class="autohouse-logo">{if isset($data.autohouse.logo) && $data.autohouse.logo}<a rel="nofollow" href="{$data.autohouse.link}" target="_blank"><img src="{$data.autohouse.logo}" alt="{$data.autohouse.title}"></a>{/if}</div>
	<div class="autohouse-data">
		<div class="title"><a rel="nofollow" href="{$data.autohouse.link}" target="_blank">{$data.autohouse.title}</a></div>
		<div class="data">
			<span>Всего {$data.autohouse.count} {$data.autohouse.count_word}</span>
			{if isset($data.autohouse.url) && $data.autohouse.url}<a rel="nofollow" href="{$data.autohouse.url}" target="_blank">{$data.autohouse.url}</a>{/if}
		</div>
	</div>
</div>
{/if}