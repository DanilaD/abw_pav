{include file='section/adds/control-panel-dekstop.tpl'}
<div class="single-page-dekstop-content clearfix" id="js-fixed-content-basis">
	<div class="right-sidebar">
		{include file='section/adds/right-sidebar.tpl'}
	</div>
	<div class="main-content">
		{include file='section/adds/main-content.tpl'}
	</div>
	<div class="left-sidebar">
		{include file='section/adds/left-sidebar.tpl'}
	</div>
</div>