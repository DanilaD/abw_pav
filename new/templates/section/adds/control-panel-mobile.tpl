<div class="single-product-buttons-mobile">
	{include file='content/adds/action-buttons.tpl'}
	{include file='section/adds/admin-buttons.tpl' nocache}
</div>
<div class="single-product-panel mobile clearfix">
	<div class="panel-item left-sidebar">
		{include file='content/adds/price.tpl'}
	</div>
	<div class="panel-item main-content">
		{include file='content/adds/data.tpl'}
		{include file='content/adds/control-buttons.tpl'}
	</div>
</div>