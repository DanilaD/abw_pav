<!-- поисковик по авто -->
<div class="filter-form-block active js-select-content" id="search-tab-auto">
	<form action="{$static_www}/car/sell/" method="GET" class="js-filter-form filter-form">
        <input type="hidden" name="search" value="1">
		<input type="hidden" name="type" value="1">
		<div class="form-inner">
			<div class="select-wrapper box-wrapper">
              <select name="marka[]" class="js-select-marka" title="Марка" multiple>
					{html_options options=$search.marks}
				</select>
			</div>
			<div class="select-wrapper box-wrapper">
				<select name="model[]" class="js-select-model" title="Модель" disabled multiple>
				</select>
			</div>
			<div class="select-wrapper box-wrapper">
				<select name="year1" title="Год, от">
					{html_options options=$search.year}
				</select>
			</div>
			<div class="select-wrapper box-wrapper">
				<select name="cost_val2" title="Цена, до">
					{html_options options=$search.price}
				</select>
			</div>
			<input type="submit" value="Найти">
		</div>
	</form>
</div>