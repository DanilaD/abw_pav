<!-- поисковик по новости -->
<div class="filter-form-block js-select-content" id="search-tab-news">
	<form action="//www.abw.by/" method="GET" class="js-filter-form filter-form">
		<input type="hidden" name="act" value="search">
        <input type="hidden" name="area" value="news">
		<div class="form-inner">
			<div class="box-wrapper">
				<input type="text" name="search-text" placeholder="Введите текст для поиска">
			</div>
			<input type="submit" value="Найти">
		</div>
	</form>
</div>