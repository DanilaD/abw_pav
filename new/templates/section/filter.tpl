<div class="filter">
	<div class="title">Поиск по объявлениям о продаже легковых автомобилей</div>
	{if isset($brands)}
		<ul class="filter-marka">
			{foreach from=$brands key=key item=brand}
				<li class="filter-marka-item"><a href="{$brand.link}">
					{$brand.marka}<span class="count">{$brand.count}</span>
				</a></li>
			{/foreach}
			<li class="filter-marka-item"><a href="{$link.car.link}" class="grey-link">
				{$link.car.name}
			</a></li>
		</ul>
	{/if}
	<div class="filter-form-block-outer js-get-filter-form-result">	
		{include file='section/filter-forms/form-auto.tpl'}
		<div class="result-count"><span class="count" id="js-filter-result"></span></div>
	</div>
	
</div>