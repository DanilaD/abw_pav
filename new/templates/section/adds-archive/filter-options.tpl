<div class="products-filter-options">
	<div class="products-filter-options-inner js-filter-options-redirect">
		{*<div class="box-wrapper">
          {if isset($links_sort.day)}
            <select name="day" title="Показать за">
                {foreach from=$links_sort.day key=key item=day}
                  <option value="{$day.value}" data-url="{$day.url}" {if (isset($smarty.session.search.car.day) && $smarty.session.search.car.day == $day.value)}selected{/if}>{$day.text}</option>
                {/foreach}
	        </select>
          {/if}
		</div>*}{if isset($links_sort.sort)}<div class="box-wrapper hide-mobile">
              <p> <span><b>Сортировать по:</b></span>
                {foreach from=$links_sort.sort key=key item=sort}
                  <span value="{$sort.value}" class="{$sort.class}" data-url="{$sort.url}">{$sort.text}</span>
                {/foreach}
                </p>
        </div>{/if}
	    <div class="box-wrapper show-mobile">
	    	<div class="carousel-options js-carousel-options">
	    		<button type="button" name="sort" value="" data-url="">По актуальности</button>
				<button type="button" name="sort" value="model" data-url="">По модели</button>
				<button type="button" name="sort" value="cost" data-url="">По цене</button>
				<button type="button" name="sort" value="date_add" data-url="">По дате подачи</button>
				<button type="button" name="sort" value="year" data-url="">По году выпуска</button>
				<button type="button" name="sort" value="probeg" data-url="">По пробегу</button>
				<button type="button" name="na_rf" value="2" data-url="">С пробегом</button>
				<button type="button" name="na_rf" value="3" data-url="">Новый</button>
				<button type="button" name="na_rf" value="1" data-url="">Аварийный</button>
				<button type="button" name="na_rf" value="4" data-url="">Целиком на з/ч</button>
	    	</div>
		</div>
	</div>
</div>