{if (isset($car_links.list_car_top))}
  <div class="super-links"><div class="extra-links-item">
    {foreach from=$car_links.list_car_top item=v}
      <a href="{$v.link}" target="{$v.target}">{$v.name}</a>
    {/foreach}
  </div></div>
{/if}