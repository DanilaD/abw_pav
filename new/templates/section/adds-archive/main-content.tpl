{*'product_cat' должен принимать категорию продукта продукта: car, moto, truck, parts_cars и т.д. - для вывода разных форм поиска*}
{assign 'product_cat' 'car'}
{if isset($viewed)}
  <h1 class="title-h1">Избранное</h1>
  {include file='section/adds-archive/favorites-panel.tpl'}
{elseif isset($company)}
  {include file='section/adds-archive/company-list.tpl'}
{else}
  <div class="archive-page-quick-links">
  	{if isset($smarty.session.search.car.type) && $smarty.session.search.car.type == 2}
		<a href="{$static_www}/car/sell/" class="item">Продажа</a><h1 class="item active">Покупка</h1>
  	{else}
		<h1 class="item active">Продажа</h1><a href="{$static_www}/car/buy/" class="item">Покупка</a>
  	{/if}
  </div>
    {if !isset($marks_count_buy) && !isset($models_count_buy)} 
      {include file="section/adds-archive/filter-form/filter-form-`$product_cat`.tpl"}
      {include file='section/adds-archive/adds-models.tpl'}
    {else}
      {include file='section/adds-archive/adds-models.tpl'}
    {/if}
    {include file='section/adds-archive/banners-text.tpl'}
    {include file='section/adds-archive/btn-favourites-page.tpl'}
{/if}
{include file='section/adds-archive/adds-list.tpl' nocache}