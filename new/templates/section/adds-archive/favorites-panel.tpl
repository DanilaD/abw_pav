<div class="favorites-panel clearfix">
    <input type="checkbox" name="favorites-all" id="favorites-all" class="checkbox">
    <label for="favorites-all">Выделить всё</label>
    <button class="btn-light-blue" id="defavorite-all" disabled>Удалить из избранного</button>
</div>
{if isset($deleted_advs) && $deleted_advs}
<p>
  Объявления с ID <b>{$deleted_advs}</b> не найдены в базе, видимо владельц удалил их.
</p>
{/if}