{if isset($models_count)}
<div class="product-models clearfix">
	<div class="title">{$models_count.marka}</div>
	<ul class="filter-marka">
		{foreach from=$models_count.models key=key item=model}
			<li class="filter-marka-item"><a href="{$model.link}">
				{$model.name}<span class="count">{$model.count}</span>
			</a></li>
		{/foreach}
		{*<li class="filter-marka-item"><a href="{$link.car.link}" class="grey-link">
			{$link.car.name}
		</a></li>*}
	</ul>
</div>
{elseif isset($models_count_buy)}
<div class="product-models products-buy-filter clearfix">
	<div class="title">{$models_count_buy.marka}</div>
	<ul class="filter-marka">
		{foreach from=$models_count_buy.models key=key item=model}
			<li class="filter-marka-item"><a href="{$model.link}">
				{$model.name}<span class="count">{$model.count}</span>
			</a></li>
		{/foreach}
		{*<li class="filter-marka-item"><a href="{$link.car.link}" class="grey-link">
			{$link.car.name}
		</a></li>*}
	</ul>
	<a href="{$static_www}/car/buy/" class="return">←  Другие марки</a>
</div>
{elseif isset($marks_count_buy)}
<div class="product-models products-buy-filter clearfix">
	<ul class="filter-marka">
		{foreach from=$marks_count_buy key=key item=marka}
			<li class="filter-marka-item"><a href="{$marka.link}">
				{$marka.name}<span class="count">{$marka.count}</span>
			</a></li>
		{/foreach}
		{*<li class="filter-marka-item"><a href="{$link.car.link}" class="grey-link">
			{$link.car.name}
		</a></li>*}
	</ul>
</div> 
{/if}
{*isset(models_count_buy)}блок моделей{/if}
{isset($marks_count_buy)}блок марок{/if*}