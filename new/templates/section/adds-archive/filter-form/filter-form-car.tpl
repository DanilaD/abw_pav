{nocache} 
<div class="products-filter-form">
	<form action="{$static_www}/car/sell/" method="GET" class="js-filter-form">
        <input type="hidden" name="search" value="1"> 
        <input type="hidden" name="type" value="{if isset($smarty.session.search.car.type)}{$smarty.session.search.car.type}{/if}">
        {*<input type="hidden" name="day" value="{$smarty.session.search.car.day}">*}
        <input type="hidden" name="sort" value="{if isset($smarty.session.search.car.sort)}{$smarty.session.search.car.sort}{/if}">
		<div class="form-inner">
			<div class="box-wrapper">
				<select name="marka[]" class="js-select-marka" title="Выберите марки авто" multiple>
					{html_options options=$search.marks selected=$smarty.session.search.car.marka}                    
				</select>
			</div><div class="box-wrapper">
				<select name="model[]" class="js-select-model" title="Выберите модель авто" data-current="{$smarty.session.search.car.model_str}" disabled multiple>
				</select>
			</div><div class="box-wrapper">
				<select name="engine[]" title="Выберите тип двигателя" multiple>
                    {html_options options=$search.engine selected=$smarty.session.search.car.engine}
				</select>
			</div><div class="box-wrapper">
				<select name="transm[]" title="Выберите трансмиссию" multiple>
					{html_options options=$search.trans  selected=$smarty.session.search.car.transm}
				</select>
			</div><div class="box-wrapper">
				<div class="box-inner-wrapper">
					<select name="capacity1" title="Объём, от">
                        {html_options options=$search.volume  selected=$smarty.session.search.car.capacity1}
					</select>
				</div><div class="box-inner-wrapper">
					<select name="capacity2" title="до">
						{html_options options=$search.volume  selected=$smarty.session.search.car.capacity2}
					</select>
				</div>
			</div><div class="box-wrapper">
				<div class="box-inner-wrapper">
                    <select name="mileage1" title="Пробег, от" >
						{html_options options=$search.mileage1 selected=$smarty.session.search.car.mileage1}
					</select>
				</div><div class="box-inner-wrapper">
					<select name="mileage2" title="до" >
						{html_options options=$search.mileage2 selected=$smarty.session.search.car.mileage2}
					</select>
				</div>
			</div><div class="box-wrapper">
				<div class="box-inner-wrapper">
					<select name="year1" title="Год, от" >
						{html_options options=$search.year  selected=$smarty.session.search.car.year1}
					</select>
				</div><div class="box-inner-wrapper">
					<select name="year2" title="до">
						{html_options options=$search.year  selected=$smarty.session.search.car.year2}
					</select>
				</div>
			</div><div class="box-wrapper">
				<div class="box-inner-wrapper">
					<select name="price1" title="Цена, от">
						{html_options options=$search.price1  selected=$smarty.session.search.car.price1}
					</select>
				</div><div class="box-inner-wrapper">
					<select name="price2" title="до">
						{html_options options=$search.price  selected=$smarty.session.search.car.price2}
					</select>
				</div>
			</div><div class="box-wrapper">
				<select name="body[]" title="Кузов" multiple>
                    {html_options options=$search.body  selected=$smarty.session.search.car.body}
				</select>
			</div><div class="box-wrapper hide">
				<select name="country" title="Страна" class="js-select-country">
					{html_options options=$search.country  selected=$smarty.session.search.car.country}
				</select>
			</div><div class="box-wrapper hide">
				<select name="rigion[]" title="Область"  class="js-select-region" data-current="{$smarty.session.search.car.rigion_str}" multiple>
				</select>
			</div><div class="box-wrapper hide">
				<select name="wheel[]" title="Привод" multiple>
                    {html_options options=$search.wheel selected=$smarty.session.search.car.wheel}
		        </select>
			</div>{*<div class="box-wrapper hide">
				<input type="text" name="text" value="" placeholder="По тексту">
			</div>*}<div class="box-wrapper hide">
	    	<select name="condition[]" title="Состояние" multiple>
                {html_options options=$search.condition selected=$smarty.session.search.car.condition}
	        </select>
            </div>{*<div class="box-wrapper hide">
				<select name="seller[]" title="Продавец" multiple>
					{html_options options=$search.seller selected=$smarty.session.search.car.seller}
		        </select>
			</div>*}<div class="box-wrapper hide">
                <input name="text" type="text" placeholder="поиск по ключевому слову">
			</div><div class="box-wrapper hide">
				<select name="day" title="Показать за период">
                    {html_options options=$search.day  selected=$smarty.session.search.car.day}
				</select>
			</div><div class="box-wrapper hide">
				<div class="checkbox" title="Не показывать авто под заказ, объявления автохаусов и автодилеров">
					<input type="checkbox" id="private" name="private" value="1" {if $smarty.session.search.car.private}checked{/if}>
					<label for="private"><span>Только частные объявления</span></label>
				</div>
			</div><div class="box-wrapper hide">
				<div class="checkbox" title="Показывать только объявления автохаусов и дилеров">
					<input type="checkbox" id="companies" name="companies" value="1" {$smarty.session.search.car.companies} {if $smarty.session.search.car.companies}checked{/if}>
					<label for="companies"><span>Объявления компаний</span></label>
				</div>
			</div>
		</div>
		<div class="form-bottom form-inner">
			<div class="box-wrapper">
				<a href="" class="show-more js-show-options">Больше параметров</a>
			</div><div class="box-wrapper">
				{*<a href="">Любой регион</a>*}
			</div><div class="box-wrapper">
				<a href="" class="clean js-form-clean">Очистить</a>
				<input type="submit" value="Найти" id="js-filter-result">
			</div>
		</div>
	</form>
</div>
{/nocache} 