{assign 'show_amount' 5}
<div class="categories-link-outer">
	<div class="categories-link-block" id="js-categories-list" data-show_amount="{$show_amount}">{*
		*}{if (isset($main_page_cat)) }{*
			*}{assign 'i' 0}{*
			*}{foreach from=$main_page_cat item=cat_data key=cat}{*
				*}{if $i == '0'}{*
					*}<div class="category-item main js-tips-target" id="js-categories-main"><div class="category-main-inner">
						<div class="category-item-inner">
							<a href="{$cat_data['link']}"><div class="cat-icon cat-{$cat}"></div><div class="title">{$cat_data['name']}</div></a>
						</div>
					</div></div>{*
				*}{else}{*
					*}<div class="category-item{if $i >= $show_amount} category-item-dekstop-hide{/if}">
						<a href="{$cat_data['link']}"><div class="category-item-inner">
							<div class="cat-icon cat-{$cat}"></div><div class="title">{$cat_data['name']}</div>
						</div></a>
					</div>{*
				*}{/if}{*
				*}{assign 'i' $i+1}{*
			*}{/foreach}{*
		*}{/if}{*
	*}</div>
	<div class="btn-show-more" id="js-categories-btn"><div class="btn-show-more-inner"></div></div>
</div>