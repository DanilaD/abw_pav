{*{if $tpl['file'] == 'main.tpl' && !$close_tips}
	{assign 'is_show_tips' 1}
{else}
	{assign 'is_show_tips' 0}
{/if}*}
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9) ]><!-->
<html>
<!--<![endif]-->
<head>
	<title>{if isset($seo.social.title) && $seo.social.title}{$seo.social.title}{/if}</title>
	<meta charset="utf-8">
	{*<meta name="viewport" content="width=device-width, initial-scale=1.0">*}
	<meta name="viewport" content="width=1265,user-scalable=yes">

	{*<!-- meta tags for social sharing -->*}
	<meta name="description" content="{if isset($seo.social.desc) && $seo.social.desc}{$seo.social.desc}{/if}">
	<meta name="keywords" content="{if isset($seo.social.keyw) && $seo.social.keyw}{$seo.social.keyw}{/if}" />

	<meta property="og:title" content="{if isset($seo.social.title) && $seo.social.title}{$seo.social.title}{/if}">
	<meta property="og:description" content="{if isset($seo.social.desc) && $seo.social.desc}{$seo.social.desc}{/if}">
	<meta property="og:image" content="{if isset($seo.social.img) && $seo.social.img}{$seo.social.img}{/if}">
	
	{*<!-- Twitter Cards -->*}
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="{if isset($seo.social.title) && $seo.social.title}{$seo.social.title}{/if}">
	<meta name="twitter:description" content="{if isset($seo.social.desc) && $seo.social.desc}{$seo.social.desc}{/if}">
	<meta name="twitter:image" content="{if isset($seo.social.img) && $seo.social.img}{$seo.social.img}{/if}">
	<meta name="twitter:site" content="{if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")}https://{else}http://{/if}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}">
	<!-- meta tags for social sharing -->

    {*<!-- Google Tag Manager -->*}
    {literal}<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W6KVN8B');</script>{/literal}
    
    {if isset($seo.header)}{$seo.header}{/if}
    {if $is_localhost}
    	<script src="//localhost:35729/livereload.js" type="text/javascript"></script>
    	<link rel="stylesheet" href="{$current_www}/des/css/styles.css" type="text/css" media="all">
    	{*<!-- <link rel="stylesheet" href="{$current_www}/des/css/news15.css" type="text/css" media="all"> -->
    	<!-- <link rel="stylesheet" href="{$current_www}/des/css/styles_header_footer.min.css" type="text/css" media="all"> -->*}
    {else}
        <link rel="stylesheet" href="{$current_www}/des/css/styles.min.css?ver=7" type="text/css" media="all">
    {/if}
    {if $is_correct_branding}
        <link rel="stylesheet" href="{$current_www}/branding/branding.css?ver=7" type="text/css" media="all">
    {/if}
    <link rel="shortcut icon" href="{$current_www}/images/favicon/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" sizes="32x32" href="{$current_www}/images/favicon/favicon_32.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="{$current_www}/images/favicon/favicon_180.ico">
    {literal}
      <script type='text/javascript'>
        var crtg_nid = '5048';
        var crtg_cookiename = 'dgtlby_rta';
        var crtg_varname = 'dgtlby_content';
        function crtg_getCookie(c_name){ var i,x,y,ARRCookies=document.cookie.split(";");for(i=0;i<ARRCookies.length;i++){x=ARRCookies[i].substr(0,ARRCookies[i].indexOf("="));y=ARRCookies[i].substr(ARRCookies[i].indexOf("=")+1);x=x.replace(/^\s+|\s+$/g,"");if(x==c_name){return unescape(y);} }return'';}
        var dgtlby_content = crtg_getCookie(crtg_cookiename);
        var crtg_rnd=Math.floor(Math.random()*99999999999);
        (function(){
        var crtg_url=location.protocol+'//rtax.criteo.com/delivery/rta/rta.js?netId='+escape(crtg_nid);
        crtg_url +='&cookieName='+escape(crtg_cookiename);
        crtg_url +='&rnd='+crtg_rnd;
        crtg_url +='&varName=' + escape(crtg_varname);
        var crtg_script=document.createElement('script');crtg_script.type='text/javascript';crtg_script.src=crtg_url;crtg_script.async=true;
        if(document.getElementsByTagName("head").length>0)document.getElementsByTagName("head")[0].appendChild(crtg_script);
        else if(document.getElementsByTagName("body").length>0)document.getElementsByTagName("body")[0].appendChild(crtg_script);
        })();
      </script>
    {/literal}  
</head>
<body class="{$body_classes} {$mobile.body_device} {$logined_as_admin} {if $is_correct_branding} branding-activate{/if}">

  {*<!-- Google Tag Manager (noscript) -->*}
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W6KVN8B" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	{if $is_correct_branding}
        {include file="branding/branding-header.tpl"}
    {/if}
    <div class="wrapper main-wrapper">
        {if !$is_correct_branding}
		<div class="before-header">
			{include file="ban/BN1.tpl"} {*баннер над шапкой*}
		</div>
        {/if}
		<div class="ie-fixMinHeight">
			<div id="page">                
				{include file='header.tpl'}
				<div id="content">
                  {*{if isset($tpl.cache_time)}{include file=$tpl.file cache_lifetime=$tpl.cache_time cache_id=$tpl.cache_id}{else}{include file=$tpl.file nocache}{/if}*}
                  {include file=$tpl.file}
				</div><!-- #content -->
				{include file='footer.tpl'}
			</div>
		</div>
	</div>
    {*if $is_show_tips}
        {include file='section/tips.tpl'}
    {/if*}
    {if $is_correct_branding}
        {include file="branding/branding-footer.tpl"}
    {/if}
	<script type="text/javascript">
	    var BASE_PATH = "{$current_www}/index.php";
	</script>
	{if $is_localhost}
    	<script type='text/javascript' src='{$current_www}/des/js/common.js'></script>
    	<!-- <script type='text/javascript' src='{$current_www}/des/js/common_header_footer.min.js'></script> -->
    {else}
    	<script type='text/javascript' src='{$current_www}/des/js/common.min.js?ver=7'></script>
    {/if}
    <script type='text/javascript' src='{$current_www}/des/js/mt-bank.js'></script>
    {*<!-- Yandex.Metrika counter -->*}
    {literal}<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter16755670 = new Ya.Metrika({ id:16755670, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/16755670" style="position:absolute; left:-9999px;" alt="" /></div></noscript>{/literal}
    {if isset($seo.footer)}{$seo.footer}{/if}
</body>
</html>