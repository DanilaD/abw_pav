<div class="section clearfix">
	<div class="filter-block hide-filter-right clearfix" id="js-front-filter-block">
		<div class="filter-right">{include file="ban/BN3.tpl"} {*баннер 300х600 правый в блоке объявлений*}</div>
		<div class="filter-center">
			{include file='section/categories-links.tpl'}
			{include file='section/filter.tpl'}
		</div>
	</div>
	{include file='ban/front/after-filter.tpl'}
	{if isset($cars)}
		{*{include file='content/adds/cars-special-offer-link.tpl'}
		{include file='content/adds/cars-overtracking-link.tpl'}*}
		<div class="clearfix"></div>
		{include file='section/adds/cars-handler.tpl' cars=$cars}
	{/if}
</div>