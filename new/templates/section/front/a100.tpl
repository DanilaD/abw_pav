{if isset($fuel)}
<div class="a100">
	<div class="a100-item-icon">
		<span class="icon-a100"></span>
	</div>
	<div class="a100-inner">
		{foreach from=$fuel item=i key=k}
			<div class="a100-item">
				<span class="type">{$i.name}</span><span class="cost">{$i.cost}</span>
			</div>
	  {/foreach}
	</div>
</div>
{/if}