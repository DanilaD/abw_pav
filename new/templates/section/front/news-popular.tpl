{if (isset($NewsDayPopular))}
	<div class="news-popular">
	<h3 class="title-h3">Популярное за 3&nbsp;дня</h2>
	{foreach from=$NewsDayPopular  item=news key=k}
		{include file='content/news-item/item-simple.tpl' 
			class=$news.class
			is_spectator=$news.type
			link=$news.link 
			title=$news.name 
			excerpt=$news.descrip  
			date=$news.date_add2 
			views=$news.views 
			comments=$news.comment 
			thumb=$news.img}
	{/foreach}
	</div>
{/if}