{if (isset($NewsLast))}
<div class="section clearfix">
	<div class="news-block">
		<div class="column-right">
			{include file='section/front/exchange-rates.tpl'}
			{include file='section/front/news-popular.tpl'}
			{include file='content/all-news-link.tpl'}
		</div>
		<div class="column-center">
			<div class="title-block">
				<h2 class="title-h2">Автомобильные новости</h2>
				{include file='section/front/a100.tpl'}
			</div>
			{include file='section/news/news-handler.tpl' news_all=$NewsLast}
		</div>
	</div>
</div>
{/if}