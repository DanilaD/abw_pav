<div class="section clearfix section-top-news">
	{if (isset($NewsTop))}
		{assign "top_news_count" $NewsTop|@count}

		<div class="top-news-block top-news-block-{$top_news_count} clearfix">
		{foreach from=$NewsTop item=news key=k}
			{assign "top_news_item_size" "s"}
			{*assign "top_news_thumb_size" "thumb_310_195"*}

			{if $top_news_count == 5 && $k == 0} 
				{assign "top_news_item_size" "m"}
			{elseif $top_news_count == 4} 
				{if $k == 0}
					{assign "top_news_item_size" "m"}
				{elseif $k == 2}
					{assign "top_news_item_size" "m"}
					{*assign "top_news_thumb_size" "thumb_630_195"*}
				{/if}
			{elseif $top_news_count == 3 && $k == 0} 
				{assign "top_news_item_size" "l"}
				{*assign "top_news_thumb_size" "thumb_630_400"*}
			{/if}

			{include file='content/news-item-top.tpl' 
				item_size=$top_news_item_size 
				link=$news.link 
				title=$news.name 
				excerpt=(($top_news_count == 5 || $top_news_count == 4) && $k == 0) ? $news.descrip : ''  
				date=$news.date_add2 
				views=$news.views 
				comments=$news.comment 
				thumb=$news.img
				is_adv=$news.boolean}

		{/foreach}  
		</div>
		{include file='content/all-news-link.tpl'}
	{/if}
	<div class="top-news-left">
		{include file="ban/BN2.tpl"} {*баннер 240х400 левый в ТОПе новостей*}
	</div>
</div>