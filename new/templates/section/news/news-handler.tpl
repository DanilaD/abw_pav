{if isset($news_all)}{*
	*}<div class="row row-flex row-4 super-news">
		{foreach from=$news_all  item=news key=k}{*
			*}<div class="col{if isset($news.news_type) && ($news.news_type=='super' || $news.news_type=='img')} col-{$news.news_type}{/if}">
				{if $news.news_type == 'img'}
					{include file="content/news-item/item-banner.tpl"
						link=$news.link 
						thumb=$news.img}
				{else}
					{if $news.news_type == 'super'}
						{assign 'template' 'super'}
					{else}
						{assign 'template' 'full'}
					{/if}
					{include file="content/news-item/item-`$template`.tpl" 
						class=$news.class
						is_spectator=$news.type
						link=$news.link 
						title=$news.name 
						excerpt=$news.descrip  
						date=$news.date_add2 
						views=$news.views 
						comments=$news.comment 
						thumb=$news.img}
				{/if}
			</div>{*
		*}{/foreach}
	</div>{*
*}{/if}