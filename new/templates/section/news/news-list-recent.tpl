{if (isset($news_popular) && $news_popular)}
<div class="section clearfix">
	<div class="news-block news-block-recent"><h2 class="title-h2">Самое читаемое за&nbsp;неделю</h2>{*
		*}<div class="row row-flex row-4 cols-with-borders">{*
			*}{foreach from=$news_popular item=news key=k}{*
				*}<div class="col">
					{include file='content/news-item/item-simple.tpl' 
						class=$news.class
						is_spectator=$news.type
						link=$news.link 
						title=$news.name 
						excerpt=$news.descrip  
						date=$news.date_add2 
						views=$news.views 
						comments=$news.comment 
						thumb=$news.img}
				</div>{*
			*}{/foreach}{*
		*}</div>
	</div>
	<a class="bold-link" href="/news/">Все новости</a>
</div>
{/if}