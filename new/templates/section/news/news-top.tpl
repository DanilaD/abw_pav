<div class="section clearfix section-top-news">
	{if (isset($NewsTop))}
		{assign "top_news_count" $NewsTop|@count}

		<div class="top-news-block top-news-block-{$top_news_count} clearfix">
		{foreach from=$NewsTop item=news key=k}
			{assign var="item_data" value=[
				'link'		=>$news.link,
				'title'		=>$news.name,
				'excerpt'	=>$news.descrip,
				'date'		=>$news.date_add2,
				'views'		=>$news.views,
				'comments'	=>$news.comment,
				'thumb'		=>$news.img,
				'is_adv'	=>$news.boolean]}

			{if $top_news_count == 3 && $k == 0}
				{include file='content/news-top/item-big.tpl' item_data=$item_data}
			{elseif ($top_news_count == 4 && ($k == 0 || $k == 2)) || ($top_news_count == 5 && $k == 0)}
				{if isset($news.descrip) && $news.descrip}
					{include file='content/news-top/item-with-excerpt.tpl' item_data=$item_data}
				{else}
					{include file='content/news-top/item-wide.tpl' item_data=$item_data}
				{/if}
			{else}
				{include file='content/news-top/item-simple.tpl' item_data=$item_data}
			{/if}
			
		{/foreach}  
		</div>
		{include file='content/all-news-link.tpl'}
		<div class="clearfix"></div>
	{/if}
	<div class="top-news-left">
		{include file="ban/BN2.tpl" nocache} {*баннер 240х400 левый в ТОПе новостей*}
	</div>
</div>