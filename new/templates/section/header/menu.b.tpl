{if isset($header_menu)}
	<div class="primary-navigation-outer">
	<ul id="js-primary-navigation" class="primary-navigation">
	{foreach from=$header_menu item=top_item}{*
		*}<li>
			{if isset($top_item.link) && $top_item.link}
				<a class="top-menu-item js-top-menu-item" href="{$top_item.link}">{$top_item.name}</a>
			{else}
				<span class="top-menu-item js-top-menu-item">{$top_item.name}</span>
			{/if}
			
			{if isset($top_item.sub)}
			<div class="submenu js-submenu">
				<div class="submenu-inner js-tabs tabs clearfix">
					<div class="submenu-left">
					{assign 'tab_content' ''}
					{foreach from=$top_item.sub item=sub_data}
						{if isset($sub_data.title) && $sub_data.title && $sub_data.type != 'block'}
							<div class="title-grey">{$sub_data.title}</div>
						{/if}
						{if $sub_data.type == 'link' && isset($sub_data.items) && $sub_data.items}
							{foreach from=$sub_data.items item=link_item}
								<div class="link-item"><a href="{$link_item.link}">{$link_item.name}</a></div>
							{/foreach}
						{/if}
						{if $sub_data.type == 'block'}
							<div class="bottom-block">
								<div class="title-grey">{$sub_data.title}</div>
								{if $sub_data.content == 'subscription'}
									{include file='section/header/subscribe.tpl'}
								{else if $sub_data.content == 'add'}
									{include file='section/header/add_117.tpl'}
								{else if $sub_data.content == 'company'}
									{include file='section/header/company.tpl'}
								{/if}
							</div>
						{/if}

						{if isset($sub_data.items) && $sub_data.items && $sub_data.type == 'tab'}
							<div class="tabs-titles">
							{foreach from=$sub_data.items item=sub_data_item key=key}
								{if isset($sub_data_item.items)}
									{* tab title *}
									{if isset($sub_data_item.title)}
										<div class="tab-title js-tab-nav{if $key==0} active{/if}" data-target="#tab-content-{$key}">{$sub_data_item.title}</div>
									{/if}

									{* tab data *}
									{capture append="tab_content"}
										{if isset($sub_data_item.title)}
											<div class="tab-title tab-mobile js-tab-nav{if $key==0} active{/if}" data-target="#tab-content-{$key}">{$sub_data_item.title}</div>
										{/if}
										<div class="tab-content js-tab-item js-top-menu-subitem{if $key==0} active{/if}" id="tab-content-{$key}">
											<div class="tab-content-inner">
											{foreach from=$sub_data_item.items item=sub_data_tabs}{*
												*}{if isset($sub_data_tabs.items)}{*
													*}{if $sub_data_item.items|@count > 1 }<div class="column">{/if}{*
													*}{if isset($sub_data_tabs.title) && $sub_data_tabs.title}
														<div class="title-grey">{$sub_data_tabs.title}</div>
													{/if}
													{foreach from=$sub_data_tabs.items item=sub_data_tabs_items}
														{if isset($sub_data_tabs_items.name)}
															<div class="tab-item-link{if isset($sub_data_tabs_items.style) && $sub_data_tabs_items.style}  {$sub_data_tabs_items.style}{/if}">
																{if isset($sub_data_tabs_items.link) && $sub_data_tabs_items.link}
																	<a href="{$sub_data_tabs_items.link}">
																{/if}
																{$sub_data_tabs_items.name}
																{if isset($sub_data_tabs_items.link) && $sub_data_tabs_items.link}
																	</a>
																{/if}
																{if isset($sub_data_tabs_items.desc) && $sub_data_tabs_items.desc}
																	<div class="desc">{$sub_data_tabs_items.desc}</div>
																{/if}
															</div>
														{/if}
													{/foreach}{*
													*}{if $sub_data_item.items|@count > 1 }</div>{/if}{*
												*}{/if}{*
											*}{/foreach}
											</div>
											{if isset($sub_data_item.img) && $sub_data_item.img && isset($sub_data_item.link) && $sub_data_item.link}
												{* здесь могла бы быть ваша реклама *}

												<a href="{$sub_data_item.link}">{$sub_data_item.img}</a>
											{/if}
										</div>
									{/capture}
								{/if}
							{/foreach}
							</div>
						{/if}
					{/foreach}
					</div>
					<div class="submenu-right">
						{foreach from=$tab_content item=tab_content_item}
							{$tab_content_item}
						{/foreach}
					</div>
				</div>
			</div>
			{/if}
		</li>{*
	*}{/foreach}
	</ul>
	</div>
{/if}
{*if isset($m.rel)}rel="{$m.rel}"{/if} {if isset($m.target)}target="{$m.target}"{/if*}