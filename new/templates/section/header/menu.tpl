{if isset($header_menu)}
	<div class="primary-navigation-outer">
	<div id="js-primary-navigation" class="primary-navigation">
	<ul>
	{foreach from=$header_menu item=top_item}{*
		*}<li>
			{if isset($top_item.link) && $top_item.link}
				<a class="top-menu-item" href="{$top_item.link}">{$top_item.name}</a>
			{else}
				<span class="top-menu-item">{$top_item.name}</span>
			{/if}
			
			{if isset($top_item.sub)}
			<div class="submenu js-submenu">
				<div class="submenu-inner js-tabs tabs clearfix">
					<div class="submenu-left">
					{assign var='tab_content' value=''}
					{foreach from=$top_item.sub item=sub_data}
						{if isset($sub_data.title) && $sub_data.title && $sub_data.type != 'block'}
							<div class="title-grey">{$sub_data.title}</div>
						{/if}
						{if $sub_data.type == 'link' && isset($sub_data.items) && $sub_data.items}
							{foreach from=$sub_data.items item=link_item}
								<div class="link-item"><a href="{$link_item.link}">{$link_item.name}</a></div>
							{/foreach}
						{/if}
						{if $sub_data.type == 'block'}
							<div class="bottom-block">
								<div class="title-grey">{$sub_data.title}</div>
								{if $sub_data.content == 'subscription'}
									{include file='section/header/subscribe.tpl'}
								{elseif $sub_data.content == 'add'}
									{include file='section/header/add_117.tpl'}
								{elseif $sub_data.content == 'company'}
									{include file='section/header/company.tpl'}
								{/if}
							</div>
						{/if}

						{if isset($sub_data.items) && $sub_data.items && $sub_data.type == 'tab'}
							<div class="tabs-titles">
								<div class="tabs-titles-inner">
								<div class="js-tracker tracker"></div>
								{foreach from=$sub_data.items item=sub_data_item key=key}
									{if isset($sub_data_item.items)}
										{* tab title *}
										{if isset($sub_data_item.title)}
											<div class="tab-title js-tab-nav{if $key==0} active{/if}" data-target="#tab-content-{$key}">{$sub_data_item.title}</div>
										{/if}

										{* tab data *}
										{capture append="tab_content"}
											{if isset($sub_data_item.title)}
												<div class="tab-title tab-mobile js-tab-nav{if $key==0} active{/if}" data-target="#tab-content-{$key}">{$sub_data_item.title}</div>
											{/if}
											<div class="tab-content js-tab-item js-top-menu-subitem{if $key==0} active{/if}" id="tab-content-{$key}">
												{foreach from=$sub_data_item.items item=sub_data_tabs}{*
													*}{if isset($sub_data_tabs.items)}{*
														*}<div class="column js-column">{*
														*}{if isset($sub_data_tabs.title) && $sub_data_tabs.title}
															<div class="title-grey">{$sub_data_tabs.title}</div>
														{/if}
														{foreach from=$sub_data_tabs.items item=sub_data_tabs_items}
															{if isset($sub_data_tabs_items.name)}
																<div class="tab-item-link{if isset($sub_data_tabs_items.style) && $sub_data_tabs_items.style} {$sub_data_tabs_items.style}{/if}">
																	{if isset($sub_data_tabs_items.link) && $sub_data_tabs_items.link}
																		<a href="{$sub_data_tabs_items.link}" {if isset($sub_data_tabs_items.target)}target='_blank'{/if}>
																	{/if}
																	{$sub_data_tabs_items.name}
																	{if isset($sub_data_tabs_items.link) && $sub_data_tabs_items.link}
																		</a>
																	{/if}
																	{if isset($sub_data_tabs_items.desc) && $sub_data_tabs_items.desc}
																		<div class="desc">{$sub_data_tabs_items.desc}</div>
																	{/if}
																</div>
															{/if}
														{/foreach}{*
														*}</div>{*
													*}{/if}{*
												*}{/foreach}
												{if isset($sub_data_item.link) && $sub_data_item.link}
													{* здесь могла бы быть ваша реклама *}
													<div class="background-block">
														<a href="{$sub_data_item.link}" {if isset($sub_data_item.target)}target='_blank'{/if}>
														{if isset($sub_data_item.img) && $sub_data_item.img}
															<img src="{$sub_data_item.img}" alt="" class="dekstop-img">
														{/if}	
														{if isset($sub_data_item.img_mob) && $sub_data_item.img_mob}
															<img src="{$sub_data_item.img_mob}" alt="" class="mobile-img">
														{/if}
														</a>
													</div>
												{/if}
											</div>
										{/capture}
									{/if}
								{/foreach}
								</div>
							</div>
						{/if}
					{/foreach}
					</div>
					<div class="submenu-right">
						{foreach from=$tab_content item=tab_content_item}
							{$tab_content_item}
						{/foreach}
					</div>
				</div>
			</div>
			{/if}
		</li>{*
	*}{/foreach}
	{if isset($link.add) && $link.add}<li class="mobile-item"><a href="{$link.add.link}" class="add-product"><span class="icon-plus"></span>{$link.add.name}</a></li>{/if}
	</ul>
	</div>
	</div>
{/if}
{*if isset($m.rel)}rel="{$m.rel}"{/if} {if isset($m.target)}target="{$m.target}"{/if*}