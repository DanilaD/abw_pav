<div class="header-search-block js-search-tabs" id="js-search-block">
	<div class="header-search-block-inside">
		<div class="select-wrapper select-type">
			<select name="type[]" class="js-select-type">
				<option value="auto" data-target="#search-tab-auto" selected>Легковые авто</option>
				<option value="zpt" data-target="#search-tab-zpt">Запчасти для авто</option>
				<option value="trucks" data-target="#search-tab-trucks">Грузовики и спецтехника</option>
				<option value="tires" data-target="#search-tab-tires">Шины</option>
				<option value="disks" data-target="#search-tab-disks">Диски</option>
				<option value="news" data-target="#search-tab-news">Новости</option>
			</select>
		</div>
		<div class="select-content">
			{include file='section/filter-forms/form-auto.tpl'}
			{include file='section/filter-forms/form-autoparts.tpl'}
			{include file='section/filter-forms/form-trucks.tpl'}
			{include file='section/filter-forms/form-tires.tpl'}
			{include file='section/filter-forms/form-disks.tpl'}
			{include file='section/filter-forms/form-news.tpl'}
		</div>
		
	</div>
</div>