<div id="js-header-navigation" class="header-navigation">
	<div class="header-navigation-inner">
		<div class="header-search-mobile">Поиск<span class="icon-search"></span></div>
		{*временно. пока нету меню для гамбургера*}
		{*if isset($header_menu)}
			{foreach from=$header_menu item=m key=k}
				<div class="header-navigation-column">
					<div class="title js-mobile-submenu-toggle">{$m.name}</div>
					{if isset($m.sub)}
					<div class="header-navigation-list-block js-mobile-submenu">
						<ul class="header-navigation-list">
							{foreach from=$m.sub item=ms key=ks}
							<li>
								{if isset($ms.link)}
									<a href="{$ms.link}" {if isset($ms.title)}title="{$ms.title}"{/if} {if isset($ms.rel)}rel="{$ms.rel}"{/if} {if isset($ms.target)}target="{$ms.target}"{/if}>{$ms.name}</a>
								{else}
									{$ms.name}
								{/if}
							</li>
							{/foreach}
						</ul>
					</div>
					{/if}
				</div>
			{/foreach}
		{/if*}
		<a href="{if isset($smarty.session.login)}авторизирован{else}не авторизирован{/if}" class="login-mobile">Личный кабинет<span class="icon-login"></span></a>
	</div>
	<div class="header-navigation-inner clearfix">
		{if isset($social)}
			<div class="header-navigation-block">
				<div class="social-icons-block small">
					{foreach from=$social item=v key=k}
						<a href="{$v.link}" rel="nofollow" class="icon-item with-name {$k}" target="_blank"><span class="icon"></span>{$v.name}</a>
					{/foreach}  
				</div>
			</div>
		{/if}
		<div class="header-navigation-block">
			Подать объявление через:
			<div class="messaging-apps">
				<div class="social-icons-block messaging-apps-block">
					<a rel="nofollow" class="icon-item with-name telegram" target="_blank" href=""><span class="icon"></span>Telegram</a>
					<a rel="nofollow" class="icon-item with-name viber" target="_blank" href=""><span class="icon"></span>Viber</a>
				</div>	
			</div> 
		</div>
		<div class="header-navigation-block">
			Подписка на новости:
            <form action="https://www.abw.by/rss/" method="POST">
            <input type="hidden" name="act" value="mailing">
            <input type="hidden" name="act_adv" value="step_3">
            <input type="hidden" name="option" value="news">
            <input type="hidden" name="setting" value="0">
            <input type="email" name="email" maxlength="50" placeholder="Введите ваш e-mail" required="">
            <input type="submit" name="Подписаться">
          </form>
		</div>
	</div>
</div>