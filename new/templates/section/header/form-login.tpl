<div id="form-login" class="white-popup-block mfp-hide">
	<div class="form-login">
		<form class="form-login-inner js-form-login" method="POST">
			<div class="title">Вход в аккаунт</div>
			<label for="login">Логин</label>
			<input type="text" name="login" id="login" required>
			<label for="pass">Пароль</label>
			<input type="password" name="pass" id="pass" required>
			<div class="form-login-left">
				<input type="checkbox" name="autologin" id="autologin" checked>
				<label for="autologin">Запомнить</label>
			</div><div class="form-login-right">
				{if isset($link['foget']) && $link['foget']}
					<a href="{$link['foget']['link']}">Забыли пароль?</a>
				{/if}
			</div>
			<button type="submit" data-sleepmode="Пожалуйста, подождите">Войти</button>
		</form>
		{if isset($link['registration']) && $link['registration']}
			<a rel="nofollow" class="registrate" href="{$link['registration']['link']}">Регистрация</a>
		{/if}
	</div>
</div>