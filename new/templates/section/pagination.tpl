{if isset($pages)}
<div class="pagination clearfix">
	{if $pages.prev} <a href="{$pages.prev}" class="prev"><span class="icon icon-arrow-next-blue"></span>Предыдущая страница</a>{/if}
	{if $pages.next} <a href="{$pages.next}" class="next">Следующая страница<span class="icon icon-arrow-next-blue"></span></a>{/if}
</div>
{/if}