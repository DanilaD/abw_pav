{include file='section/breadcrumbs.tpl'}
<h1 class="title-h1">К сожалению, объявление ID {$data.main.adv_id} не найдено :(</h1>

{if ($breadcrumbs)}
  <div><a class="link-light-blue fz-16 fw-bold" href="{$breadcrumbs.link4}">Открыть все объявления {$breadcrumbs.name3} {$breadcrumbs.name4}</a></div>
  <div><a class="link-light-blue fz-16 fw-bold" href="{$breadcrumbs.link3}">Перейти к объявлениям {$breadcrumbs.name3}</a></div>
{/if}

<a class="btn-light-blue" href="{$static_www}">Вернуться на главную</a>

{include file='section/adds/similar-adds.tpl' nocache}