<div class="stock-item simple-item-full"><a href="{$link}">
	{if isset($thumb)}
		<div class="thumb simple-item-thumb">
			<img src="{$thumb}" alt="">
		</div>
	{/if}
	<div class="title">{$title}</div>
	{if isset($label) && $label}
		<div class="label">{$label}</div>
	{else}	
		<div class="label icon-present"></div>	
	{/if}

</a></div>