{if isset($item_data) && $item_data}
<div class="top-news-item simple-item top-news-item-m with-excerpt{if isset($item_data.is_adv) && $item_data.is_adv} with-label{/if}"><a href="{$item_data.link}">
		<div class="top-news-item-inner-excerpt">{include file='content/news-top/content.tpl' item_data=$item_data}</div>
		<div class="top-news-item-excerpt">{$item_data.excerpt}</div>
</a></div>
{/if}