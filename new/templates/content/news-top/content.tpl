<div class="top-news-item-inner" style="background-image: url({$item_data.thumb})">
	<div class="content">
		<div class="title">{$item_data.title}</div>
	</div>
	<div class="data">
		<div class="data-inner">
			{if isset($item_data.date) && $item_data.date}
				<span class="data-item date">{$item_data.date}</span>
			{/if}
			{if isset($item_data.views) && $item_data.views}
				<span class="data-item views">{$item_data.views}</span>
			{/if}
			{if isset($item_data.comments) && $item_data.comments}
				<span class="data-item comments">{$item_data.comments}</span>
			{/if}
		</div>
	</div>
	{if isset($item_data.is_adv) && $item_data.is_adv}
		<div class="label">Реклама</div>
	{/if}
</div>