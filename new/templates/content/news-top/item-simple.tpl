{if isset($item_data) && $item_data}
<div class="top-news-item simple-item top-news-item-s{if isset($item_data.is_adv) && $item_data.is_adv} with-label{/if}"><a href="{$item_data.link}">
		{include file='content/news-top/content.tpl' item_data=$item_data}
</a></div>
{/if}