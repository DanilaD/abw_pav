<div class="news-item simple-item special"><a href="{$link}" target="_blank">
	<div class="simple-item-thumb" {if isset($thumb) && $thumb}style="background-image: url({$thumb});"{/if}>
		{if !$thumb}
			{include file='content/no-photo.tpl'}
		{/if}
	</div>
</a></div>