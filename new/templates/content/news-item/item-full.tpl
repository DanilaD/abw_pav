<div class="news-item simple-item simple-item-full{if isset($class) && $class} {$class}{/if}"><a href="{$link}">
	<div class="simple-item-thumb">
		{if isset($thumb) && $thumb}
			<img src="{$thumb}" alt="">
		{else}
			{include file='content/no-photo.tpl'}
		{/if}
		<div class="simple-item-thumb-inner">
			<div class="data">
				<div class="data-inner">
					{if isset($date) && $date}
						<span class="data-item date">{$date}</span>
					{/if}
					{if isset($views) && $views}
						<span class="data-item views">{$views}</span>
					{/if}
					{if isset($comments) && $comments}
						<span class="data-item comments">{$comments}</span>
					{/if}
				</div>
			</div>
		</div>
		{if isset($is_spectator) && $is_spectator}
			<div class="label">ОЧЕВИДЕЦ ABW</div>
		{/if}
	</div>
	<div class="simple-item-content">
		<div class="title">{$title}</div>
		{if isset($excerpt) && $excerpt}
			<div class="excerpt">{$excerpt}</div>
		{/if}
	</div>
</a></div>