<div class="news-item simple-item super{if isset($class) && $class} {$class}{/if}"><a href="{$link}">
	<div class="simple-item-thumb" {if isset($thumb) && $thumb}style="background-image: url({$thumb});"{/if}>
		{if isset($thumb) && $thumb}
			
		{else}
			{include file='content/no-photo.tpl'}
		{/if}
	</div>
	<div class="simple-item-content">
		<div class="simple-item-content-inner">
			{if isset($is_spectator) && $is_spectator}
				<div class="label">ОЧЕВИДЕЦ ABW</div>
			{/if}
			<div class="title">{$title}</div>
			<div class="data">
				<div class="data-inner">
					{if isset($date) && $date}
						<span class="data-item date">{$date}</span>
					{/if}
					{if isset($views) && $views}
						<span class="data-item views">{$views}</span>
					{/if}
					{if isset($comments) && $comments}
						<span class="data-item comments">{$comments}</span>
					{/if}
				</div>
			</div>
		</div>
	</div>
</a></div>