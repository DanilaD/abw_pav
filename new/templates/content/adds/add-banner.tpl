<div class="product-item simple-item special"><a href="{$link}" target="_blank">
	<div class="simple-item-thumb">
		{if isset($thumb) && $thumb}
			<img src="{$thumb}" alt="">
		{else}
			{include file='content/no-photo.tpl'}
		{/if}
	</div>
</a></div>