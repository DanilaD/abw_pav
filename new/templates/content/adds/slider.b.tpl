{* 
размер -> ширина
(максимальная высота у каждой 500)
big - 1200 (для поп-апа)
large - 748 
medium - 620 
small - 480 
thumb - 100x66
*}
{assign 'isVideo' 1}
{assign 'video_id' 'Ecdo8RLan7w'}
<div class="js-product-slider product-slider{if $isVideo} product-slider-with-video{/if}" data-popup="true">
	<div class="owl-carousel" data-type="main">
		<div class="item-img"><span>1</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w, {$current_www}/images/slider-big.jpg 1200w">
		</div>
		<div class="item-img"><span>2</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w, {$current_www}/images/slider-big.jpg 1200w">
		</div>
		<div class="item-img"><span>3</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w, {$current_www}/images/slider-big.jpg 1200w">
		</div>
		<div class="item-img"><span>4</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w, {$current_www}/images/slider-big.jpg 1200w">
		</div>
		<div class="item-img"><span>5</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w">
		</div>
		<div class="item-img"><span>6</span>
			<img src="{$current_www}/images/slider-large.jpg" srcset="{$current_www}/images/slider-small.jpg 480w, {$current_www}/images/slider-medium.jpg 620w, {$current_www}/images/slider-large.jpg 748w, {$current_www}/images/slider-big.jpg 1200w">
		</div>
		<div class="item-video"><img src="{$current_www}/images/video-large.png" srcset="{$current_www}/images/video-small.jpg 480w, {$current_www}/images/video-medium.png 620w, {$current_www}/images/video-large.png 748w, {$current_www}/images/video-big.png 1200w"><a class="owl-video" href="https://www.youtube.com/watch?v={$video_id}"></a></div>
	</div>
	<div class="thumb-slider">
		<div class="thumb-slider-wrapper">
			<div class="owl-carousel" data-type="thumb">
				<div>1<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
				<div>2<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
				<div>3<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
				<div>4<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
				<div>5<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
				<div>6<img src="{$current_www}/images/slider-thumb.jpg" alt=""></div>
			</div>
		</div>
		{if $isVideo}
		<div class="thumb-video js-thumb-video"><span class="icon-play"></span><img class="thumb-video-img" src="https://img.youtube.com/vi/{$video_id}/default.jpg" alt=""></div>
		{/if}
		<div class="owl-prev js-thumb-nav" data-direction="prev"></div><div class="owl-next js-thumb-nav" data-direction="next"></div>
	</div>
</div>

