{if ($data.photos)}
<div id="js-product-slider" class="product-slider loading{if $data.youtube} product-slider-with-video{/if}" data-popup="true">
	<div class="product-slider-main-content{if $data.main.is_microlising} microleasing-activated{/if}">
		<div class="owl-carousel" data-type="main">      
	        {foreach from=$data.photos item=i}
	          {if isset($i.big)}
	            <div class="item-img">
	              <img class="owl-lazy" data-src="{$i.big}">
	            </div>
	          {/if}
	        {/foreach}  
	        {if $data.youtube}
	        	<div class="item-video"><img class="owl-lazy" data-src="{$current_www}/images/video-big.png" ><a class="owl-video" href="https://www.youtube.com/watch?v={$data.youtube}"></a></div>
	        {/if}    
	    </div>
	    {if $data.main.is_microlising}
	    	{include file='section/adds/microleasing.tpl'}
	    {/if}
	</div>
	<div class="thumb-slider">
		<div class="thumb-slider-wrapper">
			<div class="owl-carousel" data-type="thumb">              
                {foreach from=$data.photos item=i key=k}
                  {if isset($i.thumb) && $i.thumb}
                    <div class="item-thumb">
	                    <div class="item-thumb-inner" style="background-image: url('{$i.thumb}')">
	                    	<img src="{$current_www}/images/video-thumb.png">
	                    </div>
                    </div>
                  {else}
                  	<div class="item-thumb item-no-photo">
                  		<div class="item-thumb-inner">
	                  		<div class="no-photo"></div>
	                  		<img src="{$current_www}/images/video-thumb.png">
	                  	</div>
                  	</div>
                  {/if}
                {/foreach}              
			</div>
		</div>
		{if $data.youtube}
			<div class="thumb-video js-thumb-video">
				<div class="item-thumb-inner" style="background-image: url('https://img.youtube.com/vi/{$data.youtube}/default.jpg')">
					<span class="icon-play"></span>
					<img src="{$current_www}/images/video-thumb.png">
				</div>
			</div>
		{/if}
		<div class="owl-prev js-thumb-nav" data-direction="prev"></div><div class="owl-next js-thumb-nav" data-direction="next"></div>
	</div>
</div>
{else}
<div class="product-slider-no-photo">
	<div class="item-no-photo">
		<div class="no-photo"></div>
		<img src="{$current_www}/images/video-large.png" srcset="{$current_www}/images/video-small.jpg 480w, {$current_www}/images/video-medium.png 620w, {$current_www}/images/video-large.png 748w, {$current_www}/images/video-big.png 1200w">
	</div>
</div>
{/if}
