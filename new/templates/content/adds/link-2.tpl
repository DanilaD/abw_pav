{if (isset($car_links.car_links_under_photo))}
  <div class="super-links">
    {foreach from=$car_links.car_links_under_photo item=v}
      <a href="{$v.link}" target="{$v.target}" class="extra-links-item">{$v.name}</a>
    {/foreach}
  </div>
{/if}