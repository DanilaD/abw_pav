<div class="product-full clearfix js-product">
    <a target="_blank" href="https://www.abw.by/car/autohouse/{$comp.name}" class="main-link"></a>
    <div class="product-full-inner">
        <div class="ah-logo">
            <div class="ah-logo-thumb">
                <img src="{$comp.logo}" alt="{$comp.name}">
            </div>            
        </div>
            <div class="ah-full-content">
                <div class="ah-main-title">
                    {$comp.name}<span> {$comp.count}</span>
                </div>
                <div class="ah-main-line ah-main-info">                                                            
                    <p><span>Адрес</span>{$comp.address} <a href="#">Показать на карте</a></p>
                    <p><span>Телефон</span>{$comp.phone}</p>
                    <p><span>Сайт</span><a target="_blank" href="{$comp.site}">{$comp.site}</a></p>
                </div>
                <div class="ah-main-description">
                    <p>{$comp.description}</p>
                    <p>Юр информация: {$comp.name_ofic}</p>
                </div>
            </div>
            <div>{*$comp.is_car_order*}</div>
    </div>
</div>