{if isset($data.is_mail) && $data.is_mail}
    {assign 'is_mailed' 1}
{else}
    {assign 'is_mailed' 0}
{/if}
<div class="owner-data{if $is_mailed} is-mailed{/if}">
    {if $data.user.name || $data.user.place}
    <div class="owner-user">
        <div class="name">{$data.user.name}</div>
        <div class="place">{$data.user.place}</div>
    </div>{/if}<div class="owner-number">
        {if $is_mailed}
            <button class="btn-chat js-popup-with-form" href="#form-mail-owner" title="Написать продавцу"><span class="icon-chat"></span></button>
        {else}
            <div class="owner-info">Продавец отключил отправку писем</div>
        {/if}
        <div class="show-phone js-show-phone">
            <div class="mobile js-mobile">
                {$data.phone.country}.. <span data-type="1" data-adv_id="{$data.main.adv_id}">Показать номер</span>
            </div>
            <div class="user-buttons js-user-buttons">
                <button class="js-show-ajax-buttons">Пожаловаться</button>
                <button href="#complain-number" class="js-ajax-button js-popup-with-form" title="
                Если вы обнаружили, что в объявлении
указан неправильный номер телефона,
пожалуйста, сообщите администраторам.">Неправильный номер</button><button class="js-ajax-button js-popup-with-form" title="Если вы выяснили, что товар уже продан,
пожалуйста, сообщите администраторам." href="#complain-sale">Продано</button>
            </div>
        </div>
    </div>
</div>
<div id="form-mail-owner" class="white-popup-block mfp-hide">
    <div class="form-login">
        <form class="form-login-inner js-form-submit" method="POST" data-url="ajax_send_mes_user">
            <input type="hidden" name="adv_id" value="{$data.main.adv_id}">
            <div class="title">
                Написать продавцу<br>{$data.main.title}
                {if $data.user.name}
                <div class="subtitle">Продавец: {$data.user.name}</div>
                {/if}
            </div>
            <label for="phone">Телефон *</label>
            <input type="text" name="phone" id="phone" required>
            <label for="mail">E-mail *</label>
            <input type="mail" name="mail" id="mail" required>
            <label for="text">Сообщение *</label>
            <textarea name="text" id="text" required></textarea>
            <button type="submit">Отправить</button>
        </form>
    </div>
</div>
<div id="complain-number" class="white-popup-block mfp-hide">
    <div class="form-login">
        <form class="form-login-inner js-form-submit" method="POST" data-url="ajax_phone_send_mes">
            <input type="hidden" name="adv_id" value="{$data.main.adv_id}">
            <input type="hidden" name="type" value="1">
            <input type="hidden" name="mes" value="2">
            <input type="hidden" name="url" value="{$data.main.url}">
            <div class="title">Если Вы обнаружили, что в объявлении указанный номер телефона НЕВЕРНЫЙ, сообщите нам!</div>
            <button type="submit" data-sleepmode="Пожалуйста, подождите">Отправить</button>
        </form>
    </div>
</div>
<div id="complain-sale" class="white-popup-block mfp-hide">
    <div class="form-login">
        <form class="form-login-inner js-form-submit" method="POST" data-url="ajax_phone_send_mes">
            <input type="hidden" name="adv_id" value="{$data.main.adv_id}">
            <input type="hidden" name="type" value="1">
            <input type="hidden" name="mes" value="1">
            <input type="hidden" name="url" value="{$data.main.url}">
            <div class="title">Если Вы связались с продавцом и выяснили, что товар уже продан, сообщите нам!</div>
            <button type="submit" data-sleepmode="Пожалуйста, подождите">Отправить</button>
        </form>
    </div>
</div>