<div class="action-buttons">
	<button class="btn-border-light-grey first js-popup-tabs" data-popup="js-popup-offers" id="action-button-main" data-target="#tabs-item-2" type="button" data-adv_id="{$data.main.adv_id}" disabled><span class="icon-fire"></span>Продать быстрее</button>
	{*<div class="action-buttons-menu">
		<button class="btn-border-light-grey" type="button">Обогнать</button>
		<button class="btn-border-light-grey" type="button">Спецпредложения</button>
		<button class="btn-border-light-grey" type="button">Разместить в газете</button>	
	</div>*}
</div>
<div id="js-popup-offers" class="popup-offers white-popup-block mfp-hide" data-iu=""data-bets-iu="">
	<div class="js-custom-tabs" data-adv_id="{$data.main.adv_id}" data-user="{if isset($ses.user.user_id)}{$ses.user.user_id}{/if}">
		<ul class="tabs-nav" role="tabs-nav">
			<li data-target="#tabs-item-1" style="width: calc(100%/3)"><div class="title"><span class="icon-arrow-pink"></span>Обгон</div><div class="desc"><b>х3</b> просмотров</div></li>
			<li class="active" data-target="#tabs-item-2" style="width: calc(100%/3)"><div class="title"><span class="icon-fire"></span>Спедпредложение</div><div class="desc"><b>х10</b> просмотров</div></li>
			<li data-target="#tabs-item-3" style="width: calc(100%/3)"><div class="title"><span class="icon-newspaper-pink"></span>Разместить в газете</div><div class="desc"><b>15 000</b> экземпляров</div></li>
		</ul>
		<div class="tabs-content">
			<div class="tab-pane" id="tabs-item-1" data-type="1">
				<div class="title">Обогнать</div>
				<div class="js-desc desc t-center"></div>
				<div class="text-red"><span class="js-conversion-iu"></span> И.Е. - <span class="js-conversion-byn"></span> рублей</div>
				<div class="text-light">1 И.Е. (интернет-единица) = <span class="js-cost_ie"></span> рублей</div>
				<div class="columns buttons">
					<div><button class="btn-light-border-grey js-close-magnific-popup">Отказаться</button></div><div><button class="btn-blue js-send-special-offer">Поднять объявление</button></div>
				</div>
			</div>
			<div class="tab-pane active" id="tabs-item-2" data-type="2">
				<div class="title">Спедпредложение</div>
				<div class="js-desc desc"></div>
				<div class="columns">
					<div>
						<div class="js-bets"></div>
						<!-- <select name="bets" class="js-bets"></select> -->
					</div><div><span class="text-red"><span class="js-count-iu"></span> И.Е. - <span class="js-count-byn"></span> рублей</span></div>
				</div>
				<div class="text-light">
					<div>Стоимость 1 ставки = <span class="js-cost"></span> И.Е.</div>
					<div>1 И.Е. (интернет-единица) = <span class="js-cost_ie"></span> рублей</div>
				</div>
				<div class="columns buttons">
					<div><button class="btn-light-border-grey js-close-magnific-popup">Отказаться</button></div><div><button class="btn-blue js-send-special-offer">Поднять объявление</button></div>
				</div>
			</div>
			<div class="tab-pane" id="tabs-item-3" data-type="3" data-np_week="">
				<div class="title">Разместить в газете</div>
				<div class="js-desc desc"></div>
				<div class="columns custom-radio js-news-type"></div>
				<div class="select-inside"><div class="js-news-week"></div></div>
				<div class="text-light">1 И.Е. (интернет-единица) = <span class="js-cost_ie"></span> рублей</div>
				<div class="columns buttons">
					<div><button class="btn-light-border-grey js-close-magnific-popup">Отказаться</button></div><div><button class="btn-blue js-send-special-offer">Поднять объявление</button></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="action-buttons-menu" data-visibility-target="action-button-main">
	<button class="btn-border-pink" type="button" data-popup="js-popup-offers" data-target="#tabs-item-1" disabled><span class="icon-arrow-pink"></span></button>
	<button class="btn-border-pink" type="button" data-popup="js-popup-offers" data-target="#tabs-item-2" disabled><span class="icon-fire"></button>
	<button class="btn-border-pink" type="button" data-popup="js-popup-offers" data-target="#tabs-item-3" disabled><span class="icon-newspaper-pink"></span></button>	
</div>