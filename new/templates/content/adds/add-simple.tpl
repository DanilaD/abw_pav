<div class="product-item simple-item{if !isset($car.photo) || !$car.photo} item-no-photo{/if}"><a href="{$car.link}">
	<div class="simple-item-thumb">
		{if isset($car.photo) && $car.photo}
			<img src="{$car.photo}" alt="">
		{else}
			{include file='content/no-photo.tpl'}
			<img src="{$current_www}/images/no-photo-product.png" alt="">
		{/if}
	</div>
	<div class="simple-item-content">
		<div class="simple-item-content-inner">
			{if $bets_amount}
			<div class="label label-special-offer"><span class="icon icon-fire-white"></span><span>{$bets_amount}</span></div>
			{elseif $car.last_time_up}
			<div class="label label-special-overtaking"><span class="icon icon-arrow-up"></span><span>{$car.last_time_up}</span></div>
			{/if}
			<div class="data">
				<div class="title">{$car.title}</div>
				<div class="data-price">
					{if isset($car.byn) && $car.byn}
						<span class="data-price-byn">{$car.byn}</span>
					{/if}
					{if isset($car.usd) && $car.usd}
						<span class="data-price-usd">{$car.usd}$</span>
					{/if}
				</div>
				{if isset($car_data) && $car_data}
				<div class="data-inner">
					<span class="data-item">{'</span><span class="data-item">'|implode:$car_data}</span>
					{if isset($car.show) && $car.show}
						<span class="data-item views">{$car.show}</span>
					{/if}
				</div>
				{/if}
			</div>
		</div>
	</div>
</a></div>