<div class="product-full clearfix js-product {$car.adds_type}{if !isset($car.thumb) || !$car.thumb} item-no-photo{/if}{if isset($viewed)} item-with-checkbox{/if}" set-hide="{$car.hide_button}">
	{if isset($viewed)}
		<input type="checkbox" value="{$car.adv_id}" name="favorites-item" class="favorites-checkbox">
	{/if}
	<div class="product-full-inner{if (isset($car.adds_type) && $car.adds_type == 'site-highlight') && (isset($car.thumb1) && $car.thumb1 || isset($car.thumb2) && $car.thumb2)} product-thumb-big{/if}"><a target="_blank" href="{$car.link}" class="main-link"></a>
		<div class="control-buttons">
		{if $car.adds_type == 'site-highlight'}
			<button class="control-button vip" title=""><span class="icon icon-vip">VIP</span></button>
			<div class="control-button-tip js-button-tip">
				<div class="control-button-tip-inner">
					<div class="tip-title">Это приоритетное объявление</div>
					<div>Оно увеличивает количество просмотров и тем самым повышает шансы продать авто быстрее.</div>
					<a href="https://www.abw.by/index.php?act=public_edit_list" class="btn-yellow action">Применить к своему объявлению</a>
				</div>
			</div>
		{elseif $car.adds_type == 'newspaper-highlight'}
			<button class="control-button newspaper" title=""><span class="icon icon-newspaper"></span></button>
			<div class="control-button-tip js-button-tip">
				<div class="control-button-tip-inner">
					<div class="tip-title">Это объявление размещено в газете “Автобизнес”</div>
					<div>За счет увеличения аудитории повышает шансы продать авто быстрее.</div>
					<a href="https://www.abw.by/index.php?act=public_edit_list" class="btn-yellow action">Применить к своему объявлению</a>
				</div>
			</div>
		{/if}
			<button class="control-button favourites js-favourites {$car.favourite}" data-adv_id="{$car.adv_id}" data-type="1" title="Добавить в избранное"><span class="icon icon-favourites"></span></button>
			<button data-adv_id="{$car.adv_id}" data-type="1" data-action="2" class="control-button hide js-toggle-product-visibility" title="Скрыть объявление" hide-title="Показать объявление"><span class="icon icon-hide"></span></button>
		</div>
		<div class="product-full-content">
			<div class="data-first">
				<div class="title">{$car.title}</div>
				{if ($car.location || $car.autohouse)}
				<div class="location-data">
					{if ($car.autohouse)}
						<div class="autohouse">
							{if ( $car.autohouse.link )}<a targe="_blank" href="{$car.autohouse.link}">
								{if isset($car.autohouse.brend) && $car.autohouse.brend}
									<img src="{$car.autohouse.brend}" alt="{$car.autohouse.title}">
								{else}
									{$car.autohouse.title}
								{/if}
							</a>{/if}
						</div>
					{/if}
					{if ( $car.location )}
						<div class="location">
							{$car.location}
						</div>
					{/if}
				</div>
				{/if}
				{if $car.data}
                  <div class="description"><span class="data-item">{$car.data}</span></div>
				{/if}
			</div><div class="data-second clearfix">
				<div class="product-thumb">
					{if isset($car.thumb) && $car.thumb}
						<img src="{$car.thumb}" alt="">

						{if isset($car.adds_type) && $car.adds_type == 'site-highlight'}
						<div class="thumbs-block">
							{if isset($car.thumb1) && $car.thumb1}
							<div class="thumb"><img src="{$car.thumb1}" alt=""></div>
							{/if}
							{if isset($car.thumb2) && $car.thumb2}
							<div class="thumb"><img src="{$car.thumb2}" alt=""></div>
							{/if}
						</div>
						{/if}
					{else}
						{include file='content/no-photo.tpl'}
					{/if}
					{if isset($car.autohouse.is_car_order) && $car.autohouse.is_car_order}
						<div class="auto-onorder">Авто под заказ</div>
					{elseif $car.condition}
						<div class="auto-onorder">{$car.condition}</div>
					{/if}
				</div><div class="data-second-item data-price">
					{if isset($car.price_byn)}
						<div class="data-price-byn">
							{if isset($car.price_old)}<span title="Цена была снижена" class="icon icon-return-pink"></span>{/if}
							{if $car.adds_type == 'price-selected'}
								<span>
									<span>{$car.price_byn}</span>
									{if $car.adds_type == 'price-selected'}
									<div class="control-button-tip js-button-tip">
										<div class="control-button-tip-inner">
											<div class="tip-title">Это платное объявление</div>
											<div>За счет выделения цены, повышает интерес покупателя.</div>
											<a href="https://www.abw.by/index.php?act=public_edit_list" class="btn-yellow action">Применить к своему объявлению</a>
										</div>
									</div>
									{/if}
								</span>
							{else}
								{$car.price_byn}
							{/if}
						</div>
					{/if}
					{if isset($car.price_usd)}
						<div class="data-price-usd">{$car.price_usd}$</div>
					{/if}
				</div>{*
				*}<div class="data-second-item data-year"><div>{if ( $car.year )}{$car.year}{/if}</div><div>{if ( $car.mileage )}{$car.mileage}{/if}</div></div>{*
				*}{if isset($car.time)}
					<div class="data-time{if $car.overtaking == '1'} is-overtaking{/if}">
						<span class="icon icon-arrow-up-grey"></span><span>{$car.time}</span>
					</div>
				{/if}
			</div>
            {if $car.excerpt}<div class="excerpt">{$car.excerpt}</div>{/if}
		</div>
	</div>
</div>
