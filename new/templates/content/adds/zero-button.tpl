{if isset($ses.status) && ($ses.status == 4 || $ses.status == 1)}
<div class="zero-btn"><button rel="nofollow" class="btn-light-blue-underline js-set-to-zero" data-adv_id="{$data.main.adv_id}" data-user="{if isset($ses.user.user_id)}{$ses.user.user_id}{/if}" disabled>Обнулить</button><span class="icon-round" title="Обнуление позволяет сбросить количество просмотров">?</span></div>
{/if}
