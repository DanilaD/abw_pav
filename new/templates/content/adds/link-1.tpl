{if (isset($car_links.car_links_top))}
  <div class="super-links">
    {foreach from=$car_links.car_links_top item=v}
      <a href="{$v.link}" class="extra-links-item" target="{$v.target}">{$v.name}</a>
    {/foreach}
  </div>
{/if}
