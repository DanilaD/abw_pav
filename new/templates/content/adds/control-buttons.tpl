<!--<button href="" class="control-button rise-up"><span class="icon icon-arrow-blue"></span></button> -->
<button class="control-button ban js-popup-with-form" href="#form-mail-admin" title="Письмо администратору"><span class="icon icon-ban"></span></button>
<button class="control-button favourites js-favourites" data-adv_id="{$data.main.adv_id}" data-type="1" title="Добавить в избранное" disabled><span class="icon icon-favourites"></span></button>
<div id="form-mail-admin" class="white-popup-block mfp-hide">
    <div class="form-login">
        <form class="form-login-inner js-form-submit" method="POST" data-url="ajax_send_mes_admin">
            <input type="hidden" name="adv_id" value="{$data.main.adv_id}">
            <div class="title">Написать письмо администратору</div>
            <label for="phone">Телефон *</label>
            <input type="text" name="phone" id="phone" required>
            <label for="mail">E-mail *</label>
            <input type="email" name="mail" id="mail" required>
            <label for="text">Сообщение *</label>
            <textarea name="text" id="text" required></textarea>
            <button type="submit" data-sleepmode="Пожалуйста, подождите">Отправить</button>
        </form>
    </div>
</div>