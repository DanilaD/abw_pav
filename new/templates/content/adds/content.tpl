{if isset($data.options) && $data.options|count}
<div class="product-options">
	<div class="title">Дополнительные опции</div>
	<ul>
		{foreach from=$data.options item=v}
		 <li>{$v.name}</li>
		{/foreach}
	</ul>
</div>
{/if}
{include file='content/adds/link-3.tpl'}
<div class="product-desc">{$data.main.text}</div>
{if (isset($car_links.car_links_bot))}
  <div class="super-links">
    {foreach from=$car_links.car_links_bot item=v}
      <a href="{$v.link}" class="extra-links-item" target="{$v.target}">{$v.name}</a>
    {/foreach}
  </div>
{/if}
<div class="product-buttons clearfix">
	<div class="social-block">{include file="section/social-likes.tpl"}</div>
	{if isset($data.subscription.link)}<a class="btn-border-grey" href="{$data.subscription.link}">{$data.subscription.text}</a>{/if}
</div>
