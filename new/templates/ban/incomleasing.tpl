<div id="incom-container" class="incom-container">
	<div class="incom-container-inner">
		<div class="title">Купи этот автомобиль в лизинг</div>
		<div class="grey">Сумма недостающих средств</div>
		<div class="select-block">
			<select id="incom-sum">
				<option value="3 000" data-old="30 000 000" data-p="159">3 000</option>
				<option value="4 000" data-old="40 000 000" data-p="212">4 000</option>
				<option value="5 000" data-old="50 000 000" data-p="265">5 000</option>
				<option value="6 000" data-old="60 000 000" data-p="318">6 000</option>
				<option value="7 000" data-old="70 000 000" data-p="371">7 000</option>
				<option value="8 000" data-old="80 000 000" data-p="424">8 000</option>
				<option value="9 000" data-old="90 000 000" data-p="477">9 000</option>
				<option value="10 000" data-old="100 000 000" data-p="530">10 000</option>
				<option value="11 000" data-old="110 000 000" data-p="583">11 000</option>
				<option value="12 000" data-old="120 000 000" data-p="636">12 000</option>
			</select>
			<span>бел. руб.</span>
		</div>
		<div class="green">Ваш платеж в месяц от <span id="incom-payment">159</span> бел. руб.</div>
	</div>
	<button id="incom-show-form-btn" class="action-btn" href="">Отправить заявку</button>
</div>