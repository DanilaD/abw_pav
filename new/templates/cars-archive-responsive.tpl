{*archive-product -> class for body*}
<div class="archive-products clearfix">
  {include file='section/breadcrumbs.tpl'}
  {if $mobile.is_true_mobile}
    {include file='section/adds-archive/adds-archive-mobile.tpl'}
  {else}
    {include file='section/adds-archive/adds-archive-dekstop.tpl'}
  {/if}
</div>
{include file='section/stock.tpl'}
{include file='section/adds/news-list.tpl'}
