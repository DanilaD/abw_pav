{if isset($data.show)}
  {include file='section/car_page.tpl'}
{elseif isset($data.error)}
  {include file='section/car_page_error.tpl'}
{elseif isset($data.viewed)}
  {include file='section/car_page_viewed.tpl'}
{else}
  {include file='cars-archive.tpl'}  
{/if}