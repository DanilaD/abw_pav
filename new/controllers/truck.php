<?
  class Truck {
      
    private $header = '<script src="https://yastatic.net/pcode/adfox/loader.js"></script>';
    private $footer = '';          
    
    public function __construct($smarty,$_db) {
      $this->smarty = $smarty;
      $this->db = $_db; 

    }
    
    public function main($smarty, $_db, $url = '', $arg = '') {
      // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Truck($this->db);

      // search
        $data = $car_class->SearchTruck();

      // pagination    
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);

      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);

      // get advs
        $cars = $this->GetAllTrucks($data,$arg);
        $smarty->assign('cars',$cars);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'abw.by',
          'text' => 'Поиск грузовиков',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Поиск по объвлениям грузовых и коммерческих автомобилей на abw.by',
            'desc'  => 'Объявление на сайте abw.by ',
            'keyw'  => 'abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);

      // links (text advertsment)
        $links = $this->GetCarListLinks();
        $smarty->assign('car_links',$links);   
    }
      
      
    public function sell($smarty, $_db, $url = '', $arg = '') {
      //id adv
      if (isset($url['2']) && isset($url['3']) && isset($url['4'])){
        $this->ShowSellCar($url);
      // display model
      }elseif (isset($url['2']) && isset($url['3'])){
        $this->ShowSellModel($url['2'], $url['3'],$arg,'');
      // display marka
      }elseif (isset($url['2'])){
        $this->ShowSellMarka($url['2'],$arg,'');
      // display sell
      }elseif (isset($url['1'])){
        $this->ShowSellAll($arg);
      }else{
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        header('Location: '.STATIC_WWW.'/new/error/');
        die();
      }
    }
    
    public function buy($smarty, $_db, $url = '', $arg = '') {
      
    }
    
    
    private function ShowSellCar($arg) {
      // class
        $class_adv = NEW Adv($this->db);
      
      // type for body
        $this->smarty->assign('body_classes','single-product-page');

      // check data
        
      // get data
        $data = $class_adv->GetAdv($arg);
        $this->smarty->assign('data',$data);

      // viewed cars
        $arg =[
          'ViewedTruck' =>['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY]
        ];  
        $ar = filter_input_array(INPUT_COOKIE,$arg);
        
        //$viewed = $this->getViewedCar($ar['ViewedTruck'], $data['main']['adv_id'], $data['count_v']);
        //$this->smarty->assign('viewed',$viewed);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'abw.by',
          'link2' => STATIC_WWW.'/truck/sell/',
          'name2' => 'Объявления',
          'link3' => STATIC_WWW.'/truck/sell/'.$data['main']['marka_url'].'/',
          'name3' => $data['main']['marka'],
          'link4' => STATIC_WWW.'/truck/sell/'.$data['main']['marka_url'].'/'.$data['main']['model_url'].'/',
          'name4' => $data['main']['model'],
          'text' => $data['main']['adv_id'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);

      // isProductOwner
        $isProductOwner = (isset($_SESSION['user']['login']) && (in_array($_SESSION['status'], [1,4]) || $_SESSION['user']['login'] == $data['main']['user'])) ? TRUE : FALSE;
        $this->smarty->assign('isProductOwner',$isProductOwner);
        
      // seo
        $seo['seo'] = [
            'title' => $data['main']['title'].' №'.$data['main']['adv_id'],
            'desc'  => 'Объявление на сайте abw.by - '.$data['main']['title'].' '.$data['main']['description'].'',
            'keyw'  => $data['main']['marka'].', '.$data['main']['model'].', '.$data['main']['year'].', abw.by, объявление, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = "<script charset='Utf-8' src='https://www.mtbank.by/credit-calculator3.js?abw.by'></script>";
        $this->smarty->assign('seo',$seo);

      // links
        $car_links = $class_adv->GetLinks();
        $this->smarty->assign('car_links',$car_links);   
    }    
    
  }