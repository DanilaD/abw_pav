<?

class Allpublic {
    
    public function __construct($smarty,$_db) {
      // seo
        $seo = $this->getSeo();
        $smarty->assign('seo',$seo); 
        
      // type for body
        $smarty->assign('body_classes','car');
        
    }
    
    
    public function sell($smarty, $_db, $arg = false) {
      //id adv
      if (isset($arg['2'])){
        $id = filter_var($arg['2'], FILTER_SANITIZE_STRING);
        // get data on id        
        $car_class = NEW Cars($_db);
        // get main data
        $url = $car_class->GetUrlCar($id);
        if (!empty($url)){
          header($_SERVER['SERVER_PROTOCOL'].' 301 Moved Permanently');
          header('Location: '.$url); 
        }else{
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
      }  
    }
    
    public function buy($smarty, $_db, $arg = false) {
      //id adv
      if (isset($arg['2'])){
        $id = filter_var($arg['2'], FILTER_SANITIZE_STRING);
        // get data on id        
        $car_class = NEW Cars($_db);
        // get main data
        $car = $car_class->GetUrlCar($id);
        if (!empty($url)){
          header($_SERVER['SERVER_PROTOCOL'].' 301 Moved Permanently'); 
          header('Location: '.$url); 
        }else{
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
      }  
    }
    
    private function getSeo(){
      $seo = [
        'title' => 'Объявление на ABW.BY',
        'desc'  => 'Описание страницы объявления',
        'keyw'  => 'Ключевые слова страницы объявления'
      ];
      $seo['social'] = [
        'title' => $seo['title'],
        'desc'  => $seo['desc'],
        'keyw'  => $seo['keyw'],
        'img' => ''
      ];
      return $seo;
    }
}