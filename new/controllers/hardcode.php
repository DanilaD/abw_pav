<?php
  class hardcode {
    
   public function __construct($smarty,$_db) {
    // breadcrumbs
        $breadcrumbs = [
          'link1' => '',
          'name1' => 'линк1',
          'link2' => '',
          'name2' => 'линк2',
          'link3' => '',
          'name3' => 'линк3',
          'text' => 'текст',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);
        
      // for banners in car & news
        $file_banners = ROOT_DIR.'/array/banners_main.php';
        file_exists($file_banners) ? require($file_banners) : '';
        $this->banners_news = $banners_news;
        $this->banners_car = $banners_car;
        
    // last advs
      $cars = $this->GetLastPaidAdv($_db);
      $smarty->assign('cars',$cars);
        
   }
   
    private function GetLastPaidAdv($_db){
      $car = new Cars($_db);  
      $cars = $car->GetLastPaidAdv($_db);
      
      // add vip car
      $cars = (!empty($this->vip_car) && count($this->vip_car) > 0) ? array_merge($this->vip_car,$cars) : $cars;
      
      foreach ($cars as $k => &$v)
      {
        // title
        $v['title'] = $v['marka'].' '.$v['model'].' '.$v['version'];
        // car mileage
        $pr =['probeg_col' => $v['probeg_col'], 'probeg_type' => $v['probeg_type']];
        $v['probeg'] = $car->CarProbeg($pr);
        // engine capacity
        $v['volume'] = $car->CarVolumeEngine($v['volume']).' '.$v['engine'];
        // show
        $v['show'] = isset($v['show']) ? $v['show'] : '';
        // price
        $cost_list = $car->multiPrice($v['cost_val'], $v['cost_type']);  
        foreach ($cost_list as $k_curr => $curr) {
          if ($k_curr == 'byrn') $v['byn'] = $curr['cost'].' руб.';
          if ($k_curr == 'usd') $v['usd'] = '~ '.$curr['cost'];
        } 
        // link
        $v['link'] = STATIC_WWW.'/allpublic/sell/'.$v['adv_id'].'/';
        // photo
        $path = PHOTO_AUTO_CASH.$v['adv_id'].'_1_650.jpg';
        resize_img('300','200',DIR.PHOTO_AUTO.$v['adv_id'].'_1_650.jpg',DIR.$path);
        $v['photo'] = (file_exists(DIR.$path)) ? STATIC_WWW.$path : '';        
      }
            
      // add banners      
      if ((count($this->banners_car)) > 0){
        foreach ($this->banners_car as $value){
          $cars_new = array_slice ($cars, $value['pos']);
          array_unshift ($cars_new,$value);
          array_splice ($cars, $value['pos'],  count($cars), $cars_new);
        }
      } 
      
      // check for limit
      $cars = array_slice ($cars, 0, 12);
      
      return $cars;
    }
    
  }
