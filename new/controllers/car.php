<?
ini_set('display_errors', false);
  class Car {
      
    private $header = '<script src="https://yastatic.net/pcode/adfox/loader.js"></script>';
    private $footer = '';     
    
    public function __construct($smarty,$_db) {
      $this->smarty = $smarty;
      $this->db = $_db; 

    }
    
    public function main($smarty, $_db, $url = '', $arg = '') {
      
      // redirect
        header($_SERVER['SERVER_PROTOCOL'].' 301 Moved Permanently');
        header('Location: '.STATIC_WWW.'/car/sell/');
        die();
      // REMOVE THIS
      
      // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Cars($this->db);

      // search
        $data = $car_class->SearchCar();

      // pagination    
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);

      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);

      // get advs
        $cars = $this->GetAllCars($data,$arg);
        $smarty->assign('cars',$cars);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'abw.by',
          'text' => 'Поиск автомобилей',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Поиск по объвлениям на abw.by',
            'desc'  => 'Объявление на сайте abw.by ',
            'keyw'  => 'abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $smarty->assign('car_links',$car_links);   
    }
      
      
      public function sell($smarty, $_db, $url = '', $arg = '') {
        
      //id adv
      if (isset($url['2']) && isset($url['3']) && isset($url['4'])){
        $this->ShowSellCar($url);
      // display model
      }elseif (isset($url['2']) && isset($url['3'])){
        $this->ShowSellModel($url['2'], $url['3'],$arg,'');
      // display marka
      }elseif (isset($url['2'])){
        $this->ShowSellMarka($url['2'],$arg,'');
      // display sell
      }elseif (isset($url['1'])){
        $this->ShowSellAll($arg);
      }else{
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        header('Location: '.STATIC_WWW.'/new/error/');
        die();
      }
    }
    
    public function buy($smarty, $_db, $url = '', $arg = '') {
        
      //id adv
      if (isset($url['2']) && isset($url['3']) && isset($url['4'])){
        $this->ShowBuyCar($url);
      // display model
      }elseif (isset($url['2']) && isset($url['3'])){
        $this->ShowBuyModel($url['2'], $url['3'],$arg,'');
      // display marka
      }elseif (isset($url['2'])){
        $this->ShowBuyMarka($url['2'],$arg,'');
      // display sell
      }elseif (isset($url['1'])){
        $this->ShowBuyAll($arg);
      }else{
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        header('Location: '.STATIC_WWW.'/new/error/');
        die();
      }
    }
    
    private function ShowSellCar($arg) {
      // type for body
        $this->smarty->assign('body_classes','single-product-page');
        
      // sales
        //$sales = $this->GetAllSales();

      // news
        //$news_all = $this->getLastNews($this->db);

      // display
        //$this->smarty->assign('sales',$sales);          
        //$this->smarty->assign('news_all',$news_all);

      // get data
        $data = $this->getDataCar($this->db,$arg);
        $this->smarty->assign('data',$data);
          $this->smarty->assign('cnt',$data['count_s']);
      // similar cars
        $similar = $this->getSimilarCar($this->db,['model' => $data['main']['model_id'],'adv_id' => $data['main']['adv_id'], 'count' => $data['count_s']]);
        $this->smarty->assign('similar',$similar);

      // viewed cars
        $arg =[
          'ViewedCar' =>['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY]
        ];  
        $ar = filter_input_array(INPUT_COOKIE,$arg);
        
        $viewed = $this->getViewedCar($ar['ViewedCar'], $data['main']['adv_id'], $data['count_v'], $arg); 

        $this->smarty->assign('viewed',$viewed);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW.'/car/sell/',
          'name1' => 'Объявления',
          'link2' => STATIC_WWW.'/car/sell/',
          'name2' => 'Продажа',
          'link3' => STATIC_WWW.'/car/sell/'.$data['main']['marka_url'].'/',
          'name3' => $data['main']['marka'],
          'link4' => STATIC_WWW.'/car/sell/'.$data['main']['marka_url'].'/'.$data['main']['model_url'].'/',
          'name4' => $data['main']['model'],
          'text' => $data['main']['adv_id'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);

      // isProductOwner
        $isProductOwner = (isset($_SESSION['user']['login']) && (in_array($_SESSION['status'], [1,4]) || $_SESSION['user']['login'] == $data['main']['user'])) ? TRUE : FALSE;
        $this->smarty->assign('isProductOwner',$isProductOwner);
        
      // seo
        $seo['seo'] = [
            'title' => $data['main']['title'].' №'.$data['main']['adv_id'].' - продажа авто на abw.by',
            'desc'  => 'Объявление на сайте abw.by - '.$data['main']['title'].' '.$data['main']['description'].'',
            'keyw'  => $data['main']['marka'].', '.$data['main']['model'].', '.$data['main']['year'].', abw.by, объявление, покупка, продажа',
        ];

        // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

        // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = "<script charset='Utf-8' src='https://www.mtbank.by/credit-calculator3.js?abw.by'></script>";

        $this->smarty->assign('seo',$seo);

        // links
        $car_links = $this->GetCarLinks();
        $this->smarty->assign('car_links',$car_links);   
    }    
    
    private function ShowSellModel($marka,$model,$arg,$data){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // get all models
        $car_class = new Cars($this->db); 
        $model_get = $car_class->GetMarkaModelID($marka,$model);
        
      // error
        if (empty($model_get)){
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
        
      // search
        $in['type'] = '1';
        $data = $car_class->SearchCar($in);
        
      // сформировать данные
        $data['type'] = 'model';
        $data['marka'] = $model_get['0']['marka_id']; 
        $data['model'] = $model_get['0']['model_id'];
        $data['adv_type'] = '1';  
        
      // pagination    
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // add for search  
        $_SESSION['search']['car']['type'] = '1';
        $_SESSION['search']['car']['marka'] = $model_get['0']['marka_id'];
        $_SESSION['search']['car']['model'][] = $_SESSION['search']['car']['model_str'] = $model_get['0']['model_id'];
        
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);
      
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
      
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'link3' => STATIC_WWW.'/car/sell/',
          'name3' => 'Продажа',
          'link4' => STATIC_WWW.'/car/sell/'.$marka.'/',
          'name4' => $model_get['0']['marka'],
          'text' => $model_get['0']['model'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
        
      // seo
        $seo['seo'] = [
            'title' => $model_get['0']['marka'].' '.$model_get['0']['model'].' - продажа - все объявления на abw.by',
            'desc'  => 'Объявления по продаже автомобилей марки '.$marka.' на сайте abw.by ',
            'keyw'  => $model_get['0']['marka'].' '.$model_get['0']['model'].', abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }

    private function ShowSellMarka($marka,$arg,$data){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // get all models
        $car_class = new Cars($this->db); 
        $marka_get = $car_class->GetMarkaID($marka);
      
      // error
        if (empty($marka_get)){
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
        
        // search
        $in['type'] = '1';
        $data = $car_class->SearchCar($in);
      
      // сформировать данные
        $data['type'] = 'marka';
        $data['marka'] = $marka_get['marka_id']; 
        $data['adv_type'] = '1';  
              
      // pagination
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);
        
      // add for search  
        $_SESSION['search']['car']['type'] = '1';
        $_SESSION['search']['car']['marka'] = $marka_get['marka_id'];
      
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
      
      // all models  
        $models_count['models'] =  isset($marka_get['marka_id']) ? $car_class->GetCountModelID($marka_get['marka_id'], '1') : '';
        foreach ($models_count['models'] as $key => $value) {
          $models_count['models'][$key]['link'] =  $car_class->GetLinksModel('1',$marka,$value['name']);   
        }
        $models_count['marka'] = isset($marka_get['name_eng']) ? $marka_get['name_eng'] : '';   
        $this->smarty->assign('models_count',$models_count);

        
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'link3' => STATIC_WWW.'/car/sell/',
          'name3' => 'Продажа',
          'text' => $marka_get['name_eng'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
      
      // seo
        $seo['seo'] = [
            'title' => $marka_get['name_eng'].' - продажа - все объявления на abw.by',
            'desc'  => 'Объявления по продаже автомобилей марки '.$marka.' на сайте abw.by ',
            'keyw'  => $marka_get['name_eng'].', abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }
    
     private function ShowSellAll($arg){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // connect to class
        $car_class = new Cars($this->db);
      
      // search
        $in['type'] = '1';
        $data = $car_class->SearchCar($in);
        
      // for search sell
        $_SESSION['search']['car']['type'] = '1';
        
      // pagination        
        $pages = $car_class->Pagination($arg,$data);
        
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // сформировать данные
        $data['type'] = 'all'; 
        $data['adv_type'] = '1';  
            
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'abw.by',
          'link2' => STATIC_WWW.'/car/sell/',
          'name2' => 'Объявления',
          'text' => 'Продажа',
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
        
      // seo
        $seo['seo'] = [
          'title' => 'Продажа авто на abw.by',
          'desc'  => 'Поиск объявлений по продаже автомобилей, микроавтобусов от частных лиц и компаний по базе объявлений белорусского сайта abw.by',
          'keyw'  => 'abw.by, объявления, поиск, продажа',
        ];

      // for social links
        $seo['social'] = [
          'title' => $seo['seo']['title'],
          'desc'  => $seo['seo']['desc'],
          'keyw'  => $seo['seo']['keyw'],
          'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }
    
     private function ShowBuyCar($arg) {
      // type for body
        $this->smarty->assign('body_classes','single-product-page');
        
      // sales
        //$sales = $this->GetAllSales();

      // news
        //$news_all = $this->getLastNews($this->db);

      // display
        //$this->smarty->assign('sales',$sales);          
        //$this->smarty->assign('news_all',$news_all);

      // get data
        $data = $this->getDataCar($this->db,$arg);
        $this->smarty->assign('data',$data);

      // similar cars
        $similar = $this->getSimilarCar($this->db,['model' => $data['main']['model_id'],'adv_id' => $data['main']['adv_id'], 'count' => $data['count_s']]);
        $this->smarty->assign('similar',$similar);

      // viewed cars
        $arg =[
          'ViewedCar' =>['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY]
        ];  
        $ar = filter_input_array(INPUT_COOKIE,$arg);
        
        $viewed = $this->getViewedCar($ar['ViewedCar'], $data['main']['adv_id'], $data['count_v'], $arg);
        $this->smarty->assign('viewed',$viewed);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW.'/car/',
          'name1' => 'Объявления',
          'link2' => STATIC_WWW.'/car/buy/',
          'name2' => 'Покупка',
          'link3' => STATIC_WWW.'/car/buy/'.$data['main']['marka_url'].'/',
          'name3' => $data['main']['marka'],
          'text' => $data['main']['adv_id'],
          ];
        
        // проверка, есть ли модель
        if ($data['main']['model']){
          $breadcrumbs['link4'] = STATIC_WWW.'/car/buy/'.$data['main']['marka_url'].'/'.$data['main']['model_url'].'/';
          $breadcrumbs['name4'] = $data['main']['model'];
        }
            
        $this->smarty->assign('breadcrumbs',$breadcrumbs);

      // isProductOwner
        $isProductOwner = (isset($_SESSION['user']['login']) && (in_array($_SESSION['status'], [1,4]) || $_SESSION['user']['login'] == $data['main']['user'])) ? TRUE : FALSE;
        $this->smarty->assign('isProductOwner',$isProductOwner);
        
      // seo
        $seo['seo'] = [
            'title' => $data['main']['title'].' №'.$data['main']['adv_id'].' - покупка авто на abw.by',
            'desc'  => 'Объявление на сайте abw.by - '.$data['main']['title'].' '.$data['main']['description'].'',
            'keyw'  => $data['main']['marka'].', '.$data['main']['model'].', '.$data['main']['year'].', abw.by, объявление, покупка, продажа',
        ];

        // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

        // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = "<script charset='Utf-8' src='https://www.mtbank.by/credit-calculator3.js?abw.by'></script>";

        $this->smarty->assign('seo',$seo);

        // links
        $car_links = $this->GetCarLinks();
        $this->smarty->assign('car_links',$car_links);   
    }    
    
    private function ShowBuyModel($marka,$model,$arg,$data){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // get all models
        $car_class = new Cars($this->db); 
        $model_get = $car_class->GetMarkaModelID($marka,$model);
        
      // error
        if (empty($model_get)){
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
        
      // search
        $in['type'] = '2';
        $data = $car_class->SearchCar($in);
        
      // сформировать данные
        $data['type'] = 'model';
        $data['marka'] = $model_get['0']['marka_id']; 
        $data['model'] = $model_get['0']['model_id'];
        $data['adv_type'] = '2';  
        
      // pagination    
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // add for search  
        $_SESSION['search']['car']['type'] = '2';
        $_SESSION['search']['car']['marka'] = $model_get['0']['marka_id']; 
        $_SESSION['search']['car']['model'][] = $_SESSION['search']['car']['model_str'] = $model_get['0']['model_id'];
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);
      
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
      
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'link3' => STATIC_WWW.'/car/buy/',
          'name3' => 'Покупка',
          'link4' => STATIC_WWW.'/car/buy/'.$marka.'/',
          'name4' => $model_get['0']['marka'],
          'text' => $model_get['0']['model'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
        
      // seo
        $seo['seo'] = [
            'title' => $model_get['0']['marka'].' '.$model_get['0']['model'].' - покупка - все объявления на abw.by',
            'desc'  => 'Объявления по покупке автомобилей марки '.$marka.' на сайте abw.by ',
            'keyw'  => $model_get['0']['marka'].' '.$model_get['0']['model'].', abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }

    private function ShowBuyMarka($marka,$arg,$data){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // get all models
        $car_class = new Cars($this->db); 
        $marka_get = $car_class->GetMarkaID($marka);
      
      // error
        if (empty($marka_get)){
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
        
        // search
        $in['type'] = '2';
        $data = $car_class->SearchCar($in);
      
      // сформировать данные
        $data['type'] = 'marka';
        $data['marka'] = $marka_get['marka_id']; 
        $data['adv_type'] = '2';  
              
      // pagination
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // models and count
        $models_count['models'] =  isset($marka_get['marka_id']) ? $car_class->GetCountModelID($marka_get['marka_id'], '2') : '';
        foreach ($models_count['models'] as $key => $value) {
          $models_count['models'][$key]['link'] =  $car_class->GetLinksModel('2',$marka,$value['name']);   
        }
        $models_count['marka'] = isset($marka_get['name_eng']) ? $marka_get['name_eng'] : '';   
        $this->smarty->assign('models_count_buy',$models_count);
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);
        
      // add for search  
        $_SESSION['search']['car']['type'] = '2';
        $_SESSION['search']['car']['marka'] = $marka_get['marka_id'];
      
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
              
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'link3' => STATIC_WWW.'/car/buy/',
          'name3' => 'Покупка',
          'text' => $marka_get['name_eng'],
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
      
      // seo
        $seo['seo'] = [
            'title' => $marka_get['name_eng'].' - покупка - все объявления на abw.by',
            'desc'  => 'Объявления по покупке автомобилей марки '.$marka.' на сайте abw.by ',
            'keyw'  => $marka_get['name_eng'].', abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }
    
     private function ShowBuyAll($arg){
      // type for body
        $this->smarty->assign('body_classes','archive-products');
        
      // connect to class
        $car_class = new Cars($this->db);
      
      // search
        $in['type'] = '2';
        $data = $car_class->SearchCar($in);
        
      // сформировать данные
        $data['type'] = 'all'; 
        $data['adv_type'] = '2';  
        
      // for search buy
        $_SESSION['search']['car']['type'] = '2';
        
      // pagination        
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);
        
      // all marks
        $file_search = ROOT_DIR.'/array/marks.php';
        file_exists($file_search) ? require($file_search) : '';
      
      // marks and count
        foreach ($mar_count as $k => $v){ 
          if ($v['count_buy'] > 0) {
            $res[$k] = ['name' => $v['marka'], 'count' => $v['count_buy'], 'link' => STATIC_WWW.'/car/buy/'.mb_strtolower($v['marka']).'/']; 
          } 
        }
        $this->smarty->assign('marks_count_buy',$res);
        
      // last advs
        $cars = $this->GetAllCars($data,$arg);
        $this->smarty->assign('cars',$cars);
        
      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'text' => 'Покупка',
          ];
        $this->smarty->assign('breadcrumbs',$breadcrumbs);
        
      // seo
        $seo['seo'] = [
          'title' => 'покупка авто на abw.by',
          'desc'  => 'Поиск объявлений по покупке автомобилей, микроавтобусов от частных лиц и компаний по базе объявлений белорусского сайта abw.by',
          'keyw'  => 'abw.by, объявления, поиск, покупка',
        ];

      // for social links
        $seo['social'] = [
          'title' => $seo['seo']['title'],
          'desc'  => $seo['seo']['desc'],
          'keyw'  => $seo['seo']['keyw'],
          'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';

        $this->smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $this->smarty->assign('car_links',$car_links);  
    }
    
    public function viewed($smarty, $_db, $url = '', $arg = '') {

       // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Cars($this->db);
      
      // get id advs info  
        $advs_id = $car_class->getCookieCar('ViewedCar');
        
        if (empty($advs_id)) {return FALSE;}
        
      // get data for advs
        $ar['cars'] = $advs_id;        
        $data = $car_class->getViewedCar($ar);
        
      // pagination    
        $pages = $car_class->Pagination($arg,$data);  
        $data['page'] = $pages['sql'];  
        $this->smarty->assign('pages',$pages);

      // links for sort & date
        $links_sort = $car_class->LinksSortDate($arg);
        $this->smarty->assign('links_sort',$links_sort);

      // get advs
        $cars = $this->GetAllCars($data,$arg);
        
        $smarty->assign('viewed',TRUE);  
        
        $smarty->assign('cars',$cars);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'text' => 'Просмотренные объявления',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Поиск по объвлениям на abw.by',
            'desc'  => 'Объявление на сайте abw.by ',
            'keyw'  => 'abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $smarty->assign('car_links',$car_links);   
        
      }

      public function favorites($smarty, $_db, $url = '', $arg = '') {
      // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Cars($this->db);
      
      // get id advs info  
        $advs_id = $car_class->getCookieCar('note');

        
        if (empty($advs_id)) {return FALSE;}
      
      // get data for advs
        $ar['cars'] = $advs_id;    
        $ar['count'] = 150;
 
        $data = $car_class->getViewedCar($ar);   
        
      // check if ADV is set
        $deleted_advs = $car_class->CheckIdViewedCar($data,$ar);        
        $smarty->assign('deleted_advs',$deleted_advs['bad']);
        
      //write in cookie
        if (!empty($deleted_advs['bad'])){
          $car_class->saveCookieCar('note',$deleted_advs['good']);
        }

      // get advs        
        $cars = $this->GetAllCars($data,$arg);
      
        $smarty->assign('viewed',TRUE);  
        $smarty->assign('cars',$cars);

      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/',
          'name2' => 'Объявления',
          'text' => 'Блокнот с объявлениями',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Блокнот с объявлениями на abw.by',
            'desc'  => 'Объявление на сайте abw.by ',
            'keyw'  => 'abw.by, объявления, поиск, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);

      // links (text advertsment)
        $car_links = $this->GetCarListLinks();
        $smarty->assign('car_links',$car_links);   
      }
    
    public function autohouse($smarty, $_db, $url = '', $arg = '') {

       // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Cars($this->db);
      
      // get all autohouse
        $autohouse_sql = $car_class->getAutohouse('9');

        if (empty($autohouse_sql)) {return FALSE;}
      
      // get design for display
        $data = $car_class->getAutohouseDisplay($autohouse_sql);
        $smarty->assign('company',$data);
      
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/sell/',
          'name2' => 'Объявления',
          'text' => 'Автохаусы',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Автохаусы в Беларуси на abw.by',
            'desc'  => 'Автохаусы на сайте abw.by ',
            'keyw'  => 'автохаусы, объявления от площадок, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);
        
      }
      
      public function dealer($smarty, $_db, $url = '', $arg = '') {

       // type for body
        $smarty->assign('body_classes','archive-products');

      // class for work
        $car_class = new Cars($this->db);
      
      // get all autohouse
        $autohouse_sql = $car_class->getAutohouse('8');

        if (empty($autohouse_sql)) {return FALSE;}
      
      // get design for display
        $data = $car_class->getAutohouseDisplay($autohouse_sql);
        $smarty->assign('company',$data);
      
      // breadcrumbs
        $breadcrumbs = [
          'link1' => STATIC_WWW,
          'name1' => 'Abw.by',
          'link2' => STATIC_WWW.'/car/sell/',
          'name2' => 'Объявления',
          'text' => 'Дилеры',
          ];
        $smarty->assign('breadcrumbs',$breadcrumbs);

      // seo
        $seo['seo'] = [
            'title' => 'Автохаусы в Беларуси на abw.by',
            'desc'  => 'Автохаусы на сайте abw.by ',
            'keyw'  => 'автохаусы, объявления от площадок, покупка, продажа',
        ];

      // for social links
        $seo['social'] = [
              'title' => $seo['seo']['title'],
              'desc'  => $seo['seo']['desc'],
              'keyw'  => $seo['seo']['keyw'],
              'img' => isset($data['photos']['1']['medium']) ? $data['photos']['1']['medium'] : '',
        ];      

      // scripts header or footer
        $seo['header'] = $this->header;
        $seo['footer'] = '';
        $smarty->assign('seo',$seo);
        
      }
      
    private function GetAllCars($data,$arg){
      
      $car_class = new Cars($this->db);
      $advs = $car_class->GetAllCars($data, 'getid');
      
      if (count($advs)>0 && isset($advs['0']['adv_id'])){
        foreach ($advs AS $k => $v) {
          $advs[$k] = $v['adv_id']; 
        }
        $advs = is_array($advs) ? implode(',', $advs) : $advs;
        $data['where'] = '`car`.`adv_id` IN ('.$advs.')';
        $data['limit'] = 'LIMIT 0,30';
        $cars = $car_class->GetAllCars($data, '');
      }
      
      if (!empty($cars)) {
        
        // favourite
          $favourits = $car_class->CheckCookie('1','note');
        
        // hide button
          $arr_hide = $car_class->CheckCookie('1','hide');
        
        // для работы с картинками
          $class_img = NEW Img();
      
        // перебор данных
        foreach ($cars as $k => $v)
        {
          
          // type paid adv
            $res[$k]['adds_type'] = ($v['status'] == '2') ? $car_class->TypePaidAdvCode($v['code']) : '' ;
                        
          // adv_id
            $res[$k]['adv_id'] = $v['adv_id'];
          
          // link
            $res[$k]['link'] = $car_class->GetLinksCar($v['adv_type'], $v['marka'], $v['model'], $v['adv_id']);

          // show
            $res[$k]['views'] = $v['show'];

          // title
            $res[$k]['title'] = $v['marka'].' '.$v['model'].' '.$v['version'];

          // price
            if ($v['cost_val'] > 0 && isset($v['cost_type'])){
              $cost_list = $car_class->multiPrice($v['cost_val'], $v['cost_type']);  
              if (!empty($cost_list)){
                foreach ($cost_list as $k_curr => $curr) {
                  if ($k_curr == 'byrn') $res[$k]['price_byn'] = $curr['cost'].' руб.';
                  if ($k_curr == 'usd') $res[$k]['price_usd'] = '~ '.$curr['cost'];
                }
                // old price
                ((($v['cost_val_pred'] > 0) && ($v['cost_val_d'] - $v['cost_val_pred'])) > 0) ? $res[$k]['price_old'] = $v['cost_val_pred'] : '';
              }
            }
            
          
          // year
            $res[$k]['year'] = $v['year'];  

          // info
            !empty($v['volume']) ? $res_data['engine'] = $car_class->VolumeEngine($v['volume']).' '.$v['engine'] : '';
            !empty($v['trans']) ? $res_data['trans'] = $v['trans'] : '';
            $res[$k]['data'] = empty($res_data) ? '' : implode(', ', $res_data);

          // photo
            $res[$k]['thumb'] = $class_img->PhotoListCar($v['adv_id'], 'list1');
          
          //  thumb1 & thumb2 for super-foto
            if (!empty($res[$k]['thumb']) && $res[$k]['adds_type'] == 'site-highlight'){
              $res[$k]['thumb1'] = $class_img->PhotoListCar($v['adv_id'], 'list2');
              $res[$k]['thumb2'] = $class_img->PhotoListCar($v['adv_id'], 'list3');  
            }
            
          // car mileage
            $pr =['probeg_col' => $v['probeg_col'], 'probeg_type' => $v['probeg_type']];
            $res[$k]['mileage'] = $car_class->CarProbeg($pr);

          // location
            $city_array = ['Минск','Брест','Витебск','Гродно','Гомель','Могилев'];

            (!empty($v['city'])) ? $location_pr['city'] = $v['city'] : '';
            (!empty($v['region']) && !empty($v['city']) && !in_array($v['city'], $city_array)) ? $location_pr['region'] = $v['region'] : '';
            ($v['country_id'] == '1' || empty($v['country_id'])) ? '' : $location_pr['country'] = $v['country']; 
            $res[$k]['location'] = empty($location_pr) ? '' : implode(', ', $location_pr);
            unset($location_pr);

          // time
            $res[$k]['time'] = show_rus_date3($v['add_time3']);

          // overtaking
            $res[$k]['overtaking'] = '';

          // autohouse
            $res[$k]['autohouse'] = (in_array($v['status'], ['8','9'])) ? $car_class->GetInfoAutohouseSmall($v['user']) : '';

          // description
            $length = ($res[$k]['adds_type'] == 'site-highlight') ? 450 : 150;
            $res[$k]['excerpt'] = (strlen($v['desc'])>$length) ? substr($v['desc'], 0, strrpos(substr($v['desc'], 0, $length),' ')).'...' : $v['desc'];

          // condition
            $res[$k]['condition'] = ($v['na_rf'] == '2') ? '' : $v['condition'];
            
          // favourite 
             $res[$k]['favourite'] = (!empty($favourits) && in_array($v['adv_id'], $favourits)) ? 'active' : '';
             
          // hide button
             $res[$k]['hide_button'] = (!empty($arr_hide) && in_array($v['adv_id'], $arr_hide)) ? '1' : '';
        }          
      return $res;
      }
    }
    
    
    private function getDataCar($_db,$arg){
      
      $res = FALSE;
      // get data on id
        $car_class = NEW Cars($_db);
      if (isset($arg['2']) && isset($arg['3']) && isset($arg['4'])){
        $id = filter_var($arg['4'], FILTER_SANITIZE_STRING);
        $marka = urldecode(filter_var($arg['2'], FILTER_SANITIZE_STRING));
        $model = urldecode(filter_var($arg['3'], FILTER_SANITIZE_STRING));
        
        // get main data
          $main = $car_class->GetCar($id,$marka,$model);
       
        if (!empty($main['0'])){
          // get option for car
          $options = $car_class->GetOptionsForCar($id);
          // add 1 view
          //$car_class->AddViewAdv($id);
          // get info for autohouse
          if (in_array($main['0']['status'], ['8','9'])){            
            $main['0']['autohouse'] = $car_class->GetInfoAutohouse($main['0']['user']);
          }
          // photos
          $main['0']['photos'] = ($main['0']['photo'] > 0) ? $car_class->GetPhotosCar($main['0']['adv_id']) : '';
          
          // get youtube
          $main['0']['youtube'] = $car_class->PlayYoutube($main['0']['link']);
          // get url
          $main['0']['url'] = '//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          // display
          $res = $car_class->DisplayAdv($main['0'],$options);
        }
      }
      
      if ($res == FALSE){
        $quary = $car_class->GetMarkaModelID($marka,$model);
        // если нет ID в базе, то выводим специальную страницу
        if (isset($quary['0'])){
          $res['main'] = $quary['0'];
          $res['main']['marka_url'] = $marka;
          $res['main']['model_url'] = $model;          
          $res['main']['adv_id'] = $id;
          $res['main']['user'] = '';
          $res['main']['title'] = 'Не найдено объявление';
          $res['main']['description'] = 'Объявление № '.$id.' не найдено в базе сайта. Возможно оно удалено и восстановить его невозможно.';
          $res['main']['year'] = '';    
          $res['error'] = TRUE;
          $res['count_s'] = 16;
          $res['count_v'] = 12;
          // type for body
          $this->smarty->assign('body_classes','product-404');
        }else{
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }       
      }
      return $res;
    }
    
      
    private function GetAllSales(){
      $file = ROOT_DIR.'/array/sto_sales.php';
      file_exists($file) ? require $file : '';
      return $sto_sales;
    }
            
    private function GetCarLinks(){
      $file = ROOT_DIR.'/array/car_links.php';
      file_exists($file) ? require $file : '';

      // страница объявления вверху
      if (isset($car_links_top)){
        foreach ($car_links_top as $key => $value){
          $res['car_links_top'][$key] = $car_links_top[$key][array_rand($value)];
        }
      }
      
      // страница объявления под фотографиями
      if (isset($car_links_under_photo)){
        foreach ($car_links_under_photo as $key => $value){
          $res['car_links_under_photo'][$key] = $car_links_under_photo[$key][array_rand($value)];
        }
      }
      
      // страница объявления под опциями
      if (isset($car_links_under_options)){
        foreach ($car_links_under_options as $key => $value){
          $res['car_links_under_options'][$key] = $car_links_under_options[$key][array_rand($value)];
        }
      }
      
      // страница объявления внизу
      if (isset($car_links_bot)){
        foreach ($car_links_bot as $key => $value){
          $res['car_links_bot'][$key] = $car_links_bot[$key][array_rand($value)];
        }
      }     

      return $res;
    }
    
    private function GetCarListLinks(){
      $file = ROOT_DIR.'/array/car_list_links.php';      
      file_exists($file) ? require $file : '';
      // список объявлений вверху
      if (isset($list_car_top)){
        foreach ($list_car_top as $key => $value){
          $res['list_car_top'][$key] = $list_car_top[$key][array_rand($value)];
        }
      }   
      
      return $res;
    }
    
    private function getLastNews($_db){
      //comments
      $f = new forum();
      //news
      $news = new News($_db);
      //days for news 
      $days = (time()-25920000); //259200 дни перевели в секунды и отняли от текущего времени (3 дня)
      //get top news
      $arr = ['select'=>'`n`.`news_id`, `n`.`name`, `n`.`descrip`, `n`.`date_add2`, `n`.`views`, `n`.`topic_id`, `n`.`type`', 'sort'=>'`n`.`date_add2` DESC', 'where'=>' AND `n`.`date_add2` > \''.$days.'\' AND `n`.`top_glavn` !=\'1\'', 'from'=>'0', 'onpage'=>'4'];
      $news = $news->getAllNews($arr);    
      
      // для работы с картинками
      $class_img = NEW Img();
      
      // work with news
      foreach ($news as $k => &$v)
      {
        // status for super news
        $v['news_type'] = ($v['views']>5000)? 'super' : 'simple';
        // description
        $str = strip_tags($v['descrip']);
        $str = (strlen($str)>170) ? substr($str, 0, strpos($str,' ',170)).'...' : $str;            
        $v['descrip'] = (($v['news_type']) == 'super') ? '' : $str;
        // link to the news
        $v['link'] = STATIC_WWW.'/news/'.$v['news_id'].'/';
        // main photo in news
        if (($v['news_type']) == 'super'){
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_350.jpg';
          $class_img->resize_img('300','350',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path);          
        }else{
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_200.jpg';
          $class_img->resize_img('300','200',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path); 
        }    
        $v['img'] = STATIC_WWW.$path;
        // date in rus
        $v['date_add2'] = show_rus_date($v['date_add2']);
        // count messages from forum
        $v['comment'] = $v['comment_status'] = $v['class'] = '';
        if ($v['topic_id']>'0'){
          $v['comment'] = $f->getComment($v['topic_id']);
          $v['class'] = ($v['comment'] > 19) ? 'hot-comments' : '';                 
        }
        // eyewitness
          $v['class'] .= ($v['type'] == '1') ? ' spectator' : '';
      }
      
      // add banners      
      /*
      if ((count($this->banners_news)) > 0){
        foreach ($this->banners_news as $value){
          $news_new = array_slice ($news, $value['pos']);
          array_unshift ($news_new,$value);
          array_splice ($news, $value['pos'],  count($news), $news_new);
        }
        $news = array_slice ($news, 0, 18);
      } 
      */
      return $news;
    }
    
    private function getSimilarCar($_db,$arr){
      $car = new Cars($_db);  
      $cars = $car->GetSimilarCars($arr);
      
      // для работы с картинками
      $class_img = NEW Img();
      
      foreach ($cars as $k => &$v)
      {
        // title
        $v['title'] = $v['marka'].' '.$v['model'].' '.$v['version'];
        // car mileage
        $pr =['probeg_col' => $v['probeg_col'], 'probeg_type' => $v['probeg_type']];
        $v['probeg'] = $car->CarProbeg($pr);
        // engine capacity
        $v['volume'] = $car->CarVolumeEngine($v['volume']).' '.$v['engine'];
        // show
        $v['show'] = isset($v['show']) ? $v['show'] : '';
        // up time
        $v['last_time_up'] = isset($v['last_time_up']) ? show_rus_date2(strtotime($v['last_time_up'])) : ''; 
        // price
        if ($v['cost_val'] > 0 && isset($v['cost_type'])){
          $cost_list = $car->multiPrice($v['cost_val'], $v['cost_type']);  
          if (!empty($cost_list)){
            foreach ($cost_list as $k_curr => $curr) {
              if ($k_curr == 'byrn') $v['byn'] = $curr['cost'].' руб.';
              if ($k_curr == 'usd') $v['usd'] = '~ '.$curr['cost'];
            }
          }
        }
        // link
        $v['link'] = $car->GetLinksCar($v['adv_type'], $v['marka'], $v['model'], $v['adv_id']);
        // photo
        $v['photo'] = $class_img->PhotoListCar($v['adv_id'], 'small');
      }      
      return $cars;
    }
    
    private function getViewedCar($json,$car_id='',$count, $arg){

      if (empty($car_id)){
        return FALSE;
      }
/*
      if (empty($json) || $json == "null"){
        setcookie('ViewedCar', json_encode([$car_id]), strtotime( '+30 days' ), "/" );
        return FALSE;
      }
*/            
      $car = new Cars($this->db);  
      $cars_id = $car->getCookieCar('ViewedCar', $car_id);
            
      // получить объявления
      $data = $car->getViewedCar(['cars' => $cars_id, 'count' => $count]);
      
      $cars = $car->GetAllCars($data);
      
      // сформировать вывод
      if (empty($cars)) return FALSE;
      
      // для работы с каринками
      $class_img = NEW Img();
              
      foreach ($cars as $k => &$v)
      {
        // title
        $v['title'] = $v['marka'].' '.$v['model'].' '.$v['version'];
        // car mileage
        $pr =['probeg_col' => $v['probeg_col'], 'probeg_type' => $v['probeg_type']];
        $probeg = $car->CarProbeg($pr);
        $v['probeg'] = ($probeg > 0) ? $probeg : ''; 
        // engine capacity
        $volume = $car->CarVolumeEngine($v['volume']);
        $v['volume'] = ($volume > 10) ? $volume.' '.$v['engine'] : '';
        // year
        $v['year'] = ($v['year'] > '1900') ? $v['year'] : '';  
        // show
        $v['show'] = isset($v['show']) ? $v['show'] : '';
        // up time
         $v['last_time_up'] = isset($v['last_time_up']) ? show_rus_date2(strtotime($v['last_time_up'])) : ''; 
        // price
        if ($v['cost_val'] > 0 && isset($v['cost_type'])){
          $cost_list = $car->multiPrice($v['cost_val'], $v['cost_type']);  
          if (!empty($cost_list)){
            foreach ($cost_list as $k_curr => $curr) {
              if ($k_curr == 'byrn') $v['byn'] = $curr['cost'].' руб.';
              if ($k_curr == 'usd') $v['usd'] = '~ '.$curr['cost'];
            }
          }
        }
        // link
        $v['link'] = $car->GetLinksCar($v['adv_type'], $v['marka'], $v['model'], $v['adv_id']);
        // photo
        $v['photo'] = $class_img->PhotoListCar($v['adv_id'], 'small');        
      }      
    
      return $cars;
    }
  
  }
