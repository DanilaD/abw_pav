<?
 class ExitUser {
    
    public function __construct($smarty,$_db) {
      // seo
      $seo = $this->getSeo();
      // выход
      $this->exitUser($_db);
      // type for body
      $smarty->assign('body_classes','page');
      
    }
    
    // страница выхода
    private function exitUser($_db){
      $user = new Users($_db);
      $user->exitUser('on');
    }
    
    private function getSeo(){
      $seo = [
        'title' => 'Выход из личного кабинета ABW.BY',
        'desc'  => 'Страница выхода из личного кабинета ABW.BY',
        'keyw'  => ''
      ];
      $seo['social'] = [
        'title' => $seo['title'],
        'desc'  => $seo['desc'],
        'keyw'  => $seo['keyw'],
        'img' => ''
      ];
      return $seo;
    }
 }
  
