<?
  class Main {
    
    public function __construct($smarty,$_db) {    
      
      // type for body
       $smarty->assign('body_classes','front-page');
      
      // for comments
        $file_comments = ROOT_DIR.'/array/fuel_cost.php';
        file_exists($file_comments) ? require($file_comments) : '';
        
      // for car
        $file_car = ROOT_DIR.'/array/main_page.php';
        file_exists($file_car) ? require($file_car) : '';
        
      // for search
        $file_search = ROOT_DIR.'/array/marks.php';
        file_exists($file_search) ? require($file_search) : '';
      
      // for banners in car & news
        $file_banners = ROOT_DIR.'/array/banners_main.php';
        file_exists($file_banners) ? require($file_banners) : '';
        $this->banners_news = $banners_news;
        $this->banners_car = $banners_car;
        
      // for vip lider
        $file_vip = ROOT_DIR.'/array/vip_car.php';
        file_exists($file_vip) ? require($file_vip) : '';        
        $this->vip_car = isset($vip_car) ? $vip_car : '';
        
      // top news  
        $news_top = $this->getTop($_db);

      // last news
        $news_last = $this->getLastNews($_db);

      // top stories of the day
        $news_popular_day = $this->getPopularDayNews($_db);

      // popular news
        $news_popular = $this->getPopularNews($_db);

      // course      
        $exchange_rate= $this->GetBestCourse($_db);

      // last advs
        $cars = $this->GetLastPaidAdv($_db);

      // marks with count adv
        $brands = $this->GetCountAdvMarks($mar_count);

      // all count car
        $count_brands = $mar_all_count['car']; 

      // sales
        $sales = $this->GetAllSales();
        
      // date for echange
        $date = show_rus_date_month(time());        

      // display
        $smarty->assign('NewsTop',$news_top);
        $smarty->assign('NewsLast',$news_last);
        $smarty->assign('NewsDayPopular',$news_popular_day);
        $smarty->assign('news_popular',$news_popular);						
        $smarty->assign('main_page_cat',$main_page_cat);
        $smarty->assign('fuel',$fuel);
        $smarty->assign('exchange_rate',$exchange_rate);
        $smarty->assign('cars',$cars);
        $smarty->assign('brands',$brands);
        $smarty->assign('count_brands',$count_brands);
        $smarty->assign('sales',$sales);
        $smarty->assign('date',$date);
    }
    
    private function getTop($_db){
      //comments
      $f = new forum();
      //news
      $news = new News($_db);
      //days for news 
      $days = (time()-25920000); //259200 дни перевели в секунды и отняли от текущего времени (3 дня)
      //get top news
      $arr = ['select'=>'`n`.`news_id`, `n`.`cat_id`, `n`.`name`, `n`.`descrip`, `n`.`date_add2`, `n`.`views`, `n`.`topic_id`, `c`.`cat_name`, `n`.`type`', 'where' => ' AND `n`.`date_add2` > \''.$days.'\' AND `n`.`top_glavn` =\'1\'', 'sort'=>'`n`.`pos`', 'top'=>'1'];
      $top = $news->getAllNews($arr);      
      // work with news
      $count_top = count($top);
      
      // для работы с картинками
      $class_img = NEW Img();
      
      foreach ($top as $k => &$v)
      {
        // link to the news
        $v['link'] = STATIC_WWW.'/news/'.$v['news_id'].'/';
        // description for the first news
        $v['descrip'] = (in_array($count_top, ['4']) && $k == '2') ? strip_tags($v['descrip']) : '';
        // main photo in news
        if ($k=='0' && in_array($count_top, ['4','5'])){
        // тумнейл 650x196px
          $width = '650';
          $height = '196';
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_650_196.jpg';
        }elseif($count_top=='3' && $k=='0'){
        // тумнейл 650x400px
          $width = '650';
          $height = '400';
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_650_400.jpg';
        }else{ 
        // тумнейл 300x200px
          $width = '300';
          $height = '200';
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_200.jpg';
        }
        $class_img->resize_img($width,$height,DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path);
        $v['img'] = STATIC_WWW.$path;
        // date in rus
        $v['date_add2'] = show_rus_date($v['date_add2']);
        // count messages from forum
        $v['comment'] = $v['comment_status'] = $v['class'] = '';
        if ($v['topic_id']>'0'){
          $v['comment'] = $f->getComment($v['topic_id']);
          //$v['class'] = ($v['comment'] > 19) ? 'hot-comments' : '';                 
        }
        // eyewitness
        $v['class'] .= ($v['type'] == '1') ? ' spectator' : '';
        // reklama 
        //$v['boolean'] = ($v['cat_id']=='3')? '1' : '';
        $v['boolean'] = '';
      }     
      return $top;
    }
    
    private function getLastNews($_db){
      //comments
      $f = new forum();
      //news
      $news = new News($_db);
      //days for news 
      $days = (time()-25920000); //259200 дни перевели в секунды и отняли от текущего времени (3 дня)
      //get top news
      $arr = ['select'=>'`n`.`news_id`, `n`.`name`, `n`.`descrip`, `n`.`date_add2`, `n`.`views`, `n`.`topic_id`, `n`.`type`', 'sort'=>'`n`.`date_add2` DESC', 'where'=>' AND `n`.`date_add2` > \''.$days.'\' AND `n`.`top_glavn` !=\'1\'', 'from'=>'0', 'onpage'=>'18'];
      $news = $news->getAllNews($arr); 
      // для работы с картинками
      $class_img = NEW Img();
      // work with news
      foreach ($news as $k => &$v)
      {
        // status for super news
        $v['news_type'] = ($v['views']>5000)? 'super' : 'simple';
        // description
        $str = strip_tags($v['descrip']);
        $str = (strlen($str)>170) ? substr($str, 0, strpos($str,' ',170)).'...' : $str;            
        $v['descrip'] = (($v['news_type']) == 'super') ? '' : $str;
        // link to the news
        $v['link'] = STATIC_WWW.'/news/'.$v['news_id'].'/';
        // main photo in news
        if (($v['news_type']) == 'super'){
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_350.jpg';
          $class_img->resize_img('300','350',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path);          
        }else{
          $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_200.jpg';
          $class_img->resize_img('300','200',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path); 
        }    
        $v['img'] = STATIC_WWW.$path;
        // date in rus
        $v['date_add2'] = show_rus_date($v['date_add2']);
        // count messages from forum
        $v['comment'] = $v['comment_status'] = $v['class'] = '';
        if ($v['topic_id']>'0'){
          $v['comment'] = $f->getComment($v['topic_id']);
          $v['class'] = ($v['comment'] > 19) ? 'hot-comments' : '';                 
        }
        // eyewitness
          $v['class'] .= ($v['type'] == '1') ? ' spectator' : '';
      }
      
      // add banners      
      if ((count($this->banners_news)) > 0){
        foreach ($this->banners_news as $value){
          $news_new = array_slice ($news, $value['pos']);
          array_unshift ($news_new,$value);
          array_splice ($news, $value['pos'],  count($news), $news_new);
        }
        $news = array_slice ($news, 0, 18);
      } 
      return $news;
    }
    
    private function getPopularDayNews($_db){
      //comments
      $f = new forum();
      //news
      $news = new News($_db);
      //days for news 
      $days = (time()-25920000); //259200 дни перевели в секунды и отняли от текущего времени (3 дня)
      //get top news
      $arr = ['select'=>'`n`.`news_id`, `n`.`name`, `n`.`descrip`,`n`.`date_add2`, `n`.`views`, `n`.`topic_id`, `n`.`type`', 'where'=>' AND `n`.`date_add2` > \''.$days.'\' AND `n`.`top_ned` != \'1\'', 'sort'=>'`n`.`views` DESC', 'from'=>'0','onpage'=>'9'];
      $news = $news->getAllNews($arr); 
      // для работы с картинками
      $class_img = NEW Img();
      // work with news
      foreach ($news as $k => &$v)
      {
        // description
        $str = strip_tags($v['descrip']);
        $v['descrip'] = (strlen($str)>170) ? substr($str, 0, strpos($str,' ',170)).'...' : $str; 
        // link to the news
        $v['link'] = STATIC_WWW.'/news/'.$v['news_id'].'/';
        // main photo in news
        $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_200.jpg';
        $class_img->resize_img('300','200',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path);
        $v['img'] = STATIC_WWW.$path;
        // date in rus
        $v['date_add2'] = show_rus_date($v['date_add2']);
        // count messages from forum
        $v['comment'] = $v['comment_status'] = $v['class'] = '';
        if ($v['topic_id']>'0'){
          $v['comment'] = $f->getComment($v['topic_id']);
          //$v['class'] = ($v['comment'] > 19) ? 'hot-comments' : '';                 
        }
        // eyewitness
        //$v['class'] .= ($v['type'] == '1') ? ' spectator' : '';
        $v['type'] = '';	
      }
      return $news;
    }
    
    private function getPopularNews($_db){
      //comments
      $f = new forum();
      //news
      $news = new News($_db);
      //days for news 
      $days = (time()-60480000); //604800 дни перевели в секунды и отняли от текущего времени (7 дней)
      //get top news
      $arr = ['select'=>'`n`.`news_id`, `n`.`name`, `n`.`descrip`, `n`.`date_add2`, `n`.`views`, `n`.`topic_id`, `n`.`type`', 'where'=>' AND `n`.`date_add2` > \''.$days.'\' AND `n`.`cat_id` NOT IN (4,29)', 'sort'=>'`n`.`views` DESC', 'from'=>'0','onpage'=>'8'];
      $news = $news->getAllNews($arr);
      // для работы с картинками
      $class_img = NEW Img();
      // work with news
      foreach ($news as $k => &$v)
      {
        // description
        $str = strip_tags($v['descrip']);
        $v['descrip'] = (strlen($str)>170) ? substr($str, 0, strpos($str,' ',170)).'...' : $str; 
        // link to the news
        $v['link'] = STATIC_WWW.'/news/'.$v['news_id'].'/';
        // main photo in news
        $path = PHOTO_NEWS_CASH.$v['news_id'].'_300_200.jpg';
        $class_img->resize_img('300','200',DIR.PHOTO_NEWS.$v['news_id'].'_1_350.jpg',DIR.$path);
        $v['img'] = STATIC_WWW.$path;
        // date in rus
        $v['date_add2'] = show_rus_date($v['date_add2']);
        // count messages from forum
        $v['comment'] = $v['comment_status'] = $v['class'] = '';
        if ($v['topic_id']>'0'){
          $v['comment'] = $f->getComment($v['topic_id']);
          //$v['class'] = ($v['comment'] > 19) ? 'hot-comments' : '';                 
        }
        // eyewitness
        $v['class'] .= ($v['type'] == '1') ? ' spectator' : '';
      }
      return $news;
    }
    
    private function GetBestCourse($_db){
      $course = new Currency($_db);      
      return $course->GetBestCourse($_db);
    }
        
    private function GetLastPaidAdv($_db){
      $car = new Cars($_db);  
      $cars = $car->GetLastPaidAdv($_db);
      
      // add vip car
      if (!empty($cars)){
        $cars = (!empty($this->vip_car) && count($this->vip_car) > 0) ? array_merge($this->vip_car,$cars) : $cars;

        // для работы с картинками
        $class_img = NEW Img();
      
        foreach ($cars as $k => &$v)
        {
          // title
          $v['title'] = $v['marka'].' '.$v['model'].' '.$v['version'];
          // car mileage
          $pr =['probeg_col' => $v['probeg_col'], 'probeg_type' => $v['probeg_type']];
          $v['probeg'] = $car->CarProbeg($pr);
          // engine capacity
          $v['volume'] = $car->CarVolumeEngine($v['volume']).' '.$v['engine'];
          // show
          $v['show'] = isset($v['show']) ? $v['show'] : '';
          // up time
          $v['last_time_up'] = isset($v['last_time_up'] ) ? date('H:i',strtotime($v['last_time_up'])) : '';
          // price
          $cost_list = $car->multiPrice($v['cost_val'], $v['cost_type']);  
          foreach ($cost_list as $k_curr => $curr) {
            if ($k_curr == 'byrn') $v['byn'] = $curr['cost'].' руб.';
            if ($k_curr == 'usd') $v['usd'] = '~ '.$curr['cost'];
          } 
          // link
          //$v['link'] = STATIC_WWW.'/allpublic/sell/'.$v['adv_id'].'/';
          $v['link'] = $car->GetLinksCar($v['adv_type'], $v['marka'], $v['model'], $v['adv_id']);
          // photo
          $v['photo'] = $class_img->PhotoListCar($v['adv_id'], 'small');     
        }

        // add banners      
        if ((count($this->banners_car)) > 0){
          foreach ($this->banners_car as $value){
            $cars_new = array_slice ($cars, $value['pos']);
            array_unshift ($cars_new,$value);
            array_splice ($cars, $value['pos'],  count($cars), $cars_new);
          }
        } 

        // check for limit
        $cars = array_slice ($cars, 0, 12);
      }
      return $cars;
    }
    
    private function GetCountAdvMarks($mar_count){
      foreach ($mar_count as $k => $v){ 
        if ($v['count'] > 0) {
          $res[$k] = ['marka' => $v['marka'], 'count' => $v['count'], 'link' => STATIC_WWW.'/car/sell/'.mb_strtolower($v['marka']).'/']; 
        } 
      }
      return $res;   
    }
    
    private function GetAllSales(){
      require ROOT_DIR.'/array/sto_sales.php';
      return $sto_sales;
    }    
  }
