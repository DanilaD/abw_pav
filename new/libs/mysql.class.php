<?
//mysql
require_once(ROOT_DIR.'/libs/utils.class.php');

class Mysql {
  var $dbname = null;
  var $dbhost = null;
  var $dbuser = null;
  var $dbpassw = null;
  var $charset = 'cp1251';
  var $connection=null;
  var $debug = FALSE;
	
  function connect(){
    $this->connection = new mysqli($this->dbhost, $this->dbuser, $this->dbpassw, $this->dbname) or put_error( 'База данных','Не могу соединиться с Базой данных. ошибка Х945Н');
    mysqli_query($this->connection , 'SET NAMES '.$this->charset);
  }
    
  function SelectCell($table, $IN){
    return @array_pop(@$this->SelectOne($table,$IN));
  }	
	
  function SelectOne($table, $IN){
    $IN['limit']=array('from'=>0,'to'=>1);
    return @array_pop(@$this->Select($table,$IN));
  }
	
	
	function Select($table, $IN = array(
	'select'=>'',
	'where'=>array(),
	'limit'=>array('from'=>0,'to'=>0),
	'order'=>''
	) ){
		if(@is_array($IN['select']) && sizeof($IN['select'])>0){
			$select = implode(' , ', $IN['select']);
		}elseif(@is_string($IN['select']) && strlen(trim($IN['select']))>0 ){
			$select = $IN['select'];
		}else{
			$select='*';
		}

		// get parameters
		if(@is_array($IN['where']) && sizeof($IN["where"])>0){
			$params = array();
			foreach($IN['where'] as $k=>$v){
				if(strpos($v,',') && !strpos($v,'\'') && !strpos($v,'"')){
					$params[$k] = $k.' IN ('.$v.')';
				}else{
					$params[$k] = $k."='".$v."'";
				}
			}
			$params = implode(' AND ', $params);
			$params = ' WHERE '.$params;
		}elseif(@is_string($IN['where']) && !empty($IN['where']) ){
			$params = ' WHERE '.$IN['where'];
		}else{
			$params = '';
		}
		if(isset($IN['_xtra'])){
			if($params){
				$params .= ' '.$IN['_xtra']; // AND
			}else{
				$params = ' WHERE '.$IN['_xtra'];
			}
		}
		// get limit
		$sql_limit['from'] = (isset($IN['limit']['page']) && isset($IN['limit']['to']))
			?($IN['limit']['page']*$IN['limit']['to']-$IN['limit']['to'])
			:@(int)$IN['limit']['from'];
		$sql_limit['to'] = @(int)$IN['limit']['to'];
		if(empty($sql_limit['to']) && empty($sql_limit['from'])){
			$limit = '';
		}elseif(empty($sql_limit["to"])){
			$limit = ' limit '.$sql_limit['from'].','.NODESIZE;
		}else{
			$limit = ' limit '.$sql_limit['from'].','.$sql_limit['to'];
		}
		// get order
		$order = '';
		if(@is_array($IN['order'])){
			$key = array_keys($IN['order']);
			$key = $key[0];
			$order = ' order by '.$IN['order'][$key].' '.$key;
		}

		$sql='SELECT '.$select.' FROM '.$table.$params.$order.$limit;

		if($this->debug) {
			$starttime = Utils::getmicrotime();
			echo $sql.'<br>';
		}

		if(!$result = mysqli_query($this->connection,$sql)){
			if($this->debug) echo mysqli_error($this->connection).'<br>';
			return false;
		}
		$return = array();
		while ($row = mysqli_fetch_assoc($result)) $return[] = $row;
		if($this->debug) {
			$endtime = Utils::getmicrotime();
			echo ($endtime-$starttime).'<br>';
		}
		return $return;
	}

	function sql($sql){
		if($this->debug) {
			$starttime = Utils::getmicrotime();
			echo $sql.'<br>';
		}
		if (!$result = mysqli_query($this->connection,$sql)){
			if($this->debug) echo mysqli_error($this->connection).'<br>';
			return false;
		}
		$return = array();
		while (@$row = mysqli_fetch_assoc($result))
		$return[] = $row;
		if($this->debug) {
			$endtime = Utils::getmicrotime();
			echo ($endtime-$starttime).'<br>';
		}
		return $return;
	}

	function Delete ($table, $IN=array()){
		if(is_array($IN) && sizeof($IN)>0){
			$params = array();
			foreach($IN as $k=>$v){
				if(strpos($v,',') && !strpos($v,'\'') && !strpos($v,'"')){
					$params[$k] = $k.' IN ('.$v.')';
				}else{
					$params[$k] = $k."='".$v."'";
				}
			}
			$params = implode(' and ', $params);
		}elseif(is_string($IN) && !empty($IN) ){
			$params = $IN;
		}else{
			$params = '';
		}
		$sql='DELETE FROM '.$table.' WHERE '.$params;
		if($this->debug) {
			$starttime = Utils::getmicrotime();
			echo $sql.'<br>';
		}
		if(mysqli_query($this->connection,$sql)){
			if($this->debug) {
				$endtime = Utils::getmicrotime();
				echo ($endtime-$starttime).'<br>';
			}
			return true;
		}
		if($this->debug) echo mysqli_error($this->connection).'<br>';
		return false;
	}

	function Insert ($table, $IN){
		$values = array();
		$keys = array();
		if(!$IN) return false;
		foreach($IN as $k=>$v){
			$keys[] = "`$k`";
			$values[] = "'".$v."'";
		}
		$keys = implode(',', $keys);
		$values = implode(',', $values);
		$sql='INSERT INTO '.$table.'('.$keys.') values('.$values.')';
		if($this->debug) {
			$starttime = Utils::getmicrotime();
			echo $sql.'<br>';
		}
		if(mysqli_query($this->connection,$sql)){
			if($this->debug) {
				$endtime = Utils::getmicrotime();
				echo ($endtime-$starttime).'<br>';
			}
			return mysqli_insert_id($this->connection);
		}
		if($this->debug) {
			echo mysqli_error($this->connection).'<br>';
		}
		return false;
	}

	function Update ($table, $set, $where=''){

		if(is_array($set) && sizeof($set)>0){
			$var = array();
			foreach($set as $k=>$v){
				$var[] = '`'.$k."`='".$v."'";
			}
			$var = implode(', ', $var);
		}elseif(is_string($set) && !empty($set) ){
			$var = $set;
		}
		$params = '';
		if(is_array($where) && sizeof($where)>0){

			$params = array();
			foreach($where as $k=>$v){
				if(strpos($v,',') && !strpos($v,'\'') && !strpos($v,'"')){
					$params[$k] = '`'.$k.'` IN ('.$v.')';
				}else{
					$params[$k] = '`'.$k."`='".$v."'";
				}
			}
			$params = implode(' AND ', $params);
			$params = ' WHERE '.$params;
		}elseif(is_string($where) && !empty($where) ){
			$params = ' WHERE '.$params;
		}

		$sql = 'UPDATE '.$table.' SET '.$var.$params;
		if($this->debug) {
			$starttime = Utils::getmicrotime();
			echo $sql.'<br>';
		}
		if(mysqli_query($this->connection,$sql)){
			if($this->debug) {
				$endtime = Utils::getmicrotime();
				echo ($endtime-$starttime).'<br>';
			}
			return true;
		}
		if($this->debug) echo mysqli_error($this->connection).'<br>';
		return false;
	}
}
?>
