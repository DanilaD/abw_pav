<?
class Seo
{
  public function display($var){
    switch ($var) {
    // ГЛАВНАЯ СТРАНИЦА
      case 'main':
        // seo
        $res['seo'] = [
              'title' => 'ABW.BY - автомобильный сайт Беларуси: объявления о продаже, покупке авто и запчастей',
              'desc'  => 'Покупка и продажа легковых, грузовых авто, мотоциклов, запчастей, шин и дисков. Частные объявления, услуги компаний, автомобильные новости Беларуси',
              'keyw'  => 'авто, автомобили, запчасти, грузоперевозки, купить машину, перевозки грузов, грузовики, доска бесплатные частные объявления, часные обьявления, фотообъявления машины, фотографии, статьи, новости, газета автобизнес,иномарки, авторынок, форум',
        ];

        // for social links
        $res['social'] = [
              'title' => $res['seo']['title'],
              'desc'  => $res['seo']['desc'],
              'keyw'  => $res['seo']['keyw'],
              'img' => ''
        ];      

        // scripts header or footer
        $res['header'] = '<script src="https://yastatic.net/pcode/adfox/loader.js"></script>';
        $res['footer'] = '';
        break;

    // СТРАНИЦА ОБЪЯВЛЕНИЯ Л/А
      case 'car':
        // seo
        $res['seo'] = [
              'title' => 'Продажа авто',
              'desc'  => '',
              'keyw'  => '',
        ];

        // for social links
        $res['social'] = [
              'title' => $res['seo']['title'],
              'desc'  => $res['seo']['desc'],
              'keyw'  => $res['seo']['keyw'],
              'img' => ''
        ];      

        // scripts header or footer
        $res['header'] = '<script src="https://yastatic.net/pcode/adfox/loader.js"></script>';
        $res['footer'] = '';
        break;
      
    // СТРАНИЦА ОШИБКИ
      case 'error':
        // seo
        $res['seo'] = [
              'title' => 'Не найдена страница',
              'desc'  => 'Извините, но по вашему запросу мы не смогли найти информацию!',
              'keyw'  => '404, ошибка, не найдено',
        ];

        // for social links
        $res['social'] = [
              'title' => $res['seo']['title'],
              'desc'  => $res['seo']['desc'],
              'keyw'  => $res['seo']['keyw'],
              'img' => ''
        ];      

        // scripts header or footer
        $res['header'] = '';
        $res['footer'] = '';
        break;
      
    // ЕСЛИ НЕТ ОПИСАНИЕ К СТРАНИЦЕ
      default:
        // seo
        $res['seo'] = [
              'title' => 'ABW.BY',
              'desc'  => '',
              'keyw'  => '',
        ];

        // for social links
        $res['social'] = [
              'title' => $res['seo']['title'],
              'desc'  => $res['seo']['desc'],
              'keyw'  => $res['seo']['keyw'],
              'img' => ''
        ];      

        // scripts header or footer
        $res['header'] = '';
        $res['footer'] = '';
    }
    return $res;
  }  
}