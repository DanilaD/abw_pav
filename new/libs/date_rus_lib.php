<?php

// функция вывода даты публикации в нужно формате
function show_rus_date($date){
  if (rus_date('dmy', ($date)) == rus_date('dmy', (time()))){   
    $date = rus_date('сегодня в G:i', ($date));
  }elseif(rus_date('dmy', ($date)) == rus_date('dmy', strtotime('-1 day'))){
    $date = rus_date('вчера в G:i', ($date));
  }else{
    $date = rus_date('j F Y в G:i', ($date));
  }
  return $date;
}

// функция вывода даты публикации в нужно формате
function show_rus_date2($date){
  if (rus_date('dmy', ($date)) == rus_date('dmy', (time()))){   
    $date = rus_date('сегодня', ($date));
  }elseif(rus_date('dmy', ($date)) == rus_date('dmy', strtotime('-1 day'))){
    $date = rus_date('вчера', ($date));
  }else{
    $date = rus_date('j F Y', ($date));
  }
  return $date;
}

// функция вывода даты публикации в нужно формате
function show_rus_date3($date){
  if (rus_date('dmy', ($date)) == rus_date('dmy', time())){   
    $date = rus_date('сегодня в G:i', ($date));
  }elseif(rus_date('dmy', ($date)) == rus_date('dmy', strtotime('-1 day'))){
    $date = rus_date('вчера', ($date));
  }elseif(rus_date('y', ($date)) == rus_date('y', time())){
    $date = rus_date('j F', ($date));
  }else{
    $date = rus_date('j F Y', ($date));
  }
  return $date;
}


// функция вывода даты
function show_rus_date_month($date){
    return rus_date('j F, Y', ($date));
}

//функция перевода даты на русский язык
function rus_date() {
  $translate = array(
    "am" => "дп",
    "pm" => "пп",
    "AM" => "ДП",
    "PM" => "ПП",
    "Monday" => "понедельник",
    "Mon" => "Пн",
    "Tuesday" => "вторник",
    "Tue" => "Вт",
    "Wednesday" => "среда",
    "Wed" => "Ср",
    "Thursday" => "четверг",
    "Thu" => "Чт",
    "Friday" => "пятница",
    "Fri" => "Пт",
    "Saturday" => "суббота",
    "Sat" => "Сб",
    "Sunday" => "воскресенье",
    "Sun" => "Вс",
    "January" => "января",
    "Jan" => "янв",
    "February" => "февраля",
    "Feb" => "фев",
    "March" => "марта",
    "Mar" => "мар",
    "April" => "апреля",
    "Apr" => "апр",
    "May" => "мая",
    "May" => "мая",
    "June" => "июня",
    "Jun" => "июн",
    "July" => "июля",
    "Jul" => "июл",
    "August" => "августа",
    "Aug" => "авг",
    "September" => "сентября",
    "Sep" => "сен",
    "October" => "октября",
    "Oct" => "окт",
    "November" => "ноября",
    "Nov" => "ноя",
    "December" => "декабря",
    "Dec" => "дек",
    "st" => "ое",
    "nd" => "ое",
    "rd" => "е",
    "th" => "ое"
  );
  // если передали дату, то переводим ее
  if (func_num_args() > 1) {
    $timestamp = func_get_arg(1);
    return strtr(date(func_get_arg(0), $timestamp), $translate);
  }else{
    // иначе текущую дату
    return strtr(date(func_get_arg(0)), $translate);
  }
 }

