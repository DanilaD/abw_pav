<?
class Img{
  
  // получить директорию для фотографий
    public function AdvDirPhoto($id, $dirhome){
      // получить по id директорию
      $dirs = $this->GetArUrlId($id);
      // создать директорию 
      return $this->CreateDirPhoto($dirs, $dirhome);      
    }
    
  // преобразовать массив в урл
    public function GetUrl($id, $dirhome){
      $dirs = $this->GetArUrlId($id);
      foreach ($dirs AS $k => $dir){
        $path = ($k == '0') ? $dirhome.$dir : $path.'/'.$dir;
      }
      return $path;
    }  

  // получить ссылки для фотографий к объявлению
    public function PhotoAdv($id, $dirhome){ 
    // получить директорию
      $path = $this->AdvDirPhoto($id, $dirhome);
      $res = '';
    // формирование фотографий
      for ($i=1;$i<=20;$i++)
      {
        if (file_exists(DIR.PHOTO_AUTO.$id.'_'.$i.'_650.jpg')){
          $res[$i] = $this->GetPhotosCar($id, $i, $path);
        }else{
          break;
        }
      }
      return $res;
    }
    
  // фото для листинга  
    public function PhotoListCar($id, $type){
      // type = small | list
      
      // получить путь
        $path = $this->AdvDirPhoto($id, PHOTO_AUTO_CASH);
      
      // получить имя фотографии
        if ($type == 'small'){
          $photo = $this->GetPhotoSmallCar($id, $path);
        }elseif (in_array ($type, ['list1','list2','list3'])){
          $photo = $this->GetPhotoListCar($id, $path, $type);
        }
        
      return !empty($photo) ? STATIC_WWW.$photo : '';
    }
    
  // удаление директории с её содержимым
    public function removeDirectory($dir) {
      if (!file_exists($dir)) {return FALSE;}
      if ($objs = glob($dir."/*")) {
         foreach($objs as $obj) {
           is_dir($obj) ? '' : unlink($obj);
         }
      }
      rmdir($dir);
      return TRUE;
    }
    
  // получить массив для url из id объявления
    private function GetArUrlId($id){
      return str_split($id, 3);     
    }
    
  // проверить и создать директорию
    private function CreateDirPhoto($dirs, $dirhome){
      foreach ($dirs AS $k => $dir){
        $path = ($k == '0') ? $dirhome.$dir : $path.'/'.$dir;
        file_exists(DIR.$path) ? '' : mkdir(DIR.$path);
      }
      return $path;
    }

  // размеры фотографий для слайдера
    private function GetPhotosCar($id, $i, $path){
      $photo = [
        ['width'=>'100','height'=>'66','name'=>'thumb'],
        /*['width'=>'300','height'=>'200','name'=>'small'],
        ['width'=>'480','height'=>'360','name'=>'medium'],*/
        ['width'=>'620','height'=>'460','name'=>'large'],
        ['width'=>'748','height'=>'560','name'=>'big'],
      ];
      return $this->GetAllPhotosCar($photo, $id, $i, $path);
    }
    
  // фотография для листинга
    private function GetPhotoSmallCar($id, $path){
      $photo = ['width'=>'300','height'=>'200','name'=>'small1'];
      $p = $path.'/'.$photo['name'].'.jpg';
      return (file_exists(DIR.$p)) ? $p : $this->ChangePhoto($photo, $p, $id, '1');
    }
    
  // фотографии для ВИП-объявлений
    private function GetPhotoListCar($id, $path, $type){
      
      switch ($type) {
        case 'list1':
          $photo = ['width'=>'204','height'=>'140','name'=>'list1']; 
          $i = '1';
          break;
        case 'list2':
          $photo = ['width'=>'100','height'=>'68','name'=>'list2']; 
          $i = '2';
          break;
        case 'list3':
          $photo = ['width'=>'100','height'=>'68','name'=>'list3']; 
          $i = '3';
          break;        
        default:
          return false;
      }     
      $p = $path.'/'.$photo['name'].'.jpg';
      return (file_exists(DIR.$p)) ? $p : $this->ChangePhoto($photo, $p, $id, $i);
    }

  // получить ссылки к фотографиям
    private function GetAllPhotosCar($photo, $id, $i, $path){
      $res = '';
      foreach ($photo as $k => $v) {
        $p = $path.'/'.$v['name'].$i.'.jpg';
        $this->ChangePhoto($v, $p, $id, $i);
        $res[$v['name']] = STATIC_WWW.$p;
        unset($p);
      }
      return $res;
    }  
    
  // изменить рамзер фотографии для авто
    private function ChangePhoto($v, $p, $id, $i){
      return $this->resize_img($v['width'], $v['height'], DIR.PHOTO_AUTO.$id.'_'.$i.'_650.jpg', DIR.$p);
    }

  // изменение размера фотографии
    public function resize_img($maxx, $maxy, $from_file, $to_file){  
      // $maxx - размер по ширине в px, $maxy - размер по высоте в px, $from_file - файл оригинал, $to_file - имя файла для записи
       
      // проверить существует ли фотография
        if (file_exists($to_file)) {return false;}
      // проверить есть ли оригинал
        if (!file_exists($from_file)) {return false;}
        // работа с размерами фотографией
        list($width, $height, $type, $attr) = getimagesize($from_file);
        if($width>=$maxx || $height>=$maxy){
            $k = ($width>$height)?($width/$maxx):($height/$maxy);
            $mw = $width/$maxx;
            $mh = $height/$maxy;
            $otn = $mw/$mh;
            if($otn<1){
              $width2 = $mw*$maxx;
              $height2 = $mw*$maxy;
              $cut_x = $width - $width2;
              $cut_y = $height - $height2;
            }else{
              $width2 = $mh*$maxx;
              $height2 = $mh*$maxy;
              $cut_x = $width - $width2;
              $cut_y = $height - $height2;
            }
            $cut_x/=2;
            $cut_y/=2;
            $x=$width/$k;
            $y=$height/$k;            
            // допустимые форматы фотографий для создания
            switch ($type){
                case 1: $im = @imagecreatefromgif($from_file);
                break;
                case 2:	$im = @imagecreatefromjpeg($from_file);
                break;
                case 3: $im = @imagecreatefrompng($from_file);
                break;
                case 15: $im = @imagecreatefromwbmp($from_file);
                break;
                case 16: $im = @imagecreatefromxbm($from_file);
                break;
                default: return false;
            }
            $nim = imagecreatetruecolor($maxx,$maxy);
            
            // ресайз по заданным значениям
            imagecopyresampled($nim,$im,0,0,$cut_x,$cut_y,$maxx,$maxy,$width2,$height2);
            // задайтся качество фотографии
            imagejpeg($nim,$to_file, 90);
        }else{
            copy($from_file,$to_file);
        }
    }
}

function resize_img($maxx, $maxy, $from_file, $to_file){  
      // $maxx - размер по ширине в px, $maxy - размер по высоте в px, $from_file - файл оригинал, $to_file - имя файла для записи
       
      // проверить существует ли фотография
        if (file_exists($to_file)) {return false;}
      // проверить есть ли оригинал
        if (!file_exists($from_file)) {return false;}
        // работа с размерами фотографией
        list($width, $height, $type, $attr) = getimagesize($from_file);
        if($width>=$maxx || $height>=$maxy){
            $k = ($width>$height)?($width/$maxx):($height/$maxy);
            $mw = $width/$maxx;
            $mh = $height/$maxy;
            $otn = $mw/$mh;
            if($otn<1){
              $width2 = $mw*$maxx;
              $height2 = $mw*$maxy;
              $cut_x = $width - $width2;
              $cut_y = $height - $height2;
            }else{
              $width2 = $mh*$maxx;
              $height2 = $mh*$maxy;
              $cut_x = $width - $width2;
              $cut_y = $height - $height2;
            }
            $cut_x/=2;
            $cut_y/=2;
            $x=$width/$k;
            $y=$height/$k;            
            // допустимые форматы фотографий для создания
            switch ($type){
                case 1: $im = @imagecreatefromgif($from_file);
                break;
                case 2:	$im = @imagecreatefromjpeg($from_file);
                break;
                case 3: $im = @imagecreatefrompng($from_file);
                break;
                case 15: $im = @imagecreatefromwbmp($from_file);
                break;
                case 16: $im = @imagecreatefromxbm($from_file);
                break;
                default: return false;
            }
            $nim = imagecreatetruecolor($maxx,$maxy);
            
            // ресайз по заданным значениям
            imagecopyresampled($nim,$im,0,0,$cut_x,$cut_y,$maxx,$maxy,$width2,$height2);
            // задайтся качество фотографии
            imagejpeg($nim,$to_file, 90);
        }else{
            copy($from_file,$to_file);
        }
    }