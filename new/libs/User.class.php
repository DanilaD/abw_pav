<?php

class User
{

  var $sess;
  var $db;
  var $table = 'a_base';

  public function __construct($_db) {
    $this->sess = $_SESSION;
    $this->db = $_db;
  }

  function isAuthenticated()
  {
    if ($this->sess['login'])
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }

  function getStatus()
  {
    if ($this->isAuthenticated())
    {
      return $this->sess['status'];
    }
    else
    {
      return FALSE;
    }
  }

  public function getUser($user, $by = 'user_id', $select = '*')
  {
    if (!in_array($by, array('user_id', 'login')))
    {
      return FALSE;
    }

    $cell = ($select == '*') ? 'SelectOne' : 'SelectCell';
    $where[$by] = $user;
    $result = $this->db->$cell(
      $this->table,
      array(
        'select' => $select,
        'where' => $where
      )
    );

    return $result;
  }
}
// END User Class
