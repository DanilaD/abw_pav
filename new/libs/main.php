<?
class abw{
  function utf8_converter($array){
      array_walk_recursive($array, function(&$item, $key){
              $item = mb_convert_encoding($item, 'UTF-8', 'windows-1251');
      });
      return $array;
  }
  
  function win1251_converter($array){
      array_walk_recursive($array, function(&$item, $key){
              $item = mb_convert_encoding($item, 'windows-1251', 'UTF-8');
      });
      return $array;
  }
}
