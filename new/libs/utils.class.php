<?php
/**
 * yagrysha@gmail.com
 * Yaroslav Gryshanovich
 * 31.10.2007
 * 
 */
class Utils{
	static function getmicrotime()	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	function  resize_img($from_file, $to_file, $maxx, $maxy){
		list($width, $height, $type, $attr) =getimagesize($from_file);
		if($width>$maxx || $height>$maxy){
			$k = ($width>$height)?($width/$maxx):($height/$maxy);
			$x=$width/$k;
			$y=$height/$k;
			switch ($type){
				case 1: $im = @imagecreatefromgif($from_file);
				break;
				case 2:	$im = @imagecreatefromjpeg($from_file);
				break;
				case 3: $im = @imagecreatefrompng($from_file);
				break;
				case 15: $im = @imagecreatefromwbmp($from_file);
				break;
				case 16: $im = @imagecreatefromxbm($from_file);
				break;
				default: return false;
			}
			
			$nim = imagecreatetruecolor($x,$y);
			imagecopyresampled($nim,$im,0,0,0,0,$x,$y,$width,$height);
			imagejpeg($nim,$to_file);
		}
	}
	
	static function getPages($count, $npage, $onpage=20, $nn=8){
		$lastpage = ceil($count/$onpage);
		$end = ceil($npage/$nn)*$nn;
		$start = $end - ($nn-1);
		$end = ($end>$lastpage)? $lastpage:$end;
		$pages = array();
		if($start>1) $pages[$start-1] = '...';
		for($i=$start; $i<=$end;$i++){
			$pages[$i]=$i;
		}
		if($end<$lastpage) $pages[$end+1] = '...';
		return $pages;
	}
}
