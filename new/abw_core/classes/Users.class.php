<?php

class Users extends Core
{
  var $table = 'a_base';
  var $table_service = 'a_service';
  var $table_balance_history = 'a_balance_history';

  function __construct($_db)
  {
    parent::__construct($_db);
  }

  function newUser()
  {
    $ip = ($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : (($_ENV["HTTP_X_FORWARDED_FOR"]) ? $_ENV["HTTP_X_FORWARDED_FOR"] : 0);
    $bd = @htmlspecialchars($_POST['birthYear'] . "-" . $_POST['birthMonth'] . "-" . $_POST['birthDay'], ENT_COMPAT | ENT_HTML401, 'cp1251');
    $res = mysql_query("INSERT INTO
                          `a_base` 
                        SET
                          `login` = '".$this->post['login']."',
                          `pass` = '".md5(trim($_POST['pass']))."',
                          `mail` = '".@htmlspecialchars($_POST['email'], ENT_COMPAT | ENT_HTML401, 'cp1251')."',
                          `url` = '".@htmlspecialchars($_POST['url'], ENT_COMPAT | ENT_HTML401, 'cp1251')."',
                          `lastname` = '".@htmlspecialchars($_POST['lastname'], ENT_COMPAT | ENT_HTML401, 'cp1251')."',
                          `name` = '".@htmlspecialchars($_POST['name'], ENT_COMPAT | ENT_HTML401, 'cp1251')."',
                          `birth` = '".$bd."',
                          `country` = '".@htmlspecialchars($_POST['country'], ENT_COMPAT | ENT_HTML401, 'cp1251')."',
                          `last_login` = '".time()."',
                          `ip` = '".$ip."',
                          `phones` = '".serialize($_POST['ph'])."'
                        ");	
    $query = mysql_query("SELECT `user_id`, `mail`, `login` FROM `a_base` WHERE `login`='".$this->post['login']."'");
    list($user_id,$email,$login) = mysql_fetch_array($query);    
    $message = 'https://www.abw.by/index.php?activ='.md5($email).''.$user_id;
    $this->sendMail($email, $login, $_POST['pass'], $message);
  }
  
  function regUser($user)
  {
    $ip = ($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : (($_ENV["HTTP_X_FORWARDED_FOR"]) ? $_ENV["HTTP_X_FORWARDED_FOR"] : 0);
    $bd = @htmlspecialchars($user['birthYear'] . "-" . $user['birthMonth'] . "-" . $user['birthDay'], ENT_COMPAT | ENT_HTML401, 'cp1251');
    $res = mysql_query("INSERT INTO
                          `a_base` 
                        SET
                          `login` = '".$user['login']."',
                          `pass` = '".md5($user['pass'])."',
                          `mail` = '".$user['email']."',
                          `last_login` = '".time()."',
                          `ip` = '".$ip."'
                        ");												
    $user_id = mysql_insert_id();
    $message = 'https://www.abw.by/index.php?activ='.md5($user['email']).''.$user_id;
    $this->sendMail($user['email'], $user['login'], $user['pass'], $message);
    return $user_id;
  }
  
  function sendMail($email, $name, $pass, $url)
  {
	$password=(empty($pass)?'':'<br/>Ваш пароль: <b>'.$pass.'</b>');
    $text_2 = '<p>Благодарим за регистрацию на сайте ABW.BY</p><p>Для активации аккаунта необходимо перейти по ссылке: <a href="'.$url.'">'.$url.'</p><hr/><p>Ваш логин: <b>'.$name.'</b>'.$password.'</p><hr/><p>Не пересылайте данное письмо! Не показывайте никому Ваш пароль и логин. Сохраните данные в надёжном месте. Изменить пароль можно в личном кабинете abw.by.</p><p><i>Добро пожаловать на ABW.BY!</i></p>';					 
    require_once(ROOT_DIR.'/phpmailer/class.phpmailer.php');
    $mail = new PHPMailer();
    $mail->CharSet = "windows-1251";
    $mail->From = "registration@abw.by";
    $mail->setFrom('registration@abw.by', 'ABW.BY');
    $mail->AddCustomHeader('Return-Path: registration@abw.by');	
    $mail->FromName = "ABW.BY";
    $mail->AddAddress($email);
    $mail->AddBCC("support@abw.by");
    $mail->IsHTML(true);
    $mail->Subject = 'Активация аккаунта на ABW.BY';
    $mail->Body = $text_2;
    $mail->Send();
    $mail->ClearAddresses();
    $mail->ClearCustomHeaders();
    $mail->ClearAttachments();
  }

  function activationUser($user_data,$site='')
  {
    $str_user_id = substr($user_data,32);
    $str_user_email = substr($user_data,0,32);
    $check_user = $this->db->SelectOne(
    'a_base',
      array(
        'where' => array(
          'user_id' => $str_user_id
        )
      )
    );
    if(empty($check_user)){
		put_error2("Aктивация отклонена", "Неверная ссылка активации. Попробуйте повторить, если сообщение появляется повторно, обратитесь в службу тех.поддержки ABW.BY", "/contact/");                      
    }
      // борьба со спамером
      /*
      if (in_array($check_user['ip'], array('134.249.177.32', '5.45.73.45'))){
        mysql_query("UPDATE `a_base` SET `ban` = '2' WHERE `user_id`='".$str_user_id."'");        
        exit;
      }
      */
      if($check_user['ban'] == 3)
        {
        if ($str_user_email == md5($check_user['mail']))
          {          
          // статус активного пользователя
          $query = mysql_query("UPDATE `a_base` SET `ban` = '0' WHERE `user_id` = ".$str_user_id);
          
          // авторизация
          $this->authUser($check_user);
				
          //add mailing news
          $code=rand(1000000000,9999999999);
              
          $check = $this->db->SelectOne(
            'a_mailing',
              array(
                'where' => array('email' => $check_user['mail'], 'mailing' => top)
            ));
          if (empty($check)){
            $this->db->Insert(
              'a_mailing',
              array(
                'email' => $check_user['mail'],
                'code' => $code,
                'mailing' => top,
                'activation' => 1
              )
            );
          }
 
          if (empty($site)){
						
						
						$redirect='https://www.abw.by/profile_edit/first';		
							
						if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1") {
							$url = urldecode($redirect);
						}else{
							$url='/forum/include.php?username='.$check_user['login'].'&password='.$check_user['pass'].'&where='.$_REQUEST['where'].'&login=Вход&redirect='.$redirect;
						}
            header('Location: '.$url);
            exit();
          }else{
            $url='http://gruzon.abw.by';
            header('Location: '.$url);
            exit;
          }
		}else{
          put_error2("Неверная ссылка", "Неверная ссылка активации. Попробуйте повторить, если сообщение появляется повторно, обратитесь в службу тех.поддержки ABW.BY", "/");
        }     
      }else if($check_user['ban'] == 2){
        put_error2("Повторная активация", "Аккаунт пользователя заблокирован на форуме. <br />Обратитесь в службу тех.поддержки ABW.BY.  ", "/contact/");                      
      }else if($check_user['ban'] == 1){
        put_error2("Повторная активация", "Аккаунт пользователя заблокирован. <br />Обратитесь в службу тех.поддержки ABW.BY.  ", "/contact/");         
      }else if($check_user['ban'] == 0){
        put_error2("Повторная активация", "Аккаунт пользователя уже активирован. Если возникли проблемы со входом в аккаунт, обратитесь в службу тех.поддержки ABW.BY", "/contact/"); 
      }
 }


  function resend($login, $mail)
  {
    $user = $this->db->SelectOne(
        'a_base',
        array(
          'select' => array('user_id','ban','ip'),
          'where' => array(
            'login' => $login,
            'mail' => $mail
          )
        )
      );
        if (!$user)
    {
      put_error2('Повторный запрос на активацию акаунта', 'Логин не соответствует е-mail', '/index.php?act=resend');
    }elseif($user['ban'] == 2){
      put_error2("Повторная активация", "Аккаунт пользователя заблокирован на форуме.<br /> Обратитесь в службу тех.поддержки ABW.BY.  ", "/contact/");                      
    }elseif($user['ban'] == 1){
      put_error2("Повторная активация", "Аккаунт пользователя полностью заблокирован.<br /> Обратитесь в службу тех.поддержки ABW.BY.  ", "/contact/");         
    }elseif($user['ban'] == 0){      
      put_error2("Повторная активация", "Аккаунт пользователя уже активирован. Если возникли проблемы со входом в аккаунт, обратитесь в службу тех.поддержки ABW.BY", "/contact/"); 
    }else{     
      $message = 'https://www.abw.by/index.php?activ='.md5($mail).''.$user['user_id'];
      $this->sendMail($mail,$login,'',$message);
    }
  }
  
  function log($data)
  {
    $this->db->Insert('a_log_user',$data);
  }

  function validateUserId($user_id)
  {    
    $user = $this->db->SelectOne(
      $this->table,
      array(
        'where' => array(
          'user_id' => $user_id
        )
      )
    );    
    return $user;
  }
  
  function validateLogin($login)
  {    
    $user = $this->db->SelectOne(
      $this->table,
      array(
        'where' => array(
          'login' => $login
        )
      )
    );    
    return $user;
  }

  function validateEmail($email)
  {
    if (filter_var($email, FILTER_VALIDATE_EMAIL))
    {
      $res = $this->db->SelectCell(
        $this->table,
        array(
          'where' => array(
            'mail' => $email
          )
        )
      );
    }
    else
    {
      
    }
  }

  function getBalanceHistory($user_id, $from, $to)
  {
    $act_types = array(
      10 => 'обгон №',
      9 => 'спецпредложение №',
      11 => 'коммерческий транспорт №',
      12 => 'платное объявление для сайта №',  
      15 => 'просмотр видео'
    );
    $from = ($from == '') ? date('Y-m-d', strtotime("-1 month")) : $from;
    $from = strtotime($from.' 00:00:00');
    $to = ($to == '') ? date('Y-m-d') : $to;
    $to = strtotime($to.' 23:59:59');

    $sql = '
      SELECT
        bh.*,
        s.type
      FROM
        a_balance_history bh
      LEFT JOIN
        a_service s
      ON
        s.id = bh.service_id
      WHERE
        bh.user_id = '.$user_id.'
      AND
        bh.time >= '.$from.'
      AND
        bh.time <= '.$to.'
      ORDER BY
       bh.time DESC
    ';

    $result = $this->db->sql($sql);
    $history = array(
      'in' => array(),
      'out' => array(),
    );

    foreach ($result as $k => $v)
    {
      $line = array();
      $descr = ($v['action'] == '2' && isset($act_types[$v['type']])) ? $act_types[$v['type']] : 'платное обьявление №';
      $line['time'] = date('Y-m-d H:i:s',$v['time']);
      $line['text_action'] = ($v['action'] == '1') ? 'пополнение '.$v['data'] : $descr.$v['data'];
      $line['text_value'] = ($v['action'] == '1') ? '+'.$v['value'] : '-'.$v['value'];

      if ($v['action'] == '1')
      {
        $history['in'][] = $line;
      }
      else
      {
        $history['out'][] = $line;
      }
    }
    return $history;
  }
  
  
  function SetGeoDataUser($user){
    if (isset($_SESSION['geo'])){
      if (empty($user['city'])){
        $sql = 'SELECT
                  `city`.`id`,`city`.`country_id`,`city`.`region_id` 
                FROM 
                  `a_city2` AS `city`
                LEFT JOIN 
                  `a_country` AS `count` ON `city`.`country_id` = `count`.`country_id`
                WHERE 
                  `city`.`name`="'.$_SESSION['geo']['city'].'" AND `count`.`name` = "'.$_SESSION['geo']['country'].'"';        
        // $user['region'] - для точности выставления города, ещё нужна проверка по региону        
        $res = $this->db->Sql($sql);
        if ($res->num_rows==1){
          $geo = $res->fetch_assoc();
          $this->db->Update('a_base', array('city' => $geo['id'], 'region' => $geo['region_id'], 'country' => $geo['country_id']), array('user_id' => $user['user_id']));
        }
      }
    }
  }
  
  function connectDb(){
    $connect = connect_to_company();
    return $connect;
  }
  
  function setYiiKey($user){   
    $mysqli = $this->connectDb();
    $sql = 'SELECT `auth_key` FROM `users_auth_keys` WHERE `user_id` = \''.$user['user_id'].'\'';
    $mysqli = $this->connectDb();
    $result = $mysqli->query($sql);    
    if (!empty($result)){
      $arr = $result->fetch_assoc();
      $users_auth_keys = $arr['auth_key'];      
    }    
    if (empty($users_auth_keys)){
      $users_auth_keys = md5(rand());
      $sql = 'INSERT INTO `users_auth_keys` SET `user_id` = \''.$user['user_id'].'\', `auth_key` = \''.$users_auth_keys.'\'';
      $result = $mysqli->query($sql); 
    }
    return $users_auth_keys;
  }
  
  function checkUserAuth($data){
    $user = (isset($data['login'])) ? $this->validateLogin($data['login']) : '';
    $status = FALSE;
    if (empty($user)){
      $mes = 'Некорректный логин: '.$data['login'].' <a href="">Воостановить доступ</a>';
    }elseif($user['pass'] != md5(mb_convert_encoding($data['pass'], "windows-1251"))){
      $mes = 'Некорректный пароль: '.$data['pass'];
    }elseif($user['ban']=='1'){
      $mes = 'Аккаунт заблокирован! Обращайтесь к администратору.';
    }elseif($user['ban']=='3'){
      $mes = 'Аккаунт неактивен! Воспользуйтесь повторной активацией.';
    }else{
	$user['autologin'] = $data['autologin']; 
      $this->authUser($user);
      $mes = '';
      $status = TRUE;
    }    
    return ['status' => $status, 'text' => $mes];
  }
      
  function authUser($user)
  {     
    // del old session
    $this->db->Delete('a_sessions', array('user_id' => $user['user_id']));
    // add new session
    $ip = $_SERVER["REMOTE_ADDR"];
    if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
      $mes = 'Ошибка проверки. Обратитесь к администратору и сообщите код ошибки 003';
      return $mes;
      exit;
    }    
    $this->db->Insert('a_sessions', array('user_id'=>$user['user_id'], 'sid'=>session_id(),'time'=>time(), 'ip'=>$ip));
    // add new time login for user
    $this->db->Update('a_base', array('last_login' => time()), array('login' => $user['login']));
    // tokin for API
    $user['token'] = $this->db->SelectCell('a_base_tokens',array('select' => array('token'),'where' => array('user_id' => $user['user_id'])));
    if (empty($user['token'])){
      $user['token'] = md5(rand());
      $lifetime = time()+1209600;
      $this->db->Insert('a_base_tokens', array('user_id'=>$user['user_id'], 'token'=> $user['token'], 'created' => time(), 'expires' => $lifetime));
    }    
    // записываем город-регион-страну в личный кабинет
    $this->SetGeoDataUser($user);    
    //авторизация в yii (компании и мото)
    $users_auth_keys = $this->setYiiKey($user);     
    // convert
		$user['login'] = iconv('utf-8','windows-1251',$user['login']);
		$user['firstname'] = iconv('utf-8','windows-1251',$user['firstname']);
		$user['lastname'] = iconv('utf-8','windows-1251',$user['lastname']);
		$user['name'] = iconv('utf-8','windows-1251',$user['name']);
        $user['name_ofic'] = iconv('utf-8','windows-1251',$user['name_ofic']);        
        $user['about'] = iconv('utf-8','windows-1251',$user['about']);
        $user['text_link'] = iconv('utf-8','windows-1251',$user['text_link']);
        $user['address'] = iconv('utf-8','windows-1251',$user['address']);
        $user['pay_info'] = iconv('utf-8','windows-1251',$user['pay_info']);
        
	// создаём сессию
    $ses = [
      'login' => $user['login'],
      'status' => $user['status'],
      'user_keys' => $users_auth_keys,
      'user' => $user      
    ];
    $_SESSION = $ses;
	// автологин	время для coockie (3600*24*99)
	(isset($user['autologin']))?setcookie('autologin',serialize(array($user['user_id'], md5($user['user_id'].$user['login'].$user['pass']))), time()+8553600, '/'):'';	
  }
  
  function exitUser($url=''){
    // удаляем      
    $this->db->delete('a_sessions', array('sid'=>session_id()));
    setcookie('autologin','', time()-864000,'/');
    unset ($_COOKIE['autologin']);
    unset($_SESSION);
    session_destroy();
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Expires: " . date("r"));
    empty($url)? '': header('Location: '.STATIC_WWW);
    return TRUE;
  }
  
  public function UpdateBalanceUser($user_id,$cost) {
    $res = FALSE;
    // проверить пользователя и баланс    
    $balance = $this->db->SelectCell($this->table,array('select' => array('balance'),'where' => array('user_id' => $user_id)));
    if ($balance >= $cost){
      $balance_new = $balance - $cost;  
      // записать новый баланс
      $this->db->Update($this->table, array('balance' => $balance_new), array('user_id' => $user_id));
      // записать в сессию
      $_SESSION['user']['balance'] = $balance_new;
      $res = TRUE;
    }
    return $res;
  }
  
  public function SaveLogBalance($user_id,$type,$action,$cost,$data) {
    $time = time();
    $ip = ($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : (($_ENV["HTTP_X_FORWARDED_FOR"]) ? $_ENV["HTTP_X_FORWARDED_FOR"] : 0);
    //$action = 2 - списание, 1 - пополнение
    
    $service_id = $this->db->Insert(
      $this->table_service,
      array(
        'user_id' => $user_id,
        'type' => $type,
        'time' => $time
      )
    );
    
    $balance_history = $this->db->Insert(
      $this->table_balance_history,
      array(
        'user_id' => $user_id,
        'time' => $time,
        'value' => $cost,
        'action' => $action,
        'ip' => $ip,
        'service_id' => $service_id,
        'data' => $data
      )
    );
    
    return TRUE;
  }
}
