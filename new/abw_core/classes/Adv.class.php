  <?

    class Adv extends Core
    {
      
      private $table_adv = 'a_adv_advertisement';
      
      
      private $table_adv_marks = 'a_adv_marka';
      private $table_adv_models= 'a_adv_model';
      
      private $table_adv_trans = 'a_adv_transmission';
      private $table_adv_eng = 'a_adv_engine';

      private $table_city = 'a_city'; 
      private $table_country = 'a_country';   
      private $table_condition = 'a_condition'; 
      
      private $shina_profile = 'a_adv_shina_profile';
      private $shina_season = 'a_adv_shina_season';
      private $shina_size = 'a_adv_shina_size';
      private $shina_width = 'a_adv_shina_width';

      public $per_page = 30;



      public function __construct($_db)
      {
        parent::__construct($_db);
      }

      // основной вызов для поиска
      public function SearchWork($cat, $post='') {
        // сформировать доступный список значений из фильтра
        // truck - грузовики
        $arg = $this->GetSearch($cat);
        // получить данные
        $ar = $this->PrepareSearch($arg,$post);
        // записать в сессию значения из фильтра если это не POST запрос
        (empty($post)) ? $this->SearchSetSession($cat,$ar) : '';
        // сформировать sql запрос
      }
      
      // переменные для поисковика
      private function Search($cat) {
        switch ($cat) {
          // грузовики
          case truck:
              $res = [                
                'cat' => FILTER_VALIDATE_INT,
                'sub' => FILTER_VALIDATE_INT,
                'marka' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
                'model' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
                'year1' => FILTER_VALIDATE_INT,
                'year2' => FILTER_VALIDATE_INT,
                'capacity1' => FILTER_VALIDATE_INT,
                'capacity2' => FILTER_VALIDATE_INT,
                'transm' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
                'mileage1' => FILTER_VALIDATE_INT,
                'mileage2' => FILTER_VALIDATE_INT,
                'price1' => FILTER_VALIDATE_INT,
                'price2' => FILTER_VALIDATE_INT,
                'weight1' => FILTER_VALIDATE_FLOAT,
                'weight2' => FILTER_VALIDATE_FLOAT,
                'volume1' => FILTER_VALIDATE_FLOAT,
                'volume2' => FILTER_VALIDATE_FLOAT,
                'country' => FILTER_VALIDATE_INT,
                'rigion' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
                'city' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],                
                'day' => FILTER_VALIDATE_INT,
                'sort' => FILTER_VALIDATE_INT,      
              ];            
              break;
          case tire:
              
              break;
          case disc:
              
              break;
 
          default:
              $res = '';
      }
        return $res;
      }        

      // подготовить для SQL where
      private function GetSqlWhere($cat, $ar){
        switch ($cat) {
          // грузовики
          case truck:
            $w[] = '`car`.`hide` = \'0\'';
            empty($ar['cat']) ? '' : $w[] = '`a`.`id_cat` = \''.$ar['cat'].'\'';
            empty($ar['sub']) ? '' : $w[] = '`a`.`id_sub` = \''.$ar['sub'].'\'';
            empty($ar['year1']) ? '' : $w[] = '`a`.`year` >= \''.$ar['year1'].'\'';
            empty($ar['year2']) ? '' : $w[] = '`a`.`year` <= \''.$ar['year2'].'\'';
            empty($ar['capacity1']) ? '' : $w[] = '`a`.`capacity` >= \''.$ar['capacity1'].'\'';
            empty($ar['capacity2']) ? '' : $w[] = '`a`.`capacity` <= \''.$ar['capacity2'].'\'';
            empty($ar['country']) ? '' : $w[] = '`a`.`country` = \''.$ar['country'].'\'';
            empty($ar['mileage1']) ? '' : $w[] = '`a`.`run_value` >= \''.$ar['mileage1'].'\'';
            empty($ar['mileage2']) ? '' : $w[] = '`a`.`run_value` <= \''.$ar['mileage2'].'\'';
            empty($ar['price1']) ? '' : $w[] = '`a`.`cost_val_d` >= \''.$ar['price1'].'\'';
            empty($ar['price2']) ? '' : $w[] = '`a`.`cost_val_d` <= \''.$ar['price2'].'\'';
            empty($ar['weight1']) ? '' : $w[] = '`a`.`weight` = \''.$ar['weight1'].'\'';
            empty($ar['weight2']) ? '' : $w[] = '`a`.`weight` = \''.$ar['weight2'].'\'';
            empty($ar['volume1']) ? '' : $w[] = '`a`.`value_furgon` = \''.$ar['volume1'].'\'';
            empty($ar['volume2']) ? '' : $w[] = '`a`.`value_furgon` = \''.$ar['volume2'].'\'';
            empty($ar['marka']) ? '' : $w[] = '`a`.`marka` IN ('.$this->ArrayToStr($ar['marka']).')';
            empty($ar['model']) ? '' : $w[] = '`a`.`model` IN ('.$this->ArrayToStr($ar['model']).')';
            empty($ar['transm']) ? '' : $w[] = '`a`.`transmission` IN ('.$this->ArrayToStr($ar['transm']).')';
            empty($ar['rigion']) ? '' : $w[] = '`a`.`town` IN ('.$this->ArrayToStr($ar['rigion']).')';
            empty($ar['city']) ? '' : $w[] = '`a`.`town_other` IN ('.$this->ArrayToStr($ar['city']).')';        
            empty($ar['day']) ? '' : $w[] = '`a`.`add_time` > NOW() - INTERVAL '.$ar['day'].' DAY';
            empty($ar['sort']) ? '' : $data['order'] = $this->GetSort($ar['sort']);
          break;
 
            default:
              $res = '';
          }
          $data['where'] = implode(' AND ', $w);          
        return $data;        
      }
      
      // сортировка в листинге объявлений для грузовиков
      private function GetSortTruck($sort) {
        switch ($sort) {
        // 1- day
          case '1':
            $res = '`a`.`add_time` DESC';
          break;
        // 2- cost
          case '2':
            $res = '`a`.`cost_val_d` ASC';
          break;
        // 3 -year
          case '3':
            $res = '`a`.`year` DESC';
          break;
        // default
          default:
            $res ='`up`.`last_time_up` DESC';
        }
        return $res;
      }
      
      // получить реальные значения из поисковика
      private function PrepareSearch($arg, $post=''){  
        return (empty($post)) ? filter_input_array(INPUT_GET,$arg) : filter_input_array(INPUT_POST,$arg);
      }
      
      // записать значения из поисковика в сессию
      private function SearchSetSession($cat, $ar){
        foreach ($ar as $key => $value) {
          $_SESSION['search'][$cat][$key] = $value;
        }        
      }  
              
      // привести массив к строке для SQL запроса, пример IN ('1','2')
      private function ArrayToStr($arr){
        if (is_array($arr)){
          foreach ($arr as $value) {
            $check[] = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
          }
          $res = implode(', ', $check);
        }else{
          $res = filter_var($arr, FILTER_SANITIZE_NUMBER_INT);
        }
        return $res; 
      }
      
      // получение файла для вывода ссылок
      public function GetLinks($file){
        //$file = ROOT_DIR.'/array/car_links.php';
        file_exists($file) ? require $file : '';

        // страница объявления вверху
        if (isset($car_links_top)){
          foreach ($car_links_top as $key => $value){
            $res['car_links_top'][$key] = $car_links_top[$key][array_rand($value)];
          }
        }

        // страница объявления под фотографиями
        if (isset($car_links_under_photo)){
          foreach ($car_links_under_photo as $key => $value){
            $res['car_links_under_photo'][$key] = $car_links_under_photo[$key][array_rand($value)];
          }
        }

        // страница объявления под опциями
        if (isset($car_links_under_options)){
          foreach ($car_links_under_options as $key => $value){
            $res['car_links_under_options'][$key] = $car_links_under_options[$key][array_rand($value)];
          }
        }

        // страница объявления внизу
        if (isset($car_links_bot)){
          foreach ($car_links_bot as $key => $value){
            $res['car_links_bot'][$key] = $car_links_bot[$key][array_rand($value)];
          }
        }
        
        return $res;
      }
      
      // проверить данные для вывода на экран страницы объявления
      public function CheckData($arg) {
        if (isset($arg['2']) && isset($arg['3']) && isset($arg['4'])){
          $ar['id'] = filter_var($arg['4'], FILTER_SANITIZE_STRING);
          $ar['url1'] = urldecode(filter_var($arg['2'], FILTER_SANITIZE_STRING));
          $ar['url2'] = urldecode(filter_var($arg['3'], FILTER_SANITIZE_STRING));
          // получить данные по объявлению
          return $this->GetAdv($ar);
        }else{
          $this->ErrorPageAdv($ar);
        }
      }
      
      // данные для страницы объявления
      public function GetAdv($ar) {
        print_r($this->GetPSelectSqlPage('cat','7778890'));
        
      }
      
      private function GetPSelectSqlPage($cat,$id){
        $sql = 'SELECT
                `a`.`id_cat`, 
                `a`.`id_sub`, 
                `a`.`login`,
                `a`.`marka`,
                `a`.`model`,
				`a`.`version_name`,
				`a`.`run_value`,
                `a`.`run_unit`,                        
				`a`.`transmission`,
                `a`.`condition`,
				`a`.`year`,
                `a`.`engine_value`,
                `a`.`engine`,
				`a`.`a_body`,
                `a`.`weight`,
                `a`.`value_furgon`,
				`a`.`price_value`,
                `a`.`currency_id`,
				`a`.`text`,	
                `a`.`country`,	
                `a`.`town`,	
                `a`.`town_other`,
				`a`.`seller`,
                `a`.`email`,
				`a`.`code1`,
                `a`.`code2`,
                `a`.`code3`,
				`a`.`phone1`,
                `a`.`phone2`,
                `a`.`phone3`,
				`a`.`datetime`,
                `a`.`nocache`,
				`a`.`pokazov`,
                `a`.`link`,
                `a`.`status`
              FROM '.$this->table_adv.' AS `a`
              LEFT JOIN `'.$this->table_adv_marks.'` AS `mar` ON `a`.`marka` = `mar`.`marka_id`
              LEFT JOIN `'.$this->table_adv_models.'` AS `mod` ON `a`.`model` = `mod`.`model_id`
              LEFT JOIN `'.$this->table_adv_trans.'` AS `tr` ON `a`.`transmission` = `tr`.`id`
              LEFT JOIN `'.$this->table_adv_eng.'` AS `e` ON `a`.`engine` = `e`.`engine_id`
              LEFT JOIN `'.$this->table_country.'` AS `country` ON `a`.`country` = `country`.`country_id`
              LEFT JOIN `'.$this->table_city.'` AS `region` ON `a`.`town` = `region`.`city_id` 
              WHERE `a`.`id` = \''.$id.'\' 
          ';
          return $this->db->Sql($sql);
        
      } 




      // если нет ID объявления в БД, то выводит сообщение об отсутствии объявления
      public function ErrorPageAdv($ar){
        // если нет ID в базе, то выводим специальную страницу
        $quary = $this->GetUrl1Url2ID($ar);
        if (isset($quary['0'])){
          $res['main'] = $quary['0'];
          $res['main']['marka_url'] = $ar['url1'];
          $res['main']['model_url'] = $ar['url2'];  
          $res['main']['adv_id'] = $ar['id'];
          $res['main']['user'] = '';
          $res['main']['title'] = 'Не найдено объявление';
          $res['main']['description'] = 'Объявление № '.$ar['id'].' не найдено в базе сайта. Возможно оно удалено и восстановить его невозможно.';
          $res['main']['year'] = '';    
          $res['error'] = TRUE;
          $res['count_s'] = 16;
          $res['count_v'] = 12;
          // type for body
          $this->smarty->assign('body_classes','product-404');
        }else{
          header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
          header('Location: '.STATIC_WWW.'/new/error/');
          die();
        }
      }  
      
      // получить из ссылок url1 и url2
      private function GetUrl1Url2ID($ar){
        $sql = 'SELECT `mar`.`marka_id`, `mar`.`name_eng` AS `marka`, `mod`.`model_id`, `mod`.`name_eng` AS `model`
                FROM `'.$this->table_adv_marks.'` AS `mar` 
                LEFT JOIN `'.$this->table_adv_models.'` AS `mod` ON `mod`.`marka_id` = `mar`.`marka_id`
                WHERE `mar`.`name_eng` = \''.$ar['url1'].'\' AND `mod`.`name_eng` = \''.$ar['url2'].'\'';
        return $this->db->Sql($sql);
      }
  
   }  


