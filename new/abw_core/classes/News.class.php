<?php

class News extends Core
{
  private $lock_table = 'a_news_lock';
  private $log_table = 'a_news_log';

  public function __construct($_db)
  {
    parent::__construct($_db);
  }

  public function lockEditingNews($id)
  {
    $check = $this->lockCheckNews($id);
    if (!$check)
    {
      $user_id = $this->sess['user']['user_id'];      
      if ($user_id>0){
        $result = $this->db->Insert(
          $this->lock_table,
          array(
            'news_id' => $id,
            'user_id' => $user_id,
            'end_lock' => date('Y-m-d H:i:s', strtotime('+30 min'))
          )
        );
        $this->insertLogNews($id,2,$user_id);
      }  
    }
    else
    {
      if ($check[$id]['user_id'] == $this->sess['user']['user_id'])
      {
        $this->lockUpdateNews($id);
      }
    }

    return (isset($result) || $check) ? true : false;
  }

  public function unlockEditingNews($id)
  {
    $result = $this->db->Delete(
      $this->lock_table,
      array(
        'news_id' => $id
      )
    );    
    return $result;
  }

  public function lockCheckNews($id)
  {
    $news_lock_status = $this->getStatusNews($id);
    return $news_lock_status;
  }

   public function lockCheckNews2($id)
  {
    $news_lock_status = $this->getStatusNews($id);
    if ($news_lock_status[$id]['end_lock'] < date('Y-m-d H:i:s'))
    {
      $this->unlockEditingNews($id);
      return false;
    }
    else
    {
      return $news_lock_status;
    }
  }
  
  function lockUpdateNews($id)
  {
    $user_id = $this->sess['user']['user_id'];      
    if ($user_id>0){
      $result = $this->db->Update(
        $this->lock_table,
        array(
          'end_lock' => date('Y-m-d H:i:s', strtotime('+30 min'))
        ),
        array(
          'news_id' => $id,
          'user_id' => $user_id
        )
      );
      $this->insertLogNews($id,2,$user_id);
    }else{
      $this->unlockEditingNews($id);
    }
  }

  public function getStatusNews($id, $one = false)
  {
    $news_id = (is_array($id) && count($id) > 0) ? 'IN ('.implode(",", $id).')' : '= "'.$id.'"';
    $where = 'news_id '.$news_id;
    $data = $this->db->Select(
      $this->lock_table,
      array(
        'where' => $where
      )
    );

    $result = array();
    foreach ($data as $k => $v)
    {
      $result[$v['news_id']] = $v;
      $result[$v['news_id']]['login'] = $this->getUser($v['user_id'], 'user_id', 'login');
    }
    return (count($result) > 0) ? $result : false;
  }
  
  public function insertLogNews($id,$status,$user_id)
  {
    $this->db->Insert(
    $this->log_table,
    array(
      'news_id' => $id,
      'user_id' => $user_id,
      'status' => $status,
      'date' => date('Y-m-d H:i:s')
      )
    );
  }
  
  public function getLogNews($id)
  {
    //статус
    $sel_logs = array(
      '1' => 'создание',
      '2' => 'открытие для редактирования',
      '3' => 'удаление',
      '4' => 'выход без сохранения',
      '5' => 'сохранение редактирования',
      '6' => 'скрытие',
      '7' => 'открытие с изменением даты'
    );
    
    //пользователи 

    $logs = $this->db->Select(
    $this->log_table,
    array(
      'select' => '*',
      'where' => array('news_id' => (int)$id),
      'order' => array('DESC' => 'date')
     )
    );
    $result = array();
    foreach ($logs as $v)
    {
      $v['date'] = date('d-m-Y в H:i:s',strtotime($v['date']));
      $v['status'] = ($sel_logs[$v['status']]);
      $v['user'] = $this->getUser($v['user_id'], 'user_id', 'login');
      $result[]= $v;
    }
    return $result;
    
  } 

  function getAllAuthorsNews()
  {
    $data = $this->db->Select(
      'a_news_author'
    );
    $result = array();
    foreach ($data as $k => $v)
    {
      $result[$v['id']] = $v['author'];
    }

    return $result;
  }

  function getOneNews($id)
  {
    return $this->db->SelectOne(
      'a_news',
      array(
        'select' => '*',
        'where' => array('news_id' => (int)$id)
      )
    );
  }

  function getAllCategory(){
    return $this->db->Select(
        'a_news_cat',
        array(
          'select' => 'cat_id,cat_name',
          'order' => array('DESC' => 'cat_name')
        )
      );
  }
      
  function getCategory($id='')
  {
    if ($id){
      $res = $this->db->SelectOne(
        'a_news_cat',
        array(
          'select' => '*',
          'where' => array('cat_id' => $id),
          'order' => array('DESC' => 'cat_name')
        )
      );
    }
    if (empty($res)){  
      $res = [
        's_title' => 'Автомобильные новости Беларуси | Новости ABW.BY',
        's_desc' => 'Ежедневные автомобильные новости на ABW.BY. Будьте в курсе всех автомобильных событий вместе с нами',
        's_keyw' => 'новости, автоновости, abw.by, автобизнес',
        'cat_name' =>''
      ];
    }
    return $res;
  }
  
  function getAllNews($arr)
  {   
    $where = '`n`.`hide` = \'0\'';
    $where .= (isset($arr['where'])) ? $arr['where'] : '';
    $where .= (isset($arr['idcat'])) ? ' AND `c`.`cat_id` ='.$arr['idcat'].'' : '';
    $where .= (isset($arr['idauthor'])) ? ' AND `n`.`author` ='.$arr['idauthor'].'' : '';
    $where .= (isset($arr['idtag'])) ? ' AND (`n`.`cat_id2` ='.$arr['idtag'].' OR `n`.`cat_id3` ='.$arr['idtag'].')' : '';
    $where .= (isset($arr['marakid'])) ? ' AND `m`.`make_id` =\''.$arr['marakid'].'\'' : '';
    $where .= (isset($arr['modelid'])) ? ' AND `m`.`model_id` =\''.$arr['modelid'].'\'' : '';
    $where .= (isset($arr['word'])) ? ' AND `n`.`text like`\'%'.$arr['word'].'%\'' : '';    
    $where .= (isset($arr['top'])) ? ' AND `n`.`top_glavn` =\'1\'' : '';
    
    if (isset($arr['date'])){
      if ($arr['date']=='day'){
        $date = time()-86400;
      }elseif ($arr['date']=='week'){
        $date = time()-604800;
      }elseif ($arr['date']=='month'){
        $date = time()-2592000;
      }elseif ($arr['date']=='year'){
        $date = time()-31104000;        
      }
    }
    
    $where .= (isset($date)) ? ' AND n.date_add2 >=\''.$date.'\'' : '';
    $join = (isset($arr['join'])) ? $arr['join'] : '';
    $join .= (isset($arr['marakid']) || isset($arr['modelid'])) ? 'LEFT JOIN `a_news_cars` AS `m` ON `n`.`news_id`=`m`.`news_id` ' : '';     
    $select = (isset($arr['select'])) ? $arr['select'] : 'n.*,c.cat_name';
    $order = (isset($arr['sort'])) ? $arr['sort'] : '`n`.`date_add2` DESC';
    $limit = (isset($arr['from']) && isset($arr['onpage'])) ? 'LIMIT '.$arr['from'].','.$arr['onpage'].'' : '';
    $sql = 'SELECT
            '.$select.'
          FROM
            `a_news` AS `n`
          LEFT JOIN
            `a_news_cat` AS `c` ON `n`.`cat_id`=`c`.`cat_id` 
          '.$join.'
          WHERE
           '.$where.'
          ORDER BY '.$order.' 
          '.$limit.'
          ';    
    return $this->db->sql($sql);
  }
    
  function getPreviousNews($id)
  {
    $news = $this->db->sql('SELECT `news_id`,`name` FROM `a_news` WHERE `news_id` < '.$id.' AND hide = 0 ORDER BY `news_id` DESC LIMIT 0,1');
    if (isset($news['0'])) {return $news['0'];}
  }
  
  function getNextNews($id)
  {
     $news = $this->db->sql('SELECT `news_id`,`name` FROM `a_news` WHERE `news_id` > '.$id.' AND hide = 0 ORDER BY `news_id` ASC LIMIT 0,1');
     if (isset($news['0'])) {return $news['0'];}
  }
  
  function getUrlCat($id){
    return $this->db->SelectOne(
      'a_news_cat',
      array(
        'select' => '*',
        'where' => array('cat_id' => (int)$id)
      )
    );
  }
  
  function ErrorPage404($arr){
    header('HTTP/1.1 404 Not Found');
    header('Status: 404 Not Found');
    $arr['news'] = $this->getAllNews($arr);
    $arr['error'] = 1;    
    return $arr; 
  }

}
// END News Class