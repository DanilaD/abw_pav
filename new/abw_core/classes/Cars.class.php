  <?

    class Cars extends Core
    {
      private $table_marks_truck = 'a_adv_marka';
      private $table_models_truck = 'a_adv_model'; 
      private $table_body = 'a_body';
      private $table_colour = 'a_colour';
      private $table_city = 'a_city';
      private $table_city2 = 'a_city2';
      private $table_country = 'a_country';   
      private $table_condition = 'a_condition';    
      private $table_drive_unit = 'a_drive_unit';  
      private $table_eng = 'a_engine';
      private $table_marks = 'a_marka';
      private $table_models = 'a_model'; 
      private $table_probeg = 'a_probeg_type';
      private $table = 'a_public';
      private $table_up = 'a_public_up';
      private $table_newspaper = 'a_public_newspaper';
      private $table_shows = 'a_public_shows';
      private $table_trans = 'a_transmission';
      private $table_base = 'a_base';
      private $table_base_ax = 'a_base_ax';
      private $table_lider = 'a_lider';
      private $table_lider_hist = 'a_lider_hist';




      private $shina_profile = 'a_adv_shina_profile';
      private $shina_season = 'a_adv_shina_season';
      private $shina_size = 'a_adv_shina_size';
      private $shina_width = 'a_adv_shina_width';



      private $vip_car_file = 'vip_car.php'; 

      public $per_page = 30;
      
      private $delete_marks = '118, 116, 132, 30, 33, 34, 52, 124, 153, 154, 165';



      public function __construct($_db)
      {
        parent::__construct($_db);
      }

      public function SearchCar($in = '') {
        $arg =[
          'type' => FILTER_VALIDATE_INT,
          'marka' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'model' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'engine' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'transm' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'capacity1' => FILTER_VALIDATE_INT,
          'capacity2' => FILTER_VALIDATE_INT,
          'mileage1' => FILTER_VALIDATE_INT,
          'mileage2' => FILTER_VALIDATE_INT,
          'year1' => FILTER_VALIDATE_INT,
          'year2' => FILTER_VALIDATE_INT,
          'price1' => FILTER_VALIDATE_INT,
          'price2' => FILTER_VALIDATE_INT,
          'body' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'country' => FILTER_VALIDATE_INT,
          'rigion' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'city' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'wheel' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          'condition' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY],
          /*'seller' => ['filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY], */
          'text' => FILTER_SANITIZE_STRING,
          'private' => FILTER_VALIDATE_INT,
          'companies' => FILTER_VALIDATE_INT,
          'toorder' => FILTER_VALIDATE_INT,
          'day' => FILTER_VALIDATE_INT,
          'sort' => FILTER_VALIDATE_INT,
        ];
        
        if (isset($in['post'])){
          $ar = filter_input_array(INPUT_POST,$arg);
        }else{
          $ar = filter_input_array(INPUT_GET,$arg);
          // special for model
          $ar['model_str'] = (empty($ar['model'])) ? '' : implode(',', $ar['model']); 
          // special for region
          $ar['rigion_str'] = (empty($ar['rigion'])) ? '' : implode(',', $ar['rigion']); 
          // record session for search
          $this->SetSessionSearch($ar);
        }
        
        // покупка продажа
        isset($in['type']) ? $ar['type'] == $in['type'] : '';
        
        // push data
        return $this->GetSqlSearch($ar);      
      }
      
      private function GetSqlSearch($ar){
        // parametrs for search
        $w[] = '`car`.`hide` = \'0\' AND `car`.`marka` NOT IN ('.$this->delete_marks.')';
        empty($ar['type']) ? '' : $w[] = '`car`.`adv_type` = \''.$ar['type'].'\'';
        empty($ar['marka']) ? '' : $w[] = '`car`.`marka` IN ('.$this->ArrayToStr($ar['marka']).')';
        empty($ar['model']) ? '' : $w[] = '`car`.`model` IN ('.$this->ArrayToStr($ar['model']).')';
        empty($ar['year1']) ? '' : $w[] = '`car`.`year` >= \''.$ar['year1'].'\'';
        empty($ar['year2']) ? '' : $w[] = '`car`.`year` <= \''.$ar['year2'].'\'';
        empty($ar['mileage1']) ? '' : $w[] = '`car`.`probeg_col` >= \''.$ar['mileage1'].'\'';
        empty($ar['mileage2']) ? '' : $w[] = '`car`.`probeg_col` <= \''.$ar['mileage2'].'\'';
        empty($ar['transm']) ? '' : $w[] = '`car`.`transmission` IN ('.$this->ArrayToStr($ar['transm']).')';
        empty($ar['wheel']) ? '' : $w[] = '`car`.`privod` IN ('.$this->ArrayToStr($ar['wheel']).')';
        // engine
        if (!empty($ar['engine'])){
          $sql = 'SELECT `engine_id` FROM `'.$this->table_eng.'` WHERE `tipe` IN ('.$this->ArrayToStr($ar['engine']).')';
          $engine = $this->db->Sql($sql);
          if (sizeof($engine) > 0){
            foreach ($engine AS $value) {
              $engine_id[] = $value['engine_id'] ;
            }
            unset($value);
            $w[] = '`car`.`type_engine` IN ('.$this->ArrayToStr($engine_id).')';
          }
        }
        empty($ar['capacity1']) ? '' : $w[] = '`car`.`volume` >= \''.$ar['capacity1'].'\'';
        empty($ar['capacity2']) ? '' : $w[] = '`car`.`volume` <= \''.$ar['capacity2'].'\'';  
        empty($ar['body']) ? '' : $w[] = '`car`.`type_body` IN ('.$this->ArrayToStr($ar['body']).')';
        empty($ar['country']) ? '' : $w[] = '`car`.`u_country` = \''.$ar['country'].'\'';
        empty($ar['rigion']) ? '' : $w[] = '`car`.`u_city` IN ('.$this->ArrayToStr($ar['rigion']).')';
        empty($ar['city']) ? '' : $w[] = '`car`.`u_city_o` IN ('.$this->ArrayToStr($ar['city']).')';
        empty($ar['price1']) ? '' : $w[] = '`car`.`cost_val_d` >= \''.$ar['price1'].'\'';
        empty($ar['price2']) ? '' : $w[] = '`car`.`cost_val_d` <= \''.$ar['price2'].'\'';
        // seller
        /*
        if (!empty($ar['seller'])){
          foreach ($ar['seller'] AS $value){
            ($value == '1') ? $s[] = '`car`.`status` IN (0,1,2,3,4,5,6,7)' : '';
            ($value == '2') ? $s[] = '`car`.`status` = \'8\'' : '';
            ($value == '3') ? $s[] = '`car`.`status` = \'9\'' : '';
            ($value == '4') ? $s[] = '`car`.`user` like \'newspaper\'' : '';
            ($value == '5') ? $s[] = '`car`.`user` like \'auto.ru\'' : '';
            ($value == '6') ? $s[] = '`car`.`user` like \'vostochni\'' : '';
          }
          $w[] = implode(' OR ', $s);
        }
         */
        empty($ar['private']) ? '' : $w[] = '`car`.`status` NOT IN (8,9)';
        empty($ar['companies']) ? '' : $w[] = '`car`.`status` IN (8,9)';
        //empty($ar['seller']) ? '' : $w[] = '`car`.`status` IN ('.$this->ArrayToStr($ar['seller']).')';
        empty($ar['condition']) ? '' : $w[] = '`car`.`na_rf` IN ('.$this->ArrayToStr($ar['condition']).')';
        empty($ar['text']) ? '' : $w[] = '`car`.`u_info` LIKE \'%'.$ar['text'].'%\'';
        empty($ar['day']) ? '' : $w[] = $this->GetDayLimit($ar['day']);
        empty($ar['sort']) ? '' : $data['order'] = $this->GetSort($ar['sort']);
        $data['where'] = implode(' AND ', $w);        
        return $data;        
      }
      
      private function GetDayLimit($day) {
        return '`car`.`add_time2` > NOW() - INTERVAL '.$day.' DAY'; 
      }
      
      private function GetSort($sort) {
        switch ($sort) {
        // 1- day
          case '1':
            $res = '`car`.`add_time3` DESC';
          break;
          case '4':
            $res = '`car`.`add_time3` ASC';
          break;
        // 2- cost
          case '2':
            $res = '`car`.`cost_val_d` ASC';
          break;
          case '5':
            $res = '`car`.`cost_val_d` DESC';
          break;
        // 3 -year
          case '3':
            $res = '`car`.`year` DESC';
          break;
          case '6':
            $res = '`car`.`year` ASC';
          break;
        // default
          default:
            $res ='`up`.`last_time_up` DESC';
        }
        return $res;
      }
      
      private function SetSessionSearch($data){ 
        foreach ($data as $key => $value) {
          $_SESSION['search']['car'][$key] = $value;
        }                
      }

      public function Pagination($arg,$data) {
        // get number of page  
          $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);

        // получить общее количество строк
          $rows = $this->GetAllCars($data, 'count');
          $total_rows = (isset($rows['0']['count'])) ? $rows['0']['count'] : 0;

        // получить количество страниц
          $pages['total'] = ceil($total_rows/$this->per_page);

        // исключить отрицательные страницы и 0
          $page = (empty($page) || ($page) < 1) ? 1 : $page;

        // исключить превышение страниц
          $page = ((!empty($pages['total'])) && ($page > $pages['total'])) ? $pages['total'] : $page;
          
        // текущая страница
          $pages['current'] = $page;
          
        // для SQL запроса
          $pages['sql'] = ($page == '1') ? 0 : ($page - 1) * $this->per_page;

        // delete parameters for paging
          unset($arg['page']);
          $link = $this->getUrl($arg);
          
        //print_r(STATIC_WWW.$_SERVER['REQUEST_URI']);

        // ссылка на следующую страницу
          $pages['next'] = ($pages['current'] < $pages['total']) ? ''.$link.'page='.($pages['current']+1).'' : '';

        // ссылка на предыдущую страницу
          $pages['prev'] = ($pages['current'] > 1) ? ''.$link.'page='.($pages['current']-1).'' : '';

        return $pages;
      }
      
      public function LinksSortDate($arg) {
        
        // default
        $sort_d = '1';
        $class_d = '';
        $sort_p = '2';
        $class_p = '';
        $sort_y = '3';
        $class_y = '';
        
        switch ($arg['sort']) {
        // day
          case '1':
            $sort_d = '4';
            $class_d = 'arr-down';
          break;
          case '4':
            $sort_d = '1';
            $class_d = 'arr-up';
          break;
        // price
          case '2':
            $sort_p = '5';
            $class_p = 'arr-up';
          break;
          case '5':
            $sort_p = '2';
            $class_p = 'arr-down';
          break;
        // year
          case '3':
            $sort_y = '6';
            $class_y = 'arr-down';
          break;
          case '6':
            $sort_y = '3';
            $class_y = 'arr-up';
          break;
        }
                 
        // delete parameters for sort  
          unset($arg['sort']);  
        // delete parameters for limit
          //unset($arg['day']);          
        // delete page
          unset($arg['page']);  
        // get url
          $url = $this->getUrl($arg);
          
        // LIMIT DAYS         
        /*
        $sort['day'] = [
          ['value' => '', 'url' => $url, 'text' => 'Весь период'], // default
          ['value' => '1', 'url' => $url.'day=1', 'text' => 'за 1 день'], // 1 day
          ['value' => '3', 'url' => $url.'day=3', 'text' => 'за 3 дня'], // 3 days
          ['value' => '7', 'url' => $url.'day=7', 'text' => 'за неделю'], // 1 week
          ['value' => '14', 'url' => $url.'day=14', 'text' => 'за 2 недели'], // 2 weeks
          ['value' => '30', 'url' => $url.'day=30', 'text' => 'за месяц'], // 1 month
        ];
        */
          
        // SORT
        $sort['sort'] = [
          ['value' => '1', 'url' => $url.'sort='.$sort_d, 'text' => 'По дате', 'class' => $class_d], // date
          ['value' => '2', 'url' => $url.'sort='.$sort_p, 'text' => 'По цене', 'class' => $class_p], // price  
          ['value' => '3', 'url' => $url.'sort='.$sort_y, 'text' => 'По году выпуска', 'class' => $class_y], // year  
        ];
        
        return $sort;
      }
      
      public function getUrl($arg){
        // get all parametrs from url
          if (!isset($arg['search']) && empty($arg)){
            $link = '?';
          }else{          
        // create link
          foreach ($arg as $key => $value) {
            if (is_array($value)){
              foreach ($value as $k => $v) {
                  $link_array[] = $key.'[]='.$v;
              }
            }else{
              $link_array[] = $key.'='.$value;
            }
          }
          $link = '?'.implode('&', $link_array).'&';
        }
        return $link;
      }

      public function GetCar($adv_id,$marka='',$model=''){
        $where = '`p`.`adv_id` = \''.$adv_id.'\'';
        if (!empty($marka) && !empty($model)){
          $where .= ' AND `mar`.`name_eng` = \''.$marka.'\' AND `mod`.`name_eng` = \''.$model.'\''; 
        }

        $sql = 'SELECT
          `p`.`adv_id`,
          `p`.`user`,
          `p`.`adv_type`,
          `p`.`marka` AS `marka_id`,
          `p`.`model` AS `model_id`,
          `mar`.`name_eng` AS `marka`,
          `mod`.`name_eng` AS `model`,
          `p`.`version`,
          `p`.`year`,
          `p`.`probeg_col`,
          `p`.`hide`,
          `pr`.`name` AS `probeg_type`,
          `tr`.`name` AS `transmission`,
          `dr`.`name` AS `drive_unit`,
          `e`.`name` AS `engine`,
          `e`.`short` AS `short_engine`,
          `p`.`volume`,
          `b`.`name` AS `body`,
          `p`.`doors`,
          `p`.`cylinders`,
          `c`.`name` AS `color`,
          `p`.`cost_type`,
          `p`.`status`,
          `p`.`cost_val`,
          `p`.`u_country` AS `country_id`,
          `country`.`name` AS `country`,
          `country`.`code` AS `phone_code`,
          `region`.`name` AS `region`,
          `p`.`u_city_o` AS `city`,
          `p`.`u_name`,
          `p`.`u_mail`,
          `p`.`u_tel1`,
          `p`.`u_code1`,
          `p`.`u_tel1_time`,
          `p`.`u_tel2`,
          `p`.`u_code2`,
          `p`.`u_tel2_time`,
          `p`.`u_tel3`,
          `p`.`u_code3`,
          `p`.`u_tel3_time`,
          `p`.`u_info`,
          `p`.`photo`,
          `p`.`link`,
          `p`.`add_time`,
          `p`.`send_mail`,
          `p`.`add_time3`,
          `p`.`cost_val_d`,
          `p`.`na_rf`,
          `con`.`name` AS `condition`,
          `p`.`carrier_ids`,
          `sh`.`show`
          FROM `'.$this->table.'` AS `p` 
          LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `p`.`marka` = `mar`.`marka_id`
          LEFT JOIN `'.$this->table_models.'` AS `mod` ON `p`.`model` = `mod`.`model_id`
          LEFT JOIN `'.$this->table_shows.'` AS `sh` ON `p`.`adv_id`=`sh`.`pub_id`
          LEFT JOIN `'.$this->table_trans.'` AS `tr` ON `p`.`transmission`=`tr`.`trans_id`
          LEFT JOIN `'.$this->table_body.'` AS `b` ON `p`.`type_body`=`b`.`body_id`
          LEFT JOIN `'.$this->table_eng.'` AS `e` ON `p`.`type_engine`=`e`.`engine_id`
          LEFT JOIN `'.$this->table_colour.'` AS `c` ON `p`.`color`=`c`.`color_id`
          LEFT JOIN `'.$this->table_country.'` AS `country` ON `p`.`u_country`=`country`.`country_id`
          LEFT JOIN `'.$this->table_city.'` AS `region` ON `p`.`u_city`=`region`.`city_id` 
          LEFT JOIN `'.$this->table_probeg.'` AS `pr` ON `p`.`probeg_type`=`pr`.`id` 
          LEFT JOIN `'.$this->table_drive_unit.'` AS `dr` ON `p`.`privod`=`dr`.`id` 
          LEFT JOIN `'.$this->table_condition.'` AS `con` ON `p`.`na_rf`=`con`.`id`
          WHERE '.$where.'';
        return $this->db->Sql($sql);
      }

      public function GetMarkaModelID($marka,$model){
        $sql = 'SELECT `mar`.`marka_id`, `mar`.`name_eng` AS `marka`, `mod`.`model_id`, `mod`.`name_eng` AS `model`
                FROM `'.$this->table_marks.'` AS `mar` 
                LEFT JOIN `'.$this->table_models.'` AS `mod` ON `mod`.`marka_id` = `mar`.`marka_id`
                WHERE `mar`.`name_eng` = \''.$marka.'\' AND `mod`.`name_eng` = \''.$model.'\'';
        return $this->db->Sql($sql);
      }

      public function GetMarkaID($marka){
        return $this->db->SelectOne(
          $this->table_marks,
          array(
            'select' => 'marka_id, name_eng',
            'where' => array('name_eng' => $marka)
          )
        );
      }

      public function GetCountModelID($marka_id, $type){
        $count = ($type == '2') ? 'adv_type_2' : 'adv_type_1';
        return $this->db->Sql('SELECT `name_eng` AS `name`, `'.$count.'` AS count FROM '.$this->table_models.' WHERE `marka_id` = \''.$marka_id.'\' AND `'.$count.'` > \'0\' ORDER BY `name_eng`');
      }


      public function GetCarPhone($adv_id){   
        $sql = 'SELECT  `u_country`,`u_code1`,`u_tel1`,`u_code2`,`u_tel2`,`u_code3`,`u_tel3`,`adv_id`, `add_time3` FROM `a_public` WHERE `adv_id`=\''.$adv_id.'\'';
        $row = $this->db->Sql($sql);
        // если нет объявления
        if (empty($row)){
          $res = ['status'=>FALSE, 'text'=>'Не получается получить телефон'];
          return $res;
        }
        // код страны
        if ($row['0']['u_country'] == '1'){
          $row['0']['u_form'] = '+375';
        }elseif ($row['0']['u_country'] == '2'){
          $row['0']['u_form'] = '+7';
        }else{
          $row['0']['u_form'] = '';
        }

        // номера телефонов
        if ($row['0']['u_code1'] && $row['0']['u_tel1']){
          $phone[] = $row['0']['u_form'].' ('.$row['0']['u_code1'].') '.$row['0']['u_tel1'];
        }

        if ($row['0']['u_code2'] && $row['0']['u_tel2']){
          $phone[] = $row['0']['u_form'].' ('.$row['0']['u_code2'].') '.$row['0']['u_tel2'];
        }

        if ($row['0']['u_code3'] && $row['0']['u_tel3']){
          $phone[] = $row['0']['u_form'].' ('.$row['0']['u_code3'].') '.$row['0']['u_tel3'];
        }
        $res = ['status'=>TRUE, 'text'=>$phone];

        // записать запрос в БД
        $this->WriteShowPhone($row['0']);

        return $res;
      }

      private function WriteShowPhone($data){
        $sql = 'INSERT INTO a_public_show_phone(`adv_id`,`create_adv`,`user_id`,`ip`,`time`,`ip2long`) VALUES (\''.$data['adv_id'].'\',\''.date("Y-m-d H:i:s",$data['add_time3']).'\',\''.@$_SESSION['user']['user_id'].'\',\''.@$_SERVER['REMOTE_ADDR'].'\',\''.time().'\',\''.ip2long($_SERVER['REMOTE_ADDR']).'\')';
        $this->db->Sql($sql);
      }


      public function GetVipCar($_db){
        // CAR
        $sql = '
          SELECT
            `p`.`adv_id`, 
            `p`.`adv_type`,
            `mar`.`name_eng` AS `marka`, 
            `mod`.`name_eng` AS `model`, 
            `p`.`version`, 
            `p`.`year`,
            `p`.`probeg_col`, 
            `p`.`probeg_type`, 
            `p`.`volume`, 
            `p`.`cost_val`, 
            `p`.`cost_type`, 
            `tr`.`name` AS `trans`, 
            `eng`.`short` AS `engine`,
            `lid`.`curr_bets`
          FROM `'.$this->table_lider.'` AS `lid`
          LEFT JOIN `'.$this->table.'` AS `p` ON `p`.`adv_id` = `lid`.`pub_id`
          LEFT JOIN `'.$this->table_eng.'` AS `eng` ON `eng`.`engine_id` = `p`.`type_engine`
          LEFT JOIN `'.$this->table_trans.'` AS `tr` ON `p`.`transmission` = `tr`.`trans_id`  
          LEFT JOIN `'.$this->table_models.'` AS `mod` ON `p`.`model` = `mod`.`model_id`
          LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `p`.`marka` = `mar`.`marka_id`
          WHERE 
            `p`.`adv_id` IS NOT NULL
          AND
            `p`.`hide` = \'0\'
          AND
            `lid`.`curr_bets` > \'0\' OR
          ORDER BY
            `lid`.`curr_bets` DESC
          LIMIT 5
        ';
        return $_db->sql($sql);
      }

      public function SaveVipCar($arr) {      
        $res = empty($arr) ? '' : $this->arr2str($arr, '', '$vip_car');
        $content = "<?\n";
        $content .= $res;
        $this->SaveFile(ROOT_DIR.'/array/'.$this->vip_car_file,$content);
      }

      public function GetOptionsForCar($adv_id){
        $sql = 'SELECT `o`.`name` FROM  `a_opt_sel` AS `s`, `a_options` AS `o` WHERE `s`.`public_id` = \''.$adv_id.'\' AND `s`.`opt_id` = `o`.`opt_id` ORDER BY `o`.`order`';
        return $this->db->Sql($sql);
      }

      public function GetViewsCArs($adv_id) {
        $sql = 'INSERT INTO `'.$this->table_shows.'` SET `show` = 1, `pub_id` = '.$adv_id.' ON DUPLICATE KEY UPDATE `show` = `show` + 1';
        $this->db->sql($sql);

        $sql = 'SELECT `show` FROM `'.$this->table_shows.'` WHERE `pub_id` = '.$adv_id.'';
        return $this->db->sql($sql)['0']['show'];
      }

      public function PlayYoutube($url){
        return (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) ? $match['1'] : '';
      }

      public function DisplayAdv($car,$option=''){      

        //title
        $title_name = ['marka','model','version','year','city'];
        $title_res = $this->GetArrayData($title_name,$car);
        $title = implode(' ', $title_res);

        //description
        $description_name = ['volume','short_engine','transmission','probeg_col','probeg_type'];
        $description_res = $this->GetArrayData($description_name,$car);

        // short value
        !empty($description_res['volume']) ? $description_res2['engine'] = $this->VolumeEngine($description_res['volume']).$description_res['short_engine'] : '';
        !empty($description_res['transmission']) ? $description_res2['transmission'] = $description_res['transmission'] : '';
        $description_res2['probeg'] = !empty($description_res['probeg_col']) ? $description_res['probeg_col'].' т.'.$description_res['probeg_type'] : '';
        $description = empty($description_res2) ? '' : implode(', ', $description_res2);

        // subscription
        $subscription = '';
        if ($car['adv_type']=='1' && !empty($car['marka_id']) && !empty($car['model_id']) && !empty($car['year'])){
          $subscription = ['text'=>'Подписаться на объявления '.$car['marka'].' '.$car['model'].' '.$car['year'],'link'=>'https://www.abw.by/index.php?act=mailing&marka_id='.$car['marka_id'].'&model_id='.$car['model_id'].'&year='.$car['year']];
        }

        // phone
        $phone['country'] = $car['phone_code'];
        //$operator = $this->getPhoneCarriers($car['carrier_ids']);
        /*
        for($i=1;$i<=3;$i++){
          if (!empty($car['u_tel'.$i]) || !empty($car['u_code'.$i])){
            $phone[] = [
              'mobile' => $car['phone_code'].' '.$car['u_code'.$i].' '.$car['u_tel'.$i],
              'operator' => $operator[$i-1]['logo_url'],
              'time' => $car['u_tel'.$i.'_time']
            ];          
          }
        }
        */

        // send mail
        $is_mail = (empty($car['u_mail']) || $car['send_mail']=='1') ? '0'  : '1';

        // location
        $city_array = ['Минск','Брест','Витебск','Гродно','Гомель','Могилев'];
        (!empty($car['city'])) ? $location_pr['city'] = $car['city'] : '';
        (!empty($car['region']) && !empty($car['city']) && !in_array($car['city'], $city_array)) ? $location_pr['region'] = $car['region'] : '';
        ($car['country_id'] == '1' || empty($car['country_id'])) ? '' : $location_pr['country'] = $car['country']; 
        $user['place'] = empty($location_pr) ? '' : implode(', ', $location_pr);
        unset($location_pr);
        
        // display name of autohouse
        $user['name'] = (isset($car['autohouse']['title'])) ? $car['autohouse']['title'] : $car['u_name'];

        //price
        $price = ($car['cost_val'] > 0) ? $this->multiPrice($car['cost_val'], $car['cost_type']) : '';

        //microlizing for photo
        $is_microlising = (in_array($car['status'], ['0','2']) && $car['year'] >= '2007' && $car['cost_val_d'] >= '7000') ? TRUE : FALSE;
      
        // date for adv
        $add_time = show_rus_date3($car['add_time3']);

        // data
        $data = [
          'Год выпуска' =>$car['year'],
          'Состояние' =>$car['condition'],
          'Пробег' =>$description_res2['probeg'],
          'Двигатель' =>$car['engine'],
          'Объём' =>empty($car['volume'])?'':$car['volume']." см&#179;",
          'Цилиндров' =>$car['cylinders'],
          'Трансмиссия' =>$car['transmission'],
          'Кузов' =>$car['body'],
          'Дверей' =>$car['doors'],
          'Привод' =>$car['drive_unit'],
          'Цвет' =>$car['color'],
        ];

        $data = array_diff($data,array('')); 

        $main = [
          'adv_id' => $car['adv_id'],
          'user' => $car['user'],
          'adv_type' => $car['adv_type'],
          'model_id' => $car['model_id'],
          'model_url' => urlencode(mb_strtolower($car['model'])),
          'marka_url' => urlencode(mb_strtolower($car['marka'])),
          'model' => $car['model'],
          'marka' => $car['marka'],
          'year' => $car['year'],
          'user' => $car['user'],
          'hide' => $car['hide'],
          'title' => $title,
          'date' => $add_time,
          'views' => $car['show'],
          'description' => $description,
          'text' => $car['u_info'],
          'is_microlising' => $is_microlising,
          'url' => $car['url']
        ];

        // for display
        $res = [
          'main' => $main,
          'subscription' => $subscription,
          'user' => $user,
          'phone' => $phone,
          'options' => $option,
          'data' => $data,        
          'is_mail' => $is_mail,
          'price' => $price,
          'photos' => $car['photos'],
          'youtube' => $car['youtube'],
          'show' => TRUE,
          'count_s' => 4,
          'count_v' => 4,  
        ];

        // autohouse
        $res['autohouse'] = (isset($car['autohouse'])) ? $car['autohouse'] : '';    

        return $res;
      }
      
      public function GetAllCars($data, $type = '') { 

        // default
          $where = empty($data['where']) ? '`car`.`hide` = \'0\' AND `car`.`marka` NOT IN ('.$this->delete_marks.')' : $data['where'];
          
          if (empty($data['limit'])){
            $limit = (isset($data['page'])) ? 'LIMIT '.$data['page'].','.$this->per_page.'' : 'LIMIT 0,'.$this->per_page.''; 
          }else{
            $limit = $data['limit'];  
          }
          
          $order = '`car`.`code` = \'06\' DESC, `car`.`u_country` ASC, `car`.`add_time3` DESC';
          
        // check marka model
         
        if (isset($data['type'])){
          // page model
          if ($data['type'] == 'model'){
            $where .= ' AND `car`.`marka` = \''.$data['marka'].'\' AND `car`.`model` = \''.$data['model'].'\' AND `car`.`adv_type` = \''.$data['adv_type'].'\'';  
          // page marka
          }elseif ($data['type'] == 'marka'){
            $where .= ' AND `car`.`marka` = \''.$data['marka'].'\' AND `car`.`adv_type` = \''.$data['adv_type'].'\'';
          // page sell buy
          }elseif ($data['type'] == 'all'){
            $where .= ' AND `car`.`adv_type` = \''.$data['adv_type'].'\'';            
          }
        }

        // order
        (isset($data['order'])) ? $order = $data['order'] : '';
        if (empty($type)){
          $sql = 'SELECT 
                    `car`.`adv_id`, 
                    `car`.`adv_type`, 
                    `car`.`user`,
                    `car`.`status`,
                    `car`.`code`,
                    `mar`.`name_eng` AS `marka`, 
                    `mod`.`name_eng` AS `model`, 
                    `car`.`version`,
                    `car`.`u_info` AS `desc`,
                    `car`.`year`,
                    `car`.`probeg_col`, 
                    `car`.`probeg_type`, 
                    `car`.`volume`, 
                    `car`.`cost_val`,
                    `car`.`add_time3`,
                    `car`.`cost_type`,
                    `car`.`cost_val_d`,
                    `car`.`cost_val_pred`,
                    `car`.`na_rf`,
                    `show`.`show`, 
                    `tr`.`name` AS `trans`, 
                    `eng`.`short` AS `engine`, 
                    `up`.`last_time_up`, 
                    `car`.`u_country` AS `country_id`,
                    `country`.`name` AS `country`,
                    `region`.`name` AS `region`,
                    `car`.`u_city_o` AS `city`,
                    `con`.`name` AS `condition`
                  FROM `'.$this->table.'` AS `car`
                  LEFT JOIN `'.$this->table_up.'` AS `up` ON `up`.`adv_id` = `car`.`adv_id`
                  LEFT JOIN `'.$this->table_shows.'` AS `show` ON `up`.`adv_id` = `show`.`pub_id`              
                  LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `car`.`marka` = `mar`.`marka_id` 
                  LEFT JOIN `'.$this->table_models.'` AS `mod` ON `car`.`model` = `mod`.`model_id`
                  LEFT JOIN `'.$this->table_trans.'` AS `tr` ON `car`.`transmission` = `tr`.`trans_id`  
                  LEFT JOIN `'.$this->table_eng.'` AS `eng` ON `car`.`type_engine` = `eng`.`engine_id`
                  LEFT JOIN `'.$this->table_country.'` AS `country` ON `car`.`u_country`=`country`.`country_id`
                  LEFT JOIN `'.$this->table_city.'` AS `region` ON `car`.`u_city`=`region`.`city_id`
                  LEFT JOIN `'.$this->table_condition.'` AS `con` ON `car`.`na_rf`=`con`.`id`
                  WHERE '.$where.'
                  ORDER BY '.$order.' 
                  '.$limit.'';
        }elseif ($type == 'getid'){
          $sql = 'SELECT 
                    `car`.`adv_id` 
                  FROM `'.$this->table.'` AS `car`
                  WHERE '.$where.'
                  ORDER BY '.$order.' 
                  '.$limit.'';
        }elseif ($type == 'count'){
          $sql = 'SELECT 
                  COUNT(`adv_id`) AS `count` 
                  FROM `'.$this->table.'` AS `car`
                  WHERE '.$where.'
                  ';
        }        

        return $this->db->Sql($sql);
      }

      public function GetUrlCar($adv_id){      
        $sql = 'SELECT
          `p`.`adv_id`,
          `p`.`adv_type`,
          `mar`.`name_eng` AS `marka`,
          `mod`.`name_eng` AS `model`
          FROM `'.$this->table.'` AS `p` 
          LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `p`.`marka` = `mar`.`marka_id`
          LEFT JOIN `'.$this->table_models.'` AS `mod` ON `p`.`model` = `mod`.`model_id`
          WHERE `p`.`adv_id` = '.$adv_id.'';  
        $res = $this->db->Sql($sql);
        if (isset($res['0'])){
          return $this->GetLinksCar($res['0']['adv_type'],$res['0']['marka'],$res['0']['model'],$res['0']['adv_id']);
        }
      }

      public function GetUserId($login){
        return $lider = $this->db->SelectOne(
          $this->table_base,
          array(
            'select' => 'user_id',
            'where' => array('login' => $login)
          )
        );  
      }

      public function GetInfoAutohouse($login){

        $res = $this->db->SelectOne(
          $this->table_base,
          array(
            'select' => 'user_id, firstname, url, photo',
            'where' => array('login' => $login)
          )
        );    

        if (!empty($res)){
          $autohouse['count'] = $this->db->SelectCell( $this->table, array( 'select' => 'COUNT(adv_id)', 'where' => array('user' => $login) ) );
          if (!empty($autohouse['count'])){
            require_once ROOT_DIR.'/libs/getwordform_lib.php';
            // склоняем
            $getwordform = new getwordform();
            $forms = ['автомобиль','автомобиля','автомобилей'];
            $autohouse['count_word'] = $getwordform->getword($autohouse['count'], $forms);
          }else{
            $autohouse['count_word'] = '';
          }
          // логотип
          $dir_a = '/photos/diller_logo/';
          $autohouse['logo'] = file_exists(DIR.$dir_a.$res['photo']) ? STATIC_WWW.$dir_a.$res['photo'] : '';
          
          // брендирвоание
          $autohouse['brend'] = ($res['user_id'] == '2214720') ? STATIC_WWW.'/photos/diller_logo/autoru-logo.png' : '';
          
          //title
          $autohouse['title'] = $res['firstname'];

          // web site
          $autohouse['url'] = $res['url'];

          // link
          $autohouse['link'] = 'https://www.abw.by/saler-'.$res['user_id'].'/';

          // авто под заказ
          $autohouse['is_car_order'] = $this->GetInfoAutohouseCarToOrder($res['user_id']);       
          
          return $autohouse;
        }        
      }

      public function GetInfoAutohouseSmall($login){

        $res = $this->db->SelectOne(
          $this->table_base,
          array(
            'select' => 'user_id, firstname',
            'where' => array('login' => $login)
          )
        );    

        if (!empty($res)){

          //title
          $autohouse['title'] = $res['firstname'];

          // link
          $autohouse['link'] = 'https://www.abw.by/saler-'.$res['user_id'].'/';

          // авто под заказ
          $autohouse['is_car_order'] = $this->GetInfoAutohouseCarToOrder($res['user_id']);     
          
          // брендирвоание
          $autohouse['brend'] = ($res['user_id'] == '2214720') ? STATIC_WWW.'/photos/diller_logo/autoru-logo.png' : '';
          
          return $autohouse;
        }        
      }

      private function GetInfoAutohouseCarToOrder($user_id){
        $sql = 'SELECT `type` FROM `'.$this->table_base_ax.'` WHERE `user_id` = \''.$user_id.'\'';
        $row = $this->db->Sql($sql);
        return (isset($row['0']['type']) && $row['0']['type'] == '1') ? TRUE : FALSE;   
      }

      private function GetArrayData($arr,$data){
        $res='';
        foreach ($arr as $v) {  
          if (array_key_exists($v, $data)){
            ($data["$v"] != '0') ? $res["$v"] = $data["$v"] : '';
          }            
        }
        return $res;
      }

      //код оператора разобрать
        public function getPhoneCarriers($data)
        {
          $carrierIDs = explode(',', $data);
          $carriersInfo = $this->db->Select(
              'a_phone_carriers',
              array(
                  'order' => array('ASC'=>'`id`')
                )
              );

          foreach ($carrierIDs as $k => $v)
          {
            $carriers[] = $carriersInfo[$v];
          }
          return $carriers;
        }

      //объём двигателя
      public function VolumeEngine($volume) {
        return $volume_s  = (strlen($volume)>3) ? substr($volume, 0, 1).'.'.substr($volume, 1, 1) : $volume;
      }

      //добавить просмотр к объявлению
      public function AddViewAdv($id) {
        $sql = 'UPDATE `a_public_shows` SET `show` = `show`+1 WHERE `pub_id` = \''.$id.'\'';
        $this->db->sql($sql);
      }

      public function GetCountCArs($array){
        $where = '`hide` = \'0\' AND `marka` NOT IN ('.$this->delete_marks.')';
        $where .= empty($array['adv_type']) ? '' : ' AND `adv_type` = \''.$array['adv_type'].'\'';
        $where .= empty($array['marka']) ? '' : ' AND `marka` IN ('.$array['marka'].')';
        $where .= empty($array['model']) ? '' : ' AND `model` IN ('.$array['model'].')';
        $where .= empty($array['year1']) ? '' : ' AND `year` >= \''.$array['year1'].'\'';
        $where .= empty($array['cost_val2']) ? '' : ' AND `cost_val_d` <= \''.$array['cost_val2'].'\'';
        $sql = 'SELECT COUNT(`adv_id`) AS `COUNT`       
                FROM `'.$this->table.'`
                WHERE '.$where.'
               ';       
        return $this->db->Sql($sql)['0']['COUNT'];
      }

      public function GetAllMarka($select,$where='',$order='',$limit=''){
        $where = ($where) ? $where : array('main'=> '0');
        $order = ($order) ? $order : array('ASC'=> 'name_eng');
        return $this->db->Select(
        $this->table_marks,
          array(
          'select'=>$select,
          'where'=>$where,
          'order'=>$order,
          'limit' => $limit
          )
        );
      }

      public function GetAllMarkaTruck($select){
        return $this->db->Select(
        $this->table_marks_truck,
          array(
          'select'=>$select,
          'order'=>array('ASC'=> 'name_eng')
          )
        );
      }

      public function GetAllModel($select){
        return $this->db->Select(
        $this->table_models,
          array(
          'select'=>$select,
          'order'=>array('ASC'=> 'name_eng')
          )
        );
      }

      public function GetAllModelTruck($select){
        return $this->db->Select(
        $this->table_models_truck,
          array(
          'select'=>$select,
          'order'=>array('ASC'=> 'name_eng')
          )
        );
      }

      public function GetAllShinaProfile(){
        return $this->db->Select(
        $this->shina_profile,
          array(
          'select'=>'`id`,`name`',
          'order'=>array('ASC'=> 'name')
          )
        );
      }

      public function GetAllShinaSeason(){
        return $this->db->Select(
        $this->shina_season,
          array(
          'select'=>'`id`,`name`',
          'order'=>array('ASC'=> 'name')
          )
        );
      }

      public function GetAllShinaSize(){
        return $this->db->Select(
        $this->shina_size,
          array(
          'select'=>'`id`,`name`',
          'order'=>array('ASC'=> 'name')
          )
        );
      }

      public function GetAllShinaWidth(){
        return $this->db->Select(
        $this->shina_width,
          array(
          'select'=>'`id`,`name`',
          'order'=>array('ASC'=> 'name')
          )
        );
      }   

      public function GetLastPaidAdv($arr=''){

        $sql = 'SELECT
                  `car`.`adv_id`, 
                  `car`.`adv_type`, 
                  `mar`.`name_eng` AS `marka`, 
                  `mod`.`name_eng` AS `model`, 
                  `car`.`version`, 
                  `car`.`year`,
                  `car`.`probeg_col`, 
                  `car`.`probeg_type`, 
                  `car`.`volume`, 
                  `car`.`cost_val`, 
                  `car`.`cost_type`, 
                  `show`.`show`, 
                  `tr`.`name` AS `trans`, 
                  `eng`.`short` AS `engine`, 
                  `up`.`last_time_up`
                FROM `'.$this->table_up.'` AS `up`
                LEFT JOIN `'.$this->table.'` AS `car` ON `up`.`adv_id` = `car`.`adv_id`
                LEFT JOIN `'.$this->table_shows.'` AS `show` ON `up`.`adv_id` = `show`.`pub_id`              
                LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `car`.`marka` = `mar`.`marka_id` 
                LEFT JOIN `'.$this->table_models.'` AS `mod` ON `car`.`model` = `mod`.`model_id`
                LEFT JOIN `'.$this->table_trans.'` AS `tr` ON `car`.`transmission` = `tr`.`trans_id`  
                LEFT JOIN `'.$this->table_eng.'` AS `eng` ON `car`.`type_engine` = `eng`.`engine_id` 
                LEFT JOIN `'.$this->table_lider.'` AS `lid` ON `car`.`adv_id` = `lid`.`pub_id`  
                WHERE `car`.`hide` = \'0\' AND `car`.`adv_type` = \'1\' AND (`lid`.`curr_bets` is NULL OR `lid`.`curr_bets` = \'0\') 
                ORDER BY `up`.`last_time_up` DESC 
                LIMIT 0,12';
        return $this->db->Sql($sql);
      }

      public function GetSimilarCars($arr){
        $adv_id = (isset($arr['adv_id'])) ? 'AND `car`.`adv_id` != \''.$arr['adv_id'].'\'' : '';
        $sql = 'SELECT `car`.`adv_id`, `car`.`adv_type`, `mar`.`name_eng` AS `marka`, `mod`.`name_eng` AS `model`, `car`.`version`, `car`.`year`,`car`.`probeg_col`, `car`.`probeg_type`, `car`.`volume`, `car`.`cost_val`, `car`.`cost_type`, `show`.`show`, `tr`.`name` AS `trans`, `eng`.`short` AS `engine`, `up`.`last_time_up`
                FROM `'.$this->table.'` AS `car`
                LEFT JOIN `'.$this->table_up.'` AS `up` ON `car`.`adv_id` = `up`.`adv_id`
                LEFT JOIN `'.$this->table_shows.'` AS `show` ON `up`.`adv_id` = `show`.`pub_id`              
                LEFT JOIN `'.$this->table_marks.'` AS `mar` ON `car`.`marka` = `mar`.`marka_id` 
                LEFT JOIN `'.$this->table_models.'` AS `mod` ON `car`.`model` = `mod`.`model_id`
                LEFT JOIN `'.$this->table_trans.'` AS `tr` ON `car`.`transmission` = `tr`.`trans_id`  
                LEFT JOIN `'.$this->table_eng.'` AS `eng` ON `car`.`type_engine` = `eng`.`engine_id` 
                WHERE `car`.`model` = \''.$arr['model'].'\' '.$adv_id.' AND `car`.`hide` = \'0\' AND `car`.`adv_type` = \'1\'
                ORDER BY `up`.`last_time_up` DESC 
                LIMIT 0,'.$arr['count'].'';
        return $this->db->Sql($sql);
      }
      
      public function CheckIdViewedCar($data,$ar){
        $is_advs = $this->GetAllCars($data, 'getid');
        foreach ($is_advs as $v) {
          $advs[] = $v['adv_id'];
        }        
				if (count($advs)>0){
					// good adv in Db
						$res['good'] = $advs; 
					// deleted adv in Db
						$first_adv = explode(',',$ar['cars']);
						$bad = array_diff($first_adv, $advs);
					// display deleted adv in Db
						$res['bad'] = implode(',', $bad);
				}else{
					$res['good'] = [];
					$res['bad'] = $ar['cars'];
				}
        return $res;
      }

      public function getViewedCar($arr){
        $data['where'] = '`car`.`adv_id` IN ('.$arr['cars'].') AND `car`.`hide` = \'0\'';
        isset($arr['count']) ? $data['limit'] = 'LIMIT 0,'.$arr['count'].'' : '';
        $data['order'] = 'FIELD(`car`.`adv_id`, '.$arr['cars'].')';
        return $data;
      }

      public function CarProbeg($arr){
        $probeg = '';
        if ($arr['probeg_col'] > 0){        
          $probeg = $arr['probeg_col'].(($arr['probeg_type'] != 2) ? ' т.км' : ' т.миль');
        }
        return $probeg; 
      }

      public function CarVolumeEngine($volume) {
        return  ((strlen($volume)>3) ? substr($volume, 0, 1).'.'.substr($volume, 1, 1) : $volume);
      }


      private function convertPrice($cost, $currency, $to = 'br')
      {
        $currency_a = array(
          'd' => '$',
          'br' => 'р.',
          'brn' => 'руб.',
          'e' => '&#8364;',
          'rr' => '&#8381;',
        );
        //$delim = ($to == 'br') ? 10000 : 1;

        if ($currency == $to || !array_key_exists($to, $currency_a))
        {

          //$after_dot = ($to == 'brn') ? 2 : 0;
          $after_dot = 0;
          //$cost = $cost*$delim;
          $cost = number_format($cost, $after_dot, '.', ' ');
          $currency = $currency_a[$to];
        }
        else
        { 
          $rate = 1;
          $rate = ($to == 'br' && $currency == 'd') ? NBRB_USD : $rate;
          $rate = ($to == 'br' && $currency == 'e') ? NBRB_EUR : $rate;
          $rate = ($to == 'br' && $currency == 'rr') ? NBRB_RUB : $rate;
          $rate = ($to == 'brn' && $currency == 'd') ? NBRB_USD : $rate;
          $rate = ($to == 'brn' && $currency == 'e') ? NBRB_EUR : $rate;
          $rate = ($to == 'brn' && $currency == 'rr') ? NBRB_RUB : $rate;
          $rate = ($to == 'd' && $currency == 'e') ? NBRB_EUR/NBRB_USD : $rate;
          $rate = ($to == 'd' && $currency == 'rr') ? NBRB_RUB/NBRB_USD : $rate;
          $rate = ($to == 'd' && $currency == 'br') ? 1/NBRB_USD : $rate;
          $rate = ($to == 'e' && $currency == 'd') ? NBRB_USD/NBRB_EUR : $rate;
          $rate = ($to == 'e' && $currency == 'rr') ? NBRB_RUB/NBRB_EUR : $rate;
          $rate = ($to == 'e' && $currency == 'br') ? 1/NBRB_EUR : $rate;
          $rate = ($to == 'rr' && $currency == 'd') ? NBRB_USD/NBRB_RUB : $rate;
          $rate = ($to == 'rr' && $currency == 'e') ? NBRB_EUR/NBRB_RUB : $rate;
          $rate = ($to == 'rr' && $currency == 'br') ? 1/NBRB_RUB : $rate;
          $cost = $cost * $rate;
          $after_dot = ($to == 'brn') ? 2 : 0;
          //$cost = $cost*$delim;
          $cost = ($to == 'br') ? $cost : number_format($cost, $after_dot, '.', ' ');
          $currency = $currency_a[$to];
        }
      return array($cost, $currency);
      }

      public function multiPrice($cost, $currency)
      {
        $currencies = array(
          'byrn' => 'brn',
          'br' => 'br',
          'usd' => 'd'
        );

        $res = array();
        foreach ($currencies as $k => $v)
        {
          $res[$k] = array();
          list($res[$k]['cost'], $res[$k]['currency']) = $this->convertPrice($cost, $currency, $v);
          $res[$k]['sys_currency'] = $v;
        }
        return $res;
      }

      // ссылка для объявления
      public function GetLinksCar($type,$marka,$model,$adv_id) {
        $t = ($type == '2') ? 'buy' : 'sell';
        $marka = ($marka) ? urlencode(mb_strtolower($marka)) : '0';
        $model = ($model) ? urlencode(mb_strtolower($model)) : '0';
        return STATIC_WWW.'/car/'.$t.'/'.$marka.'/'.$model.'/'.$adv_id.'/';
      }

      // ссылка модели
      public function GetLinksModel($type,$marka,$model) {
        $t = ($type == '2') ? 'buy' : 'sell';
        return STATIC_WWW.'/car/'.$t.'/'.urlencode(mb_strtolower($marka)).'/'.urlencode(mb_strtolower($model)).'/';
      }

      // приобразовать для записи массива
      public function arr2str($arr, $cont = '', $bf = '')
      {
        foreach ($arr as $k => $v)
        {
          $cbf = ($bf == '') ? "\$arr['".$k."']" : $bf."['".$k."']";
          if (is_array($v))
          {
            $cont = $this->arr2str($v, $cont, $cbf);
          }
          else
          {
            $cont .= $cbf." = '".$v."';\n";
          }
        }
        return $cont;
      }

    // сохранить файл
    public function SaveFile($file,$text){
      $f = fopen($file, 'w');
      fputs($f, $text);
      fclose($f); 
    }


    // обнулить просмотры в объявлении
    public function car_reset_show($adv_id) {
      $res = FALSE;
      $pub_id = $this->db->SelectCell($this->table_shows,array('select' => array('pub_id'),'where' => array('pub_id' => $adv_id)));
      if (!empty($pub_id)){
        $this->db->Update($this->table_shows, ['show' => '0'], ['pub_id' => $adv_id]);
        $res = TRUE;
      }
      return $res;
    }
    
   // изменить статус и тип при платном размещении, продлении
    public function car_update_status($adv_id, $status, $code, $period) {
      $this->db->Update($this->table, ['status' => $status, 'code' => $code, 'add_time2' => date("Y-m-d", time()), 'add_time3' => time(), 'period' => $period], ['adv_id' => $adv_id]);     
      return TRUE;
    }

   // поднять объявление - ОБГОН
    public function car_add_up($adv_id) {
      $res = FALSE;
      $pub_id = $this->db->SelectCell($this->table,array('select' => array('adv_id'),'where' => array('adv_id' => $adv_id)));
      if (!empty($pub_id)){
        $this->db->sql('INSERT INTO `'.$this->table_up.'` SET `total_ups` = 1, `adv_id` = \''.$adv_id.'\' ON DUPLICATE KEY UPDATE `total_ups` = `total_ups` + 1');
        $this->db->sql('UPDATE `'.$this->table.'` SET `add_time3` = '.time().' WHERE `adv_id` = \''.$adv_id.'\'');
        $res = TRUE;
      }
      return $res;
    }

   // разместить в спецпредложении
    public function car_add_sp($adv_id, $bets, $phone, $user, $phone, $operator = '[self]') {
      $res = FALSE;
      $pub_id = $this->db->SelectCell($this->table,array('select' => array('adv_id'),'where' => array('adv_id' => $adv_id)));
      if (!empty($pub_id)){

        $lider = $this->db->SelectOne(
          $this->table_lider,
          array(
            'select' => 'id, bets, curr_bets',
            'where' => array('pub_id' => $adv_id)
          )
        );

        if ($lider){

          $ok = $this->db->Update(
            $this->table_lider,
            array(
              'bets' => $lider['bets'] + $bets,
              'curr_bets' => $lider['curr_bets'] + $bets,
            ),
            array(
              'pub_id' => $adv_id
            )
          );
        }else{

          $ok = $this->db->Insert(
            $this->table_lider,
            array(
              'operator' => $operator,
              'pub_id' => $adv_id,
              'n_tel' => $adv['u_tel1'],
              'user' => ($operator == '[self]') ? $_SESSION['login'] : $adv['user'],
              'bets' => $bets,
              'curr_bets' => $bets,
              'add_time' => time()
            )
          );
        }

        if ($ok){
          $this->db->Insert(
            $this->table_lider_hist,
            array(
              'operator' => $operator,
              'pub_id' => $adv_id,
              'n_tel' => $phone,
              'user' => $user,
              'bets' => $bets,
              'curr_bets' => $bets,
              'add_time' => time()
            )
          );
          $res = TRUE;
        }
      }
      return $res;
    }

   // разметсить в газете
    public function car_add_np($adv_id, $type, $period) {
      $res = FALSE;
      $adv_type = $this->db->SelectCell($this->table,array('select' => array('adv_type'),'where' => array('adv_id' => $adv_id)));

      if (!empty($adv_type)){

        // get info
          $ns = $this->GetPaidInfo($type);

        if (!empty($ns)){
          // add status & class
            $this->car_update_status($adv_id, '2', $ns['code'],$period);
          
          // add UP
            $this->car_add_up($adv_id);

          // add newspapers          
            for ($i=0; $i<$period; $i++){
              //дата выхода в ближайшую газету
              $pubdate = ($this->DateNewspaper() + $i*604800);       
              $date_newspaper = date("Y-m-d", $pubdate);
              $this->db->Insert(
                'a_public_newspaper',   
                  array('adv_id' => $adv_id,
                    'code' => $ns['code'],
                    'adv_type' => $adv_type,    
                    'date_newspaper' => $date_newspaper,
                    'adv_id' => $adv_id, 
                    )
                  );
            }
            $res = TRUE;
        }
      }
      return $res;
    }

   // for PAID ADV
    public function GetPaidInfo($type='') {
      if ($type){
        $res = in_array($type, ['4','5','6','7','8']);
      }
        $paid_type = array(
         4 => array(
           'id' => 4,
           'name' => 'Обычный шрифт (сайт + газета)',
           'price' => SER_COST4,
           'code' => '21',
           'type' =>'4'
         ),
         5 => array(
           'id' => 5,
           'name' => 'Жирный шрифт (сайт + газета)',
           'price' => SER_COST5,
           'code' => '03',
           'type' =>'5'
         ),
         6 => array(
           'id' => 6,
           'name' => 'Жирный шрифт в рамке (сайт + газета)',
           'price' => SER_COST6,
           'code' => '02',
           'type' =>'6'
         ),
         7 => array(
           'id' => 7,
           'name' => 'Фото-объявление в газете (сайт + газета)',
           'price' => SER_COST7,
           'code' => '06',
           'type' =>'7'
         ),
         8 => array(
           'id' => 8,
           'name' => 'Приоритетное (только на сайте)',
           'price' => SER_COST12,
           'code' => '0',
           'type' =>'12'
         )
       );
        if ($res == TRUE){
          $res = $paid_type[$type];
        }else{
          $res = $paid_type;
        }
        return $res;
    }

    //определяем дату ближайшего выхода газеты
    public function DateNewspaper(){
      // день недели выхода газеты
      $w_last = 1;
      //час последнего приема в газету: 00-24 часов
      $h_last = 14;

      $day_of_week = (date("w", time()));
      $day_of_time = (date("H", time()));
      $day = 4 - $day_of_week;
      $week = (empty($day_of_week) || ($day_of_week == $w_last && $day_of_time < $h_last)) ? 0 : 604800;
      $date_newspaper = (time() + $day*86400 + $week);
      if (date("d-m-y",$date_newspaper) == "29-12-16"){
        $date_newspaper = $date_newspaper + 604800;
      }  
    return $date_newspaper;
    }

    public function TypePaidAdvCode($type){
      // для конвертации code к типу объявления
      if (in_array($type, ['21','03','02','06','0'])){
        $arr = [
          '21' => 'newspaper-highlight',
          '03' => 'newspaper-highlight',
          '02' => 'newspaper-highlight',
          '06' => 'site-highlight',
          '0' => 'price-selected'
        ];
        return $arr[$type];
      }
    }

    public function TypePaidAdv($type){
      // типы плаатных объявлений
      return [
        '4' => [
          'id' => '4',
          'name' => 'Обычный шрифт (сайт + газета)',
          'price' => SER_COST4,
          'code' => '21',
          'type' =>'vip-standart'
        ],
        '5' => [
          'id' => '5',
          'name' => 'Жирный шрифт (сайт + газета)',
          'price' => SER_COST5,
          'code' => '03',
          'type' =>'vip-bold'
        ],
        '6' => [
          'id' => '6',
          'name' => 'Жирный шрифт в рамке (сайт + газета)',
          'price' => SER_COST6,
          'code' => '02',
          'type' =>'vip-frame'
        ],
        '7' => [
          'id' => '7',
          'name' => 'Фото-объявление в газете (сайт + газета)',
          'price' => SER_COST7,
          'code' => '06',
          'type' =>'vip-photo'
        ],
        '8' => [
          'id' => '8',
          'name' => 'Приоритетное (только на сайте)',
          'price' => SER_COST12,
          'code' => '0',
          'type' =>'vip-site'
        ]
      ];
    }

    private function ArrayToStr($arr){
      if (is_array($arr)){
        foreach ($arr as $value) {
          $check[] = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        }
        $res = implode(', ', $check);
      }else{
        $res = filter_var($arr, FILTER_SANITIZE_NUMBER_INT);
      }
      return $res; 
    }
    
    public function getRegionId($country) {
      $sql = 'SELECT `city_id`, `name` FROM `'.$this->table_city.'` WHERE `country_id` = \''.$country.'\' AND `city_id` != \'1\' ORDER BY `city_id` ASC';
      foreach ($this->db->Sql($sql) as $key => $value) {
        $res[$value['city_id']] = $value['name'];
      }
      return $res;
    }
    
    public function CheckCookie($type, $name) {
      // автомобили
      if ($type == '1'){
        $ar = filter_input(INPUT_COOKIE, $name);
        $res = (empty($ar) || $ar == "null") ? '' : json_decode($ar);
        return is_array($res) ? $res : '';
      }
    }
    
    public function getCookieCar($type, $adv_id='') {
      // $type = ViewedCar | note
      
      $ViewedCar = filter_input(INPUT_COOKIE, $type);

      if (empty($ViewedCar) || $ViewedCar == "null"){      
        setcookie($type, json_encode([$adv_id]), strtotime( '+30 days' ), "/" );
        return FALSE;
      }
      
      // раскодировать JSON
      $arr = json_decode($ViewedCar, TRUE);

      if (is_array($arr)){
        // удалить пустоту
        $arr = array_diff($arr,['']);
        // исключить из массива данное объявление
        (empty($adv_id)) ? '' : $arr = array_diff($arr,[$adv_id]);
        // поставить лимит
        array_slice($arr, 0, 150);
        // получить результат ID для выборки
        $res = implode(',', $arr);
        // добавить просмотренное объявление
        (empty($adv_id)) ? '' : array_unshift($arr,$adv_id);
        
      }else{
        $res = is_int($arr);
        $arr = [$adv_id];
      }
      // првоерить, есть ли объявление в БД
      //$is_adv = $this->checkId($res); 
      
      // записать куки
      $this->saveCookieCar($type,$arr);
      
      // сформировать для вывода
      return $res;
    }
    
    // записать куки
    public function saveCookieCar($type,$arr){
      setcookie($type, json_encode($arr), strtotime( '+30 days' ), "/" );  
    }



    public function GetPhotosCar($id){
      $class_img = NEW Img();
      return $class_img->PhotoAdv($id, PHOTO_AUTO_CASH); 
    }  

    

    
    
    public function GetPhotosCarOLD($adv_id){
      $photos='';

      for ($i=1;$i<=20;$i++)
      {
        if (file_exists(DIR.PHOTO_AUTO.$adv_id.'_'.$i.'_100.jpg'))
        {        
          $photos[$i]['thumb'] = STATIC_WWW.PHOTO_AUTO.$adv_id.'_'.$i.'_100.jpg'; 

          if (file_exists(DIR.PHOTO_AUTO.$adv_id.'_'.$i.'_350.jpg'))
          {        
            $photos[$i]['medium'] = STATIC_WWW.PHOTO_AUTO.$adv_id.'_'.$i.'_350.jpg'; 
          }

          if (file_exists(DIR.PHOTO_AUTO.$adv_id.'_'.$i.'_650.jpg'))
          {        
            $photos[$i]['large'] = STATIC_WWW.PHOTO_AUTO.$adv_id.'_'.$i.'_650.jpg'; 
            $photos[$i]['big'] = STATIC_WWW.PHOTO_AUTO.$adv_id.'_'.$i.'_650.jpg'; 
          } 
        }
      }
      return (!empty($photos)) ? $photos : FALSE;
    }
    
    
    
    // выбрать все автохаусы или автодлеры 
    public function getAutohouse($user_status){
      // выбрать всех юрлиц с указанным статусом
      // 8 - автозаусы
      // 9 - автодилеры
      $sql = 'SELECT 
                `user`.`user_id`,
                `user`.`login`,
                `user`.`url`,
                `user`.`firstname`,
                `user`.`about`,
                `user`.`address`,
                `user`.`phones`,
                `user`.`photo`,
                `user`.`name_ofic`,
                `ax`.`type`
              FROM 
                `'.$this->table_base.'` AS `user`
              LEFT JOIN 
                `'.$this->table_base_ax.'` AS `ax` ON `user`.`user_id`= `ax`.`user_id`
              WHERE
                `user`.`status` = '.$user_status.'';

      $aut = $this->db->Sql($sql);
      if (count($aut) < 1) {return FALSE;}

      foreach ($aut as $v) {
        $salers[$v['login']] = $v;
        $login[] = $v['login'];
      }
      
      // получить кол-во объявлений
      $count = $this->getCountAdvs($login);
      
      if (count($count) > 0){
        foreach ($count as $v) {
          $salers[$v['user']]['count'] = $v['count'];
        }
      }
      return $salers;
    }
  
    
    // получить количевто активных объявлений по автохаусам
    private function getCountAdvs($logins) {
      $sql = 'SELECT `user`, COUNT(`adv_id`) AS `count` 
        FROM `'.$this->table.'`
        WHERE `hide` = \'0\' AND `user` IN (\''.implode("','", $logins).'\')
        GROUP BY `user` 
        ORDER BY `user` ASC';
      return $this->db->Sql($sql);
    }


    // подготовить информацию по автохаусам для листинга
    public function getAutohouseDisplay($data){
      if (count($data)<1) {return false;}
      foreach ($data as $k => $v) {
        // название 
          $res[$k]['name'] = $v['firstname'];
          
        // подсчет кол-ва активных объявлений
          $res[$k]['count'] = isset($v['count']) ? $v['count'].' авто' : '';

        // логотип
          $res[$k]['logo'] = $this->IsPhoto(PHOTO_DILERS,$v['photo']); // https://www.abw.by/photos/diller_logo/4f79a90239e5a.jpg

        // авто под заказ
          $res[$k]['is_car_order'] = $v['type']; // подумать в базе как отмечать
        
        // адрес
          $res[$k]['address'] = $v['address'];
        
        // телефон
          $res[$k]['phone'] = $this->getPhoneUnserialze($v['phones']);
        
        // сайт
          $res[$k]['site'] = $v['url'];
        
        // описание
          $res[$k]['description'] = $v['about'];
        
        // юр.информация
          $res[$k]['name_ofic'] = $v['name_ofic'];
      }
      return $res;
    }
    
    // првоерить есть ли картинка физически на сервере
    private function IsPhoto($path, $photo){
      return (!empty($photo) && is_file(DIR.$path.$photo)) ? STATIC_WWW.$path.$photo : '';
    }
    
    // получить номера телефонов
    private function getPhoneUnserialze($data) {
      $arr = unserialize($data);
      for ($i = 1; $i < 4; $i++){  
        $res[] = (!empty($arr['code'.$i]))? '('.$arr['code'.$i].')'.$arr['tel'.$i] : '';
      }
      $res = array_diff($res,['']);
      return implode(', ', $res);
    }
    
  }
