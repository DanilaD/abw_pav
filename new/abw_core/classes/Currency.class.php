<?
  
  class Currency extends Core
  {
    private $bank_course_table = 'a_banks_xml';

    public function __construct($_db)
    {
      parent::__construct($_db);
    }
    
    public function GetBestCourse(){
    
     return $this->db->SelectOne(
      $this->bank_course_table,
        array(
        'select'=>'`name`, `usd_b`, `usd_s`, `eur_b`, `eur_s`, `rub_b`, `rub_s`',
        'where'=>array('last'=> '1', 'name'=>'best'),
        )
      );
    }
  }
  
  
    