<?  
class MailSend extends Core
{
  
  function __construct($_db)
  {
    parent::__construct($_db);
  }  
  
 public function SendMesPhone($data) {
   
   
  if ($data['mes'] == '1'){
    // продано
    $data['subj'] = 'Продан товар по объявлению № '.$data['adv_id'];
    $data['text'] = 'Дата отправки сообщения: '.date("d-m-Y H:i" ,time()).'.<br/>Товар продан.<br/>Ссылка на объявление <a href="'.$data['url'].'">'.$data['url'].'</a>';
    $data['mail'] = 'no-reply@abw.by';
    
  }else{
    // не правильный номер телефона
    $data['subj'] = 'Неправильный телефон в объявлении № '.$data['adv_id'];
    $data['text'] = 'Дата отправки сообщения: '.date("d-m-Y H:i" ,time()).'.<br/>Неправильный телефон.<br/>Ссылка на объявление <a href="'.$data['url'].'">'.$data['url'].'</a>';
    $data['mail'] = 'no-reply@abw.by';
  }
    return $this->SendAdminMail($data);
 }
  
  public function CheckUserMail($data) {

    $res='';
    // check login & password
    if (empty($data['adv_id'])) {
      $res = ['status'=>FALSE, 'text'=>'Некорректный ID объявления.'];

    }elseif(empty($data['mail'])) {
      $res = ['status'=>FALSE, 'text'=>'Не указан e-mail.'];

    }elseif(empty($data['phone'])) {
      $res = ['status'=>FALSE, 'text'=>'Не указан телефон.'];
      
    }elseif(empty($data['text'])) {
      $res = ['status'=>FALSE, 'text'=>'Введите текст сообщения.'];

    }else{
      $cars = $this->GetUserMailFromCar($data);
      if (empty($cars['0']['u_mail'])){
        $res=['status'=>FALSE, 'text'=>'Не указан e-mail продавца.'];
      }else{
				$data['u_mail'] = $cars['0']['u_mail'];
        $res = $this->SendUserMail($data); 
      }
    }
    return $res;
  }  
  
  private function GetUserMailFromCar($data) {
    // получить e-mail из объявления
    $car = new Cars($this->db);
    $cars = $car->GetCar($data['adv_id']);
    if (!empty($cars)){
      return $cars; 
    }
  }
  
  private function SendUserMail($data) {
     // текст для письма
    $subj = 'По поводу Вашего объявления '.$data['adv_id'];
    $text = 'Время: '.date("d-m-Y H:i" ,time()).'<br/>E-mail: '.$data['mail'].'<br/>Тел. '.$data['phone'].'<br/>Сообщение от покупателя ABW: '.$data['text'].'';
    require(DIR.'/phpmailer/class.phpmailer.php');
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->From = 'no-reply@abw.by';
    $mail->setFrom('no-reply@abw.by', 'ABW.BY');
    $mail->AddCustomHeader('Return-Path: '.$data['mail']);
    $mail->FromName = 'От пользователя ABW.BY';
    $mail->AddAddress($data['u_mail']);
    $mail->AddReplyTo($data['mail']);
    $mail->IsHTML(TRUE);
    $mail->Subject = $subj;
    $mail->Body = $text;
    if ($mail->Send()){
      $res = ['status'=>TRUE, 'text'=>'Ваше сообщение отправлено.'];
      $mail->ClearAddresses();
      $mail->ClearCustomHeaders();
      $mail->ClearAttachments();
    }else{
      $res = ['status'=>FALSE, 'text'=>'Сообщение не отправлено. Ошибка: '.$mail->ErrorInfo.''];
    }
    return $res;
  }

  public function CheckAdminMail($data) {
    $res='';
    // check login & password
    if (empty($data['adv_id'])) {
      $res = ['status'=>FALSE, 'text'=>'Некорректный ID объявления.'];

    }elseif(empty($data['text'])) {
      $res = ['status'=>FALSE, 'text'=>'Введите текст сообщения.'];

    }elseif(empty($data['mail'])) {
      $res = ['status'=>FALSE, 'text'=>'Обязательно укажите e-mail.'];

    }else{
      $data['subj'] = 'Со страницы объявления №'.$data['adv_id'];
      $car_class = NEW Cars($this->db);
      $url = $car_class->GetUrlCar($data['adv_id']);
      $data['text'] = 'Время: '.date("d-m-Y H:i" ,time()).'<br/>E-mail: '.$data['mail'].'<br/>Тел. '.$data['phone'].'<br/>Сообщение: '.$data['text'].'<br/>Объявление: <a href="'.$url.'">'.$url.'</a>';
      $res = $this->SendAdminMail($data);
    }
    
    return $res;
  }
  
  private function SendAdminMail($data) {
     // текст для письма

    require(DIR.'/phpmailer/class.phpmailer.php');
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->From = 'no-reply@abw.by';
    $mail->setFrom('no-reply@abw.by', 'ABW.BY');
    $mail->AddCustomHeader('Return-Path: '.$data['mail']);
    $mail->FromName = 'От пользователя ABW.BY';
    $mail->AddAddress('support@abw.by');
    $mail->AddReplyTo($data['mail']);
    $mail->IsHTML(true);
    $mail->Subject = $data['subj'];
    $mail->Body = $data['text'];
    if ($mail->Send()){
      $res = ['status'=>TRUE, 'text'=>'Ваше сообщение отправлено.'];
      $mail->ClearAddresses();
      $mail->ClearCustomHeaders();
      $mail->ClearAttachments();
    }else{
      $res = ['status'=>FALSE, 'text'=>'Сообщение не отправлено. Ошибка: '.$mail->ErrorInfo.''];
    }
    return $res;
  }
  
}
