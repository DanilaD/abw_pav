<?php

class Core
{
  function __construct($_db)
  {
    $this->db = $_db;

    if (isset($_POST))
    {
      $this->post = $this->getData($_POST);
    }

    if (isset($_GET))
    {
      $this->get = $this->getData($_GET);
    }

    if (isset($_SERVER['REQUEST_METHOD']))
    {
      $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }
  }

  function loadClass($class_name)
  {
    if (!class_exists($class_name))
    {
      $path = __DIR__.'/classes/'.$class_name.'.class.php';
      if (file_exists($path))
      {
        require_once $path;
        $method = strtolower($class_name);
        $this->$method = new $class_name($this->db);
      }
      else
      {
        print_r('no file '.$class_name.'.class.php');exit;
      }
    }
    else
    {
      print_r('no class '.$class_name);exit;
    }
  }

  public function getData($data)
  {
    if (is_array($data))
    {
      foreach ($data as $k => $v)
      {
        $data[$k] = $this->getData($v);
      }
    }
    else
    {
      $data = trim($data);
      //$data = mysql_real_escape_string($data);
    }

    return $data;
  }
  
  public function IP()
  {
    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
      $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    elseif(isset($_SERVER["HTTP_CLIENT_IP"]))
    {
      $ip = $_SERVER["HTTP_CLIENT_IP"];
    }
    else
    {
      $ip = @$_SERVER["REMOTE_ADDR"];
    }

    $ip = (array)explode(',', $ip);
    return array_pop($ip);
  }
}

/* End of file core.php */