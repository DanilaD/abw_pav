<?
header("Content-Type: text/html; charset=utf-8");
//START
session_start();

// print_r($_SESSION['search']);

// important
  require 'configs/setup.php';  	
  
// main  
  require ROOT_DIR.'/libs/main.php';
//core
  require ROOT_DIR.'/abw_core/Core.php';
  $core = new Core($_db);
  $core->loadClass('Users');
  //$user = new Users($_db);
  $core->loadClass('News');
  //$news = new News($_db);
  $core->loadClass('Currency');
  //$course = new Currency($_db);
  $core->loadClass('Cars');
  $core->loadClass('Adv');
  // send mail
  $core->loadClass('MailSend');
  //load exchange rate
  require DIR.'/cron/exchange_rate_new.php';
  //marks & models  
  require(ROOT_DIR.'/array/marks.php');
  // for car
  require(ROOT_DIR.'/array/main_page.php');
  // for comments
  require(ROOT_DIR.'/libs/get_comments_lib.php');
  // for resize images
  require(ROOT_DIR.'/libs/images.php');
  //date to russian
  require(ROOT_DIR.'/libs/date_rus_lib.php');
  
// For development
  $smarty->assign('is_localhost', 'localhost' == $_SERVER['HTTP_HOST']);
    
//check autologin
  if (empty($_SESSION['login']) && isset($_COOKIE['autologin'])){		
    $data = unserialize($_COOKIE['autologin']);
    if ($data['0'] > 0 && isset($data['1'])){
      $user = $_db->SelectOne('a_base', array('where'=>array('user_id'=>$data['0'])));
      if (md5($user['user_id'].$user['login'].$user['pass']) == $data['1']){	
        $core->users->authUser($user);
      }
    }
  }
	
// ajax 
  $ajax = filter_input(INPUT_GET, 'ajax', FILTER_SANITIZE_ENCODED);  
  if (!empty($ajax)){
    $ajax_array = [
        'ajax_get_model',
        'ajax_get_count_cars',
        'ajax_login',
        'ajax_close_tips',
        'ajax_exit',
        'ajax_paid_car',
        'ajax_car_note',
        'ajax_car_hide',
        'ajax_send_mes_admin',
        'ajax_send_mes_user',
        'ajax_show_phone',
        'ajax_phone_send_mes',
        'ajax_show_views_car',
        'ajax_get_city',
        'ajax_reset'
    ];
    if(in_array($ajax, $ajax_array)) {
        include('func/'.$ajax.'.php');
    }else {
        $smarty->assign('body_classes',setBodyClass('error'));
        error404();
    }
    exit;
  }
  
// cron
  $cron = filter_input(INPUT_GET, 'cron', FILTER_SANITIZE_ENCODED);
    if (!empty($cron)){
      $cron_array = [
          'getmarksmodels',
          'getsto',
          'viplider'
      ];
      if(in_array($cron, $cron_array)){ 
          include('cron/'.$cron.'.php');
      }else{
          $smarty->assign('body_classes',setBodyClass('error'));
          error404();
      }
      exit;
    }
	
// detect main page
	if (in_array(parse_url($_SERVER['REQUEST_URI'])['path'], ['/new/','/']) && empty(parse_url($_SERVER['REQUEST_URI'])['query']) && empty(filter_input(INPUT_GET, 'act', FILTER_SANITIZE_ENCODED))){
		$is_main = 'main';	
	}elseif ('localhost' == $_SERVER['HTTP_HOST'] && empty(parse_url($_SERVER['REQUEST_URI'])['query']) && empty(filter_input(INPUT_GET, 'act', FILTER_SANITIZE_ENCODED))){
        $is_main = 'main';
    }

	// check for controllers	
    $url = filter_input(INPUT_GET, 'act', FILTER_SANITIZE_STRING);
	//var_dump($url);
    $url_old = '';
    if(!empty($url) || isset($is_main)){
      $url_old = $url;
      $url = rtrim($url, '/');
      $url = explode('/', $url);
    
      $url['0'] = (isset($is_main))? 'main' : $url['0'];
      
      $url_array = [
          'main',
          'exituser',
          'login',
          'car',
          'allpublic',
          'hardcode',
          'page',
          'error',
          'truck'
      ];

      if(in_array($url['0'], $url_array)){ 
        $is_controller = TRUE;      
      }
      else{
        error404();
        exit;        
      }
    }
    
//controllers    
  if (isset($is_controller))
  {
  //body-mobile | body-dekstop
    /*
    if (isset($_SESSION['mobile'])){
      $mobile = $_SESSION['mobile'];
    }else{
      require ROOT_DIR.'/libs/Mobile_Detect.php';
      $detect = new Mobile_Detect;
      if (($detect->isTablet() || $detect->isMobile())){
        $mobile['is_true_mobile'] = '1';   
        $mobile['body_device'] = 'body-mobile';
      } else {
        $mobile['is_true_mobile'] = '0';
        $mobile['body_device'] = 'body-dekstop';
      }
      $_SESSION['mobile'] = $mobile;
    }
    */
    $mobile['body_device'] = 'body-dekstop'; // можно менять
    $mobile['is_true_mobile'] = '0'; // можно менять
    $smarty->assign('mobile', $mobile);
    
  // for close tips
    $close_tips = isset($_COOKIE['close_tips'])? 1 : '';
    $smarty->assign('close_tips',$close_tips);
  
  // user
    if (empty($_SESSION)){
      $ses = $logined_as_admin = '';
    }else{
      $ses = $_SESSION;  
      $logined_as_admin = (isset($ses['status']) && in_array($ses['status'], ['3','4'])) ? 'logined-as-admin' : '';
    }
    $smarty->assign('ses', $ses);
    $smarty->assign('logined_as_admin', $logined_as_admin);
    
  // social
    require ROOT_DIR.'/array/social.php';
    $smarty->assign('social',$social);
    $smarty->assign('link',$link); 
    $smarty->assign('application',$application); 
  
  //menu
    require DIR.'/cron/menu_new.php';
    //$abw = New abw();
    //$header_menu = $abw->utf8_converter($header_menu);
    $smarty->assign('header_menu',$header_menu);
    $smarty->assign('menu_user',$menu_user);
  // marks
    $search['marks'] = $marks;      
  // marks for truck
    $search['marks_truck'] = $marks_truck;      
  // year
    $search['year'] = $search_year;      
  // price
    $search['price'] = $search_price;
    $search['price1'] = $search_price1;
  // engine
    $search['engine'] = $search_engine;
  // transmission
    $search['trans'] = $search_trans;
  // volume
    $search['volume'] = $search_volume;
  // body
    $search['body'] = $search_body;
  // wheel drive
    $search['wheel'] = $search_wheel_drive;
  // condition
    $search['condition'] = $search_condition;
  //mileage
    $search['mileage1'] = $search_mileage1;
    $search['mileage2'] = $search_mileage2;
  // dillers, autohouse, adv
    $search['seller'] = $search_seller;
  // country
    $search['country'] = $search_country;
  // tires size
    $search['shina_size'] = $shina_size;
  // tires profile
    $search['shina_profile'] = $shina_profile;
  // tires width
    $search['shina_width'] = $shina_width;
  // tires season  
    $search['shina_season'] = $shina_season; 
  // discs bolts
    $search['bolts'] = $bolts;
  // days
    $search['day'] = $search_day;
  // display
    $smarty->assign('search',$search);
  // brending
  //для главной страницы вклчюение
    $is_correct_branding['first'] = '';
    $is_correct_branding['second'] = '';
    /*
      $is_correct_branding['first'] =  (($url['0'] == 'main')) ? '1' : '';
      $is_correct_branding['second'] = (($url['0'] == 'car')) ? '1' : '';
    */
    $smarty->assign('is_correct_branding', $is_correct_branding);
    
  // for tpl 
    $tpl['file'] = $url['0'].'.tpl';

  //seo for main page
    if ($url['0'] == 'main'){
      require(ROOT_DIR.'/libs/seo_scripts_lib.php');
      $seo_class = new Seo();
      $seo = $seo_class->display('main');
      $smarty->assign('seo',$seo);
    }
  // check caching
  //print_r($smarty->isCached('index.tpl', $url['0']));
  $cash_array = ['main', 'error'/*, 'car'*/];
  $id_cache = ($url['0'] == 'car' && isset($url['4'])) ? $url['4'] : '';
  if(in_array($url['0'], $cash_array) && $smarty->isCached('index.tpl', $url['0'].$id_cache) == TRUE) {
      $smarty->assign('tpl',$tpl);
      $smarty->display('index.tpl', $url['0'].$id_cache);
      exit;
  }
        
  // load controller
    require 'controllers/'.$url['0'].'.php';
    $controller = new $url['0']($smarty,$_db);
    
    $arg = '';
    if ($url['0'] == 'car'){
      // get parametrs
      unset($_GET['act']);
      $arg = $_GET;
    }

    
    
    // for CAR
    (isset($url['0']) && $url['0'] == 'car' && !isset($url['1'])) ? $url['1'] = 'main' : isset($url['1']) ? $url['1'] = $url['1'] : '';

    if(isset($url['2'])) {
      (method_exists($controller,$url['1'])) ? $controller->$url['1']($smarty, $_db, $url, $arg) : error404();
    }
    elseif(isset($url['1'])) {
      (method_exists($controller,$url['1'])) ? $controller->$url['1']($smarty, $_db, $url, $arg) : error404();
    }
    
    // slider photos for localhost
    $photos = array(
      0 => array(
        'large'   => '//test.abw.by/photos/public/8185853_1_650.jpg',
        'medium'  => '//test.abw.by/photos/public/8185853_1_350.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_1_100.jpg'
      ),
      1 => array(
        'large'   => '//test.abw.by/photos/public/8185853_2_650.jpg',
        'thumb'   => ''
      ),
      2 => array(
        'large'   => '//test.abw.by/photos/public/8185853_3_650.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_3_100.jpg'
      ),
      3 => array(
        'large'   => '//test.abw.by/photos/public/8185853_4_650.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_4_100.jpg'
      ),
      4 => array(
        'large'   => '//test.abw.by/photos/public/8185853_6_650.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_6_100.jpg'
      ),
      5 => array(
        'large'   => '//test.abw.by/photos/public/8185853_7_650.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_7_100.jpg'
      ),
      6 => array(
        'large'   => '//test.abw.by/photos/public/8185853_5_650.jpg',
        'thumb'   => '//test.abw.by/photos/public/8185853_5_100.jpg'
      )
    );
	$smarty->assign('local_photos',$photos); 
    
	// DISPLAY
    $smarty->assign('tpl',$tpl);
    
    /*(in_array($url['0'], $cash_array)) ? $smarty->display('index.tpl', $url['0'].$id_cache) : $smarty->display('index.tpl');*/
    $smarty->display('index.tpl', $url['0'].$id_cache);
  }