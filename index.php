<?php
//error_reporting(E_ALL ^ E_NOTICE  );
//ini_set('display_errors', 'Off');

// NEW DESIGN
//(empty($_SESSION['login']) && (isset($_COOKIE['autologin']))) ? include(ROOT_DIR .'/includes/autologin.php'):'';
// check for main page
if(empty($_REQUEST['act']) && empty($_POST['xajax']) && empty($_REQUEST['act_general'])  && empty($_REQUEST['activ']) && empty($_REQUEST['forget'])){
  include 'new/index.php';
  exit;
}
// END

	$url = filter_input(INPUT_GET, 'act', FILTER_SANITIZE_ENCODED);
	if (!empty($url) && $url == 'showadv'){		
		include 'new/index.php';
		exit;
	}
	
session_start();
$redirected = (isset ($_GET['redirected']) && ($_GET['redirected'] == 1)) ? 1 : 0 ;  
if(isset($_GET['mobile1']) || isset($_POST['mobile1'])){
  header('Location: https://www.abw.by/m/');
  exit();
}
require 'setup.php';

//NEW DESIGN
  //marks & models  
    require(ROOT_DIR.'/new/array/marks.php');
  // for car
	require(ROOT_DIR.'/new/array/main_page.php');
  //menu
	require ROOT_DIR.'/new/libs/main.php';
    require ROOT_DIR.'/cron/menu_new.php';
  	$abw = New abw();
    $header_menu = $abw->win1251_converter($header_menu);
    $smarty->assign('header_menu',$header_menu);
	$menu_user = $abw->win1251_converter($menu_user);
    $smarty->assign('menu_user',$menu_user);
    // marks
      $marks = $abw->win1251_converter($marks);
      $search['marks'] = $marks;      
    // marks for truck
      $marks_truck = $abw->win1251_converter($marks_truck);
      $search['marks_truck'] = $marks_truck;      
    // year
      $search_year = $abw->win1251_converter($search_year);
      $search['year'] = $search_year;      
    // price
      $search_price = $abw->win1251_converter($search_price);
      $search['price'] = $search_price;
    // tires size
      $shina_size = $abw->win1251_converter($shina_size);
      $search['shina_size'] = $shina_size;
    // tires profile
      $shina_profile = $abw->win1251_converter($shina_profile);
      $search['shina_profile'] = $shina_profile;
    // tires width
      $shina_width = $abw->win1251_converter($shina_width);
      $search['shina_width'] = $shina_width;
    // tires season  
      $shina_season = $abw->win1251_converter($shina_season);
      $search['shina_season'] = $shina_season; 
    // discs bolts
      $bolts = $abw->win1251_converter($bolts);
      $search['bolts'] = $bolts;
    // display
      $smarty->assign('search',$search);
  // social
    require ROOT_DIR.'/new/array/social.php';
    $social = $abw->win1251_converter($social);
    $smarty->assign('social',$social);
    $link = $abw->win1251_converter($link);
    $smarty->assign('link',$link); 
    $smarty->assign('application',$application); 
//END NEW DESIGN

require_once 'definition.php';
//load routes
require_once ROOT_DIR.'/routes.php';
//load cached vars
require_once ROOT_DIR.'/cron/vars.php';
//load filters vars
require_once ROOT_DIR.'/cron/filters_vars.php';
//load header menu
//require_once ROOT_DIR.'/cron/menu.php';
//load weather data
//require_once ROOT_DIR.'/cron/weather.php';
//load vip lider data
require_once ROOT_DIR.'/cron/vip_lider.php';
require_once ROOT_DIR.'/cron/vip_lider_new.php';
//load sto data
require_once ROOT_DIR.'/cron/sto.php';
//load up advs
//require_once ROOT_DIR.'/cron/up_car.php';
//load up tires
require_once ROOT_DIR.'/cron/up_tires.php';
//load exchange rate
require_once ROOT_DIR.'/cron/exchange_rate_new.php';
//load html helper
require_once ROOT_DIR.'/html.php';
//load helper
require_once ROOT_DIR.'/helper.php';
//for brending
require_once ROOT_DIR.'/cron/brend.php';
//      ( )
$_COOKIE['currency'] = 'brn';
SetCookie('currency', 'brn', 0x6FFFFFFF);
define('CURRENCY', $_COOKIE['currency']);
// .
require_once ROOT_DIR.'/abw_core/marka_adv.php';
//  ..
require_once ROOT_DIR.'/abw_core/marka_public.php';
//    301      
//include("301.php"); --     
include("models.php");
require_once (ROOT_DIR . '/libs/News.class.php');
$news_obj = new News($_db);
// 
$smarty->assign('header_menu', $header_menu);
include_once('libs/PageBlock.class.php');
require_once ROOT_DIR.'/abw_core/Core.php';
$abw_core = new Core($_db);
$abw_core->loadClass('Users');
$abw_core->loadClass('Banlist');
$abw_core->loadClass('Countries');
$abw_core->loadClass('Catalog');
$abw_core->loadClass('Pages');
$abw_core->loadClass('PageBlocks');
$abw_core->loadClass('CarAd');
$abw_core->loadClass('AbwNews');
$abw_core->loadClass('Stemming');
$abw_core->loadClass('TruckAd');

// 
$brend = "style='background-image: url('/images/arbat/brend.jpg') !important;background-position: center top !important;background-repeat: no-repeat !important;background-color: #FCDE02 !important;'";
$active = filter_input(INPUT_GET, 'activ', FILTER_SANITIZE_SPECIAL_CHARS);
if (isset($active))
{
  $gruzon = filter_input(INPUT_GET, 'gruzon', FILTER_SANITIZE_SPECIAL_CHARS);
  if (empty($gruzon)){
    $site = '';
  }else{
    $site = $gruzon;
  }
  $user = new Users($_db);
  $user->activationUser($active,$site);
}
function _filter($var, $sql = 0, $url = 0)
{
  $var = strip_tags($var);
  $var = str_replace("\n", " ", $var);
  $var = str_replace("\r", "", $var);
  $var = ($url == 1) ? urlencode($var) : $var;
//  $var = htmlentities($var, ENT_QUOTES, 'cp1251');
  $var = ($sql == 1) ? mysql_real_escape_string($var) : $var;
  return $var;
}
function extUrls($output, &$smarty)
{
  $link='/go.php?url='; 
  $hash='';
  $patterns = '/<a(.+?)href=("|\')(http\:\/\/)(?!abw|users.abw.by|www.abw|static.abw|autosport.by)(.+?)("|\')(.*?)>(.*?)<\/a>/i';
  $replacements = '<a$1href='.$link.'$4'.$hash.'$6>$7</a>';
  $output =  preg_replace($patterns, $replacements, $output);  
  return $output;
}
//discounts
function apply_discounts($cost, $type, $period)
{
  //photo discount, 4 weeks less price
  $cost = ($type == 7 && $period == 4) ?  DISC_COST7 : $cost;
  return $cost;
}
function apply_period_discounts($period, $type)
{
  //simple ads discount, 4 weeks + 1 week bonus
  $period = ($type == 4 && $period > 1) ?  $period + 1  : $period;
  return $period;
}
/* ajax */
$ajax_array = array(
  'add_video',
  'ajax_anekdot',
  'ajax_poll',
  'ajax_poll_n',
  'ajax_atr_data',
  'get_anons_newsite',
  'ajax_get_new_ads',
  'ajax_public_ad_up',
  'ajax_public_ad_res',
  'ajax_adv_ad_up',
  'ajax_get_model_by_id',
  'ajax_public_ad_vip',
  'ajax_truck_ad_vip',
  'ajax_truck_ad_res',
  'ajax_search',
  'ajax_upload_photo',
  'ajax_get_region_by_id',
  'ajax_get_city_by_id',
  'ajax_get_similar',
  'ajax_get_user_balance_history',
  'ajax_get_comments',
  'ajax_get_news',
  'ajax_get_news_count',
  'ajax_discus_news',
  'ajax_get_model'
);
if (!empty($_GET['act']))
{
  if (in_array($_GET['act'], $ajax_array))
  { 
    include('func/'.$_GET['act'].'.php');
    $smarty->register_outputfilter("extUrls");
    $smarty->display($_GET['act'].'.tpl');
    exit;
  }
  /* end ajax */
}
/* makes & models */
$abw_json_makes = $abw_core->carad->jsonMakes();
$smarty->assign('abw_json_makes', $abw_json_makes);
$abw_json_models = $abw_core->carad->jsonModels();
$smarty->assign('abw_json_models', $abw_json_models);
$abw_json_adv_makes = $abw_core->carad->jsonMakes('', true);
$smarty->assign('abw_json_adv_makes', $abw_json_adv_makes);
$abw_json_adv_models = $abw_core->carad->jsonModels('', '', true);
$smarty->assign('abw_json_adv_models', $abw_json_adv_models);
/* end makes & models */
if(isset($_POST['search_in'])&& isset($_POST['searchtext'])){
    include_once ROOT_DIR.'/includes/search.php';
}
include_once ROOT_DIR.'/includes/geoip/geo.php';
// DLYA SMALL FORM
require_once("small_forms.php");
// END OF DLYA SMALL FORM
include_once(ROOT_DIR .'/includes/stat/statistic.php');

if (empty($_SESSION['login']) && (isset($_COOKIE['autologin']))){
	include_once(ROOT_DIR .'/includes/autologin.php');
}
include_once(ROOT_DIR .'/libs/context_lib.php');
//      
/*
if (!empty($_GET['check_authorization'])){
	if ($_GET['check_authorization'] == 1 && empty($_SESSION['login'])){
		Header ("Location: /index.php?act=reg");
	}
}
*/
// banner
$smarty->register_function('show_banner','show_banner');
//include('smarty/plugins/function.show_banner.php');
if (!empty($_POST['act'])){ 
  $act = $_POST['act'];
}elseif (!empty($_GET['act'])){ 
  $act = $_GET['act']; 
}else{
  $act = 'main';
}
	//  
	if ($act=='finance' || $act == 'main'){
      $res=mysql_query('SELECT `name`, `usd_b`, `usd_s`, `eur_b`,	`eur_s`, `rub_b`, `rub_s` FROM `a_banks_xml` WHERE `last`=\'1\' AND (`name`=\'best\' OR `name`=\'n_bankk\')');
      $course8=array();
      while (list($namee, $usd_b, $usd_s, $eur_b, $eur_s, $rub_b, $rub_s) = mysql_fetch_array($res)) {
        if ($namee=="best"){
          $course8=array('usd_b'=>$usd_b, 'usd_s'=>$usd_s, 'eur_b'=>$eur_b, 'eur_s'=>$eur_s, 'rub_b'=>$rub_b, 'rub_s'=>$rub_s);
        }else{
          $course_nb=array('usd_b'=>$usd_b, 'eur_b'=>$eur_b, 'rub_b'=>$rub_b, );
        }
      }
      $smarty -> assign('course8', $course8);
      $smarty -> assign('course_nb', $course_nb);
      //
      $res2=mysql_query('SELECT `usd_eur_b`, `usd_eur_s`, `usd_rub_b`, `usd_rub_s` FROM `a_banks_xml_conv` WHERE `name`=\'best\'');
      $course8=array();
      while (list($usd_b, $usd_s, $eur_b, $eur_s) = mysql_fetch_array($res2)) {
        $course_conv=array('usd_b'=>$usd_b, 'usd_s'=>$usd_s, 'eur_b'=>$eur_b, 'eur_s'=>$eur_s);
      }
      $smarty -> assign('course_conv', $course_conv);
	}
// 
if ($act=="catalog2"){
	$res=mysql_query("SELECT id, name, main2, a_marka_id FROM c_marka group BY name");
	$list_mark = array();
	$list_mark_main = array();
	while (list($id, $name, $main, $a_marka_id) = mysql_fetch_array($res)) {
		if ($name==" ()"){
			$name="";
		}elseif ($name=="Mercedes-Benz"){
			$name="Mercedes";
		}
		if ($main==1){
			$list_mark_main[]="<a href=\"https://www.abw.by/index.php?do=search&act=catalog2&mark_id=".$id."&model_id2=&year11=&x=33&y=14&lim=main&a_marka_id=".$a_marka_id."\" class=\"cat_noli\"><img src=/images/cat_bg4.png alt=\"\" style=\"border: 0;\" align=absmiddle width=4 height=5>&nbsp;<font size=\"2\">".$name."</font></a>"; //<img src='/images/more_cat2.jpg'  style='border: 0px;' alt=''>
		}
		
		$list_mark[]="<a href=\"https://www.abw.by/index.php?do=search&act=catalog2&mark_id=".$id."&model_id2=&year11=&x=33&y=14&lim=main&a_marka_id=".$a_marka_id."\" class=\"cat_noli\"><img src=/images/cat_bg4.png alt=\"\" style=\"border: 0;\" align=absmiddle width=4 height=5>&nbsp;<font size=\"2\">".$name."</font></a>&nbsp;";
	}
	// 
	$list_mark_main[]="<a href=\"javascript:show()\" class=\"cat_noli\"><font size=\"2\"> </font>  <img src=\"/images/more_cat2.jpg\"  style=\"border: 0px;\" alt=\"\"><img src=\"/images/more_cat2.jpg\"  style=\"border: 0px;\" alt=\"\"><strong></strong></a>";
	$list_mark[]="<a href=\"javascript:show_main()\" class=\"cat_noli\"><font size=\"2\"></font>  <img src=\"/images/more_cat2.jpg\"  style=\"border: 0px;\" alt=\"\"><img src=\"/images/more_cat2.jpg\"  style=\"border: 0px;\" alt=\"\"><strong></strong></a>";
	$smarty->assign('mark_cat_list9', $list_mark);
	$smarty->assign('mark_cat_list9_main', $list_mark_main);
}
          
if (($act=="number" && $sub_act=="rating") || $act == "archive"){
	//reiting note
	if ($act == "archive"){
		$lim_rate = "0, 3";
		$lim_simv =50;
	}else if ($sub_act == "rating"){
		$lim_rate = "0, 60";
		$lim_simv =140;
	}else{
		$lim_rate = "0, 5";
		$lim_simv =50;
	}
	if(isset($_GET['mobile1']) || isset($_POST['mobile1'])){
		if($sub_act_m == "rating"){
			$lim_rate = "0, 60";
			if($_GET['mobile1']==2) $_GET['old']=1;
		}else{
			$lim_rate = "0, 10";
			if($_GET['mobile1']==2) $_GET['old']=1;
		}
	}
	if  ($_GET['peri_r']=="week"){
	$timenumb2 = (date("U", time())-295200-604800);
	$timenumb1 = (date("U", time())-295200-(604800*2)); 
	$smarty->assign('peri_r2',1);
	}else if  ($_GET['peri_r']=="month"){
	$timenumb2 = (date("U", time())-295200);
	$timenumb1 = (date("U", time())-295200-(604800*4)); 
	$lim_rate = "0, 60";
	$smarty->assign('peri_r2',4);
	}else if  ($_GET['peri_r']=="3month"){
	$timenumb2 = (date("U", time())-295200);
	$timenumb1 = (date("U", time())-295200-(604800*12)); 
	$lim_rate = "0, 100";
	$smarty->assign('peri_r2',12);	
	}else if  ($_GET['peri_r']=="6month"){
	$timenumb2 = (date("U", time())-295200);
	$timenumb1 = (date("U", time())-295200-(604800*24)); 
	$lim_rate = "0, 100";
		if(isset($_GET['mobile1']) || isset($_POST['mobile1'])){
				$lim_rate = "0, 60";
				if($_GET['mobile1']==2) $_GET['old']=1;
		}
	$smarty->assign('peri_r2',24);	
	}else if  ($_GET['peri_r']=="12month"){
	$timenumb2 = (date("U", time())-295200);
	$timenumb1 = (date("U", time())-295200-(604800*55)); 
	$lim_rate = "0, 100";
		if(isset($_GET['mobile1']) || isset($_POST['mobile1'])){
				$lim_rate = "0, 60";
				if($_GET['mobile1']==2) $_GET['old']=1;
		}
	$smarty->assign('peri_r2',48);	
	}else if  ($_GET['peri_r']=="24month"){
	$timenumb2 = (date("U", time())-295200);
	$timenumb1 = (date("U", time())-295200-(604800*118)); 
	$lim_rate = "0, 100";
	$smarty->assign('peri_r2',96);	
	}else{
	$timenumb2 = (date("U", time())-295200);  //381600 = (24  4 + 10)  3600.    .  00:00 
	$timenumb1 = (date("U", time())-295200-604800);  // 604800  
	}
	$reit=mysql_query("SELECT p.id, p.number_id, p.title, p.text, p.views, h.id FROM a_arhiv_note p, a_arhiv_number b, a_arhiv_note_photo h WHERE h.note_id = p.id  AND p.number_id= b.id AND b.datetime < ".$timenumb2." AND b.datetime > ".$timenumb1." GROUP BY p.id ORDER BY p.views DESC LIMIT ".$lim_rate."");
	$reiting = array();
	while (list($id, $number_id, $title, $text, $views, $photo_id)=mysql_fetch_array($reit)){
			$temp1['fname'] = "./photos/number/".$photo_id."_small.jpg";
			$text = stripslashes($text);
			$text = trim($text);
			$text = strip_tags($text);
			$text = str_replace("&nbsp;", "", $text);//<P>&nbsp;</P>
			$text = str_replace("<P>", "", $text);//<P>&nbsp;</P>
			$text = str_replace("</P>", "", $text);//<P>&nbsp;</P>
			$text = str_replace("<BR>", "", $text);//<P>&nbsp;</P>
			$text = substr($text, 0, $lim_simv);
			$text = substr($text, 0, strrpos($text, " "))."...";
			$reiting[$id] = array("title"=>$title,
							 	 "text"=>$text,
							  	"ph_id"=>$photo_id,
								"views"=>$views);
	}
	$smarty->assign('reiting',$reiting);
	if ($_GET['sub_act']=="rating"){
		$reit=mysql_query("SELECT p.id, p.number_id, p.title, p.text, p.views FROM a_arhiv_note p, a_arhiv_number b, a_voting_article_choose s WHERE s.note_id=p.id AND p.number_id= b.id AND b.datetime < ".$timenumb2." AND b.datetime > ".$timenumb1." ORDER BY s.rating DESC, s.n5 DESC LIMIT ".$lim_rate."");
		$reiting_vote = array();
		
		while (list($id, $number_id, $title, $text, $views)=mysql_fetch_array($reit)){		
				$res=mysql_query("SELECT id FROM a_arhiv_note_photo WHERE note_id=".$id." ORDER BY id");
				while (list($ph_id)=mysql_fetch_array($res)){
					$temp1['fname'] = "./photos/number/".$ph_id."_small.jpg";
					if (file_exists($temp1['fname'])){
						break;
					}
				}
				$text = stripslashes($text);
				$text = trim($text);
				$text = strip_tags($text);
				$text = str_replace("&nbsp;", "", $text);//<P>&nbsp;</P>
				$text = str_replace("<P>", "", $text);//<P>&nbsp;</P>
				$text = str_replace("</P>", "", $text);//<P>&nbsp;</P>
				$text = str_replace("<BR>", "", $text);//<P>&nbsp;</P>
				$text = substr($text, 0, $lim_simv);
				$text = substr($text, 0, strrpos($text, " "))."...";
				$reiting_vote[$id] = array("title"=>$title,
								 	 "text"=>$text,
								  	"ph_id"=>$ph_id,
									"views"=>$views);
		}
		$smarty->assign('reiting_vote',$reiting_vote);
	}
}
// public
// 
    $res = mysql_query('SELECT `country_id`, `name` FROM `a_country`');
    $sel_country = '<select name="u_country" onchange="xajax_addCity(this.value);">';
    $sel_country .= '<option value=" "></option>';
    
    if (empty($_SESSION['small_form_1']['u_country'])){
      $sel_country .= '<option value="1" selected>��������</option>';
    }
    
    while (list($country_id,$name) = mysql_fetch_array($res)) {      
      if (!empty($_SESSION['small_form_1']['u_country'])) {
        if ($_SESSION['small_form_1']['u_country'] == $country_id) {
          $sel_country .= '<option value="'.$country_id.'" selected>'.$name.'</option>';
        }else{
          $sel_country .= '<option value="'.$country_id.'">'.$name.'</option>';
        }
      }else{        
        if ($country_id != '1'){
          $sel_country .= '<option value="'.$country_id.'">'.$name.'</option>';
        }
      }
    }
    $sel_country .= '</select>';
    $smarty->assign('country',$sel_country);
      
	//   
    
    $u_country = (isset($_SESSION['small_form_1']['u_country'])) ? $_SESSION['small_form_1']['u_country'] : '1';
    if(isset($u_country)){
	$res = mysql_query('SELECT `city_id`, `name` FROM `a_city` WHERE `country_id` = \''.$u_country.'\' AND `city_id` != \'1\'');
	$sel_city = "<select name='u_city'><option value=''></option>";
	if (isset ($_GET['mobile_sp'])){
		$sel_city = "<select name='city_id'><option value=''></option>";
	}else{
		$sel_city = "<select name='u_city'><option value=''></option>";
	}
	
	while (list($city_id,$name) = mysql_fetch_array($res)) {
		if (!empty($_SESSION['small_form_1']['u_city'])) {
			if ($_SESSION['small_form_1']['u_city'] == $city_id) {
				$sel_city .= "<option value='".$city_id."' selected>".$name."</option>";
			}else{
				$sel_city .= "<option value='".$city_id."'>".$name."</option>";
			}
		}else{
			$sel_city .= "<option value='".$city_id."'>".$name."</option>";
		}
	}
	$sel_city .= "</select>";
	$smarty->assign('temp_cities',$sel_city);
    }
// initialising xajax	
require_once("xajax_php/xajax.inc.php");
$xajax = new xajax("","xajax_","windows-1251");
$xajax->setCharEncoding('windows-1251');
if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1") {
	$xajax->decodeUTF8InputOff();
}
else {
	$xajax->decodeUTF8InputOn();
}
$xajax->debugOn();
$xajax->waitCursorOn();
// search auto
	function addModel2($marka_id) {
		$res = mysql_query("SELECT model_id, name_eng FROM a_model WHERE marka_id = '".$marka_id."' ORDER BY name_eng");
		$sel_model = "<select name=\"model\" onchange=\"document.my_little.model.value=document.getElementById('spec_model').value\" id=\"spec_model\"><option value=''></option>";
		while (list($model_id,$name) = mysql_fetch_array($res)) {
			if ($marka_id == 2) {
				if (!preg_match("/^[0-9]{1,3}/i",$name)) $sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
			}
			else $sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
		}
		$sel_model .= "</select>";
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("model2","innerHTML",$sel_model);
		return $objResponse;
	}
	
	function addModel_small_8($marka_id) {
		$res = mysql_query("SELECT model_id, name_eng FROM a_model WHERE marka_id = '".$marka_id."' ORDER BY name_eng");
		$sel_model = "<select name=\"model\" onchange=\"document.my_little_8.model.value=document.getElementById('spec_model').value\" id=\"spec_model\"><option value=''></option>";
		while (list($model_id,$name) = mysql_fetch_array($res)) {
			$sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
		}
		$sel_model .= "</select>";
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("model2","innerHTML",$sel_model);
		return $objResponse;
	}
	// xajax        /   /
	function addModel($marka_id) {
		$res = mysql_query("SELECT model_id, name_eng FROM a_model WHERE marka_id = '".$marka_id."' ORDER BY name_eng");
		$sel_model = "<select name='model' id='2'><option value=''>---</option>";
		while (list($model_id,$name) = mysql_fetch_array($res)) {
			if ($marka_id == 2) {
				if ($model_id != 11 && $model_id != 21 && $model_id != 34 && $model_id != 1452 && $model_id != 1490) {
					$sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
				}
			}else if ($marka_id == 28){ // mersedes
				if ($model_id != 413 && $model_id != 411 && $model_id != 382 && $model_id != 407 && $model_id != 377 && $model_id != 378 && $model_id != 380 && $model_id != 410 && $model_id != 381) {
					$sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
				}
			}else {
				$sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
			}
		}
		$sel_model .= "</select>";
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("model","innerHTML",$sel_model);
		return $objResponse;
	}
	function addModelException($marka_id) {
		$res = mysql_query("SELECT model_id, name_eng FROM a_model WHERE marka_id = '".$marka_id."' ORDER BY name_eng");
		$sel_model = '<select name="model" id="2"><option value="">---</option>';
		while (list($model_id,$name) = mysql_fetch_array($res)) {
			if ($marka_id == 2) {
				
					if (!preg_match("/^[0-9]{1,3}/i",$name)) $sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
				
			}
			else $sel_model .= "<option value='".$model_id."'>".$name."</option>\n";
		}
		$sel_model .= "</select>";
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("model","innerHTML",$sel_model);
		return $objResponse;
	}
	// xajax, 
	function addCity($country) {
		$res = mysql_query('SELECT `city_id`, `name` FROM `a_city` WHERE `country_id` = \''.$country.'\' AND `city_id` !=\'1\'');
		$sel_city = '<select name="u_city"><option value="">---</option>';
		while (list($city_id,$name) = mysql_fetch_array($res)) {
			$sel_city .= '<option value="'.$city_id.'">'.$name.'</option>';
		}
		$sel_city .= "</select>";
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("u_city","innerHTML",$sel_city);
		return $objResponse;		
	}
  
    function addCity2($region) {
		$res = mysql_query("SELECT id, name FROM a_city2 WHERE region_id = '".(int)$region."'");
		$sel_city = '<select name="u_city_o"><option value="">---</option>';
		while (list($city_id,$name) = mysql_fetch_array($res)) {
			$sel_city .= '<option value="'.$name.'">'.$name.'</option>';
		}
		$sel_city .= '</select>';
		$objResponse = new xajaxResponse('windows-1251');
		$objResponse->addAssign("city2td","innerHTML",$sel_city);
		return $objResponse;
	}
	include("func/iceboy_include.php");
	$xajax->registerFunction("addModel");	
	$xajax->registerFunction("addCity");
    $xajax->registerFunction("addCity2");
	$xajax->registerFunction("addModel2");
	$xajax->registerFunction("addModel_small_8");
	$xajax->registerFunction("addModelException");
	$xajax->processRequests();
	
	//  
	
	$z=date("Y",time());
	if (isset ($_GET['mobile_sp'])){
		$sel_year1 = "<select name='year_id'><OPTION selected value='0'></option>";
	}else{
		$sel_year1 = "<select name='year1' style='width: 65px;'><option value='1960'></option>";
    $sel_year1n = "<select name='year1' class='short left'><option value='1960'> </option>";
	}
	if (filter_input(INPUT_GET, 'act', FILTER_SANITIZE_SPECIAL_CHARS)=="truck_form_m"){
		$sel_year1 = "<select name='year_id'><OPTION selected value='0'></option>";
	}
	for($i=$z;$i>=1960;$i--) {
		if (!empty($_SESSION['small_form_1']['year1'])) {
			if ($_SESSION['small_form_1']['year1'] == $i) {
				$sel_year1 .= "<option value='".$i."' selected>".$i."</option>";
        $sel_year1n .= "<option value='".$i."' selected>".$i."</option>";
			}else{
				$sel_year1 .= "<option value='".$i."'>".$i."</option>";
        $sel_year1n .= "<option value='".$i."'>".$i."</option>";
			}
		}else{
			$sel_year1 .= "<option value='".$i."'>".$i."</option>";
      $sel_year1n .= "<option value='".$i."'>".$i."</option>";
		}
	}
	$sel_year1 .= "</select>";
  $sel_year1n .= "</select>";
	
	if (isset ($_GET['mobile_sp'])){
		$sel_year2 = "<select name='year_id_max'><option value='".$z."'>".$z."</option>";
	}else{
		$sel_year2 = "<select name='year2' style='width: 65px;'><option value='".$z."'>".$z."</option>";
    $sel_year2n = "<select name='year2' class='short right'><option value='".$z."'>".$z."</option>";
	}
	if (filter_input(INPUT_GET, 'act', FILTER_SANITIZE_SPECIAL_CHARS)=="truck_form_m"){
		$sel_year2 = "<select name='year_id_max'><OPTION selected value='0'>".$z."</option>";
    $sel_year2n = "<select name='year_id_max'><OPTION selected value='0'>".$z."</option>";
	}
	
	for($i=$z-1;$i>=1960;$i--) {
		if (!empty($_SESSION['small_form_1']['year2'])) {
			if ($_SESSION['small_form_1']['year2'] == $i) {
				$sel_year2 .= "<option value='".$i."' selected>".$i."</option>";
        $sel_year2n .= "<option value='".$i."' selected>".$i."</option>";
			}else{
				$sel_year2 .= "<option value='".$i."'>".$i."</option>";
        $sel_year2n .= "<option value='".$i."'>".$i."</option>";
			}
		}else{
			$sel_year2 .= "<option value='".$i."'>".$i."</option>";
      $sel_year2n .= "<option value='".$i."'>".$i."</option>";
		}
	}
	$sel_year2 .= "</select>";
  $sel_year2n .= "</select>";
	$smarty->assign('date',array($sel_year1,$sel_year2,$sel_year1n,$sel_year2n));
	// /  
  
  // 
  
	
	// type engine
	$res = mysql_query("SELECT engine_id, name FROM a_engine");
	$engine_search = "";
	while ($arr = mysql_fetch_row($res)) {
		$engine_search[$arr[0]] = $arr[1];
	}
	$smarty->assign('engine_search',$engine_search);
	// /type engine
    
  // get makes array id => name
    $smarty->assign('marka2', $abw_core->carad->Make('', false));
        
 // get works for sto array id => name
    /*
    $smarty->assign('work_sto', $abw_core->carad->sto_work());
    */
  
 //  
	$res = mysql_query("SELECT body_id, name FROM a_body");
	$sel_body = "<select name='type_body' class='hidden_field_search'><option value=''></option>";
	while (list($body_id,$name) = mysql_fetch_array($res))  {
    if (filter_input(INPUT_GET, 'type_body', FILTER_SANITIZE_SPECIAL_CHARS) == $body_id)
    {
      $sel_flag = "selected";
    }
    else
    {
      $sel_flag = "";
    }
		$sel_body .= "<option value='".$body_id."' ".$sel_flag." >".$name."</option>";
	}
	$sel_body .= "</select>";	
	$smarty->assign('body',$sel_body);
	
  //   
  $res = mysql_query("SELECT body_id, name FROM a_body");
	$sel_body = "<option value=''></option>";
	while (list($body_id,$name) = mysql_fetch_array($res))  {
    if (filter_input(INPUT_GET, 'a_body', FILTER_SANITIZE_SPECIAL_CHARS) == $body_id)
    {
      $sel_flag = "selected";
    }
    else
    {
      $sel_flag = "";
    }
		$sel_body .= "<option value='".$body_id."' ".$sel_flag." >".$name."</option>";
	}
	$smarty->assign('a_body',$sel_body);
if (!empty($_SESSION['small_form_1']['marka'])){
  if (is_array($_SESSION['small_form_1']['marka']))
  {
    $model_array = modelArray();
    $model2 = array();
    foreach ($_SESSION['small_form_1']['marka'] as $k => $v)
    {
      if ($v != '')
      {
        $res = $_db->Select(
          'a_model',
          array(
            'select' => '*',
            'where' => array('marka_id' => $v),
            'order' => array('ASC' => 'name_eng')
          )
        );
        if (count($res) > 0)
        {
          foreach ($res as $v)
          {
            $model2[$k][$v['model_id']]['name'] = $v['name_eng'];
          }
        }
      }
    }
    $smarty->assign('model2', $model2);
  }
  else
  {
    if (!empty($_SESSION['small_form_1']['marka']))
    {
      $res = mysql_query("SELECT model_id, name_eng FROM a_model WHERE marka_id='".$_SESSION['small_form_1']['marka']."' ORDER BY name_eng");
      $marka = "";
      while ($arr = mysql_fetch_row($res)) {
        $marka[$arr[0]]['name'] = $arr[1];
        $marka[$arr[0]]['set'] = 0;
        if (!empty($_SESSION['small_form_1']['model'])) {
          if ($_SESSION['small_form_1']['model'] == $arr[0]) {
            $marka[$arr[0]]['set'] = 1;
          }
        }
      }
      $smarty->assign('model2',$marka);
    }
  }
}
// /search auto
	/*********************/
		include_once(ROOT_DIR.'/func/redbase.inc.php');
	/************/
//4astnie ob'yavleniya
if (!empty($_REQUEST['act'])) {
	if ($_REQUEST['act'] == "search_adv") {
		if (empty($_REQUEST['cat_id']) || empty($_REQUEST['sub_id'])) {
			if (!empty($_REQUEST['act_adv']) & !empty($_SESSION['post_info']['cat_id']) & !empty($_SESSION['post_info']['sub_id'])) {
				$_REQUEST['cat_id'] = $_SESSION['post_info']['cat_id'];
				$_REQUEST['sub_id'] = $_SESSION['post_info']['sub_id'];
			}
		}
		
		settype($_REQUEST['cat_id'],"integer");
		settype($_REQUEST['sub_id'],"integer");
		
		if ($_REQUEST['cat_id'] == "1" & $_REQUEST['sub_id'] == "1") {
			$set_small_form = "&set_small_form_2=1";
		}elseif ($_REQUEST['cat_id'] == "2" & $_REQUEST['sub_id'] == "7"){
			$set_small_form = "&set_small_form_3=1";
		}elseif ($_REQUEST['cat_id'] == "4" & $_REQUEST['sub_id'] == "15"){
			$set_small_form = "&set_small_form_4=1";
		}elseif ($_REQUEST['cat_id'] == "4" & $_REQUEST['sub_id'] == "16"){
			$set_small_form = "&set_small_form_5=1";
		}elseif ($_REQUEST['cat_id'] == "5" & $_REQUEST['sub_id'] == "18"){
			$set_small_form = "&set_small_form_6=1";
		}elseif ($_REQUEST['cat_id'] == "5" & $_REQUEST['sub_id'] == "19"){
			$set_small_form = "&set_small_form_7=1";
		}else{
			$set_small_form = "";
		}
		$z="";
		if ($_REQUEST['cat_id']==1 || $_REQUEST['cat_id']==2 || $_REQUEST['cat_id']==5){
          //  
          $res=mysql_query("SELECT a.name FROM a_adv_cat as a, a_adv_subcat as b WHERE a.id=b.id_cat AND a.id='$_REQUEST[cat_id]' AND b.id='$_REQUEST[sub_id]'");
          list($name)=mysql_fetch_array($res);
          $z.="<p><b>".$name."</b></p>";
      
          //  
          if ($_REQUEST['cat_id']==1){
            $res=mysql_query("SELECT DISTINCT marka_id,name,name_eng FROM a_adv_marka ORDER BY name_eng, name");
          }else{
            $res=mysql_query("SELECT DISTINCT marka_id,name,name_eng FROM a_marka ORDER BY name_eng");
          }   
          $marki=array();
          while (list($id,$name,$name_eng)=mysql_fetch_array($res)) {
            $marki[$id]['name']=$name;
            $marki[$id]['name_eng']=$name_eng;
          }
      
          $res=mysql_query("SELECT DISTINCT marka, COUNT(id) FROM a_adv_advertisement".$_pref." WHERE id_cat='$_REQUEST[cat_id]' AND id_sub='$_REQUEST[sub_id]' AND hide !=1 GROUP BY marka");
          $i=0;
          while (list($id, $count)=mysql_fetch_array($res)) {
            $marki[$id]['isset']=1;
            $marki[$id]['count']=$count;
            $i++;
          }
          
          //     !
          if ($_REQUEST['cat_id']==5){
            if ($i!=0){
              $z.="<a  class=\"cat_noli\"  href=\"/index.php?act=adv&act_adv=see&id_cat=$_REQUEST[cat_id]".$_urlparam."&id_sub=$_REQUEST[sub_id]&marka_id=0&act_general=see&adv=2$set_small_form\"> </a> <br/>";
            }
          }	
      
      //   
      $search  = array(' ', '/');
      $replace = array('_', '~');
          
      $add = '<a class="cat_noli" href="https://car.abw.by/market"><img src="/images/cat_bg4.png" alt=""> ���������</a> <br/>
              <a class="cat_noli" href="https://moto.abw.by/market"><img src="/images/cat_bg4.png" alt=""> ���������</a> <br/>
              <a class="cat_noli" href="https://boats.abw.by/market"><img src="/images/cat_bg4.png" alt=""> �������� �����</a> <br/>
              <a class="cat_noli" href="https://boats.abw.by/engine"><img src="/images/cat_bg4.png" alt=""> �������� ������</a> <br/>
              <a class="cat_noli" href="//www.abw.by/truckparts/sell/"><img src="/images/cat_bg4.png" alt=""> �/� ��� ����������</a> <br/>';
      
      if ($_REQUEST['cat_id']==2){
          $check_cat = $check_cat_public;
          $name_cat ="���������: ";          
          $zz = $add;
        }else{
          $check_cat = $check_cat_adv;
          $name_cat ="C., ";
        }
      
      foreach ($marki as $key => $val) { 
        if ($val['name_eng']==''){
          $name_out = $val['name'];
          $name_url = str_replace($search, $replace, $val["name"]);
          
        }else{
          $name_out = $val['name_eng']; 
          $name_url = str_replace($search, $replace, $val["name_eng"]);
        }
        
        if (empty($name_out)){break;}
 
        switch ($_GET["cat_id"]) {
            case 1: { // 
                switch ($_GET["sub_id"]) {
                    case 1: $type_url = 'showtrucks/sell'; break;
                    case 2: $type_url = 'showtrucks/buy'; break;
                    case 3: $type_url = 'truckparts/sell'; break;
                    case 4: $type_url = 'truckparts/buy'; break;
                    case 5: $type_url = 'showtrucks/exchange'; break;
                    case 6: $type_url = 'showtrucks/other'; break;
                }
                if (in_array($key,$check_cat_adv)){
                    $zz .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }else{
                    $z .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }	
            }	
            case 2: { // 
                switch ($_GET["sub_id"]) {
                    case 7: $type_url = "parts/sell"; break;
                    case 8: $type_url = "parts/buy"; break;
                }
                if (!empty($val['isset'])){
                  if (in_array($key,$check_cat_public)){
                      $zz .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                  }else{
                      $z .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                  }
                }
                break;
            }
            case 5: { // 
                $z .= '<a class="cat_noli" href="/disks/';
                switch ($_GET["sub_id"]) {
                    case 18: $z .= "sell"; break;
                    case 19: $z .= "buy"; break;
                }
                $z .= '/'.$name_url.'">'.$name_out.'</a><br/>'; break;
            }
        }
      }
      $z = "<p><b>".$name_cat."</b></p>".$zz."<hr />".$z;
	
      if ($i==0){
        $z.="  .";
      }
    }
    $smarty->assign('adv_razdel2',$z);
  }
}
if (!empty($_GET['act_general'])){
	if ($_GET['act_general']=="see"){
		$smarty->assign('act_general',$_GET['act_general']);
		settype($_GET['adv'],"integer");
		settype($_GET['id_cat'],"integer");
		settype($_GET['id_sub'],"integer");
		if ($_GET['id_cat'] == "1" & $_GET['id_sub'] == "1") {
			$set_small_form = "&set_small_form_2=1";
		}elseif ($_GET['id_cat'] == "2" & $_GET['id_sub'] == "7"){
			$set_small_form = "&set_small_form_3=1";
		}elseif ($_GET['id_cat'] == "4" & $_GET['id_sub'] == "15"){
			$set_small_form = "&set_small_form_4=1";
		}elseif ($_GET['id_cat'] == "4" & $_GET['id_sub'] == "16"){
			$set_small_form = "&set_small_form_5=1";
		}elseif ($_GET['id_cat'] == "5" & $_GET['id_sub'] == "18"){
			$set_small_form = "&set_small_form_6=1";
		}elseif ($_GET['id_cat'] == "5" & $_GET['id_sub'] == "19"){
			$set_small_form = "&set_small_form_7=1";
		}else{
			$set_small_form = "";
		}
		if ($_GET['adv']==2){
			$z="";
			if ($_GET['id_cat']==1 || $_GET['id_cat']==2 || $_GET['id_cat']==5){
				$res=mysql_query("SELECT a.name, b.name FROM a_adv_cat as a, a_adv_subcat as b WHERE a.id=b.id_cat AND a.id='$_GET[id_cat]' AND b.id='$_GET[id_sub]'");
				list($name,$name2)=mysql_fetch_array($res);
				$z.="<p><b>".$name."</b></p>";
				// 'zzzz'  ,    .    ,    
				if ($_GET['id_cat']==1){
					$res=mysql_query("SELECT DISTINCT marka_id,name,name_eng FROM a_adv_marka ORDER BY name_eng, name");
                }else{
                  $res=mysql_query("SELECT DISTINCT marka_id,name,name_eng FROM a_marka ORDER BY name_eng, name");
				}
				$marki=array();
				while (list($id,$name,$name_eng)=mysql_fetch_array($res)) {
					$marki[$id]['name']=$name;
					$marki[$id]['name_eng']=$name_eng;
				}
				$res=mysql_query("SELECT DISTINCT marka, COUNT(id) FROM a_adv_advertisement WHERE id_cat='$_GET[id_cat]' AND id_sub='$_GET[id_sub]' AND hide=0 GROUP BY marka");
				$i=0;
				while (list($id, $count)=mysql_fetch_array($res)) {
                  $marki[$id]['isset']=1;
                  $marki[$id]['count']=$count;
                  $i++;
				}
				//
				if ($_GET['id_cat']==5){
					if ($i!=0){
						$z.="<a class=\"aboutmarka\" href=\"/index.php?act=adv&act_adv=see&id_cat=$_GET[id_cat]".$_urlparam."&id_sub=$_GET[id_sub]&marka_id=0&act_general=see&adv=".$_GET['adv'].$set_small_form."\"> </a> <br/>";
					}
				}	
                
                                
                //   
                  $search  = array(' ', '/');
                  $replace = array('_', '~');
                
                $add = '
                  <a class="cat_noli" href="http://car.abw.by/market"><img src="/images/cat_bg4.png" alt=""> ���������</a> <br/>
                  <a class="cat_noli" href="http://moto.abw.by/market"><img src="/images/cat_bg4.png" alt=""> ���������</a> <br/>
                  <a class="cat_noli" href="http://boats.abw.by/market"><img src="/images/cat_bg4.png" alt=""> �������� �����</a> <br/>
                  <a class="cat_noli" href="http://boats.abw.by/engine"><img src="/images/cat_bg4.png" alt=""> �������� ������</a> <br/>
                  <a class="cat_noli" href="//www.abw.by/truckparts/sell/"><img src="/images/cat_bg4.png" alt=""> �/� ��� ����������</a> <br/>';
                if ($_REQUEST['id_cat']==2){
                  $check_cat = $check_cat_public;
                  $name_cat ="���������: ";
                  $zz = $add;
                }else{
                  $check_cat = $check_cat_adv;
                  $name_cat ="C., ";
                }
                
                  
				foreach ($marki as $key => $val) {          
					if (!empty($val['isset'])){
						if ($val['name_eng'] == "") {
							$name_out = $val["name"];
							$name_url = str_replace($search, $replace, $val["name"]);
						}
						else {
							$name_out = $val["name_eng"];
							$name_url = str_replace($search, $replace, $val["name_eng"]);
						}
 
						switch ($_GET["id_cat"]) {
							case 1: { // 
								switch ($_GET["id_sub"]) {
									case 1: $type_url = 'showtrucks/sell'; break;
									case 2: $type_url = 'showtrucks/buy'; break;
									case 3: $type_url = 'truckparts/sell'; break;
									case 4: $type_url = 'truckparts/buy'; break;
									case 5: $type_url = 'showtrucks/exchange'; break;
									case 6: $type_url = 'showtrucks/other'; break;
								}
                if (in_array($key,$check_cat_adv)){
                  $zz .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }else{
                  $z .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }								
							}                            
							case 2: { // 
								switch ($_GET["id_sub"]) {
									case 7: $type_url = "parts/sell"; break;
									case 8: $type_url = "parts/buy"; break;
								}
								if (in_array($key,$check_cat_public)){
                  $zz .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }else{
                  $z .= '<a class="cat_noli" href="/'.$type_url.'/'.$name_url.'"><img src="/images/cat_bg4.png" alt=""> '.$name_out.' ('.$val["count"].')</a> <br/>'; break;
                }
							}
							case 5: { // 
								$z .= '<a class="cat_noli" href="/disks/';
								switch ($_GET["id_sub"]) {
									case 18: $z .= "sell"; break;
									case 19: $z .= "buy"; break;
								}
								$z .= '/'.$name_url.'">'.$name_out.'</a> <br/>'; break;
							}
							default: $z.="<a class=\"cat_noli\" href=\"/index.php?act=adv&act_adv=see&id_cat=$_GET[id_cat]".$_urlparam."&id_sub=$_GET[id_sub]&marka_id=$key&act_general=see&adv=".$_GET['adv'].$set_small_form."\"><img src=\"/images/cat_bg4.png\" alt=\"\"> $val[name] ($val[count])</a> <br/>";
						}
					}
				}
        
        $z = "<p><b>".$name_cat."</b></p>".$zz."<hr />".$z;
			}
			$smarty->assign('adv_razdel2',$z);
		}
	}
	
	
	
	
	if ($_GET['act_general']=="adv"){		
		if (empty($_GET['step'])){
			// s4et4ik / ????..
			$res = mysql_query('SELECT datetime FROM a_count_last WHERE id='.(($_redbase)?'2':'1'));
			$time = '';
			list($time) = mysql_fetch_array($res);
			if (time() - $time > ($_redbase)?72000:1800){
				require_once("count.php");
			}
			
			//-- VIVOD SPISKA KATEGORIJ I PODKATEGORIJ ---------------------------------//
			$smarty->assign('act_general',$_GET['act_general']);
			
			$res = mysql_query('SELECT count, adv_type, id_cat, id_sub FROM a_count'.$_pref);
			$count = array();	$count_2 = array();		$temp = array();
			while(list($temp['count'], $temp['adv_type'], $temp['id_cat'], $temp['id_sub']) = mysql_fetch_array($res)){
				if (!empty($temp['adv_type'])){
					$count[$temp['adv_type']] = $temp['count'];
				}
				if (!empty($temp['id_sub'])){
					$count_2[$temp['id_sub']] = $temp['count'];
				}
			}
			
        // otobrajenie rubrik obiavl
			$adv_razdel = '<table border=0 cellspacing=0 cellpadding=0 width="700" style="border:0px;">
            <tr>
                                    <td align="center" colspan="2">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="right" width="20%">&nbsp;</td>	
                                    <td align="left"><font size="2" color="#414249"><b>�������� ����</b></font></td>
                                  </tr>
                                  <tr>
			<tr>
				<td align=center colspan=2>';
			if(!$_redbase)						
			for ($i=1; $i<=2; $i++){
				switch ($i){
				    case 1:
	        			$name = "������� ����";
						$_path = "sell";
						$mailing_aut = "";
	        			break;
	    			case 2:
	        			$name = "������� ����";
						$_path = "buy";
						$mailing_aut = "";
	        			break;
				    case 3:
	        			$name = "����� ����";
						$_path = "exchange";
						$mailing_aut = "";
	        			break;
				}
				$gruz_pl=0;
				if ($i==1) $gruz_pl=2500;
				if ($_redbase)                   
                  $adv_razdel .= "<tr>
			 		<td align='right' width='25%' >
				  		
			 		</td>
			  		<td align='left'>
			  			<a href='/index.php?act=showalladvs&adv_type=".$i."&set_small_form_1=1&set_small_form_8=1".$_urlparam."' class=\"rubriks_show\">".$name."</a> <font color=\"#46678A\">(".$count[$i].")</font>
					</td>
			 	</tr>";
				else $adv_razdel .= "<tr>
			 		<td align='right' width='25%' >
				  		
			 		</td>
			  		<td align='left'>
			  			<a href='/showcars/".$_path."/' class=\"rubriks_show\">".$name."</a> <font color=\"#46678A\">(".($count[$i]+$gruz_pl).")</font>
					".$mailing_aut."
					</td>
			 	</tr>";
			}
				if(!$_redbase)
				$adv_razdel .= "
				<tr>
	<td colspan=2>&nbsp;</td>
</tr>
";
			
			
			$temp = array();
			
			$res = mysql_query("SELECT `id`, `name` FROM `a_adv_cat` WHERE `id` NOT IN (3,6,7,8,9,10) ORDER BY `id`");
			while (list($id, $name) = mysql_fetch_array($res)){
				$temp['cat'][$id] = $name;
			}
			$res = mysql_query("SELECT `id`, `id_cat`, `name` FROM `a_adv_subcat` WHERE `id` NOT IN (9,10,11,12,13,14) ORDER BY `id`");
			while (list($id, $id_cat, $name) = mysql_fetch_array($res)){
				$temp['sub_cat'][$id]['id_cat'] = $id_cat;
				$temp['sub_cat'][$id]['name'] = $name;
			}
			//echo "<pre>"; print_r($temp); echo "</pre>";
			
			foreach($temp['cat'] as $key => $val){
				//echo "<pre>"; print_r($val); echo "</pre>";
				$adv_razdel .= "<tr>
			 		<td align='center' colspan='2'>&nbsp;</td>
			 	</tr>
			 	<tr>
					<td align='right' width='20%'>&nbsp;</td>	
					<td align='left'>
					 	<font size=\"2\" color=\"#414249\"><b>".$val.":</b></font> 
					</td>
			 	</tr>";
				foreach ($temp['sub_cat'] as $sub_key => $sub_val){
					if ($key == $sub_val['id_cat']){
						if ($key == "1" & $sub_key == "1") {
							$set_small_form = "&set_small_form_2=1";
						}elseif ($key == "2" & $sub_key == "7"){
							$set_small_form = "&set_small_form_3=1";
						}elseif ($key == "4" & $sub_key == "15"){
							$set_small_form = "&set_small_form_4=1&order_id=0";
						}elseif ($key == "4" & $sub_key == "16"){
							$set_small_form = "&set_small_form_5=1";
						}elseif ($key == "5" & $sub_key == "18"){
							$set_small_form = "&set_small_form_6=1";
						}elseif ($key == "5" & $sub_key == "19"){
							$set_small_form = "&set_small_form_7=1";
						}else{
							$set_small_form = "";
						}
						$adv_razdel .= "
						<tr>
							<td align='right' width='25%' >
								 
							 </td>";
						
						if ($key !=3 & $key !=4 & $key<6){
							/*$adv_razdel.="<td align='left'>
								  	 <a href=\"#\" onclick=\"xajax_addContent('2','".$key."','".$sub_key."');\">".$sub_val['name']."</a>
								  </td>
							</tr>
							";*/
							$adv_razdel.="<td align='left'><a href=\"";
							if ($_redbase){
								switch ($sub_key) {
									case 1: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									case 2: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									
									case 3: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									case 4: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									
									case 5: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									case 6: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									
									case 7: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									case 8: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									
									case 18: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									case 19: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form; break;
									
									default: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form;
								}
							}else{
								switch ($sub_key) {
									case 1: $adv_razdel .= "/showtrucks/sell/"; break;
									case 2: $adv_razdel .= "/showtrucks/buy/"; break;
									
									case 3: $adv_razdel .= "/truckparts/sell/"; break;
									case 4: $adv_razdel .= "/truckparts/buy/"; break;
									
									case 5: $adv_razdel .= "/showtrucks/exchange/"; break;
									case 6: $adv_razdel .= "/showtrucks/other/"; break;
									
									case 7: $adv_razdel .= "/parts/sell/"; break;
									case 8: $adv_razdel .= "/parts/buy/"; break;
									
									case 18: $adv_razdel .= "/disks/sell/"; break;
									case 19: $adv_razdel .= "/disks/buy/"; break;
									
									default: $adv_razdel .= "/index.php?act_general=adv".$_urlparam."&step=marka&adv=2&id_cat=".$key."&id_sub=".$sub_key.$set_small_form;
								}
							}
							$adv_razdel .= "\" class=\"rubriks_show\">".$sub_val['name']." </a> <font color=\"#46678A\">(".$count_2[$sub_key].")</font> ";
							$adv_razdel .= "</td>
							</tr>
							";
						}else{
							$adv_razdel.="<td align='left'> <a href=\"";
							switch ($sub_key) {
								case 9: $adv_razdel .= "/garage/sell/"; break;
								case 10: $adv_razdel .= "/garage/buy/"; break;
								case 11: $adv_razdel .= "/garage/rent/"; break;
								case 12: $adv_razdel .= "/garage/lease/"; break;
								case 13: $adv_razdel .= "/garage/exchange/"; break;
								case 14: $adv_razdel .= "/garage/other/"; break;
                                case 15: $adv_razdel .= "/tires/sell/"; break;
								case 16: $adv_razdel .= "/tires/buy/"; break;
								
								default: $adv_razdel .= "/index.php?act=adv&act_adv=see".$_urlparam."&id_cat=$key&id_sub=".$sub_key.$set_small_form;
                            }
							$adv_razdel .= "\" class=\"rubriks_show\">".$sub_val['name']." </a> <font color=\"#46678A\">(".$count_2[$sub_key].") </font>";
							
							if ($sub_key == 15) {
							$adv_razdel .= "&nbsp;&nbsp;<a href=\"http://company.abw.by/shiny_diski\" class=\"rubriks_show\"><font color=\"#ff0000\">   </font> </a>";
							}
							
							$adv_razdel .= "<br/>
								  </td>
								</tr>
							";
						}
					}
					
				}
			}
	
		
			$adv_razdel .= "</table>";
            
			$smarty->assign('adv_razdel',$adv_razdel);
			
			//-- END OF VIVOD ----------------------------------------------------------//
			
			
			
			//vivod marok (legkovih)
			$res=mysql_query("SELECT marka_id,name,adv_type_1, trucks, name_eng FROM a_marka WHERE NOT (name_eng = '') ORDER BY name_eng");
			if (!empty($res)) {
				while (list($id,$name,$id_count, $trucks, $name_eng)=mysql_fetch_array($res)){
					$marki[$id]['name']=$name;
					$marki[$id]['count']=$id_count+$trucks;
					$marki[$id]['name_eng']=$name_eng;
				}
			}
			//    ,        
			$res=mysql_query("SELECT marka_id,name,adv_type_1, trucks, name_eng FROM a_marka WHERE name_eng = '' ORDER BY name");
			if (!empty($res)) {
				while (list($id,$name,$id_count, $trucks, $name_eng)=mysql_fetch_array($res)){
					$marki[$id]['name']=$name;
					$marki[$id]['count']=$id_count+$trucks;
					$marki[$id]['name_eng']=$name_eng;
				}
			}
			$res=mysql_query('SELECT DISTINCT marka FROM a_public'.$_pref);
			while (list($id)=mysql_fetch_array($res)){
				$marki[$id]['isset']=1;
			}
            
			$adv_razdel2="";
			$set_small_form = "&set_small_form_1=1&set_small_form_8=1";
			foreach ($marki as $key => $val) {
				//echo "<pre>"; print_r($val); echo "</pre>";
				if (!empty($val['isset']) & !empty($key)) {
					if (@$val['name_eng'] != "") {
						$title_addon = @$val['name_eng']."&nbsp;/&nbsp;";
						$url_addon = str_replace(" ","_",@$val['name_eng']);
					}
					else {
						$title_addon = "";
						$url_addon = $key;
					}
					//$adv_razdel2.="<a href=\"/index.php?act=showalladvs".$_urlparam."&marka=$key&adv_type=1".$set_small_form."\" class=\"aboutmarka\">".$title_addon.@$val['name']."</a>".(($_urlparam)?'':"&nbsp;<font color='#46678a'>(".@$val['count'].")</font>")."<br/>";
					// TODO:    SQL
					if (isset($val['count']) && ($val['count'] > 0)) {
						$adv_razdel2.="<a href=\"/showcars/sell/".$url_addon."/\" class=\"aboutmarka\">".$title_addon.@$val['name']."</a>".(($_urlparam)?'':"&nbsp;<font color='#46678a'>(".@$val['count'].")</font>")."<br/>";
					}
				}
			}
			$smarty->assign('adv_razdel2',$adv_razdel2);
	/**************************************************************************/		
		}elseif ($_GET['step'] == "marka"){
			$smarty->assign('act_general',$_GET['act_general']);
			require_once("./libs/adv_lib.php");
            
            //     
            require_once(ROOT_DIR.'/func/sto_show_sales.php');
            $sto_show_sale = StoSales();
            $smarty->assign('sto_show_sales',$sto_show_sale);
            
            $obj = new adv();
			$adv_razdel2 = "<font color=\"#B3123E\">".$obj->show_marka_right('2', $_GET['id_cat'], $_GET['id_sub'])."</font></strong>";
	
			$smarty->assign('adv_razdel2', $adv_razdel2);
			$smarty -> assign('id_cat', $_GET['id_cat']);
			$smarty -> assign('id_sub', $_GET['id_sub']);
			
			// vivod ob'yavlenij v centralnoi kolonke vmesto spiska kategorij ---------------------------------------//
			
			$act = "adv_pre";
			
			// end of vivod------------------------------------------------------------------------------------------//
			
		}
	}
}
include('func/'.$act.'.php');
$smarty->assign('file',$act.".tpl");
//for rubriks in arhive
if ($act == "number" || $act == "archive" || $act == "archive_search" || $act == "archive_spec" || $act == "tests" || $act == "about2" || $act == "showallarticls"){
	$res = mysql_query("SELECT id, name FROM a_arhiv_rubric WHERE spec=0 ORDER BY name");
	$temp = array();
	$temp['rubric_go'] = "";
	
	$temp['rubric_list'] = array();
	while(list($temp['id'], $temp['name']) = mysql_fetch_array($res)){
		$temp['rubric_list'][$temp['id']] = $temp['name'];
	}
	$res = mysql_query("SELECT DISTINCT rubric FROM a_arhiv_note  ORDER BY rubric");
	$temp['rubric_list_have'] = array();
	while(list($temp['rubric_have']) = mysql_fetch_array($res)){
		if ($temp['rubric_have'] != 0){
			$temp['rubric_list_have'][] = $temp['rubric_have'];
		}
	}
	foreach ($temp['rubric_list_have'] as $key => $val){
		if (!empty($temp['rubric_list'][$val])){
			$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/rubric/".$val."/' class=\"rubriks_left_1\">".$temp['rubric_list'][$val]."</a></strong></div>";
		}
	}
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=about2&n_teg=22' class=\"rubriks_left_1\"></a></strong></div>";
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=about2&n_teg=61' class=\"rubriks_left_1\"> </a></strong></div>";
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=about2&n_teg=37' class=\"rubriks_left_1\"> </a></strong></div>";
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=about2&n_teg=90' class=\"rubriks_left_1\"></a></strong></div>";
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=about2&n_teg=87' class=\"rubriks_left_1\"></a></strong></div>";
	$temp['rubric_go'] .= "<div style=\"margin-top: 5px;\"><strong><a href='/index.php?act=enquiry_show&id=7&enq=1' class=\"rubriks_left_1\">-</a></strong></div>";
	$smarty -> assign('rubric_go', $temp['rubric_go']);
}
//end
//marks for tests
if ($act == "tests" || $act == "number" || $act == "archive" || $act == "archive_spec" || $act == "archive_search" || $act == "shownews"  || $act == "delete_advertisement" || $act == "public_edit_list" || $act == "showallnews" || $act == "about2"  || $act == "showallarticls" || $act == "top_video" || $act == "garage") {
	if ($act != "shownews" & $act != "showallnews") {
		$smarty -> assign('tests_left', '1');
	}else{
		$smarty -> assign('tests_in_news', '1');
	}
		
	$res = mysql_query("
		(SELECT a.marka_id, b.name, a.marka_type, b.name_eng
			FROM  (a_marka b , a_arhiv_marks a)
				LEFT JOIN a_arhiv_note note ON (note.id=a.note_id) 
				LEFT JOIN a_arhiv_number number ON (number.id=note.number_id) 
				LEFT JOIN a_arhiv_links links ON (links.id=a.note_id) 
			WHERE a.marka_type='1' AND b.marka_id=a.marka_id AND NOT (b.name_eng = '') AND
				( (a.note_id=note.id AND note.number_id=number.id AND number.hidden!='1') OR (a.note_id=links.id) )
		)
		UNION 
		(SELECT a.marka_id, b.name, a.marka_type, b.name_eng
			FROM  (a_adv_marka b, a_arhiv_marks a)
				LEFT JOIN a_arhiv_note note ON (note.id=a.note_id) 
				LEFT JOIN a_arhiv_number number ON (number.id=note.number_id) 
				LEFT JOIN a_arhiv_links links ON (links.id=a.note_id) 
			WHERE a.marka_type='2' AND b.marka_id=a.marka_id AND (NOT b.name_eng = '') AND
				( (a.note_id=note.id AND note.number_id=number.id AND number.hidden!='1') OR (a.note_id=links.id) )
		)
		ORDER BY name_eng
	");
	$temp = array(); $temp['result'] = array(); $i = 0;
	
	while (list($temp['id'], $temp['name'], $temp['type'], $temp['name_eng']) = mysql_fetch_array($res)) {
		$isset = 0;
		$temp['type']=1;
		if (!empty($temp['result'])) {
			foreach ($temp['result'] as $key => $val) {
				if ($val['name'] == $temp['name']) {
					$isset = 1;
				}
			}
		}
		if (empty($isset)) {
			$temp['result'][$i]['id'] = $temp['id'];
			$temp['result'][$i]['name'] = $temp['name'];
			$temp['result'][$i]['name_eng_out'] = $temp['name_eng'];
			$temp['result'][$i]['name_eng'] = str_replace(" ", "_", $temp['name_eng']);
			$temp['result'][$i]['type'] = $temp['type']."2";
		}else{
			//$temp['result'][$key]['type'] .= "1";
		}
		$i++;
	}
	$smarty -> assign('tests_left_marks', $temp['result']);
}
// cur_time
$smarty->assign('now',array("day"=>date("d"),"month"=>date("m"),"year"=>date("Y")));
// cur ip
$smarty->assign('current_ip',$_SERVER['REMOTE_ADDR']);
// template
if ($act=="search"){
//	$smarty->assign('noleft',1);
}
$file_to_display = "index.tpl";
if($act == "shownews_m") {
  function smarty_block_dynamic($param, $content, &$smarty) {
    return $content;
  }
  $smarty->register_block('dynamic', 'smarty_block_dynamic', false);
  $smarty->caching = FALSE;
  $smarty->cache_lifetime = 1800;  
  $my_cache_id = filter_input(INPUT_GET, 'news_id', FILTER_SANITIZE_NUMBER_INT);//   id  
  $smarty->display('shownews_m.tpl',$my_cache_id);
  exit();
}elseif($act == "showallnews_m") {
  function smarty_block_dynamic($param, $content, &$smarty) {
    return $content;
  }
  $smarty->register_block('dynamic', 'smarty_block_dynamic', false);
  $smarty->caching = FALSE;
  //$smarty->cache_lifetime = 1800;  
  $my_cache_id = filter_input(INPUT_GET, 'cat_id', FILTER_SANITIZE_NUMBER_INT);//   id  
  $smarty->display('showallnews_m.tpl',$my_cache_id);
  exit();
}elseif (strpos($_SERVER['REQUEST_URI'],"adm_")) {
	$file_to_display = "admin.tpl";
}elseif (strpos($_SERVER['REQUEST_URI'],"payment")) {
	$file_to_display = "payment_templ.tpl";
}elseif ($act == "converter" || $act == "converter_txt" || $act == "adm_page" || $act == "117_page_new" || $act == "autoh_adm") {
	 $file_to_display = "index_converter.tpl";
}
//Mobile version
/*
if(isset($_GET['mobile1']) || isset($_POST['mobile1'])){
	$file_to_display = str_replace (".tpl",'2.tpl', $file_to_display);
	$act = $act."2";
	$smarty->assign('file',$act.".tpl");
}
*/
if (!empty($_REQUEST['act_adv']))  $act_adv = $_REQUEST['act_adv']; else $act_adv = "";
//    
$namber_banner2 = mt_rand(1, 2);
$smarty -> assign('namber_banner2',$namber_banner2);
$namber_banner3 = mt_rand(1, 3);
$smarty -> assign('namber_banner3',$namber_banner3);
$namber_banner4 = mt_rand(1, 4);
$smarty -> assign('namber_banner4',$namber_banner4);
$namber_banner5 = mt_rand(1, 5);
$smarty -> assign('namber_banner5',$namber_banner5);
$namber_banner6 = mt_rand(1, 6);
$smarty -> assign('namber_banner6',$namber_banner6);
$namber_banner10 = mt_rand(1, 10);
$smarty -> assign('namber_banner10',$namber_banner10);
$namber_banner20 = mt_rand(1, 20);
$smarty -> assign('namber_banner20',$namber_banner20);
//  
if (($act == "main") && !isset($act_general)){
	$smarty -> assign('show_banner_main', "1");
}
//O PROEKTE
/*
if ($act == "mailing" || $act == "showadv"){
	$smarty -> assign('not_show_o_proekte', "1");
}
*/
//HAITY AUTO
/*
if ($act_adv == "pre_add_form_adventure" || $act == "showmodels" || $act == "public_add_new" || $act == "adv"){
	$smarty -> assign('not_show_autopoisk', "1");
}
*/
//HAITY ZAPCHAST
/*
if ($act_adv == "pre_add_form_adventure" || $act == "showmodels" || $act == "public_add_new" || $act == "adv"){
	$smarty -> assign('not_show_zapchast', "1");
}
*/
//RABOTA
/*
if ($act == "mailing" || $act == "public_search" || $act == "showadv" || $act == "reg" || $act == "showpage"){
	$smarty -> assign('not_show_work', "1");
}
*/
//BANNER RIGHT1
/*
if ($act_adv == "pre_add_form_adventure"){
	$smarty -> assign('show_banner_right1', "1");
}
*/
/*
//FOTOOTCHOT
if ($act_adv == "pre_add_form_adventure" || $act == "mailing" || $act == "showadv" || $act == "reg" || $act == "showpage"){
	$smarty -> assign('not_show_fotootchot', "1");
} 
*/
//POGODA
/*
if ($act_adv == "pre_add_form_adventure" || $act == "mailing" || $act == "showadv" || $act == "reg" || $act == "showpage"){
$smarty -> assign('not_show_pogoda', "1");
} 
if (isset ($gorod)){
		$smarty -> assign('gorod_pogoda', "$gorod");
}
*/
// 
/*
if (isset($gorod_pogoda_k))
	{
	SetCookie("gorod_pogoda_kook", $gorod_pogoda_k, 0x6FFFFFFF);
	$smarty -> assign('gorod_pogoda_kook', $gorod_pogoda_k);
	}
else {
	if (isset($gorod_pogoda_kook))
		{
		$smarty -> assign('gorod_pogoda_kook', $gorod_pogoda_kook);
		}
	}
*/
//ANEKDOT
/*
if ($act == "mailing" || $act == "reg" || $act == "showpage"){
	$smarty -> assign('not_show_anekdot', "1");
} 
*/
//PEREPECHATKA
/*if (isset ($r1_perep)){
$email='web@abw.by';
$str=("
 - $r1_perep
URL    www.ab-daily.by: $textfield1
 : $textfield2 
URL : $textfield3 
 : $textfield21 
 , .: $textfield22 
URL    ab-daily.by: $textfield23    
 : $textfield24
 : $textfield25");
    mail($email, " ", $str, "From: web");} 
*/
//DATE SHORT NEWS
/*
$day_week_rus = file("day.txt");
$day_of_week = $day_week_rus[date ('w')];
$smarty -> assign('week_short_news', $day_of_week);
								
$date_sh_news = date ('d.m.Y');
$smarty -> assign('date_short_news', $date_sh_news);
$smarty -> assign('date_h', date("H", time()));
*/
$smarty->register_outputfilter("extUrls");
$smarty->display($file_to_display);
/*
if(@$_SESSION['user']['status']==4) {
  $time=number_format(Utils::getmicrotime()-$_starttime,3,'.','');
  echo $time;
}
*/  
